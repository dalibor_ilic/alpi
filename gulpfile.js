const { series, parallel } = require('gulp');

const clean = require('./tasks/clean');

const copyImages = require('./tasks/copyImages');
const copyJs = require('./tasks/copyJs');
const copyRoot = require('./tasks/copyRoot');
const copyWeb = require('./tasks/copyWeb');

const iconSvg = require('./tasks/iconSvg');
const twig = require('./tasks/twig');
const sass = require('./tasks/sass');
const webpack = require('./tasks/webpack');

const watch = require('./tasks/watch');

const buildCMS = require('./tasks/buildCMS');

const build = function build(done) {
	return series(clean, iconSvg, parallel(buildCMS, sass, webpack, twig, copyImages, copyJs, copyRoot))(done);
};

const dev = function dev(done) {
	return series(build, watch)(done);
};

const min = function min(done) {
	process.env.NODE_ENV = 'production';
	return series(build)(done);
};

const minwatch = function minwatch(done) {
	return series(min, watch)(done);
};

module.exports = {
	clean,
	copyImages,
	copyJs,
	copyRoot,
	copyWeb,
	iconSvg,
	twig,
	sass,
	watch,
	webpack,
	buildCMS,

	default: dev,
	build,
	min,
	minwatch,
};
