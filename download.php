<?php
require_once 'config.php';

$file = getFile($_GET['file']);

header('Content-Type: application/octet-stream');
header("Content-Transfer-Encoding: Binary");
header("Content-disposition: attachment; filename=\"" . basename($file->name) . "\"");
readfile($file->url);