<?php

// cesta k souboru s cache rozmeru obrazku
DEFINE('IMAGE_CACHE_PATH', '.' . DIRECTORY_SEPARATOR . 'image-size-cache.json');

$routes = json_decode(file_get_contents("./dist/tpl/routes.json"), true);
$global_settings = $routes['/global']['page'];

// seznam stránek v CZ verzi
$pagesCZ = isset($routes['pages']) ? $routes['pages'] : array();
$types = isset($global_settings['types']) ? $global_settings['types'] : array();
$products = isset($routes['products']) ? $routes['products'] : array();
$brands = isset($routes['brands']) ? $routes['brands'] : array();
$brandsOrder = isset($global_settings['brands']) ? $global_settings['brands'] : array();

$brandsByPath = array();
foreach ($brands as $key => $value) {
	$brandsByPath[$value['path']] = $value['page'];
	$brandsByPath[$value['path']]['key'] = $key;
}


$param_type = isset($_GET['type']) ? $_GET['type'] : null;
$param_brand = isset($_GET['brand']) ? $_GET['brand'] : null;
$param_page = isset($_GET['page']) ? $_GET['page'] : 1;
$items_per_page = 4;

$filtered_products = array();
$filtered_types = array();

foreach ($products as $key => $value) {
	$pagesCZ['/produkt'. $key] = $value;
	$product = $value['page'];
	$product_brand = $product['brand'] ? substr($brandsByPath[$product['brand']]['key'], 1) : null;
	$product_categories = $product['categories'] ? $product['categories'] : array(null);

	// multicategories
	foreach ($product_categories as $keyCat => $category) {
		$product_type =  $category ? str_replace(' ', '-', strtolower(trim($category))) : null;
		// var_dump($product);

		if ((!$param_type || $param_type == $product_type) && (!$param_brand || $param_brand == $product_brand)) {
			$filtered_products[$key] = $value;
		}

		// nezobrazuj filtre pokud v te kategorii neni produkt
		if ($category && (!$param_brand || $param_brand == $product_brand)) {
			$filtered_types[$category] = $category;
		}
	}


}


// $pagesCZ = array(
// 	'/' => $routes['/'],
// $pagesCZ['/obecna-stranka'] = array(
// 		'template' => 'obecna-stranka',
// 		'pageTitle' => 'Obecná stránka',
// 		'pageDescription' => '',
// 		'path' => '',
// 	);
// $pagesCZ['/references'] = array(
// 		'template' => 'references',
// 		'pageTitle' => 'References',
// 		'pageDescription' => '',
// 		'path' => '',
// 	);
// $pagesCZ['/contact'] = array(
// 		'template' => '05-contact',
// 		'pageTitle' => 'Contact',
// 		'pageDescription' => '',
// 		'path' => '',
// 	);
	// '/about-us' => array(
	// 	'template' => '03-about-us',
	// 	'pageTitle' => 'About us',
	// 	'pageDescription' => '',
	// 'path' => '',
	// );
// $pagesCZ['/products'] = array(
// 		'template' => 'products',
// 		'pageTitle' => 'Products',
// 		'path' => '',
// 	);
// $pagesCZ['/detail'] = array(
// 		'template' => '04-detail',
// 		'pageTitle' => 'Detail produktu',
// 		'pageDescription' => '',
// 		'path' => '',
// 	);
// $pagesCZ['/brands'] = array(
// 		'template' => '06-brands',
// 		'pageTitle' => 'Brands',
// 		'pageDescription' => '',
// 		'path' => '',
// 	);

// jazykové řetězce
$lgStrings = array(
	// CAREER
	'load_more_table' => 'stolů',
	'load_more_sofa' => 'sof',
	'load_more_chair' => 'židlí',
	'load_more_sofa-bed-system' => 'sof s rozkladem',
	'load_more_arm-chair' => 'křesel',
	'load_more_bar-chair' => 'židlí',
	'load_more_bed' => 'postelí',
	'load_more_sideboard' => 'komod',
	'load_more_coffee-table' => 'stolů',
	'load_more_shelf' => 'polic',
	'load_more_garden-furniture' => 'nábytku',
	'load_more_accessories' => 'doplňků',


	'sofa' => 'Sofa',
	'sofa-bed-system' => 'Sofa s rozkladem',
	'chair' => 'Židle',
	'arm-chair' => 'Křeslo',
	'bar-chair' => 'Barová židle',
	'bed' => 'Postel',
	'table' => 'Jídelní stůl',
	'sideboard' => 'Komoda',
	'coffee-table' => 'Konferenční stolek',
	'shelf' => 'Police',
	'garden-furniture' => 'Zahradní nábytek',
	'accessories' => 'Doplňky',

);

/**
 * POMOCNÉ FUNKCE
 */

function isCZ() {
	global $url;
	//if (strpos($url, '/en') === 0) return false;
	return  !preg_match('/^\/(ru|en)/', $url);
}

function isEN() {
	global $url;
	return preg_match('/^\/en/', $url);
}


function url($key) {
	global $pages;
	// var_dump($pages);
	$urls = array();
	foreach ($pages as $k => $v) {
		//$urls[$v['template']] = $k;
		$urls[$v['path']] = $k;
	}

	if (isset($urls[$key])) return $urls[$key];
	return '#';
}

function price_format($val) {
	if ($val == 0) return '-';
	return number_format($val, 0, '', ' ');
}

function date_format_by_lg($val) {
	if (isCZ()) {
		$date = date_create($val);
		return date_format($date, 'j. n. Y');
	}
	return $val;
}

function __($key) {
	global $lgStrings;

	if (isset($lgStrings[$key])) return $lgStrings[$key];
	return $key;
}

function is_ajax() {
	$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
	return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && (strtolower(getenv('HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest') || $contentType === "application/json");
}




function get_include_contents($filename) {
	if (is_file($filename)) {
		ob_start();
		include $filename;
		return ob_get_clean();
	}
	return false;
}


function slugify($text)
{
	// replace non letter or digits by -
	$text = preg_replace('~[^\pL\d]+~u', '-', $text);

	// transliterate
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	// remove unwanted characters
	$text = preg_replace('~[^-\w]+~', '', $text);

	// trim
	$text = trim($text, '-');

	// remove duplicate -
	$text = preg_replace('~-+~', '-', $text);

	// lowercase
	$text = strtolower($text);

	if (empty($text)) {
		return 'n-a';
	}

	return $text;
}

// Nacte lokalni cache velikosti obrazku do globalni promenne
function loadImagesCache()
{
	if (file_exists(IMAGE_CACHE_PATH)) {
		return json_decode(file_get_contents(IMAGE_CACHE_PATH), true);
	} else {
		return [];
	}
}

// Ulozeni lokalni cache obrazku
function saveImagesCache($image_sizes_cache)
{
	file_put_contents(IMAGE_CACHE_PATH, json_encode($image_sizes_cache));
}

function createImage($image, $isParallax = false, $isSlider = false) {
	global $image_sizes_cache;

	if (!empty($image_sizes_cache[$image])) {
		// Nacteni rozmeru obrazku z lokalni cache
		list($width, $height) = $image_sizes_cache[$image];
	} else {
		// Rozmery obrazku v cache nejsou
		// Nactu je a ulozim do lokalni cache
		list($width, $height) = getimagesize('https://res.cloudinary.com/dnr1munaa'. $image);
		$image_sizes_cache[$image] = [$width, $height];
		saveImagesCache($image_sizes_cache);
	}

	echo '
		<div class="b-media"'. ($isSlider ? ' data-ref="Slider.slide"' : '') .'>
			<canvas class="b-media__item" width="'. $width .'" height="'. ($isParallax ? round($height * 0.9) : $height) .'"></canvas>
			<img
				src="https://res.cloudinary.com/dnr1munaa/f_auto,q_auto'. $image .'"
				alt=""
				class="b-media__item'. ($isParallax ? ' b-media__item--parallax' : '') .'" >
		</div>
	';
}

function createMedia($gallery, $isParallax = true, $isClickable = false) {
	if (count($gallery) > 1) echo '<div class="l-carousel slider" data-component="Slider" data-type="fade">';

	if (count($gallery) > 1 && $isClickable) {
		createImage($gallery[count($gallery) - 1], $isParallax, true);
	}

	foreach ($gallery as $key => $item) {
		createImage($item, $isParallax, true);
	};

	if (count($gallery) > 1) echo '
		<div class="slider__counter" data-ref="Slider.counter"></div>
		<div class="slider__timer" data-ref="Slider.timer"></div>
	</div>';
}
