---
brand: data/brands/Woak.md
name: 'MARSHALL HOUPACÍ KŘESLO '
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Dřevěné části tohoto nábytku jsou ručně
  vybírány jedna část po druhé, aby dokonale pasovaly s ostatními materiály, které
  jsou zcela inovativní a recyklovatelné, jako je nezpracovaná kůže, čistý mramor
  nebo lité sklo s měnícími se barvami. Kombinace teplých a studených materiálů vede
  k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem je mužný,
  ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací pokoj nebo
  koutek na čtení.</p>'
product_images:
- "/v1606811254/forestry/WHO_MARSHALL_houpac%C3%AD_k%C5%99eslo_1_jigkkt.png"
- "/v1606811255/forestry/WHO_MARSHALL_houpac%C3%AD_k%C5%99eslo_3_q9d79j.png"
- "/v1606811254/forestry/WHO_MARSHALL_houpac%C3%AD_k%C5%99eslo_2_hgdbtt.png"
dimensions: "<p><strong>Šířka: </strong>70 cm - <strong>Délka: </strong>89 cm - <strong>Výška:</strong>
  72 cm</p>"
technical_info: "<p>materiál: ořech nebo dub,</p><p>kůže: kašmír</p><p>povrchová úprava:
  lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>65 800 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Marshall_rocking_armchair_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606811558/forestry/WHO_MARSHALL_houpac%C3%AD_k%C5%99eslo_4_tcinyo.png"
- "/v1606811559/forestry/WHO_MARSHALL_houpac%C3%AD_k%C5%99eslo_5_b6yo5u.png"

---
