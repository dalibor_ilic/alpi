---
brand: data/brands/Artisan.md
name: NEVA TABURET
categories: []
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605189715/forestry/ART_NEVA_taburet_2_ck65jy.png"
- "/v1605189715/forestry/ART_NEVA_taburet_1_zgpyyi.png"
dimensions: "<p><strong>Šířka: </strong>64 cm - <strong>Délka: </strong>47 cm - <strong>Výška:
  </strong>40 cm - <strong>Výška sedu: </strong>40 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v 2 kategoriích látek
  i 3 kůže.</p><p>Velikost sedu: 64x47 cm</p><p>Průměr nohou: spodní část 2,7 cm,
  vrchní část 3,3 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 19 400 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605189863/forestry/ART_NEVA_taburet_3_oas9fj.png"

---
