---
brand: data/brands/Woak.md
name: 'NINAS KONZOLOVÝ STOLEK '
categories:
- Accessories
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Ústředním prvkem jsou minimalistické
  linie aplikované na kusy retro nábytku. Cílem kolekce je nabídnout novou interpretaci
  klasických kusů nábytku, prostřednictvím obnovy stylu pomocí techniky zpracované
  značkou Woak. Záměrem je rozebrat společná místa tradičních forem, eliminovat ozdoby
  a prvky, které jsou příliš dekorativní, aby byl objekt moderní a zároveň funkční.</p>'
product_images:
- "/v1607004928/forestry/WHO_NINAS_konzola_5_au7kff.png"
- "/v1607004921/forestry/WHO_NINAS_konzola_1_dmecte.png"
dimensions: "<p><strong>Šířka: </strong>110 - 140 cm - <strong>Délka: </strong>35
  cm - <strong>Výška:</strong> 75 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>mramor: Emperador nebo carrara</p><p>povrchová
  úprava: olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>40 600 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Ninas_console_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607005144/forestry/WHO_NINAS_konzola_4_bmc6dl.png"
- "/v1607005147/forestry/WHO_NINAS_konzola_2_mxksnb.png"
- "/v1607005145/forestry/WHO_NINAS_konzola_3_vtzfkx.png"

---
