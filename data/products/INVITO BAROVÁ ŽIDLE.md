---
brand: data/brands/Artisan.md
name: 'INVITO BAROVÁ ŽIDLE '
categories:
- Bar chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644248613/forestry/Product_foto-15_mbpkad.png"
- "/v1644248613/forestry/Product_foto-16_fas4bv.png"
dimensions: "<p><strong>Šířka: </strong>46<strong> </strong>cm - <strong>Délka: </strong>47
  cm - <strong>Výška: </strong>99 cm - <strong>Výška sedu: </strong>62 cm - <strong>Hloubka
  sedu:</strong> 46 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Velikost sedu: 46x42 cm</p><p>Průměr
  nohou: spodní část 2,6 cm, vrchní část 3,6 cm</p><p>Možné pouze v černé kůži.</p>"
materials: <p><a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>23 100 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604583707/forestry/ART_INVITO_barov%C3%A1_%C5%BEidle_4_dfdfk2.png"
- "/v1604583707/forestry/ART_INVITO_barov%C3%A1_%C5%BEidle_3_lw3nmx.png"
- "/v1604583707/forestry/ART_INVITO_barov%C3%A1_%C5%BEidle_2_cyozak.png"

---
