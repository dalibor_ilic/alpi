---
brand: data/brands/Artisan.md
name: 'INVITO CUBE KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645440872/forestry/Product_foto-3cube_konferen%C4%8Dn%C3%AD_stolek_vcj7vm.png"
- "/v1645440823/forestry/Product_fotocube_konferen%C4%8Dn%C3%AD_stolek_xvhmbz.png"
dimensions: "<p><strong>Šířka: </strong>60/80/100/120 cm - <strong>Délka: </strong>60/80/100/120
  cm<strong> </strong>- <strong>Výška: </strong>40 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka rámu: 2 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 13 500 Kč</p>"
product_info: ''
product_photos:
- "/v1645440925/forestry/Z_interi%C3%A9rucube_konferen%C4%8Dn%C3%AD_stolek_jqlgmn.png"
- "/v1645440979/forestry/Z_interi%C3%A9ru-2cube_konferen%C4%8Dn%C3%AD_stolek_vmb0hd.png"

---
