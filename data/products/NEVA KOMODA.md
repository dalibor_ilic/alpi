---
brand: data/brands/Artisan.md
name: NEVA KOMODA
categories:
- Sideboard
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645711596/forestry/Neva_komodaProduct_foto_oadquy.png"
- "/v1645711597/forestry/Neva_komodaProduct_foto-2_abyrjo.png"
- "/v1645711596/forestry/Neva_komodaProduct_foto-3_u50cyt.png"
- "/v1645711595/forestry/Neva_komodaProduct_foto-5_eqcjqg.png"
- "/v1645711595/forestry/Neva_komodaProduct_foto-4_z5enje.png"
dimensions: "<p><strong>Šířka: </strong>140/180/200/240 cm - <strong>Délka: </strong>47
  cm - <strong>Výška: </strong>75/90 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka rámu komody: 2 cm</p><p>Tloušťka
  předních částí:  2,2 cm</p><p>Tloušťka zadních částí: 1,5 cm<br></p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a> | <a href="https://drive.google.com/file/d/1DKp-AqR_-rESWlFNEpcr1prxEig5SpKq/view?usp=sharing
  " title=""><strong>BAREVNÉ DŘEVO</strong></a> </p>
price: "<p><strong>Cena od: </strong>65 800 Kč</p>"
product_info: ''
product_photos:
- "/v1645711898/forestry/Neva_komodaZ_interi%C3%A9ru-2_ur6xiz.png"
- "/v1645711898/forestry/Neva_komodaZ_interi%C3%A9ru_ogs0sy.png"
- "/v1645711894/forestry/Neva_komodaZ_interi%C3%A9ru-5_h1tr6h.png"
- "/v1645711892/forestry/Neva_komodaZ_interi%C3%A9ru-3_ozjsf7.png"
- "/v1645711896/forestry/Neva_komodaZ_interi%C3%A9ru-4_fa8q99.png"

---
