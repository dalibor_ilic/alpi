---
brand: data/brands/Woak.md
name: 'MARSHALL LEHÁTKO '
categories:
- Accessories
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Dřevěné části tohoto nábytku jsou ručně
  vybírány jedna část po druhé, aby dokonale pasovaly s ostatními materiály, které
  jsou zcela inovativní a recyklovatelné, jako je nezpracovaná kůže, čistý mramor
  nebo lité sklo s měnícími se barvami. Kombinace teplých a studených materiálů vede
  k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem je mužný,
  ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací pokoj nebo
  koutek na čtení.</p>'
product_images:
- "/v1606812723/forestry/WHO_MARSHALL_leh%C3%A1tko_2_tpv6kj.png"
- "/v1606812731/forestry/WHO_MARSHALL_leh%C3%A1tko_6_hy8yzw.png"
- "/v1606812723/forestry/WHO_MARSHALL_leh%C3%A1tko_1_dhmviq.png"
- "/v1606812731/forestry/WHO_MARSHALL_leh%C3%A1tko_7_kigntl.png"
dimensions: "<p><strong>Šířka: </strong>63 cm - <strong>Délka: </strong>145 cm - <strong>Výška:</strong>
  90 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>kůže: kašmír</p><p>povrchová úprava:
  lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 91 100 Kč  </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Marshall_lounge_chair_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606812881/forestry/WHO_MARSHALL_leh%C3%A1tko_5_vk7pp6.png"
- "/v1606812882/forestry/WHO_MARSHALL_leh%C3%A1tko_3_ybzdft.png"
- "/v1606812882/forestry/WHO_MARSHALL_leh%C3%A1tko_4_msci7i.png"

---
