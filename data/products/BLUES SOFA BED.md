---
brand: data/brands/Atlas.md
name: BLUES SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Souprava Blues nabízí výjimečné pohodlí
  díky sedákům s péřovou výplní. Od ostatních modelů se liší tím, že nabízí větší
  možnosti vytvoření kompozice podle vašeho vkusu. Tento model se skládá z podstavců
  různých velikostí a polštářů, které jsou pohyblivé. Polštáře jsou ke konstrukci
  přivázány dřevěnou deskou a jsou vyrobeny na míru vašim požadavkům. </p>'
product_images:
- "/v1642249864/forestry/ATL_BLUES_sofa_1_h8jppb.png"
- "/v1642249864/forestry/ATL_BLUES_sofa_6_niryau.png"
- "/v1642249864/forestry/ATL_BLUES_sofa_8_lnt8h2.png"
- "/v1642249864/forestry/ATL_BLUES_sofa_9_k7k2et.png"
- "/v1642249864/forestry/ATL_BLUES_sofa_10_e2mwf3.png"
- "/v1642249863/forestry/ATL_BLUES_sofa_7_fr2xwe.png"
dimensions: "<p><strong>Šířka: </strong>281 cm - <strong>Délka: </strong>106/281 cm
  - <strong>Výška: </strong>77 cm - <strong>Výška sedu:</strong> 43 cm - <strong>Hloubka
  sedu:</strong> 78 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36, paměťová pěna, peří se silikonem</p><p><strong>Opěradlo: </strong>HR
  35/30, pěna L 25/15</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:</strong>
  kovové nohy lakované v barvě RAL</p><p><strong>Možnosti:</strong> dřevěný rám lakovaný
  PU lakem, v barvě RAL nebo s efektem „páleného“ dřeva</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>146 100 Kč </p><p><strong>Cena v látce
  od: </strong>101 500 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/e12b61e0630fc5842e8ce5beb21b9e60927330cf.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642250128/forestry/ATL_BLUES_sofa_2_d9oogg.png"
- "/v1642250128/forestry/ATL_BLUES_sofa_3_gxld0c.png"
- "/v1642250127/forestry/ATL_BLUES_sofa_4_zzjohy.png"
- "/v1642250128/forestry/ATL_BLUES_sofa_5_orsc7n.png"

---
