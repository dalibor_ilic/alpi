---
brand: data/brands/Artisan.md
name: 'FLOW STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605536902/forestry/ART_FLOW_st%C5%AFl_3_mdljno.png"
- "/v1605536902/forestry/ART_FLOW_st%C5%AFl_2_hy0gqm.png"
- "/v1605536902/forestry/ART_FLOW_st%C5%AFl_1_oy2okm.png"
dimensions: "<p><strong>Šířka: </strong>160/180/200/220/240<strong> </strong>cm -
  <strong>Délka: </strong>90 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, za příplatek 10% možnost šíře
  100 cm.</p><p>Tloušťka desky: 1,2 cm</p><p>Vizuální tloušťka desky: 4 cm</p><p>Průměr
  nohou: spodní část 6x4,2 cm, vrchní část 9,5x4,2 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 62 100 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605537082/forestry/ART_FLOW_st%C5%AFl_5_zu8yh9.png"
- "/v1605537082/forestry/ART_FLOW_st%C5%AFl_4_p80ywj.png"

---
