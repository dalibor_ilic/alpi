---
brand: data/brands/Atlas.md
name: VENUS L SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Mohutná a okouzlující, navržená tak, aby
  se stala ústředním bodem každého obývacího pokoje. Polštáře sedadel a pohyblivé
  opěrky hlavy jsou umístěny tak, aby kopírovaly linii těla, a zajišťují pohodlný
  odpočinek. Minimalistický přístup k designu, jednoduchost a elegance jsou tím, co
  oslovuje mnoho zákazníků.</p>'
product_images:
- "/v1643099938/forestry/ATL_VENUSL_sofa_1_xmg85p.png"
- "/v1643099940/forestry/ATL_VENUSL_sofa_7_zfchxg.png"
- "/v1643099940/forestry/ATL_VENUSL_sofa_5_nwenyc.png"
- "/v1643099940/forestry/ATL_VENUSL_sofa_9_cnl93t.png"
- "/v1643099940/forestry/ATL_VENUSL_sofa_8_nkgzct.png"
- "/v1643099940/forestry/ATL_VENUSL_sofa_10_dn8vsb.png"
- "/v1643099940/forestry/ATL_VENUSL_sofa_6_fyei3t.png"
dimensions: "<p><strong>Šířka: </strong>284 cm - <strong>Délka: </strong>234 cm -
  <strong>Výška: </strong>67/90 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/30 a HR 35/36 ve vatovém potahu</p><p><strong>Opěradlo: </strong>pěna
  L 25/15 ve vatovém potahu</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:
  </strong>kovové -inox nohy</p><p><strong>Možnosti:</strong> opěrky hlavy s mechanismem</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>117 700 Kč</p><p><strong>Cena v látce
  od: </strong>82 900 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/e91f1f8c69d3fb0628f5c5504a7bfa55c72e597c.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643100122/forestry/ATL_VENUSL_sofa_4_e5dyqj.png"
- "/v1643100120/forestry/ATL_VENUSL_sofa_3_nrkzgd.png"
- "/v1643100122/forestry/ATL_VENUSL_sofa_2_yrlicj.png"

---
