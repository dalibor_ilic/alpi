---
brand: data/brands/Artisan.md
name: 'INSERT LAMPA '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605173085/forestry/ART_INSERT_lampa_1_xwov5r.png"
dimensions: "<p><strong>Šířka: </strong>65<strong> </strong>cm - <strong>Délka: </strong>65
  cm - <strong>Výška: </strong>40/180 cm (S/L)</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, možnost kovového stínidla a stmívače.</p><p>Průměr
  nohou: 1,5 cm/ 2,7 cm</p><p>Rozměr rozpětí nohou po ploše: 18x18x20 cm/ 65x65x60
  cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p><p></p>
price: "<p><strong>Cena od:</strong> 15 200 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605173627/forestry/ART_INSERT_lampa_6_rljtor.png"
- "/v1605173626/forestry/ART_INSERT_lampa_5_xgng9x.png"
- "/v1605173627/forestry/ART_INSERT_lampa_4_ypmeqq.png"
- "/v1605173626/forestry/ART_INSERT_lampa_3_cvtpdt.png"
- "/v1605173627/forestry/ART_INSERT_lampa_2_go6ohb.png"

---
