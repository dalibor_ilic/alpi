---
brand: data/brands/Extraform.md
name: 'AINO SOFA '
categories:
- Sofa
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"
  title="EXTRAFORM"><strong>EXTRAFORM </strong></a><strong> </strong></p><p>Kruhová
  základna této sestavitelné pohovky je zdůrazněna dřevěnými nohami a polštáři v různých
  barvách. Díky své hloubce je mimořádně pohodlná, což z ní dělá velmi žádaný kus
  nábytku pro využití doma i v kanceláři. </p>'
product_images:
- "/v1644499198/forestry/EXT_AINO_sofa_7_txbwoh.png"
- "/v1644499183/forestry/EXT_AINO_sofa_8_ghvt3o.png"
- "/v1644499183/forestry/EXT_AINO_sofa_1_xeaydv.png"
dimensions: "<p><strong>Dvojsed: Šířka: </strong>180cm - <strong>Délka: </strong>104
  cm - <strong>Výška:</strong> 74 cm - <strong>Výška sedu:</strong> 41 cm - <strong>Hloubka
  sedu:</strong> 65 cm </p><p><strong>Trojsed:</strong> <strong>Šířka: </strong>200
  cm - <strong>Délka: </strong>104 cm - <strong>Výška:</strong> 74 cm - <strong>Výška
  sedu:</strong> 41 cm - <strong>Hloubka sedu:</strong> 65 cm </p>"
technical_info: "<p>Nosná konstrukce ze sušeného bukového dřeva, lepenky, 18 mm silné
  překližky, MDF desky. </p><p>Sedáky - vysoce elastická HR pěna, 200g tepelně pojené
  plsti, elastické popruhy.</p><p>Opěrky - ergonomická vysoce elastická pěna, 400g
  tepelně pojené plsti. </p><p>Nohy z dubového dřeva, naolejované. Volitelně z bukového
  dřeva v šesti barvách. </p>"
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>33 800 Kč</p>"
product_info: ''
product_photos:
- "/v1644499252/forestry/EXT_AINO_sofa_5_c9luyf.png"
- "/v1644499258/forestry/EXT_AINO_sofa_4_mxnbfk.png"
- "/v1644499250/forestry/EXT_AINO_sofa_2_gut8vl.png"
- "/v1644499253/forestry/EXT_AINO_sofa_6_aeyuwp.png"
- "/v1644499253/forestry/EXT_AINO_sofa_3_f5axj6.png"

---
