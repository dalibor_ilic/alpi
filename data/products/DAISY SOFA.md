---
brand: data/brands/Extraform.md
name: DAISY SOFA
categories:
- Sofa
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"
  title="EXTRAFORM"><strong>EXTRAFORM</strong></a></p><p>Zdánlivě klasickou formu
  této dvoumístné či třímístné pohovky DAISY je možné pozměnit pomocí područek, které
  nabízejí několik pozic. Kontrast mezi velkou sedací částí a jemnými kulatými područkami
  přispívá k jejímu zajímavému a okouzlujícímu charakteru. Díky svým rozměrům se snadno
  hodí do různých prostor. </p>'
product_images:
- "/v1644501536/forestry/EXT_DAISY_sofa_1_poveim.png"
- "/v1644501572/forestry/EXT_DAISY_sofa_7_evui8j.png"
- "/v1644501573/forestry/EXT_DAISY_sofa_8_vzgdfx.png"
- "/v1644501574/forestry/EXT_DAISY_sofa_9_g7sugj.png"
dimensions: "<p><strong>Dvojsed:</strong> <strong>Šířka:</strong> 154 - 194 cm - <strong>Délka:
  </strong>84 cm - <strong>Výška:</strong> 84 cm - <strong>Výška sedu:</strong> 44
  cm - <strong>Hloubka sedu: </strong>52 cm </p><p><strong>Trojsed: Šířka: </strong>174
  - 214 cm - <strong>Délka: </strong>89 cm - <strong>Výška: </strong>84 cm -<strong>
  Výška sedu: </strong>44 cm - <strong>Hloubka sedu: </strong>52 cm </p>"
technical_info: "<p>Nosná konstrukce ze sušeného bukového dřeva, dřevotříska o tloušťce
  16 mm, lepenka, překližka o tloušťce.</p><p>Kombinovaná tvrdá pěna a vysoce elastická
  pěna, gumičky, nastavitelná loketní opěrka. </p><p>Nohy z bukového dřěva, mořené
  a lakované. K dispozici v šesti barvách. Chromované nohy jsou volitelné. </p><p>Područky
  lze sklopit dolu.</p>"
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 28 200 Kč</p>"
product_info: ''
product_photos:
- "/v1644502263/forestry/EXT_DAISY_sofa_5_glzmwl.png"
- "/v1644502257/forestry/EXT_DAISY_sofa_2_s4uiow.png"
- "/v1644502264/forestry/EXT_DAISY_sofa_4_h1dfob.png"
- "/v1644502263/forestry/EXT_DAISY_sofa_3_azomkv.png"

---
