---
brand: data/brands/Artisan.md
name: 'TORSIO STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1604996777/forestry/ART_TORSIO_st%C5%AFl_3_ql5tqh.png"
- "/v1604996776/forestry/ART_TORSIO_st%C5%AFl_1_zcbgjz.png"
- "/v1604996779/forestry/ART_TORSIO_st%C5%AFl_5_oatwvu.png"
- "/v1604996777/forestry/ART_TORSIO_st%C5%AFl_2_jtlonf.png"
dimensions: "<p><strong>Šířka: </strong>100 - 260 cm - <strong>Délka: </strong>100<strong>
  </strong>cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a za příplatek 10% možnost šíře
  110 cm.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální tloušťka desky: 8 cm</p><p>Průměr
  nohou: 8 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>47 600 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605684927/forestry/ART_TORSIO_st%C5%AFl_4_ycbjza.png"

---
