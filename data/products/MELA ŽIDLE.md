---
brand: data/brands/Artisan.md
name: 'MELA ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1604586811/forestry/ART_MELA_%C5%BEidle_1_wvmkq2.png"
- "/v1604586826/forestry/ART_MELA_%C5%BEidle_4_ubqogj.png"
- "/v1604586826/forestry/ART_MELA_%C5%BEidle_5_v38v6z.png"
- "/v1604586826/forestry/ART_MELA_%C5%BEidle_6_fri5ol.png"
dimensions: "<p><strong>Šířka: </strong>53 cm - <strong>Délka: </strong>54 cm - <strong>Výška:
  </strong>78 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Průměr
  nohou: 2,7 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 24 100 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604587003/forestry/ART_MELA_%C5%BEidle_3_lzop7p.png"
- "/v1604587003/forestry/ART_MELA_%C5%BEidle_2_puujsc.png"

---
