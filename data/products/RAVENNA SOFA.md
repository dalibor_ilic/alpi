---
brand: data/brands/Atlas.md
name: RAVENA SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Sedák a opěradlo v urban stylu pro ten
  největší komfort. Specifičnost této pohovky spočívá v sedácích a polštářích, které
  mohou být vyrobeny v kombinaci různých materiálů z každé strany. Ravenna se stane
  ideální volbou pro váš domov, protože splní všechny vaše požadavky týkající se funkčnosti
  a pohodlí bez zanedbání estetiky. </p>'
product_images:
- "/v1643021122/forestry/ATL_RAVENA_sofa_1_lixzbz.png"
- "/v1643021123/forestry/ATL_RAVENA_sofa_5_qfupud.png"
- "/v1643021123/forestry/ATL_RAVENA_sofa_6_bsdni2.png"
- "/v1643021123/forestry/ATL_RAVENA_sofa_7_kdolxx.png"
- "/v1643021123/forestry/ATL_RAVENA_sofa_4_liqrrd.png"
dimensions: "<p><strong>Šířka: </strong>260 cm - <strong>Délka: </strong>102 cm -
  <strong>Výška: </strong>87 cm - <strong>Výška sedu:</strong> 47 cm- <strong>Hloubka
  sedu:</strong> 65 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36, peří, paměťová pěna, silikon</p><p><strong>Opěradlo: </strong>pěna
  N 25/38, mletá pěna a silikon</p><p><strong>Vlnové pružiny</strong></p><p><strong>Nožičky:
  </strong>kovové nerezové nohy/dřevěné nohy - lakované PU lakem, nebo v barvě RAL</p><p><strong>Možnosti:</strong>
  odnímatelný potah</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>81 000 Kč </p><p><strong>Cena v látce
  od: </strong>49 100 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/22d0e6ec6351bcbc3b412d71700a959d316857fb.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643021614/forestry/ATL_RAVENA_sofa_3_fbuu7p.png"
- "/v1643021614/forestry/ATL_RAVENA_sofa_2_n8k43v.png"

---
