---
brand: data/brands/Atlas.md
name: SPIDER STŮL
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Jeho základna má tvar pavoučí sítě a vyvrací
  obvyklý názor, že stůl má mít čtyři rovné nohy. Ocelovou základnu doplňuje rustikální
  kompaktní deska, která dodává stolu kontrast i charakter. Vyznačuje se mimořádně
  originálním designem, ale lze jej snadno implementovat do jakéhokoli prostoru.</p>'
product_images:
- "/v1643280109/forestry/ATL_SPIDER_st%C5%AFl_1_plhjyr.png"
- "/v1643280109/forestry/ATL_SPIDER_st%C5%AFl_3_i4t8vc.png"
dimensions: "<p><strong>Šířka: </strong>230 cm - <strong>Délka: </strong>110 cm -
  <strong>Výška: </strong>74 cm</p>"
technical_info: "<p><strong>Materiál:</strong> kompaktní deska</p><p><strong>Nožičky:
  </strong>ocelové</p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>45 600 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/1e328c862ebedea784fb395ab3400e71a0138f50.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643280204/forestry/ATL_SPIDER_st%C5%AFl_2_lvwlcu.png"

---
