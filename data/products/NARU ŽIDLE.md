---
brand: data/brands/Artisan.md
name: 'NARU ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644249363/forestry/Product_foto-26_zy5vbr.png"
- "/v1644249674/forestry/Product_foto_kopie_3_b57ywi.png"
dimensions: "<p><strong>Šířka: </strong>78<strong> </strong>cm - <strong>Délka: </strong>59
  cm - <strong>Výška: </strong>52 cm - <strong>Výška sedu: </strong>48 cm - <strong>Hloubka
  sedu: </strong>42 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 48x42 cm</p><p>Průměr nohou: 3 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a>  </p>
price: "<p><strong>Cena od:</strong> 21 700 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604587497/forestry/ART_NARU_%C5%BEidle_3_jgkaru.png"
- "/v1604587497/forestry/ART_NARU_%C5%BEidle_2_noichr.png"

---
