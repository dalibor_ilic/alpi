---
brand: data/brands/Atlas.md
name: ADDA SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Adda je mimořádně elegantní pohovka malých
  rozměrů s velmi pohodlnými sedáky. Je ideální pro vybavení vašeho obytného prostoru
  i vaší kanceláře. Výplň z vysoce kvalitního peří přispívá k vyššímu pohodlí. Je
  určena pro ty, kterým záleží stejnou měrou na estetice i pohodlí. Díky svým zaobleným
  liniím a nadýchaným sedákům může být vyrobena výhradně v látce.</p>'
product_images:
- "/v1642237707/forestry/ATL_ADDA_sofa_1_ilkqma.png"
- "/v1642237698/forestry/ATL_ADDA_sofa_7_drkmzp.png"
- "/v1642237676/forestry/ATL_ADDA_sofa_5_czf1of.png"
- "/v1642237642/forestry/ATL_ADDA_sofa_2_j6eju0.png"
- "/v1642237657/forestry/ATL_ADDA_sofa_3_llvc5z.png"
- "/v1642237687/forestry/ATL_ADDA_sofa_4_rrspeh.png"
dimensions: "<p>Dvojsed: </p><p><strong>Šířka: </strong>139 cm - <strong>Délka: </strong>81
  cm - <strong>Výška: </strong>73 cm - <strong>Výška sedu:</strong> 41 cm - <strong>Hloubka
  sedu:</strong> 67 cm</p><p>Trojsed: </p><p><strong>Šířka: </strong>183 cm - <strong>Délka:
  </strong>81 cm - <strong>Výška: </strong>73 cm - <strong>Výška sedu:</strong> 41
  cm - <strong>Hloubka sedu:</strong> 67 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka</p><p><strong>Sedák:</strong> peří se
  silikonem, HR 35/36 pěna, paměťová pěna</p><p><strong>Opěradlo:</strong> pěna L
  25/15</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:</strong> plastové
  nožičky 20 mm</p><p></p>"
materials: <p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p>
price: "<p><strong>Cena od: </strong>24 000 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/6b2873283140516d95370d859c9a14c113bbc204.pdf"
  title="">Stáhnout PFD</a></p>'
product_photos:
- "/v1642238273/forestry/ATL_ADDA_sofa_10_whvo9c.png"
- "/v1642238273/forestry/ATL_ADDA_sofa_9_vqbifl.png"
- "/v1642238273/forestry/ATL_ADDA_sofa_8_fukovw.png"
- "/v1642238273/forestry/ATL_ADDA_sofa_6_rgbfkq.png"

---
