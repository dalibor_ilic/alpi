---
brand: data/brands/Atlas.md
name: 'CT 15 KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Tato sada konferenčních stolků dodá vašemu
  obývacímu prostoru styl a šmrnc. Vyberte si desky z mramoru, dýhované MDF, lakované
  v barvách RAL, nebo lakované lněným olejem. Konferenční stolek CT 15 bude ideální
  pro implementaci do moderního prostoru. K dispozici jsou různé rozměry, takže si
  můžete sestavit kompozici podle konkrétního návrhu.</p>'
product_images:
- "/v1643284808/forestry/ATL_CT15_konferen%C4%8Dn%C3%AD_stolek_1_kc5oww.png"
- "/v1643284807/forestry/ATL_CT15_konferen%C4%8Dn%C3%AD_stolek_5_suwo8h.png"
- "/v1643284808/forestry/ATL_CT15_konferen%C4%8Dn%C3%AD_stolek_4_gxjurt.png"
dimensions: "<p>Rozměr I.</p><p><strong>Šířka: </strong>90 cm - <strong>Délka: </strong>90
  cm - <strong>Výška: </strong>38 cm</p><p>Rozměr II.</p><p><strong>Šířka: </strong>72
  cm - <strong>Délka: </strong>72 cm - <strong>Výška: </strong>30 cm</p><p>Rozměr
  III.</p><p><strong>Šířka: </strong>60 cm - <strong>Délka: </strong>60 cm - <strong>Výška:
  </strong>50 cm</p><p>Rozměr IV.</p><p><strong>Šířka: </strong>80 cm - <strong>Délka:
  </strong>80 cm - <strong>Výška: </strong>30 cm</p>"
technical_info: "<p><strong>Materiál:</strong> MDF, mramor, dub - olej, nebo lak</p>"
materials: ''
price: "<p><strong>Cena od: </strong>7 300 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/0a263f61ed000690063ebe089d95e3778d4a5c04.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643284983/forestry/ATL_CT15_konferen%C4%8Dn%C3%AD_stolek_3_zksz6g.png"
- "/v1643284983/forestry/ATL_CT15_konferen%C4%8Dn%C3%AD_stolek_2_ka5qs3.png"

---
