---
brand: data/brands/Woak.md
name: 'MARSHALL STOJAN NA OBLEČENÍ '
categories:
- Accessories
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Dřevěné části tohoto nábytku jsou ručně
  vybírány jedna část po druhé, aby dokonale pasovaly s ostatními materiály, které
  jsou zcela inovativní a recyklovatelné, jako je nezpracovaná kůže, čistý mramor
  nebo lité sklo s měnícími se barvami. Kombinace teplých a studených materiálů vede
  k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem je mužný,
  ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací pokoj nebo
  koutek na čtení.</p>'
product_images:
- "/v1606895905/forestry/WHO_MARSHALL_stojan_na_oble%C4%8Den%C3%AD_4_df4vfb.png"
- "/v1606895905/forestry/WHO_MARSHALL_stojan_na_oble%C4%8Den%C3%AD_1_rl4t2u.png"
- "/v1606895904/forestry/WHO_MARSHALL_stojan_na_oble%C4%8Den%C3%AD_2_jqz36a.png"
- "/v1606895905/forestry/WHO_MARSHALL_stojan_na_oble%C4%8Den%C3%AD_3_dy9v8a.png"
dimensions: "<p><strong>Šířka: </strong>91 cm - <strong>Délka: </strong>39 cm - <strong>Výška:</strong>
  150 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>mramor: Emperador nebo Carrara</p><p>povrchová
  úprava: lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>32 300 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Marshall_coat_rail_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606896148/forestry/WHO_MARSHALL_stojan_na_oble%C4%8Den%C3%AD_6_cmlhia.png"
- "/v1606896149/forestry/WHO_MARSHALL_stojan_na_oble%C4%8Den%C3%AD_5_xujeio.png"

---
