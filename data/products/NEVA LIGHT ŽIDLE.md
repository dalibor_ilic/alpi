---
brand: data/brands/Artisan.md
name: 'NEVA LIGHT ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644250299/forestry/Product_foto-31_xfcvqk.png"
- "/v1644250299/forestry/Product_foto-32_fwd7ch.png"
- "/v1644250299/forestry/Product_foto-33_lolsn7.png"
- "/v1644250315/forestry/Product_foto-34_j9drz6.png"
- "/v1604665942/forestry/ART_NEVALIGHT_%C5%BEidle_5_nkskf6.png"
- "/v1604665966/forestry/ART_NEVALIGHT_%C5%BEidle_10_yvzwik.png"
- "/v1604665941/forestry/ART_NEVALIGHT_%C5%BEidle_1_tbbvqf.png"
dimensions: "<p><strong>Šířka: </strong>47<strong> </strong>cm - <strong>Délka: </strong>51
  cm - <strong>Výška: </strong>79 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 46x45 cm</p><p>Průmer nohou: spodní část 2,7 cm, vrchní část 3,5 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 19 600 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604666229/forestry/ART_NEVALIGHT_%C5%BEidle_8_klyljg.png"
- "/v1604666229/forestry/ART_NEVALIGHT_%C5%BEidle_7_r5kq7f.png"
- "/v1604666228/forestry/ART_NEVALIGHT_%C5%BEidle_6_qrufjo.png"

---
