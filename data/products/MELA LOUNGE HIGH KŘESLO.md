---
brand: data/brands/Artisan.md
name: MELA LOUNGE HIGH KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645444131/forestry/Product_foto-4k%C5%99eslo_zw4elr.png"
- "/v1645444131/forestry/Product_foto-3k%C5%99eslo_amf4qm.png"
dimensions: "<p><strong>Šířka: </strong>89 cm - <strong>Délka: </strong>92 cm<strong>
  </strong>- <strong>Výška: </strong>99 cm - <strong>Výška sedu: </strong>42 cm<br></p>"
technical_info: "<p>Velikost sedu: 81x57 cm</p><p>Průměr nohou: spodní část  3,5 cm,
  vrchní část 4 cm </p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>73 800 Kč</p>"
product_info: ''
product_photos:
- "/v1645444240/forestry/Z_interi%C3%A9ru-2k%C5%99eslo_vlbvnw.png"

---
