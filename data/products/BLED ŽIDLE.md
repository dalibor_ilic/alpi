---
brand: data/brands/Woak.md
name: 'BLED ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Dřevěné části tohoto nábytku jsou ručně
  vybírány jedna část po druhé, aby dokonale pasovaly s ostatními materiály, které
  jsou zcela inovativní a recyklovatelné, jako je nezpracovaná kůže, čistý mramor
  nebo lité sklo s měnícími se barvami. Kombinace teplých a studených materiálů vede
  k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem je mužný,
  ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací pokoj nebo
  koutek na čtení.</p>'
product_images:
- "/v1607091262/forestry/WHO_BLED_%C5%BEidle_1_j1dssk.png"
- "/v1607091272/forestry/WHO_BLED_%C5%BEidle_5_xcgmaw.png"
- "/v1607091272/forestry/WHO_BLED_%C5%BEidle_6_bk9tk0.png"
- "/v1607091272/forestry/WHO_BLED_%C5%BEidle_7_pvfrhi.png"
dimensions: "<p><strong>Šířka: </strong>53 cm - <strong>Délka: </strong>54 cm - <strong>Výška:</strong>
  78 cm</p>"
technical_info: "<p>materiál: ořech nebo dub, látka, kůže</p><p>povrchová úprava:
  lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a></p>
price: "<p><strong>Cena od: </strong>22 400 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Bled_chair_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607091388/forestry/WHO_BLED_%C5%BEidle_4_yzbl3x.png"
- "/v1607091390/forestry/WHO_BLED_%C5%BEidle_3_cvrfkm.png"
- "/v1607091388/forestry/WHO_BLED_%C5%BEidle_2_latjl9.png"

---
