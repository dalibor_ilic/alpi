---
brand: data/brands/Atlas.md
name: MAGNUM SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Vysokou estetickou hodnotu tohoto modelu
  doplňuje kvalitní paměťová pěna, která poskytuje pocit pohodlí a relaxace. Tento
  model s rovnými liniemi je obohacen o precizní prošívané detaily na područkách,
  které dotvářejí vzhled pohovky Magnum a činí ji velmi atraktivní a žádoucí v každém
  prostoru. Je jen na vás, zda si vyberete některou z kvalitních látek a kůží, které
  nabízíme, a přizpůsobíte si model podle svého vkusu.</p><p></p>'
product_images:
- "/v1643013629/forestry/ATL_MAGNUM_sofa_1_gin36u.png"
dimensions: "<p>Rohová souprava verze I.</p><p><strong>Šířka: </strong>280 cm - <strong>Délka:
  </strong>280 cm - <strong>Výška: </strong>84 cm - <strong>Výška sedu:</strong> 42
  cm- <strong>Hloubka sedu:</strong> 61 cm</p><p>Rohová souprava verze II.</p><p><strong>Šířka:
  </strong>280 cm - <strong>Délka: </strong>197 cm - <strong>Výška: </strong>84 cm
  - <strong>Výška sedu:</strong> 42 cm- <strong>Hloubka sedu:</strong> 61 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36, paměťová pěna, peří se silikonem</p><p><strong>Opěradlo: </strong>paměťová
  pěna,<strong> </strong>peří se silikonem ve vatovém potahu</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>dřevěné nohy lakované PU lakem</p><p><strong>Možnosti:</strong>
  mechanismus rozkládací postele </p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1x3qtWSfvDZh45kB8JxujwYgvlKTEPGhz/view?usp=sharing"
  title="Látky ELEGANT"><strong>ELEGANT</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>119 300 Kč </p><p><strong>Cena v látce
  od: </strong>81 800 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/653f633f2a8b0872c175b840c49ec43dde542400.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643013985/forestry/ATL_MAGNUM_sofa_3_egj0bp.png"
- "/v1643013985/forestry/ATL_MAGNUM_sofa_2_ntqwib.png"

---
