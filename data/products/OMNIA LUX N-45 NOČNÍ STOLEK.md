---
brand: data/brands/Atlas.md
name: OMNIA LUX N-45 NOČNÍ STOLEK
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Vysoce
  kvalitní dřevo, elegantní design a nádherná povrchová úprava jistě udělají dojem
  na každém nadčasovém dekoru.</p>'
product_images:
- "/v1648838406/forestry/Product_foto_pcwwe8.png"
- "/v1648838405/forestry/Product_foto-2_gqwnpk.png"
dimensions: "<p><strong>Šířka: </strong>45<strong> </strong>cm - <strong>Délka: </strong>42
  cm - <strong>Výška: </strong>50,5 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní dubové dřevo</p><p><strong>Nožičky:</strong>
  ocelové</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p><p>Kůže:
  <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"><strong>CAT
  300-1000</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/6253ab2a884b1e72b95f4cf1907c3fdd837ef1fc.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1648838817/forestry/Z_interi%C3%A9ru-4_gp84o0.png"
- "/v1648838817/forestry/Z_interi%C3%A9ru-3_j81bhg.png"
- "/v1648838807/forestry/Z_interi%C3%A9ru-2_qc5ny9.png"

---
