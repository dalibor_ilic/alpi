---
brand: data/brands/Artisan.md
name: 'INVITO KOMODA '
categories:
- Sideboard
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645442083/forestry/Product_fotokomoda_qzeijk.png"
dimensions: "<p><strong>Šířka: </strong>140/180/240 cm cm - <strong>Délka: </strong>42
  \ cm<strong> </strong>- <strong>Výška: </strong>75 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka komody: 2 cm </p><p>Přední tloušťka:
  2,2 cm</p><p>Zadní tloušťka: 1,5 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1DKp-AqR_-rESWlFNEpcr1prxEig5SpKq/view?usp=sharing
  " title=""><strong>BAREVNÉ DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>51 300 Kč</p>"
product_info: ''
product_photos:
- "/v1645442110/forestry/Z_interi%C3%A9ru-2komoda_tks2kz.png"
- "/v1645442125/forestry/Z_interi%C3%A9ru._komoda_qpcyt8.png"

---
