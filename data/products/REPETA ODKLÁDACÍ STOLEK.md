---
brand: data/brands/Woak.md
name: 'REPETA ODKLÁDACÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Repeta je sbírka malých komod s velmi
  složitou strukturou. Počínaje montážním systémem, který mohou provádět pouze odborné
  ruce tesaře. Ale především pečlivé drážky na povrchů, která jsou výsledkem přání
  přenést grafický vzor na povrch, a tedy dvourozměrnou techniku do trojrozměrného
  prvku.</p>'
product_images:
- "/v1607087336/forestry/WHO_REPETA_odkl%C3%A1dac%C3%AD_stolek_1_oqkllw.png"
dimensions: "<p><strong>Šířka: </strong>65 cm - <strong>Délka: </strong>65 cm - <strong>Výška:</strong>
  36 cm</p>"
technical_info: "<p>materiál: ořech, dub</p><p>podnos: dřevo, mramor nebo měď</p><p>povrchová
  úprava: olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>46 000 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Repeta10_side_table_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607087449/forestry/WHO_REPETA_odkl%C3%A1dac%C3%AD_stolek_3_yudmpx.png"
- "/v1607087450/forestry/WHO_REPETA_odkl%C3%A1dac%C3%AD_stolek_4_lcfkm9.png"
- "/v1607087450/forestry/WHO_REPETA_odkl%C3%A1dac%C3%AD_stolek_2_jc4ian.png"

---
