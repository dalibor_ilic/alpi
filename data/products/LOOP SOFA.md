---
brand: data/brands/Extraform.md
name: LOOP SOFA
categories:
- Sofa
description: <p>Výrobce :<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Loop
  je sestavitelná pohovka, která má moderní vzhled. Měkká uvnitř a zvenčí výrazná.
  Toto dává sedačce Loop jeho charakter. Obvodový šev, který vede kolem opěradla.
  Loop Vám nabízí řadu modelů a materiály. Můžete si vytvořit pohovku, kterou si přejete,
  která je klasická nebo moderní a elegantní. Nabízí pohodlné sezení.</p>
product_images:
- "/v1644507565/forestry/EXT_LOOP_sofa_9_d9ze0e.png"
- "/v1644507549/forestry/EXT_LOOP_sofa_1_c6yboy.png"
- "/v1644507550/forestry/EXT_LOOP_sofa_2_znaxs4.png"
- "/v1644507564/forestry/EXT_LOOP_sofa_8_t4f0hr.png"
- "/v1644507550/forestry/EXT_LOOP_sofa_3_fmuv4c.png"
dimensions: '<p><strong>Dvojsed: Šířka: </strong>188 cm - <strong>Délka: </strong>86
  cm - <strong>Výška:</strong> 82 cm - <strong>Výška sedu:</strong> 46 cm - <strong>Hloubka
  sedu:</strong> 54 cm</p><p class="Paragraph SCXW67801239 BCX4"><strong>Trojsed:
  Šířka: </strong>208 cm - <strong>Délka:</strong> 86<strong> </strong>cm - <strong>Výška:</strong>
  82<strong> </strong>cm - <strong>Výška sedu:</strong> 46<strong> </strong>cm - <strong>Hloubka
  sedu:</strong> 54 cm</p><p class="Paragraph SCXW67801239 BCX4"><strong>Rohová sedací
  souprava: Šířka: </strong>238-278 cm - <strong>Délka:</strong> 156 cm - <strong>Výška:</strong>
  82 cm - <strong>Výška sedu:</strong> 46 cm - <strong>Hloubka sedu:</strong> 54 cm</p>'
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, lepenky, 18 mm silné
  překližky, 22 mm MDF.</p><p class="Paragraph SCXW108846968 BCX4">Sedáky - vysoce
  elastická HR pěna, 200 g tepelně pojené plsti, elastické popruhy.</p><p class="Paragraph
  SCXW108846968 BCX4">Opěrky - ergonomická pěna RG 22, 400 g tepelně pojené plsti.</p><p
  class="Paragraph SCXW108846968 BCX4">Nohy z dubového dřeva, naolejované. Volitelně
  z bukového dřeva v šesti barvách.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>44 900 Kč</p>"
product_info: ''
product_photos:
- "/v1644507519/forestry/EXT_LOOP_sofa_5_alesbi.png"
- "/v1644507431/forestry/EXT_LOOP_sofa_7_oqel9j.png"
- "/v1644507520/forestry/EXT_LOOP_sofa_4_aozxcv.png"
- "/v1644507427/forestry/EXT_LOOP_sofa_6_tpka02.png"

---
