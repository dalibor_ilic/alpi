---
brand: data/brands/Artisan.md
name: 'LASTA KONZOLOVÝ STOLEK '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605532894/forestry/ART_LASTA_konzolov%C3%BD_stolek_1_oo9tam.png"
- "/v1644248914/forestry/Product_foto-21_uvyc1n.png"
dimensions: "<p><strong>Šířka: </strong>100/120<strong> </strong>cm - <strong>Délka:
  </strong>50 cm - <strong>Výška: </strong>82 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 2 cm</p><p>Tloušťka
  přední části: 5 cm</p><p>Průměr nohou: spodní část 3x3 cm, vrchní část 4x4 cm </p><p>Stůl
  má jednu zásuvku.</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>42 800 Kč</p>"
product_info: ''
product_photos:
- "/v1605533022/forestry/ART_LASTA_konzolov%C3%BD_stolek_3_avfxyx.png"

---
