---
brand: data/brands/Artisan.md
name: CASTULA STŮL
categories:
- Table
description: ''
product_images:
- "/v1605535941/forestry/ART_CASTULA_st%C5%AFl_3_mhcnqh.png"
- "/v1644247297/forestry/Product_foto-3_egjc6b.png"
- "/v1605535950/forestry/ART_CASTULA_st%C5%AFl_5_gnrl10.png"
- "/v1605535941/forestry/ART_CASTULA_st%C5%AFl_1_dceovo.png"
dimensions: "<p><strong>Šířka: 116/180/200/220/240 </strong>cm - <strong>Délka: 106</strong>
  cm - <strong>Výška: 76</strong> cm</p>"
technical_info: ''
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: ''
product_info: "<p></p>"
product_photos:
- "/v1605536074/forestry/ART_CASTULA_st%C5%AFl_4_g8kqon.png"

---
