---
brand: data/brands/Atlas.md
name: SLEEPOVER SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Rohová sedací souprava vytvořená s cílem
  poskytnout optimální pohodlí a funkčnost. Patří k praktickým kouskům určených k
  vybavení bytů a apartmánů. Nadčasový půvab a vynikající řemeslné zpracování se odráží
  v plochých tvarech, účelné a jednoduché konstrukci. Tento model nabízí velmi útulné
  lůžko pro každodenní spaní a úložný box.</p>'
product_images:
- "/v1643023396/forestry/ATL_SLEEPOVER_sofa_bed_1_hjqsyx.png"
dimensions: "<p><strong>Šířka: </strong>276 cm - <strong>Délka: </strong>176 cm -
  <strong>Výška: </strong>98 cm - <strong>Výška sedu:</strong> 42 cm - <strong>Hloubka
  sedu:</strong> 73 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka</p><p><strong>Sedák:</strong> HR pěna
  35/36 ve vatovém potahu</p><p><strong>Opěradlo: </strong>zadní silikon ve vatovém
  potahu</p><p><strong>Vlnové pružiny a elastické popruhy</strong></p><p><strong>Nožičky:
  </strong>dřevěné nohy lakované PU lakem, nebo v barvě RAL</p><p><strong>Možnosti</strong>:
  zvedací mechanismus</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>72 600 Kč </p>"
product_info: <p>Produktová karta:<a href="https://atlassofas.eu/login/files/76ad413cd419330ff8ee70998eae87a19c2431fa.pdf"
  title=""> Stáhnout PDF</a></p>
product_photos:
- "/v1643023625/forestry/ATL_SLEEPOVER_sofa_bed_5_d0dmnf.png"
- "/v1643023625/forestry/ATL_SLEEPOVER_sofa_bed_4_gyfmfl.png"
- "/v1643023625/forestry/ATL_SLEEPOVER_sofa_bed_3_ozymlq.png"
- "/v1643023626/forestry/ATL_SLEEPOVER_sofa_bed_2_tqg9m1.png"

---
