---
brand: data/brands/Artisan.md
name: INVITO POSTEL
categories:
- Bed
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605531310/forestry/ART_INVITO_postel_1_ojuvxj.png"
dimensions: "<p><strong>Šířka: </strong>160 - 200 cm - <strong>Délka: </strong>200
  cm - <strong>Výška: </strong>90 cm - <strong>Výška sedu: </strong>35 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>76 400 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605531439/forestry/ART_INVITO_postel_5_via0bv.png"
- "/v1605531439/forestry/ART_INVITO_postel_4_e5ijx3.png"
- "/v1605531440/forestry/ART_INVITO_postel_3_hsqkpc.png"
- "/v1605531438/forestry/ART_INVITO_postel_2_pzvt9p.png"

---
