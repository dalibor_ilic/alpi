---
brand: data/brands/Woak.md
name: 'MALIN KONFEREČNÍ STOLEK SE SKLEM '
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606749232/forestry/WHO_MALIN_Odkl%C3%A1dac%C3%AD_stolek_se_sklen%C4%9Bnou_deskou_1_sfj96j.png"
- "/v1606749239/forestry/WHO_MALIN_Odkl%C3%A1dac%C3%AD_stolek_se_sklen%C4%9Bnou_deskou_3_ge5zlb.png"
dimensions: "<p><strong>Šířka: </strong>42 cm - <strong>Délka: </strong>42 cm - <strong>Výška:</strong>
  45 cm</p>"
technical_info: "<p>Tento konferenční stolek je vyroben z masivního dřeva (dubu nebo
  ořechu) a má olejovou úpravu. </p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 16 900 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_side_table_with_glass_top_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1t6Xk1S4gTIzFDlKP_FtQzqRuxY7X4UOA/view?usp=sharing"
  title=""><strong>Stáhnout PDF</strong></a><strong> </strong></p>'
product_photos:
- "/v1606749445/forestry/WHO_MALIN_Odkl%C3%A1dac%C3%AD_stolek_se_sklen%C4%9Bnou_deskou_2_ticabn.png"

---
