---
brand: data/brands/Atlas.md
name: 'CT 10 KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Sada klubových stolů CT 10 je koncipována
  jako kombinace dvou stolů různé výšky a průměru, které dohromady tvoří harmonickou
  kompozici. Díky svému jednoduchému a univerzálnímu vzhledu snadno zapadne do interiérů
  různých stylů. Deska z MDF může být lakovaná barvou RAL. Existuje také možnost natřít
  nohy jinou barvou.</p>'
product_images:
- "/v1643283914/forestry/ATL_CT10_konferen%C4%8Dn%C3%AD_stolky_rdwyiw.png"
dimensions: "<p>Rozměr I.</p><p><strong>Šířka: </strong>100 cm - <strong>Délka: </strong>100
  cm - <strong>Výška: </strong>35 cm</p><p>Rozměr II.</p><p><strong>Šířka: </strong>80
  cm - <strong>Délka: </strong>80 cm - <strong>Výška: </strong>35 cm</p>"
technical_info: "<p><strong>Materiál:</strong> MDF</p><p><strong>Nožičky: </strong>možnost
  natřít barvou</p>"
materials: "<p></p>"
price: "<p><strong>Cena od:</strong> 7 000 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/620f934d1312c9808aba1911b4169d4b19edd21a.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos: []

---
