---
brand: data/brands/Atlas.md
name: RONDO KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Křeslo se zaoblenými, pohodlnými hranami,
  které vypadá velmi originálně. Lze jej vyrobit v různých kombinacích materiálů.
  Pro milovníky neobvyklých kombinací doporučujeme kombinaci látky a kůže. Zajímavý
  detail, který zdobí tento model, je prošívaná zadní část.</p>'
product_images:
- "/v1643200243/forestry/ATL_RONDO_k%C5%99eslo_1_pzvtud.png"
dimensions: "<p><strong>Šířka: </strong>87 cm - <strong>Délka: </strong>98 cm - <strong>Výška:
  </strong>67 cm - <strong>Výška sedu:</strong> 42 cm - <strong>Hloubka sedu:</strong>
  57 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Podpěra:</strong>
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 - HR pěna 35/30</p><p><strong>Opěradlo:</strong> HR pěna 35/36 - HR
  pěna 35/30</p><p><strong>Područky:</strong> HR pěna 35/30</p><p><strong>Nožičky:
  </strong>plastové nožičky 20 mm</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>26 700 Kč</p><p><strong>Cena v látce od:
  </strong>19 200 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/17e9e8e94bf288b04790bca8eec3f9f92cc06d3b.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643200480/forestry/ATL_RONDO_k%C5%99eslo_5_fgpflm.png"
- "/v1643200480/forestry/ATL_RONDO_k%C5%99eslo_4_stjo0i.png"
- "/v1643200480/forestry/ATL_RONDO_k%C5%99eslo_3_kjsxn7.png"
- "/v1643200480/forestry/ATL_RONDO_k%C5%99eslo_2_uwygen.png"

---
