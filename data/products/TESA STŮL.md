---
brand: data/brands/Artisan.md
name: TESA STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1604995990/forestry/ART_TESA_st%C5%AFl_1_kilprl.png"
- "/v1604995990/forestry/ART_TESA_st%C5%AFl_3_ucuccc.png"
- "/v1644484413/forestry/Product_foto-52_euwr0i.png"
dimensions: "<p><strong>Šířka: </strong>180 - 240 cm - <strong>Délka: </strong>90
  - 100 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a za příplatek 10% možnost šíře
  110 cm.</p><p>Tloušťka desky: 4 cm</p><p>Průměr nohou: 1,2 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>65 800 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605683979/forestry/ART_TESA_st%C5%AFl_2_sxy2lz.png"

---
