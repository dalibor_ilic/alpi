---
brand: data/brands/Atlas.md
name: MODENA SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Inovativní mechanismus l v kombinaci s
  vysoce kvalitními potahy činí tuto pohovku výjimečnou. Velmi elegantní vzhled pohovky
  Modena doplňuje viditelný rám z masivního dubového dřeva v kombinaci s moderními
  kovovými nožkami, které lze nabarvit podle vzorníku RAL. Tato pohovka se od ostatních
  liší díky masivním dubovým panelům, které zdobí opěradla rohového prvku.</p>'
product_images:
- "/v1643016093/forestry/ATL_MODENA_sofa_1_rmfle8.png"
- "/v1643016093/forestry/ATL_MODENA_sofa_9_lidmas.png"
- "/v1643016094/forestry/ATL_MODENA_sofa_10_yevnrk.png"
- "/v1643016094/forestry/ATL_MODENA_sofa_8_yyqyz9.png"
- "/v1643016094/forestry/ATL_MODENA_sofa_7_glczfd.png"
dimensions: "<p><strong>Šířka: </strong>351 cm - <strong>Délka: </strong>249 cm -
  <strong>Výška: </strong>86 cm - <strong>Výška sedu:</strong> 43 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely/ dubovým rámem a dubovými panely v opěradle</p><p><strong>Potah:</strong>
  látka</p><p><strong>Sedák:</strong> HR pěna 35/30</p><p><strong>Opěradlo: </strong>pěna
  L 25/15</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky: </strong>kovové
  nohy lakované v barvě RAL, dubový lak lakovaný PU lakem, nebo v barvě RAL</p><p><strong>Možnosti:</strong>
  mechanismus pro nastavitelné područky a opěrky hlavy (více poloh)</p>"
materials: <p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p>
price: "<p><strong>Cena v kůži od: </strong>150 900 Kč </p><p><strong>Cena v látce
  od: </strong>109 100 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/886d2d57a6a113f349659be92ad38d40693dfae8.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643016420/forestry/ATL_MODENA_sofa_6_pzstl1.png"
- "/v1643016416/forestry/ATL_MODENA_sofa_5_fis2gm.png"
- "/v1643016420/forestry/ATL_MODENA_sofa_3_pbepj6.png"
- "/v1643016420/forestry/ATL_MODENA_sofa_2_knsueo.png"

---
