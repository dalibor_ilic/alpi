---
brand: data/brands/Artisan.md
name: 'TESA KOMODA '
categories:
- Sideboard
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645445348/forestry/Product_foto-6_xelnox.png"
- "/v1645445348/forestry/Product_foto-3_r7wdro.png"
- "/v1645445348/forestry/Product_foto-4_yg1zbh.png"
- "/v1645445348/forestry/Product_foto_kopie_edwxnc.png"
- "/v1645445344/forestry/Product_foto_aso6lw.png"
- "/v1645445349/forestry/Product_foto-5_lv8l4u.png"
- "/v1645445349/forestry/Product_foto-2_aady2f.png"
- "/v1645445348/forestry/Product_foto-7_ljf87o.png"
dimensions: "<p><strong>Šířka: </strong>160/180/200 cm - <strong>Délka: </strong>41
  cm<strong> </strong>- <strong>Výška: </strong>70 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Nohy: Ø 1,2 cm, matně broušená nerezová ocel
  v kombinaci se dřevem</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO </strong></a></p>
price: "<p><strong>Cena od: </strong>53 600 Kč</p>"
product_info: ''
product_photos:
- "/v1645445677/forestry/Z_interi%C3%A9ru-3_bglwpm.png"
- "/v1645445651/forestry/Z_interi%C3%A9ru-6_zqyjw8.png"
- "/v1645445715/forestry/Z_interi%C3%A9ru-2_uu4tql.png"
- "/v1645445651/forestry/Z_interi%C3%A9ru-5_iardc5.png"
- "/v1645445651/forestry/Z_interi%C3%A9ru_wyxl22.png"
- "/v1645445739/forestry/Z_interi%C3%A9ru-4_d4fdry.png"

---
