---
brand: data/brands/Extraform.md
name: MOOVE SOFA
categories:
- Sofa
description: <p>Výrobce<strong> </strong>:<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Tato
  „skládačka“ představuje sestavitelný systém, který lze využít jako pohovku, křeslo
  nebo stůl. Každý kus může fungovat ve spojení s ostatními nebo samostatně. Je skvěle
  přizpůsoben jakémukoli typu prostoru, zejména kancelářím.</p>
product_images:
- "/v1645089067/forestry/EXT_MOOVE_sofa_1_zbqyi6.png"
- "/v1645089052/forestry/EXT_MOOVE_sofa_10_baaret.png"
- "/v1645089047/forestry/EXT_MOOVE_sofa_9_g8oodz.png"
- "/v1645089033/forestry/EXT_MOOVE_sofa_14_zfany9.png"
- "/v1645089033/forestry/EXT_MOOVE_sofa_13_rwa8p5.png"
- "/v1645089033/forestry/EXT_MOOVE_sofa_12_i13dlc.png"
- "/v1645089047/forestry/EXT_MOOVE_sofa_8_opotax.png"
- "/v1645089033/forestry/EXT_MOOVE_sofa_11_vl7u1b.png"
- "/v1645089046/forestry/EXT_MOOVE_sofa_7_ku7p55.png"
- "/v1645089033/forestry/EXT_MOOVE_sofa_15_gobx5k.png"
dimensions: "<p><strong>Výška:</strong> 81 cm - <strong>Výška sedu:</strong> 42 cm
  - <strong>Hloubka sedu:</strong> 60 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, 18 mm silná překližka,
  16 mm silná dřevotříska, lepenka.</p><p class="Paragraph SCXW201567182 BCX4">Vysoce
  elastická HR pěna, 200 g bavlněné vatové textilie pojené za tepla, rouno, vinuté
  pružiny.</p><p class="Paragraph SCXW201567182 BCX4">Ergonomická pěna RG 25, 200
  g bavlněné textilie s tepelnou vazbou.</p><p class="Paragraph SCXW201567182 BCX4">Pěna
  RG 30, 200 g bavlněné vatové směsi s tepelnou vazbou.</p><p class="Paragraph SCXW201567182
  BCX4">Nohy z dubového dřeva, naolejované. Volitelně z bukového dřeva v šesti barvách.</p><p
  class="Paragraph SCXW201567182 BCX4">Pevná rohová pohovka.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>77 500 Kč </p>"
product_info: ''
product_photos:
- "/v1645089153/forestry/EXT_MOOVE_sofa_6_b3u2qb.png"
- "/v1645089126/forestry/EXT_MOOVE_sofa_4_ogt6ow.png"
- "/v1645089127/forestry/EXT_MOOVE_sofa_5_u7qm6w.png"
- "/v1645089126/forestry/EXT_MOOVE_sofa_2_aidwfo.png"
- "/v1645089127/forestry/EXT_MOOVE_sofa_3_jiodzv.png"

---
