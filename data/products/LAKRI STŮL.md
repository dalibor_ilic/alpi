---
brand: data/brands/Artisan.md
name: 'LAKRI STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605606092/forestry/ART_LAKRI_kulat%C3%BD_st%C5%AFl_6_lkr6wx.png"
- "/v1605606085/forestry/ART_LAKRI_kulat%C3%BD_st%C5%AFl_2_q9ughs.png"
- "/v1605606084/forestry/ART_LAKRI_kulat%C3%BD_st%C5%AFl_1_navirj.png"
dimensions: "<p><strong>Šířka: </strong>80-160<strong> </strong>cm - <strong>Výška:
  </strong>74 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální
  tloušťka desky: 0,8 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 62 600 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605606223/forestry/ART_LAKRI_kulat%C3%BD_st%C5%AFl_5_b8qft8.png"
- "/v1605606222/forestry/ART_LAKRI_kulat%C3%BD_st%C5%AFl_4_rlh7qm.png"
- "/v1605606223/forestry/ART_LAKRI_kulat%C3%BD_st%C5%AFl_3_msscej.png"

---
