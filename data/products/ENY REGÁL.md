---
brand: data/brands/Artisan.md
name: 'ENY REGÁL '
categories:
- Shelf
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605264909/forestry/ART_ENY_reg%C3%A1l_1_knknrm.png"
dimensions: "<p><strong>Šířka: </strong>50/90/100<strong> </strong>cm - <strong>Délka:
  </strong>44 cm - <strong>Výška: </strong>120/160/200 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka polic: 2 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>37 200 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605265109/forestry/ART_ENY_reg%C3%A1l_2_batzam.png"

---
