---
brand: data/brands/Artisan.md
name: 'LUC KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645443707/forestry/Product_fotokonferen%C4%8Dn%C3%AD_stolek_yrjzvj.png"
- "/v1645443707/forestry/Product_foto-2konferen%C4%8Dn%C3%AD_stolek_gnps2b.png"
- "/v1645443707/forestry/Product_foto-5konferen%C4%8Dn%C3%AD_stolek_k7glgh.png"
- "/v1645443707/forestry/Product_foto-3konferen%C4%8Dn%C3%AD_stolek_vjshcr.png"
- "/v1645443707/forestry/Product_foto-4konferen%C4%8Dn%C3%AD_stolek_acs1k7.png"
dimensions: "<p><strong>Šířka: </strong>100/120/140 cm - <strong>Délka: </strong>60/65/70
  cm<strong> </strong>- <strong>Výška: </strong>30/35/40/45 cm - <strong>Průměr: </strong>
  60/70/80 cm</p>"
technical_info: "<p>Tloušťka desky: 2 cm </p><p></p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a> | <a href="https://drive.google.com/file/d/1DKp-AqR_-rESWlFNEpcr1prxEig5SpKq/view?usp=sharing
  " title=""><strong>BAREVNÉ DŘEVO</strong></a></p>
price: ''
product_info: ''
product_photos:
- "/v1645444009/forestry/Z_interi%C3%A9ru-2konferen%C4%8Dn%C3%AD_stolek_rnronk.png"
- "/v1645443994/forestry/Z_interi%C3%A9rukonferen%C4%8Dn%C3%AD_stolek_mwzcja.png"

---
