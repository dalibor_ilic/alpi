---
brand: data/brands/Extraform.md
name: 'VIVA SOFA BED '
categories:
- Sofa bed system
description: <p>Výrobce<strong> </strong>:<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Viva
  je nábytek s rozkládacím spacím mechanismem, který má flexibilní geometrické opěradlo,
  pohodlné pro sezení i ležení. Tato pohovka má skrytý úložný box a lze ji rozložit
  na postel jediným pohybem.</p>
product_images:
- "/v1645090516/forestry/EXT_VIVA_sofa_bed_1_grpitz.png"
- "/v1645090495/forestry/EXT_VIVA_sofa_bed_4_stmkkn.png"
- "/v1645090495/forestry/EXT_VIVA_sofa_bed_3_x6ufxs.png"
- "/v1645090508/forestry/EXT_VIVA_sofa_bed_7_jyua8o.png"
- "/v1645090495/forestry/EXT_VIVA_sofa_bed_8_nbc8gb.png"
- "/v1645090508/forestry/EXT_VIVA_sofa_bed_6_w4uajt.png"
- "/v1645090495/forestry/EXT_VIVA_sofa_bed_5_we6gvv.png"
dimensions: "<p><strong>Výška:</strong> 76-94 cm - <strong>Výška sedu:</strong> 46
  cm - <strong>Hloubka sedu:</strong> 60 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, dřevotříska o tloušťce
  16 mm, lepenka, překližka o tloušťce 18 mm, 3 mm silná sololitová deska.</p><p class="Paragraph
  SCXW150538653 BCX4">Sedáky - Kombinovaná tvrdá pěna a vysoce elastická HR pěna,
  300 g tepelně pojené bavlněné vaty, 500 g rouna, vinuté pružiny.</p><p class="Paragraph
  SCXW150538653 BCX4">Opěrky - Ergonomická vysoce elastická pěna, gumičky, nastavitelná
  opěrka hlavy.</p><p class="Paragraph SCXW150538653 BCX4">Nohy z bukového dřeva,
  mořené a lakované. K dispozici v šesti barvách.</p><p class="Paragraph SCXW150538653
  BCX4">Výsuvná postel je volitelná pro dvoumístné a třímístné pohovky.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 76 200 Kč </p>"
product_info: ''
product_photos:
- "/v1645090626/forestry/EXT_VIVA_sofa_bed_2_af0a4l.png"

---
