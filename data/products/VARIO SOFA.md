---
brand: data/brands/Extraform.md
name: VARIO SOFA
categories:
- Sofa
description: <p>Výrobce<strong> </strong>:<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Flexibilní
  a elegantně navržená pohovka harmonických proporcí. Nenápadný detaily jako schované
  dřevěné nohy a lehce zaoblené opěradlo a loketní opěrky zdůrazňuji jedinečnost tohoto
  kusu nábytku.</p>
product_images:
- "/v1645090191/forestry/EXT_VARIO_sofa_5_s86blo.png"
- "/v1645090205/forestry/EXT_VARIO_sofa_6_xsn3su.png"
- "/v1645090206/forestry/EXT_VARIO_sofa_7_b1pmwo.png"
- "/v1645090206/forestry/EXT_VARIO_sofa_8_o9z3ex.png"
- "/v1645090191/forestry/EXT_VARIO_sofa_1_qcr1nx.png"
- "/v1645090191/forestry/EXT_VARIO_sofa_2_igeavo.png"
- "/v1645090190/forestry/EXT_VARIO_sofa_4_gqrsma.png"
- "/v1645090191/forestry/EXT_VARIO_sofa_3_zeymcw.png"
dimensions: "<p><strong>Výška: </strong>88 cm - <strong>Výška sedu: </strong>46 cm
  - <strong>Hloubka sedu: </strong>62 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, překližky o tloušťce
  18 mm, lepenky.</p><p class="Paragraph SCXW95015881 BCX4">Vysoce elastická HR pěna,
  500 g tepelně pojená bavlněná vata, rouno, vinuté pružiny.</p><p class="Paragraph
  SCXW95015881 BCX4">Ergonomická pěna RG 25, 300 g bavlněná vatová směs s tepelnou
  vazbou.</p><p class="Paragraph SCXW95015881 BCX4">Pěna RG 30, 300 g tepelně vázané
  bavlněné výplně.</p><p class="Paragraph SCXW95015881 BCX4">Nohy z bukového dřeva,
  mořené a lakované. K dispozici v šesti barvách.</p><p class="Paragraph SCXW95015881
  BCX4">Pevná rohová pohovka.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>77 500 Kč </p>"
product_info: ''
product_photos:
- "/v1645090385/forestry/EXT_VARIO_sofa_10_lqezt0.png"
- "/v1645090386/forestry/EXT_VARIO_sofa_9_mus7fv.png"
- "/v1645090397/forestry/EXT_VARIO_sofa_11_ftvfyp.png"

---
