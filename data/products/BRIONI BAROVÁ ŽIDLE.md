---
brand: data/brands/Woak.md
name: 'BRIONI BAROVÁ ŽIDLE '
categories:
- Bar chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a><strong> </strong></p><p>Barové židle Brioni
  jsou k dispozici ve dvou výškách a barevné možnosti jsou přírodní ořech nebo dub
  v 5 barvách. jsou navrženy, aby se vešly do jídelny nebo salonku s vytříbenou atmosférou,
  kde se design stává výrazným prvkem.</p><p>Dřevěné části tohoto nábytku jsou ručně
  vybírány jedna část po druhé, aby dokonale pasovaly s ostatními materiály, které
  jsou zcela inovativní a recyklovatelné, jako je nezpracovaná kůže, čistý mramor
  nebo lité sklo s měnícími se barvami. Kombinace teplých a studených materiálů vede
  k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem je mužný,
  ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací pokoj nebo
  koutek na čtení.</p>'
product_images:
- "/v1607088892/forestry/WHO_BRIONI_barov%C3%A1_%C5%BEidle_5_cbt7f4.png"
- "/v1607088884/forestry/WHO_BRIONI_barov%C3%A1_%C5%BEidle_1_iemch5.png"
- "/v1607088892/forestry/WHO_BRIONI_barov%C3%A1_%C5%BEidle_6_u729hz.png"
- "/v1607088892/forestry/WHO_BRIONI_barov%C3%A1_%C5%BEidle_7_qylywe.png"
dimensions: "<p><strong>Šířka: </strong>48/49 cm - <strong>Délka: </strong>50 cm -
  <strong>Výška:</strong> 78/88 cm</p>"
technical_info: "<p>materiál: ořech nebo dub, látka, kůže</p><p>povrchová úprava:
  lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>21 500 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Brioni_bar_stool_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607088907/forestry/WHO_BRIONI_barov%C3%A1_%C5%BEidle_4_js7d4u.png"
- "/v1607088910/forestry/WHO_BRIONI_barov%C3%A1_%C5%BEidle_3_vbm8e2.png"
- "/v1607088907/forestry/WHO_BRIONI_barov%C3%A1_%C5%BEidle_2_fsoi5g.png"

---
