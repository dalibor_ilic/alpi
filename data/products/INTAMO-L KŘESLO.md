---
brand: data/brands/Extraform.md
name: INTAMO-L KŘESLO
categories:
- Arm chair
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Intamo
  je křeslo, které je dynamické a lehké, se šikmými a tenkými liniemi. Zadní opěradlo
  je navrženo ve třech velikostech, které Vám umožní získat různá řešení pro Vaše
  vnitřní prostory.</p>'
product_images:
- "/v1644505748/forestry/EXT_INTAMO_k%C5%99eslo_10_qk0rxl.png"
- "/v1644505748/forestry/EXT_INTAMO_k%C5%99eslo_11_v4nbbd.png"
- "/v1644505759/forestry/EXT_INTAMO_k%C5%99eslo_13_ew7gru.png"
- "/v1644505759/forestry/EXT_INTAMO_k%C5%99eslo_12_zuhwkm.png"
dimensions: "<p><strong>Šířka: </strong>69<strong> </strong>cm - <strong>Délka: </strong>82
  cm - <strong>Výška: </strong>116 cm - <strong>Výška sedu: </strong>44 cm - <strong>Hloubka
  sedu: </strong>51 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, překližky o tloušťce
  18 mm.</p><p class="Paragraph SCXW14076062 BCX4">Sedák - ergonomická pěna RG 35,
  100 g tepelně pojené plsti, elastické popruhy.</p><p class="Paragraph SCXW14076062
  BCX4">Opěrka - ergonomická pěna RG 30, 100 g tepelně pojené plsti.</p><p class="Paragraph
  SCXW14076062 BCX4">Nohy z dubového dřeva, naolejované. Volitelně z bukového dřeva
  v šesti barvách.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>20 300 Kč</p>"
product_info: ''
product_photos:
- "/v1644505820/forestry/EXT_INTAMO_k%C5%99eslo_6_pdazc8.png"

---
