---
brand: data/brands/Artisan.md
name: NEVA STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605011400/forestry/ART_NEVA_st%C5%AFl_1_gwdmbv.png"
- "/v1644250616/forestry/Product_foto-37_mkh6bi.png"
dimensions: "<p><strong>Šířka: </strong>160 - 240 cm - <strong>Délka: </strong>90
  - 100 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a za příplatek 10% možnost šíře
  110 cm.</p><p>Tloušťka desky: 2 cm </p><p>Vizuální tloušťka desky: 2 cm</p><p>Průměr
  nohou: spodní část 6x6 cm, vrchní část 7,5x7,5 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>64 300 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605608531/forestry/ART_NEVA_st%C5%AFl_2_ygcwhq.png"

---
