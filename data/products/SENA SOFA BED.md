---
brand: data/brands/Atlas.md
name: SENA SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Kvalitní polstrování sedadel a dalších
  polštářů zaručuje výjimečné pohodlí a hřejivé rodinné chvíle. Naši řemeslníci tvrdě
  pracovali na nalezení dokonalého poměru kvalitního polstrování, směsi pěny, peří
  a paměťové pěny, aby zajistili maximální pohodlí. Extrémně hluboké sedáky v kombinaci
  s rozmanitými polštáři vybízejí k odpočinku. Moderní pohovka, která zaujme svým
  neobvyklým designem.</p>'
product_images:
- "/v1643022891/forestry/ATL_SENA_sofa_1_qxlxc6.png"
- "/v1643022892/forestry/ATL_SENA_sofa_3_egtoba.png"
dimensions: "<p><strong>Šířka: </strong>340 cm - <strong>Délka: </strong>175 cm -
  <strong>Výška: </strong>80 cm - <strong>Výška sedu:</strong> 48 cm - <strong>Hloubka
  sedu:</strong> 84 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka</p><p><strong>Sedák:</strong> peří, paměťová
  pěna a silikon, potažené paměťovou pěnovou vrstvou o specifické hustotě 55 m³/m²</p><p><strong>Opěradlo:
  </strong>peří, paměťová pěna, silikon</p><p><strong>Vlnové pružiny</strong></p><p><strong>Nožičky:
  </strong>dřevěné nohy - mořené, lakované PU lakem, nebo v barvě RAL</p><p></p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>77 800 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/56df998204db4cf6cb485a5195c5c90ee5a52793.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643023179/forestry/ATL_SENA_sofa_2_wdxbel.png"

---
