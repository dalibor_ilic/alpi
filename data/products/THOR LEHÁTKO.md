---
brand: data/brands/Artisan.md
name: 'THOR LEHÁTKO '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605175191/forestry/ART_THOR_leh%C3%A1tko_3_ia6f08.png"
- "/v1605175191/forestry/ART_THOR_leh%C3%A1tko_2_fk77qr.png"
- "/v1605175191/forestry/ART_THOR_leh%C3%A1tko_1_bovmaf.png"
dimensions: "<p><strong>Šířka: </strong>178<strong> </strong>cm - <strong>Délka: </strong>89
  cm - <strong>Výška: </strong>43 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení ve 2 kategoriích látek.</p><p>Tloušťka
  desky: 1,2 cm</p><p>Vizuální tloušťka desky: 4 cm</p><p>Průměr nohou: spodní část
  6x4,2 cm, vrchní část 9,5x4,2 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> |<a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong> DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>57 500 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605175692/forestry/ART_THOR_leh%C3%A1tko_5_cwrhhk.png"
- "/v1605175692/forestry/ART_THOR_leh%C3%A1tko_6_dmod6y.png"
- "/v1605175692/forestry/ART_THOR_leh%C3%A1tko_4_mvbowr.png"

---
