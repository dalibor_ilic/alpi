---
brand: data/brands/Woak.md
name: 'NINAS STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Ústředním prvkem jsou minimalistické
  linie aplikované na kusy retro nábytku. Cílem kolekce je nabídnout novou interpretaci
  klasických kusů nábytku, prostřednictvím obnovy stylu pomocí techniky zpracované
  značkou Woak. Záměrem je rozebrat společná místa tradičních forem, eliminovat ozdoby
  a prvky, které jsou příliš dekorativní, aby byl objekt moderní a zároveň funkční.</p>'
product_images:
- "/v1607005619/forestry/WHO_NINAS_st%C5%AFl_1_mhxxcg.png"
- "/v1607005626/forestry/WHO_NINAS_st%C5%AFl_5_fyfrm9.png"
dimensions: "<p><strong>Šířka: </strong>160 - 220 cm - <strong>Délka: </strong>90
  - 100 cm - <strong>Výška:</strong> 75 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>povrchová úprava: lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>61 600 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Ninas_dining_table_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607005752/forestry/WHO_NINAS_st%C5%AFl_4_don4mc.png"
- "/v1607005753/forestry/WHO_NINAS_st%C5%AFl_2_x3lpck.png"
- "/v1607005752/forestry/WHO_NINAS_st%C5%AFl_3_vryqou.png"

---
