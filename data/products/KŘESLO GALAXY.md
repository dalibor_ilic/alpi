---
brand: data/brands/Atlas.md
name: GALAXY KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Jednoduché tvary, minimalistická estetika
  a mírně skloněná záda jsou hlavními charakteristikami tohoto krásného křesla. Křeslo
  Galaxy je kus nábytku, který nenápadně zapadne do každého interiéru.  Křeslo Galaxy
  bude dokonalým doplňkem ve vašem obývacím prostoru, nebo v ložnici. Přinese vám
  naprostou relaxaci, ať už si budete číst, nebo sledovat film.</p>'
product_images:
- "/v1643110353/forestry/ATL_GALAXY_k%C5%99eslo_1_dv5jep.png"
dimensions: "<p><strong>Šířka: </strong>76 cm - <strong>Délka: </strong>102 cm - <strong>Výška:
  </strong>105 cm - <strong>Výška sedu:</strong> 42 cm - <strong>Hloubka sedu:</strong>
  55 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> HR pěna 35/36</p><p><strong>Opěradlo:
  </strong>HR pěna 35/30</p><p><strong>Područky: </strong>HR pěna 35/30</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>kovové</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od:</strong> 27 500 Kč</p><p><strong>Cena v látce od:
  </strong>20 300 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/47d6d1caa1a0a22a7c5bc3d6ebd0ccc4fc21f6df.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643110507/forestry/ATL_GALAXY_k%C5%99eslo_3_jyox0v.png"
- "/v1643110509/forestry/ATL_GALAXY_k%C5%99eslo_2_tnv5us.png"

---
