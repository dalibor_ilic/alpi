---
brand: data/brands/Prostoria.md
name: "Segment\tSofa"
categories:
- Sofa
description: "<p><strong>Segment </strong>křeslo je součástí sestavy Segment, který
  řeší hierarchické principy montáže komponentů modulárního čalouněného nábytku. Identita
  Segment křesla je dána formou jeho základny s charakteristickými základními oboustrannými
  konci skloněnými pod úhlem 45 °, což umožňuje sestavení dílů ve všech třech osách.
  Jedná se o koncept, který poskytuje možnost sestavit v prostoru řadu různých funkčních
  i netradičních kompozic. Nespočet kombinací a obměn základny slouží k navrhování
  různých funkčních situací v interiéru. Segment je k dispozici jako jednomístný,
  dvoumístný, trojmístný, čtyřmístný, pětimístný - zkrátka „vícemístná“ řada. Kombinace
  základny, která je na jedné straně podepřena kovovými nožičkami, zatímco podpůrná
  dřevěná součást s vysoce lesklým povrchem bude vyčnívat zpod základny na druhé straně
  systému. Geometrický motiv jeho formy je podle diktátu interiéru stejně kompatibilní
  s koženým potahem i s bohatě texturovanými látkami.</p>"
product_images:
- "/v1602513552/forestry/Product_fotoSEGMENT_sofa_l3t7dd.jpg"
- "/v1602513554/forestry/Product_fotoSEGMENT_sofa-1_ovjzbo.jpg"
- "/v1602513553/forestry/Product_fotoSEGMENT_sofa-2_ui88yb.jpg"
dimensions: "<p><strong>Šířka: </strong>237cm - <strong>Délka:</strong> 220cm - <strong>Výška:</strong>
  69cm - <strong>Výška sedu:</strong> 38cm - <strong>Hloubka sedu:</strong> 68cm<br></p>"
technical_info: "<p>Struktura: masivní dřevo, překližka</p><p>Zavěšení: vlnové pružiny</p><p>Sedák
  a opěradlo: HR pěna, polyesterová vata</p><p>Základna: kovové nohy + práškově lakovaná
  matná / lakovaná platforma MDF</p><p>Čalounění: látka / kůže</p>"
materials: '<p>Více informací: <a href="https://rajnabytku.static.s10.upgates.com/y/y5f74aa96d32d8-materials-book-contract-collection-03-2020-ver1.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
price: "<p><strong>Cena od: </strong>75 200 CZK</p>"
product_info: '<p>Více informací: <a href="https://rajnabytku.static.s10.upgates.com/z/z5f75fdcc32e14-pro-segment-sofa.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1602513576/forestry/Z_interie%CC%81ruSEGMENT_sofa_nkwyzk.jpg"
- "/v1602513577/forestry/Z_interie%CC%81ruSEGMENT_sofa-2_ohwagr.jpg"
- "/v1602513577/forestry/Z_interie%CC%81ruSEGMENT_sofa-1_xhky7u.jpg"

---
