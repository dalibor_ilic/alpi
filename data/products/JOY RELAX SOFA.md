---
brand: data/brands/Extraform.md
name: JOY RELAX SOFA
categories:
- Sofa
description: <p>Výrobce :<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Masivní
  struktura této sedací soupravy s jemnými a pohodlnými liniemi je kontrastní ke svým
  charakteristickým kovovým nohám. Možnost instalace pohyblivého relaxačního mechanismu
  a mechanismus pro nastavení opěrek hlavy, činí tuto pohovku prominentní ve své kategorii.</p>
product_images:
- "/v1644506574/forestry/EXT_JOY_sofa_1_btps6h.png"
- "/v1644506558/forestry/EXT_JOY_sofa_6_fix4no.png"
- "/v1644506584/forestry/EXT_JOY_sofa_5_xakg9n.png"
- "/v1644506559/forestry/EXT_JOY_sofa_7_bgcybd.png"
- "/v1644506564/forestry/EXT_JOY_sofa_8_xdey8w.png"
dimensions: "<p><strong>Šířka: </strong>292 cm - <strong>Délka: </strong>109 cm -
  <strong>Výška:</strong> 83-108 cm - <strong>Výška sedu:</strong> 46 cm - <strong>Hloubka
  sedu: </strong>56 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, dřevotříska o tloušťce
  16 mm, lepenka, překližka o tloušťce 18 mm, 3 mm silná sololitová deska.</p><p class="Paragraph
  SCXW99905339 BCX4">Sedáky - Vysoce elastická HR pěna, 300 g tepelně pojené bavlněné
  vatování, elastické popruhy.</p><p class="Paragraph SCXW99905339 BCX4">Opěrky -
  Ergonomická vysoce elastická pěna, elastické popruhy, nastavitelná opěrka hlavy.</p><p
  class="Paragraph SCXW99905339 BCX4">Černé kovové nohy.</p><p class="Paragraph SCXW99905339
  BCX4">Relaxační mechanismus je možnost.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> </p>
price: "<p><strong>Cena od: </strong>83 100 Kč </p>"
product_info: ''
product_photos:
- "/v1644506653/forestry/EXT_JOY_sofa_3_t4i9rx.png"
- "/v1644506653/forestry/EXT_JOY_sofa_2_ghlnpm.png"
- "/v1644506654/forestry/EXT_JOY_sofa_4_hzso1w.png"

---
