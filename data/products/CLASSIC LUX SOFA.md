---
brand: data/brands/Atlas.md
name: CLASSIC LUX SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Směs klasiky a moderny pro opravdové požitkáře.
  Svůdná estetika tohoto modelu dodá šmrnc každému prostoru a jeho osobitost ještě
  více vynikne, je-li je vyroben v některé z luxusních italských kůží. Stylový vzhled
  této pohovky vám dopřeje mimořádné pohodlí a také funkčnost, protože obsahuje mechanismus
  rozkládací postele. </p>'
product_images:
- "/v1642432956/forestry/ATL_CLASSICLUX_sofa_1_wi8jxs.png"
- "/v1642432956/forestry/ATL_CLASSICLUX_sofa_4_ody8rt.png"
- "/v1642432957/forestry/ATL_CLASSICLUX_sofa_6_dda4qy.png"
- "/v1642432957/forestry/ATL_CLASSICLUX_sofa_8_hflk1p.png"
- "/v1642432957/forestry/ATL_CLASSICLUX_sofa_7_m9zwc6.png"
- "/v1642432957/forestry/ATL_CLASSICLUX_sofa_5_jdqbq2.png"
dimensions: "<p>Dvojsed:</p><p><strong>Šířka: </strong>186 cm - <strong>Délka: </strong>104
  cm - <strong>Výška: </strong>74 cm - <strong>Výška sedu:</strong> 45 cm - <strong>Hloubka
  sedu:</strong> 64 cm</p><p>Trojsed:</p><p><strong>Šířka: </strong>239 cm - <strong>Délka:
  </strong>104 cm - <strong>Výška: </strong>74 cm - <strong>Výška sedu:</strong> 45
  cm - <strong>Hloubka sedu:</strong> 64 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  pěna HR 35/36, paměťová pěna, peří se silikonem</p><p><strong>Opěradlo: </strong>pěna
  N 25/38 </p><p><strong>Elastické popruhy</strong></p><p><strong>Dřevěné nožičky:
  </strong>mořené, lakované v PU laku, nebo v barvě RAL</p><p><strong>Možnosti:</strong>
  mechanismus rozkládací postele</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>67 200 Kč </p><p><strong>Cena v látce
  od: </strong>43 700 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/e29c016720a2562fd673c8cd63e2f34e2bb3572a.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642433240/forestry/ATL_CLASSICLUX_sofa_3_ata8z5.png"
- "/v1642433244/forestry/ATL_CLASSICLUX_sofa_2_g96qdf.png"

---
