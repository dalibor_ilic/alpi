---
brand: data/brands/Atlas.md
name: LAGOM KONFERENČNÍ STOLEK
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p>'
product_images:
- "/v1648840088/forestry/Product_foto_tl4fi2.png"
- "/v1648840087/forestry/Product_foto-2_tvshfj.png"
dimensions: "<p><strong>Šířka: </strong>70<strong> </strong>cm - <strong>Délka: </strong>70
  cm - <strong>Výška: </strong>33 cm</p>"
technical_info: "<p><strong>Tloušťka desky:</strong> 4 cm</p><p><strong>Materiál desky:
  </strong>masivní dřevo/RAL</p><p><strong>Nožičky:</strong> masivní dřevo/RAL </p><p></p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"><strong>DUB</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/sr/product/lagom/550/"
  title=""><strong>Stáhnout PDF</strong></a> </p>'
product_photos:
- "/v1648840122/forestry/Z_interi%C3%A9ru-2_r9fr4g.png"

---
