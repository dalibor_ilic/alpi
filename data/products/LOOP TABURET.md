---
brand: data/brands/Extraform.md
name: LOOP TABURET
categories:
- Accessories
description: <p>Výrobce :<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Loop
  je sestavitelná pohovka, která má moderní vzhled. Měkká uvnitř a zvenčí výrazná.
  Toto dává sedačce Loop jeho charakter. Obvodový šev, který vede kolem opěradla.
  Loop Vám nabízí řadu modelů a materiály. Můžete si vytvořit pohovku, kterou si přejete,
  která je klasická nebo moderní a elegantní. Nabízí pohodlné sezení.</p>
product_images:
- "/v1644507263/forestry/EXT_LOOP_taburet_2_jnnhja.png"
- "/v1644507263/forestry/EXT_LOOP_taburet_1_cgjhit.png"
dimensions: "<p><strong>Šířka: </strong>90 cm - <strong>Délka:</strong> 70 cm - <strong>Výška:</strong>
  46 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, lepenky, 18 mm silné
  překližky, 22 mm MDF.</p><p class="Paragraph SCXW98895589 BCX4">Sedáky - vysoce
  elastická HR pěna, 200 g tepelně pojené plsti, elastické popruhy.</p><p class="Paragraph
  SCXW98895589 BCX4">Opěrky - ergonomická pěna RG 22, 400 g tepelně pojené plsti.</p><p
  class="Paragraph SCXW98895589 BCX4">Nohy z dubového dřeva, naolejované. Volitelně
  z bukového dřeva v šesti barvách.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>12 800 Kč </p>"
product_info: ''
product_photos:
- "/v1644507249/forestry/EXT_LOOP_taburet_3_bzw8oq.png"

---
