---
brand: data/brands/Woak.md
name: 'MALIN KULATÝ KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606747621/forestry/WHO_MALIN_Kulat%C3%BD_odkl%C3%A1dac%C3%AD_stolek_1_wksyn4.png"
dimensions: "<p><strong>Šířka: </strong>42,5 cm - <strong>Délka: </strong>42,5 cm
  - <strong>Výška:</strong> 50 cm</p>"
technical_info: "<p>Tento konferenční stolek je vyroben z masivního dřeva (dubu nebo
  ořechu) a má olejovou úpravu. </p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 12 700 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_side_table_round_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1Ma_tyMDbdWlBHsyXFRTkYEBFiSkcnMWt/view?usp=sharing"
  title=""><strong>Stáhnout PDF</strong></a><strong> </strong></p>'
product_photos:
- "/v1606749015/forestry/WHO_MALIN_Kulat%C3%BD_odkl%C3%A1dac%C3%AD_stolek_4_d9caym.png"
- "/v1606749020/forestry/WHO_MALIN_Kulat%C3%BD_odkl%C3%A1dac%C3%AD_stolek_2_bccq1m.png"
- "/v1606749019/forestry/WHO_MALIN_Kulat%C3%BD_odkl%C3%A1dac%C3%AD_stolek_3_axdqdc.png"

---
