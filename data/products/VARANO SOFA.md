---
brand: data/brands/Atlas.md
name: VARANO SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Moderní design, který nezapomíná na ergonomické
  požadavky. Varano má skvělé a pevné područky z oblých tvarů, které upoutají pozornost.
  Široké sedáky s anatomickou oporou poskytují maximální pohodlí. Pokud se rozhodnete
  pro luxus v moderním stylu, pak zvolte krásnou kolekci Varano, která stejně dobře
  splňuje vysoká estetická kritéria i kritéria pohodlí.</p>'
product_images:
- "/v1643099174/forestry/ATL_VARANO_sofa_1_e01oba.png"
- "/v1643099174/forestry/ATL_VARANO_sofa_8_zlh4pq.png"
- "/v1643099174/forestry/ATL_VARANO_sofa_10_bfeibt.png"
- "/v1643099174/forestry/ATL_VARANO_sofa_12_yby6nf.png"
- "/v1643099174/forestry/ATL_VARANO_sofa_11_nyokiy.png"
- "/v1643099174/forestry/ATL_VARANO_sofa_9_gtxmhr.png"
dimensions: "<p>Dvojsed:</p><p><strong>Šířka: </strong>181 cm - <strong>Délka: </strong>105
  cm - <strong>Výška: </strong>80 cm - <strong>Výška sedu:</strong> 41 cm - <strong>Hloubka
  sedu:</strong> 64 cm</p><p>Trojsed:</p><p><strong>Šířka: </strong>217 cm -<strong>Délka:
  </strong>105 cm - <strong>Výška: </strong>80 cm - <strong>Výška sedu:</strong> 41
  cm - <strong>Hloubka sedu:</strong> 64 cm</p><p>Čtyřsed:</p><p><strong>Šířka: </strong>281
  cm - <strong>Délka: </strong>105 cm - <strong>Výška: </strong>80 cm - <strong>Výška
  sedu:</strong> 41 cm - <strong>Hloubka sedu:</strong> 64 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/30 a pěna L 25/15</p><p><strong>Opěradlo: </strong>pěna L 25/15 a N 25/38</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>dřevěné nohy - mořené, lakované
  PU lakem, nebo v barvě RAL</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>54 000 Kč </p><p><strong>Cena v látce
  od: </strong>36 500 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/6b983410249640fa670b5eac3c72171e93a1bcc7.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643099459/forestry/ATL_VARANO_sofa_7_wcyh3y.png"
- "/v1643099460/forestry/ATL_VARANO_sofa_6_idvn18.png"
- "/v1643099461/forestry/ATL_VARANO_sofa_5_tlhcm4.png"
- "/v1643099457/forestry/ATL_VARANO_sofa_2_bg6lwj.png"
- "/v1643099461/forestry/ATL_VARANO_sofa_4_hjbqru.png"
- "/v1643099460/forestry/ATL_VARANO_sofa_3_xhhcw0.png"

---
