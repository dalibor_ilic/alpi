---
brand: data/brands/Extraform.md
name: FAIRY SOFA
categories:
- Sofa bed system
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"
  title="EXTRAFORM"><strong>EXTRAFORM</strong></a></p><p>Tato sestavitelná pohovka
  s rozkládacím spacím mechanismem může být přeměněna na postel jediným pohybem. Jemné
  linie sledují základní strukturu a její jedinečnost pochází z vyměnitelných područek.</p>'
product_images:
- "/v1644503001/forestry/EXT_FAIRY_sofa_5_cqjpta.png"
- "/v1644503001/forestry/EXT_FAIRY_sofa_4_dmrvj8.png"
- "/v1644503001/forestry/EXT_FAIRY_sofa_1_qsngdb.png"
- "/v1644503000/forestry/EXT_FAIRY_sofa_3_wtlgcs.png"
dimensions: "<p><strong>Dvojsed: Šířka: </strong>184 cm - <strong>Délka:</strong>
  99 cm - <strong>Výška:</strong> 93 cm - <strong>Výška sedu:</strong> 47 cm - <strong>Hloubka
  sedu:</strong> 53 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, dřevotříska o tloušťce
  16 mm, lepenka, překližka o tloušťce 18 mm, 3 mm silná sololitová deska.</p><p class="Paragraph
  SCXW104575367 BCX4">Sedáky - Vysoce elastická HR pěna, 300 g tepelně pojené bavlněné
  vaty, 500 g rouna, vinuté pružiny.</p><p class="Paragraph SCXW104575367 BCX4">Opěrky
  - rgonomická vysoce elastická pěna, polštáře plné silikonových kuliček a vloček,
  gumičky.</p><p class="Paragraph SCXW104575367 BCX4">Nohy z bukového dřeva, mořené
  a lakované. K dispozici v šesti barvách.</p><p class="Paragraph SCXW104575367 BCX4">Výsuvná
  postel je volitelná pro dvoumístné a třímístné pohovky.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>31 300 Kč</p>"
product_info: ''
product_photos:
- "/v1644503096/forestry/EXT_FAIRY_sofa_2_tiflky.png"

---
