---
brand: data/brands/Atlas.md
name: KOMODA OMNIA LUX NK 120
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Komoda Omnia Lux NK 120 představuje kombinaci
  přímých linií a barev. Detaily v podobě barevné kůže a strukturované povrchové úpravy
  čelních ploch - skryté a odhalené jako rafinovaný estetický detail spolu se základními
  liniemi - jsou výrazem šikovných rukou řemeslníků a jejich lásky ke dřevu.</p>'
product_images:
- "/v1643204134/forestry/oma1---123f2ee4860bf4068823b7d14dec85a7580627d3_ybd3hp.jpg"
- "/v1643204134/forestry/oma2---518789eedf72e010eacc22c7dc8e1f6aa4f80f50_juydbl.jpg"
dimensions: "<p><strong>Šířka: </strong>120 cm - <strong>Délka: </strong>42 cm - <strong>Výška:
  </strong>51,5 cm</p>"
technical_info: ''
materials: '<p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>21 900 Kč</p>"
product_info: ''
product_photos: []

---
