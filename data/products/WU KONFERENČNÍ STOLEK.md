---
brand: data/brands/Artisan.md
name: 'WU KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645446136/forestry/Product_fotokonferen%C4%8Dn%C3%AD_stolek_zwsjx6.png"
- "/v1645446134/forestry/Product_foto-2konferen%C4%8Dn%C3%AD_stolek_dxizpp.png"
dimensions: "<p><strong>Šířka: </strong>60/70/80 cm cm - <strong>Délka: </strong>60/70/80
  cm<strong> </strong>- <strong>Výška: </strong>30 cm</p>"
technical_info: "<p>Tloušťka desky: 2 cm </p><p></p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO </strong></a></p>
price: "<p><strong>Cena od: </strong>21 400 Kč</p>"
product_info: ''
product_photos:
- "/v1645446232/forestry/Z_interi%C3%A9rukonferen%C4%8Dn%C3%AD_stolek_ddtedw.png"

---
