---
brand: data/brands/Atlas.md
name: LONDON SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Pohovka
  London díky svému rozkošnému designu, pohodlnému sedadlu a modernímu vzhledu dodá  Vašemu
  životu nový rozměr. Přenese Vás do života módy a přepychu s nejvyšší úrovní pohodlí.
  </p>'
product_images:
- "/v1648811129/forestry/Product_foto_vswpjh.png"
- "/v1648811128/forestry/Product_foto-6_n44z31.png"
- "/v1648811129/forestry/Product_foto-5_zpqsik.png"
- "/v1648811129/forestry/Product_foto-4_p2oe1r.png"
- "/v1648811129/forestry/Product_foto-3_cev3es.png"
- "/v1648811129/forestry/Product_foto-2_efswrk.png"
dimensions: "<p><strong>Šířka: </strong>304<strong> </strong>cm - <strong>Délka: </strong>184
  cm - <strong>Výška: </strong>84 cm - <strong>Výška sedu:</strong> 44 cm - <strong>Hloubka
  sedu:</strong> 70 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, kůže</p><p><strong>Sedák: </strong>vysoce
  elastická polyuretanová HR pěna 35/36, silikonové kuličky v potahu polštáře</p><p><strong>Opěradlo:
  </strong>vysoce elastická polyuretanová pěna L 25/15, paměťová pěna, silikonové
  kuličky v povlaku na polštář</p><p><strong>Elastické pásky široké 7 cm</strong></p><p><strong>Nožičky:
  </strong>dřevěný rám - lazura, polyuretanový lak, krycí polyuretanová barva dle
  vzorníku RAL</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p><p>Kůže:
  <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"><strong>CAT
  300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"><strong>DUB</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/78fe8e3cd2fde7ada5bba37e2b0544ac43fcde55.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1648811933/forestry/Z_interi%C3%A9ru-5_loykpz.png"
- "/v1648811933/forestry/Z_interi%C3%A9ru-8_gnyz8t.png"
- "/v1648811934/forestry/Z_interi%C3%A9ru-4_bmpeme.png"
- "/v1648811933/forestry/Z_interi%C3%A9ru-2_cwmfpn.png"
- "/v1648811932/forestry/Z_interi%C3%A9ru-6_auye97.png"
- "/v1648811931/forestry/Z_interi%C3%A9ru-7_u5ddpb.png"
- "/v1648811927/forestry/Z_interi%C3%A9ru-3_f9s1v8.png"

---
