---
brand: data/brands/Atlas.md
name: MORFEO SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Určeno především pro městské prostředí,
  harmonické linie a rozměry. Pohovka Morfeo je zdobena řadou asymetrických, dekorativních
  stehů, které jí dodávají zvláštní vzhled. Milovníky elegance a asymetrie navíc zaujme
  částečně čalouněný rám a dubové nohy lakované lněným olejem. Popusťte uzdu své fantazii
  a z velkého množství nabízených prvků Morfeo si vytvořte ideální kompozici pro svůj
  prostor.</p>'
product_images:
- "/v1643016741/forestry/ATL_MORFEO_sofa_1_pjhyqu.png"
- "/v1643016742/forestry/ATL_MORFEO_sofa_7_cug8vs.png"
- "/v1643016742/forestry/ATL_MORFEO_sofa_8_rckgmq.png"
- "/v1643016742/forestry/ATL_MORFEO_sofa_9_mvgc4z.png"
dimensions: "<p><strong>Šířka: </strong>278 cm - <strong>Délka: </strong>216 cm -
  <strong>Výška: </strong>78 cm - <strong>Výška sedu:</strong> 43 cm- <strong>Hloubka
  sedu:</strong> 63 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/30</p><p><strong>Opěradlo: </strong>pěna L 25/15 a N 25/38</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>dřevěné nohy - mořené, lakované
  v PU laku, nebo v barvě RAL, nebo s efektem „páleného“ dřeva</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>104 000 Kč </p><p><strong>Cena v látce
  od: </strong>74 000 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/d5cb70e0f57f50f78fa4c8aa703b56d82bc092f8.pdf"
  title="">Stáhnout PDF</a> </p>'
product_photos:
- "/v1643017033/forestry/ATL_MORFEO_sofa_6_hhyboo.png"
- "/v1643017034/forestry/ATL_MORFEO_sofa_5_d4j4bg.png"
- "/v1643017034/forestry/ATL_MORFEO_sofa_4_l4te21.png"
- "/v1643017034/forestry/ATL_MORFEO_sofa_3_upk01q.png"
- "/v1643017034/forestry/ATL_MORFEO_sofa_2_m5pdjd.png"

---
