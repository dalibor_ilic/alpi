---
brand: data/brands/Woak.md
name: 'BUD PRACOVNÍ STŮL '
categories:
- Accessories
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Psací stůl Bud se skládá z masivní
  dřevěné základny se zásuvkou a dělicím panelem se dvěma pohyblivými deskami, které
  vymezují prostor a tlumí hluk. Funkčnost je umocněna základnou Kvadrat, kterou lze
  použít jako nástěnku na poznámky. Díky kompaktním rozměrům je přizpůsobivý i do
  menšího prostoru.</p>'
product_images:
- "/v1607000500/forestry/WHO_BUD_pracovn%C3%AD_st%C5%AFl_3_qj5k8k.png"
- "/v1607000500/forestry/WHO_BUD_pracovn%C3%AD_st%C5%AFl_1_be36bv.png"
- "/v1607000500/forestry/WHO_BUD_pracovn%C3%AD_st%C5%AFl_2_abfewq.png"
dimensions: "<p><strong>Šířka: </strong>120 cm - <strong>Délka: </strong>50 cm - <strong>Výška:</strong>
  75 cm</p>"
technical_info: "<p>materiál: ořech, dub</p><p>přehrádka: dřevo, mramor nebo měď</p><p>povrchová
  úprava: olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 50 200 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Bud_working_desk_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/13D-Y-75vZKQUT_oOdA6Y-jZIlI0lJH1W/view?usp=sharing"
  title=""><strong>Stáhnout PDF </strong></a></p>'
product_photos:
- "/v1607001058/forestry/WHO_BUD_pracovn%C3%AD_st%C5%AFl_5_wl7jxt.png"
- "/v1607001059/forestry/WHO_BUD_pracovn%C3%AD_st%C5%AFl_4_dhiafw.png"

---
