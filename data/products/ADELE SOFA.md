---
brand: data/brands/Atlas.md
name: ADELE SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Sada Adele je jasným důkazem schopnosti
  Atlasu vytvářet produkty využívající nejnovější technologie ve spojení s odbornými
  znalostmi v oblasti klasického čalounění. Sestavu Adele vyrobíme na míru vašim potřebám
  a stane se hlavním prvkem vašeho domova. Ne kvůli pohodlí a péřové výplni, ale kvůli
  ideálním proporcím a designu.</p>'
product_images:
- "/v1642153256/forestry/ATL_ADELE_sofa_7_uxsfav.png"
- "/v1642153257/forestry/ATL_ADELE_sofa_6_dcq98v.png"
- "/v1642153257/forestry/ATL_ADELE_sofa_5_sixhrk.png"
- "/v1642153257/forestry/ATL_ADELE_sofa_4_jwp0mc.png"
- "/v1642153256/forestry/ATL_ADELE_sofa_3_a8vmsg.png"
- "/v1642065417/forestry/ATL_ADELE_sofa_1_yeo2gl.png"
dimensions: "<p><strong>Šířka: </strong>284 cm - <strong>Délka: </strong>268 cm -
  <strong>Výška: </strong>83 cm - <strong>Výška sedu:</strong> 46 cm - <strong>Hloubka
  sedu:</strong> 58 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> husí peří, HR 35/36 pěna, memory
  pěna</p><p><strong>Elastické popruhy</strong></p><p><strong>Kovové nožičky:</strong>
  lakované v RAL</p>"
materials: '<p>Látky : <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>|<strong>
  </strong><a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže : <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>148 500 Kč </p><p><strong>Cena v látce
  od: </strong>108 300 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/e46e17556531358f1789044fb274e44d001b5c27.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1642066220/forestry/ATL_ADELE_sofa_2_ibg6t8.png"

---
