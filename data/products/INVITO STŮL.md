---
brand: data/brands/Artisan.md
name: INVITO STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605603124/forestry/ART_INVITO_st%C5%AFl_2_eg4hrq.png"
- "/v1605603118/forestry/ART_INVITO_st%C5%AFl_1_rtywn2.png"
dimensions: "<p><strong>Šířka: </strong>94- 220<strong> </strong>cm - <strong>Délka:</strong>
  94-104 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a za příplatek 10% možnost šíře
  114cm.</p><p>Tloušťka desky: 2 cm </p><p>Vizuální tloušťka desky: 7,5 cm</p><p>Průměr
  nohou: spodní část 5,8x4cm, vrchní část 11x9,7 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 39 200 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605603287/forestry/ART_INVITO_st%C5%AFl_3_ih64ke.png"

---
