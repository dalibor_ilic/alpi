---
brand: data/brands/Atlas.md
name: MADRID SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Sedací souprava Madrid představuje kreativní
  kompozici svobody. Minimalistický rám z masivního buku v kombinaci s polštáři snadno
  zapadne do každého obytného prostoru. Díky dokonalým proporcím je sedací souprava
  vhodná pro všechny styly zařízení. Tam, kde se snoubí pohodlí a lineárnost, vzniká
  každodenní komfort.</p>'
product_images:
- "/v1642236379/forestry/ATL_MADRID_sofa_1_cfpqgb.png"
- "/v1642236386/forestry/ATL_MADRID_sofa_2_xhpqcj.png"
- "/v1642236397/forestry/ATL_MADRID_sofa_3_dde2ha.png"
- "/v1642236408/forestry/ATL_MADRID_sofa_4_vmv0dl.png"
- "/v1642236429/forestry/ATL_MADRID_sofa_5_m0bv11.png"
dimensions: "<p><strong>Šířka: </strong>293 cm - <strong>Délka: </strong>214 cm -
  <strong>Výška: </strong>90 cm - <strong>Výška sedu:</strong> 43 cm - <strong>Hloubka
  sedu:</strong> 66 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> HR 35/30</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Kovové nožičky:</strong> lakované v RAL</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC "><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a> </p>'
price: "<p><strong>Cena v kůži od: </strong>116 400 Kč </p><p><strong>Cena v látce
  od: </strong>75 300 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/025ab676e25935ec30dfd2cad8edf4a08434263d.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos: []

---
