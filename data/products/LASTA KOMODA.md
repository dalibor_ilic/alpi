---
brand: data/brands/Artisan.md
name: 'LASTA KOMODA '
categories:
- Sideboard
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645709563/forestry/Lasta_komodaProduct_foto-2_zssygf.png"
- "/v1645709563/forestry/Lasta_komodaProduct_foto-3_hoqent.png"
- "/v1645709562/forestry/Lasta_komodaProduct_foto_u8gvod.png"
dimensions: "<p><strong>Šířka: </strong>160/180/200/220<strong> </strong>cm - <strong>Délka:
  </strong>51<strong> </strong>cm - <strong>Výška: </strong>45/70/82 cm</p>"
technical_info: "<p>Výška nohou: 20 cm </p><p>Průměr nohou: 1,5 cm </p><p>Materiál
  nohou: nerezová ocel </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>107 000 Kč</p>"
product_info: ''
product_photos:
- "/v1645709600/forestry/Lasta_komodaZ_interi%C3%A9ru-2_dxcpqy.png"
- "/v1645709601/forestry/Lasta_komodaZ_interi%C3%A9ru_xfp4cm.png"

---
