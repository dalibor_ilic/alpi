---
brand: data/brands/Artisan.md
name: 'LATUS LAVICE '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605177753/forestry/ART_LATUS_lavice_5_myzrdf.png"
- "/v1605177744/forestry/ART_LATUS_lavice_2_bkcibn.png"
- "/v1605177744/forestry/ART_LATUS_lavice_1_k6g0bg.png"
dimensions: "<p><strong>Šířka: </strong>160 - 220<strong> </strong>cm - <strong>Délka:
  </strong>45 cm - <strong>Výška: </strong>40 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka vrchní desky:
  4 cm</p><p>Vizuální tloušťka vrchní desky: 6 cm</p><p>Průměr nohou: spodní část
  3 cm, vrchní část 6 cm</p><p>Úpravy rozměrů možné na vyžádání.</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><br></p>
price: "<p><strong>Cena od: </strong>28 300 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605177964/forestry/ART_LATUS_lavice_4_ioiihv.png"
- "/v1605177963/forestry/ART_LATUS_lavice_3_bqw82h.png"

---
