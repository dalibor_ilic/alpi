---
brand: data/brands/Extraform.md
name: 'STAGE SOFA '
categories:
- Sofa
description: <p>Výrobce<strong> </strong>:<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p><br>Modulární
  pohovka s vysokým opěradlem má možnost s velkými polštáři či bez nich. Jednou z
  funkcí tohoto typu domácí pohovky je možnost vložení boční dřevěné police.</p>
product_images:
- "/v1645089608/forestry/EXT_STAGE_sofa_6_bvnvl8.png"
- "/v1645089621/forestry/EXT_STAGE_sofa_8_n1rp3a.png"
- "/v1645089608/forestry/EXT_STAGE_sofa_2_oba6mi.png"
- "/v1645089607/forestry/EXT_STAGE_sofa_1_cfcdqn.png"
- "/v1645089608/forestry/EXT_STAGE_sofa_7_nushlg.png"
dimensions: "<p><strong>Rohová sedací souprava: Šířka: </strong>270 cm - <strong>Délka:</strong>
  165 cm - <strong>Výška:</strong> 88 cm - <strong>Výška sedu:</strong> 46 cm - <strong>Hloubka
  sedu:</strong> 62 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, překližky o tloušťce
  18 mm, lepenky.</p><p class="Paragraph SCXW114924549 BCX4">Vysoce elastická HR pěna,
  500 g tepelně pojené bavlněné vaty, rouno, vinuté pružiny</p><p class="Paragraph
  SCXW114924549 BCX4">Ergonomická pěna RG 25, 300 g bavlněné vatové textilie s tepelnou
  vazbou</p><p class="Paragraph SCXW114924549 BCX4">Pěna RG 30, 300 g tepelně vázané
  bavlněné výplně</p><p class="Paragraph SCXW114924549 BCX4">Nohy z bukového dřeva,
  mořené a lakované. K dispozici v šesti barvách.</p><p class="Paragraph SCXW114924549
  BCX4">Pevná rohová pohovka.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 73 700 Kč </p>"
product_info: ''
product_photos:
- "/v1645089671/forestry/EXT_STAGE_sofa_5_pi9mvf.png"
- "/v1645089671/forestry/EXT_STAGE_sofa_4_gpwv4x.png"
- "/v1645089671/forestry/EXT_STAGE_sofa_3_qki9pk.png"

---
