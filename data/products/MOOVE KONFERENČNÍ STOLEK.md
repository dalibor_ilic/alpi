---
brand: data/brands/Extraform.md
name: MOOVE KONFERENČNÍ STOLEK
categories:
- Coffee table
description: <p>Výrobce :<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Tato
  „skládačka“ představuje sestavitelný systém, který lze využít jako pohovku, křeslo
  nebo stůl. Každý kus může fungovat ve spojení s ostatními nebo samostatně. Je skvěle
  přizpůsoben jakémukoli typu prostoru, zejména kancelářím.</p>
product_images:
- "/v1644508036/forestry/EXT_MOOVE_konferen%C4%8Dn%C3%AD_stolek_x7a8e4.png"
dimensions: "<p><strong>Šířka: </strong>80-240 cm - <strong>Délka:</strong> 80 cm
  - <strong>Výška:</strong> cm</p>"
technical_info: "<p>Nosná konstrukce ze sušeného bukového dřeva, 18 mm silná překližka,
  16 mm silná dřevotříska, lepenka.</p>"
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 22 900 Kč</p>"
product_info: ''
product_photos: []

---
