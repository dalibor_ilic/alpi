---
brand: data/brands/Atlas.md
name: LANDSCAPE SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Elegantní, neodolatelná, exkluzivní, to
  je to, co vás jako první napadne, když uvidíte tuto pohovku. Rovné, redukované linie
  dělají z tohoto modelu tu správnou volbu pro milovníky estetiky, zatímco možnost
  kombinovat prvky umožňuje splnit všechny funkční a rozměrové požadavky. Díky neobvyklým
  válečkovým polštářům a kovovým klubovým stolkům můžete doplnit vzhled této soupravy
  a dodat jí další úroveň pohodlí a funkčnosti. Prošívání detailů na prvcích je to,
  co tento model odlišuje od zbytku naší nabídky a dává vám možnost vytvořit si dokonalou
  sedací soupravu pro váš domov.</p>'
product_images:
- "/v1643013420/forestry/ATL_LANDSCAPE_sofa_16_lbgf0x.png"
dimensions: "<p>Dvojsed:</p><p><strong>Šířka: </strong>250 cm - <strong>Délka: </strong>100
  cm - <strong>Výška: </strong>75 cm - <strong>Výška sedu:</strong> 38 cm - <strong>Hloubka
  sedu:</strong> 73 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo/kovový rám</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> HR pěna 35/36 a HR pěna 35/30, peří
  se silikonem</p><p><strong>Opěradlo: </strong>peří se silikonem</p><p><strong>Nožičky:
  </strong>kovové nohy lakované v barvě RAL a efekt surového železa</p><p><strong>Možnosti:</strong>
  stolní lakované MDF nebo naolejované masivní dubové dřevo</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od:</strong> 74 300 Kč </p><p><strong>Cena v látce
  od: </strong>52 700 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/45b5fdb43c4d62a5f47fb31d47e7e98d8ccf3645.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643013470/forestry/ATL_LANDSCAPE_sofa_9_bj1o5t.png"
- "/v1643013470/forestry/ATL_LANDSCAPE_sofa_8_nmqjeu.png"
- "/v1643013470/forestry/ATL_LANDSCAPE_sofa_5_eswyef.png"

---
