---
brand: data/brands/Atlas.md
name: ETERNA STŮL
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Jemná
  povrchová úprava a několik ostrých linií jsou navrženy tak, aby dodávaly Vašemu
  jídelnímu prostoru moderní a stylový nádech.</p>'
product_images:
- "/v1648838977/forestry/Product_foto_g4ex8q.png"
- "/v1648838977/forestry/Product_foto-2_gvuhof.png"
dimensions: "<p><strong>Šířka: </strong>180cm - <strong>Délka: </strong>90 cm - <strong>Výška:
  </strong>77 cm </p>"
technical_info: "<p><strong>Materiál desky stolu: </strong>MDF v barvě RAL</p><p><strong>Nohy
  stolu:</strong> masivní dřevo/RAL</p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"><strong>DUB</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/fa1a30bed5ae6bdd385b175f90a576674ca15125.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1648839344/forestry/Z_interi%C3%A9ru-2_dmeces.png"
- "/v1648839344/forestry/Z_interi%C3%A9ru_gojbgp.png"

---
