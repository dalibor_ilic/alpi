---
brand: data/brands/Artisan.md
name: INVITO ROZKLÁDACÍ STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605602693/forestry/ART_INVITO_rozkl%C3%A1dac%C3%AD_st%C5%AFl_1_az3kgu.png"
- "/v1644248682/forestry/Product_foto-17_eu2vsk.png"
- "/v1644248682/forestry/Product_foto-18_yfpybz.png"
- "/v1605602702/forestry/ART_INVITO_rozkl%C3%A1dac%C3%AD_st%C5%AFl_3_yept6k.png"
dimensions: "<p><strong>Šířka:</strong> 120-340<strong> </strong>cm - <strong>Délka:
  </strong>94-104 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 2 cm</p><p>Vizuální
  tloušťka desky: 7,5 cm</p><p>Průměr nohou: spodní část 11x9,7 cm, vrchní část 5,8x4
  cm</p><p>Rozměry rozšíření: 1x55, 2x45 and 2x55 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 96 000 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605602948/forestry/ART_INVITO_rozkl%C3%A1dac%C3%AD_st%C5%AFl_2_t5nvnq.png"

---
