---
brand: data/brands/Atlas.md
name: TANGO MOBILE SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Tango mobile reaguje na potřeby současné
  rodiny, která chce mít ve svém omezeném prostoru užitečný nábytek. Funkčnost v kombinaci
  s originálním designem tvoří jedinečnost tohoto modelu. Jeho jedinečnost se odráží
  v moderně navrženém mechanismu, který se otevírá do strany, takže pohodlné lůžko
  získáte za několik vteřin. Je vyrobena z nejkvalitnější kůže, nebo látky, případně
  si vždy můžete zvolit kombinaci obou materiálů.</p>'
product_images:
- "/v1643025192/forestry/ATL_TANGOMOBILE_sofa_bed_1_uhk4q5.png"
- "/v1643025192/forestry/ATL_TANGOMOBILE_sofa_bed_5_uq12rm.png"
- "/v1643025192/forestry/ATL_TANGOMOBILE_sofa_bed_7_hw7dof.png"
- "/v1643025192/forestry/ATL_TANGOMOBILE_sofa_bed_6_tzbcil.png"
dimensions: "<p><strong>Šířka: </strong>314 cm - <strong>Délka: </strong>234 cm -
  <strong>Výška: </strong>85 cm - <strong>Výška sedu:</strong> 43 cm - <strong>Hloubka
  sedu:</strong> 65 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 ve vatovém potahu</p><p><strong>Opěradlo: </strong>paměťová pěna,
  silikon</p><p><strong>Elastické popruhy/bonneli pružiny</strong></p><p><strong>Nožičky:
  </strong>dřevěné nohy lakované PU lakem, nebo v barvě RAL</p><p><strong>Možnosti</strong>:
  spací mechanismus</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>142 000 Kč </p><p><strong>Cena v látce
  od: </strong>89 900 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/8afa66ce7cc0defa2272ffe31912a9f65558d292.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643025363/forestry/ATL_TANGOMOBILE_sofa_bed_4_bk4qqf.png"
- "/v1643025363/forestry/ATL_TANGOMOBILE_sofa_bed_3_g4g5vg.png"
- "/v1643025364/forestry/ATL_TANGOMOBILE_sofa_bed_2_ezgglc.png"

---
