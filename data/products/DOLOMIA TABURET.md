---
brand: data/brands/Extraform.md
name: 'DOLOMIA TABURET '
categories:
- Accessories
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"
  title="EXTRAFORM"><strong>EXTRAFORM</strong></a></p><p>Dolomia kolekce přináší organické
  tvary do Vašeho domova a kanceláře pomocí přírodní kombinace barev pro Vaše moderní
  prostory. </p>'
product_images:
- "/v1644501050/forestry/EXT_DOLOMIA_taburet_1_lnqdhw.png"
- "/v1644501042/forestry/EXT_DOLOMIA_taburet_4_jhqz5f.png"
- "/v1644501041/forestry/EXT_DOLOMIA_taburet_3_rabhfp.png"
dimensions: "<p><strong>Šířka: </strong>90 cm - <strong>Délka:</strong> 60 cm - <strong>Výška:
  </strong>44 cm - <strong>Výška sedu: </strong>44 cm - <strong>Hloubka sedu:</strong>
  60 cm </p>"
technical_info: "<p>Nosná konstrukce ze sušeného bukového dřeva, lepenky, 18 mm silné
  překližky, MDF desky. </p><p>Sedáky - vysoce elastická HR pěna, 200 g tepelně pojené
  plsti, elastické popruhy. </p><p>Nohy z dubového dřeva, naolejované. Volitelně z
  bukového dřeva v šesti barvách. </p>"
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>12 200 Kč</p>"
product_info: ''
product_photos:
- "/v1644501264/forestry/EXT_DOLOMIA_taburet_2_zcxpnw.png"

---
