---
brand: data/brands/Atlas.md
name: OM 250 STŮL
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Tradiční stůl pravidelných, čistých tvarů.
  Je k dispozici pouze jako pevný, ve dvou různých rozměrech. Ostré úhly desky odrážejí
  klasický vzhled stolu OM 250. Moderní design jeho geometrických linií podtrhuje
  vzhled vhodný do moderně a minimalisticky řešených jídelních prostor.</p>'
product_images:
- "/v1643279151/forestry/ATL_OM250_st%C5%AFl_1_vl8ibx.png"
dimensions: "<p>Rozměr I.</p><p><strong>Šířka: </strong>80 cm - <strong>Délka: </strong>80
  cm - <strong>Výška: </strong>78 cm</p><p>Rozměr II.</p><p><strong>Šířka: </strong>120
  cm - <strong>Délka: </strong>80 cm - <strong>Výška: </strong>78 cm</p>"
technical_info: "<p><strong>Materiál:</strong> buk, RAL</p><p></p>"
materials: ''
price: "<p><strong>Cena od: </strong>10 800 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/913f36a8b815256fa7edd8704f820228a65d3fb4.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643279307/forestry/ATL_OM250_st%C5%AFl_3_waklej.png"
- "/v1643279306/forestry/ATL_OM250_st%C5%AFl_2_cs8zad.png"

---
