---
brand: data/brands/Woak.md
name: 'MARSHALL ŽEBŘÍK '
categories:
- Accessories
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Dřevěné části tohoto nábytku jsou ručně
  vybírány jedna část po druhé, aby dokonale pasovaly s ostatními materiály, které
  jsou zcela inovativní a recyklovatelné, jako je nezpracovaná kůže, čistý mramor
  nebo lité sklo s měnícími se barvami. Kombinace teplých a studených materiálů vede
  k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem je mužný,
  ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací pokoj nebo
  koutek na čtení.</p>'
product_images:
- "/v1644486333/forestry/Product_foto_kopie_6_xwcxzu.png"
- "/v1644486333/forestry/Product_foto_kopie_7_jbh0ya.png"
- "/v1644486333/forestry/Product_foto_kopie_8_uoqhso.png"
dimensions: "<p><strong>Šířka: </strong>40 cm - <strong>Délka: </strong>40 cm - <strong>Výška:</strong>
  166 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>deska stolu: mramorová carrara,
  kůže nebo dřevo</p><p>povrchová úprava: lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>25 800 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Marshall_ladders_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606897988/forestry/WHO_MARSHALL_%C5%BEeb%C5%99%C3%ADk_5_lrd6bo.png"
- "/v1606897989/forestry/WHO_MARSHALL_%C5%BEeb%C5%99%C3%ADk_4_yefadi.png"

---
