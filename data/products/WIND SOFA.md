---
brand: data/brands/Atlas.md
name: WIND SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Motorizovaný relaxační mechanismus umožňuje
  každému snadno najít polohu, ve které si po dlouhém a náročném dni odpočine. Mechanismus
  umožňuje přeměnit sedadla z polohy vsedě do polohy pro odpočinek. Zvyšování standardů
  moderního stylu a svůdné hluboké sedáky s dokonalými liniemi dělají z tohoto kusu
  nábytku tu správnou volbu pro vybavení vaší domácnosti. Wind nabízí široký výběr
  prvků, kromě standardního dvojsedáku i jednomístný, segmentový a úhlový. Existuje
  také možnost instalace neobvyklého stolku Wind, který dodá tomuto kusu zcela nový
  tvar a vzhled.</p>'
product_images:
- "/v1643100347/forestry/ATL_WIND_sofa_1_sneehs.png"
dimensions: "<p><strong>Šířka: </strong>293 cm - <strong>Délka: </strong>123/176 cm
  - <strong>Výška: </strong>72/102 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 40/30 a HD pěna 21/00 Pantera ve vatovém potahu</p><p><strong>Opěradlo:
  </strong>pěna L 25/15</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:
  </strong>kovové -inox nohy</p><p><strong>Možnosti:</strong> relaxační mechanismus</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>192 200 Kč </p><p><strong>Cena v látce
  od: </strong>159 000 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/1f73714d69d79b9966e3d739d12575063d233566.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643100497/forestry/ATL_WIND_sofa_3_vqm0c5.png"
- "/v1643100498/forestry/ATL_WIND_sofa_2_jntfur.png"

---
