---
brand: data/brands/Atlas.md
name: LOLA ŽIDLE
categories:
- Chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Od pohodlného sezení v menších prostorách
  až po dotvoření interiéru obývacího pokoje. Tato křesla dotvoří jakýkoli styl domácího
  interiéru. Dodejte svému domovu moderní vzhled s tímto elegantním čalouněným křeslem.
  Váš výběr bude snazší díky možnosti vyrobit toto křeslo v jakékoliv barvě z široké
  škály barev, a to jak z kvalitních látek, tak z přírodní hovězí kůže.</p>'
product_images:
- "/v1643282248/forestry/ATL_LOLA_%C5%BEidle_1_p8fmd8.png"
dimensions: "<p><strong>Šířka: </strong>52 cm - <strong>Délka: </strong>58 cm - <strong>Výška:
  </strong>81 cm - <strong>Výška sedu:</strong> 50 cm <br></p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> HR pěna 35/36 </p><p><strong>Opěradlo:</strong>
  pěna N 25/38</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky: </strong>dřevěné</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>11 300 Kč </p><p><strong>Cena v látce
  od: </strong>8 600 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/d5085a571ad7fc71ade5c4199eef7ff97383fa8b.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643282375/forestry/ATL_LOLA_%C5%BEidle_4_l6zd5h.png"
- "/v1643282375/forestry/ATL_LOLA_%C5%BEidle_3_h0x3oo.png"
- "/v1643282374/forestry/ATL_LOLA_%C5%BEidle_2_cw9js5.png"

---
