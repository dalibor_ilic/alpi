---
brand: data/brands/Atlas.md
name: CAPRI SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Neobvyklý model, který nabízí dokonalé
  pohodlí. Díky minimalistickému designu a nekonečným možnostem kombinování prvků
  se tento kus nábytku snadno hodí do jakéhokoli prostoru. Vzhledem k odvážné kombinaci
  materiálů se tato pohovka stává jedinečným kusem nábytku, který navždy změní a zdokonalí
  prostor, v němž žijete. Model Capri se vyznačuje pohyblivými opěradly. Je jen na
  vás, jakou polohu si zvolíte a užijete.</p>'
product_images:
- "/v1642250491/forestry/ATL_CAPRI_sofa_1_rt7mio.png"
dimensions: "<p><strong>Šířka: </strong>345 cm - <strong>Délka: </strong>250 cm -
  <strong>Výška: </strong>83 cm - <strong>Výška sedu:</strong> 38 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 a HR 35/30, paměťová pěna, peří se silikonem</p><p><strong>Opěradlo:
  </strong>pěna L 25/15 a HR pěna 35/30, paměťová pěna, peří se silikonem</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky:</strong> plastové nožičky 25 mm</p><p><strong>Možnosti:</strong>
  pohyblivé polštáře opěradla a loketní opěrky</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena od: </strong>106 400 Kč </p>"
product_info: <p>Produktová karta:<a href="https://atlassofas.eu/login/files/09aeed520e224fa756d7837e2f85fc21eb2f0e42.pdf"
  title=""> Stáhnout PDF</a></p>
product_photos:
- "/v1642250868/forestry/ATL_CAPRI_sofa_3_ezd3fd.png"
- "/v1642250868/forestry/ATL_CAPRI_sofa_2_k8yxsi.png"
- "/v1642250867/forestry/ATL_CAPRI_sofa_4_okl6en.png"

---
