---
brand: data/brands/Woak.md
name: 'MALIN REGÁL '
categories:
- Shelf
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606487894/forestry/WHO_MALIN_reg%C3%A1l_2_ilb6rz.png"
- "/v1606487894/forestry/WHO_MALIN_reg%C3%A1l_1_o9k0xh.png"
dimensions: "<p><strong>Šířka: </strong>120 cm - <strong>Délka: </strong>40 cm - <strong>Výška:</strong>
  134 - 170 cm</p>"
technical_info: "<p>Tento regál je vyroben z masivního dřeva (dubu nebo ořechu) a
  má olejovou úpravu, spodní police - dřevo, mramor Carara, mramor Emperador. </p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 37 900 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_shelf_system_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1b-RI27SEA0rFEfMidNyQQDx9pSVbtDcn/view?usp=sharing"
  title=""><strong>Stáhnout PDF</strong></a> </p>'
product_photos:
- "/v1606488153/forestry/WHO_MALIN_reg%C3%A1l_5_un6zcl.png"
- "/v1606488155/forestry/WHO_MALIN_reg%C3%A1l_4_l7qvu9.png"
- "/v1606488154/forestry/WHO_MALIN_reg%C3%A1l_3_uh8zcu.png"

---
