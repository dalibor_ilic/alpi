---
brand: data/brands/Extraform.md
name: AINO KŘESLO
categories:
- Arm chair
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"
  title="EXTRAFORM"><strong>EXTRAFORM </strong></a><strong> </strong></p><p>Kruhová
  základna této sestavitelné pohovky je zdůrazněna dřevěnými nohami a polštáři v různých
  barvách. Díky své hloubce je mimořádně pohodlná, což z ní dělá velmi žádaný kus
  nábytku pro využití doma i v kanceláři. </p>'
product_images:
- "/v1644496429/forestry/EXT_AINO_armchair_1_ezvg9q.png"
- "/v1644496431/forestry/EXT_AINO_armchair_8_hondg0.png"
- "/v1644496429/forestry/EXT_AINO_armchair_4_hsuts4.png"
- "/v1644496431/forestry/EXT_AINO_armchair_6_xvtka1.png"
- "/v1644496431/forestry/EXT_AINO_armchair_7_rr4aq4.png"
- "/v1644496431/forestry/EXT_AINO_armchair_5_y5hxsm.png"
- "/v1644496429/forestry/EXT_AINO_armchair_3_fcaewl.png"
- "/v1644496431/forestry/EXT_AINO_armchair_2_gocwch.png"
dimensions: "<p><strong>Šířka</strong>: 103 cm - <strong>Délka</strong>: 99 cm - <strong>Výška</strong>:
  74 cm - <strong>Výška sedu</strong>: 41 cm - <strong>Hloubka sedu</strong>: 65 cm</p>"
technical_info: "<p>Nosná konstrukce ze sušeného bukového dřeva, lepenky, 18mm silné
  překližky, MDF desky. </p><p>Sedáky - vysoce elastická HR pěna, 200g tepelně pojené
  plsti, elastické popruhy. </p><p>Opěrky - ergonomická vysoce elastická pěna, 400g
  tepelně pojené plsti. </p><p>Nohy z dubového dřeva, naolejované. Volitelně z bukového
  dřeva v šesti barvách. </p>"
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 20 000 Kč</p>"
product_info: ''
product_photos: []

---
