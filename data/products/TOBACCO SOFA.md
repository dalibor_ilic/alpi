---
brand: data/brands/Extraform.md
name: TOBACCO SOFA
categories:
- Sofa
description: <p>Výrobce<strong> </strong>:<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Toto
  sezení je obrazem ideálu stylové dvoumístné nebo třímístné pohovky. Vyznačuje se
  skvěle vyrobenými detaily, jako jsou dvojité stehy, štíhlé a elegantní nohy a pohodlné
  polštáře obzvlášť, pokud jsou vyrobeny z kvalitní kůže.</p>
product_images:
- "/v1645089898/forestry/EXT_TOBACCO_sofa_3_i8ugy7.png"
- "/v1645089912/forestry/EXT_TOBACCO_sofa_1_oypbme.png"
- "/v1645089898/forestry/EXT_TOBACCO_sofa_4_vhg2jr.png"
dimensions: '<p><strong>Dvojsed: Šířka: </strong>166-186 cm - <strong>Délka:</strong>
  89 cm - <strong>Výška:</strong> 84 cm - <strong>Výška sedu:</strong> 44 cm - <strong>Hloubka
  sedu:</strong> 65 cm</p><p class="Paragraph SCXW171011925 BCX4"><strong>Trojsed:
  Šířka: </strong>206 cm - <strong>Délka:</strong> 89 cm - <strong>Výška:</strong>
  84 cm - <strong>Výška sedu:</strong> 44 cm - <strong>Hloubka sedu:</strong> 65 cm</p>'
technical_info: <p>lepenka.</p><p class="Paragraph SCXW95392591 BCX4">Kombinovaná
  tvrdá pěna a vysoce elastická HR pěna, 300 g tepelně pojené bavlněné vaty, 500 g
  rouna, vinuté pružiny.</p><p class="Paragraph SCXW95392591 BCX4">Ergonomická vysoce
  elastická pěna, gumičky.</p><p class="Paragraph SCXW95392591 BCX4">Nohy z bukového
  dřeva, mořené a lakované. K dispozici v šesti barvách. Chromované nohy jsou volitelné.</p><p
  class="Paragraph SCXW95392591 BCX4">Pevná rohová pohovka.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 28 000 Kč </p>"
product_info: ''
product_photos:
- "/v1645089945/forestry/EXT_TOBACCO_sofa_2_jf3zhg.png"

---
