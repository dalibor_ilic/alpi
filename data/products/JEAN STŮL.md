---
brand: data/brands/Artisan.md
name: 'JEAN STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605605061/forestry/ART_JEAN_st%C5%AFl_8_ohpnyo.png"
- "/v1605605052/forestry/ART_JEAN_st%C5%AFl_1_tifr1b.png"
- "/v1605605061/forestry/ART_JEAN_st%C5%AFl_7_i61h2t.png"
- "/v1605605052/forestry/ART_JEAN_st%C5%AFl_2_bqj477.png"
dimensions: "<p><strong>Šířka: </strong>160 - 240<strong> </strong>cm - <strong>Délka:
  </strong>90 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a za příplatek 10% možnost šíře
  100 cm.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální tloušťka desky: 4 cm</p><p>Průměr
  nohou: 6 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 63 700 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605605297/forestry/ART_JEAN_st%C5%AFl_6_ix0arr.png"
- "/v1605605297/forestry/ART_JEAN_st%C5%AFl_3_ph716m.png"
- "/v1605605297/forestry/ART_JEAN_st%C5%AFl_5_utoneq.png"
- "/v1605605296/forestry/ART_JEAN_st%C5%AFl_4_lp2yxb.png"

---
