---
brand: data/brands/Artisan.md
name: 'TORSIO ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644485478/forestry/Product_foto_kopie_5_emte2s.png"
- "/v1644485478/forestry/Product_foto_kopie_4_waamv6.png"
- "/v1644485291/forestry/Product_foto-56_ce4gif.png"
dimensions: "<p><strong>Šířka: </strong>52<strong> </strong>cm - <strong>Délka: </strong>49
  cm - <strong>Výška: </strong>77 cm - <strong>Výška sedu: </strong>47,1 cm - <strong>Hloubka
  sedu: </strong>45,5 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 39 x 45,5 cm </p><p>Tloušťka sedací a opěrné plochy: 3 cm</p><p>Průměr nohou:
  4 cm</p><p></p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 20 800 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604669413/forestry/ART_TORSIO_%C5%BEidle_3_wldyqr.png"
- "/v1604669413/forestry/ART_TORSIO_%C5%BEidle_2_vt3kw2.png"

---
