---
brand: data/brands/Atlas.md
name: MIA POSTEL
categories:
- Bed
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Díky svým malým rozměrům se hodí do každé
  moderní ložnice. Výrazný design, který je moderní interpretací klasické postele
  s čalouněným čelem, vnese eleganci do každé ložnice. Harmonické oblé linie i precizně
  vyšívané kosočtverce na čele postele dodávají celému dojmu, který postel Mia zanechává,
  nádech něhy. Materiál si vyberte podle potřeb a podle toho, jak chcete svou ložnici
  ozdobit.</p>'
product_images:
- "/v1643104837/forestry/ATL_MIA_postel_1_cwfhfx.png"
- "/v1643104837/forestry/ATL_MIA_postel_6_ojpyen.png"
dimensions: "<p>Jednolůžko I.</p><p>100 × 200 cm - <strong>Šířka: </strong>119 cm
  - <strong>Délka: </strong>222 cm - <strong>Výška: </strong>99 cm</p><p>Jednolůžko
  II.</p><p>120 × 200 cm - <strong>Šířka: </strong>139 cm - <strong>Délka: </strong>222
  cm - <strong>Výška: </strong>99 cm</p><p>Jednolůžko III.</p><p>140 × 200 cm - <strong>Šířka:
  </strong>159 cm - <strong>Délka: </strong>222 cm - <strong>Výška: </strong>99 cm</p><p>Dvojlůžko
  I.</p><p>160 × 200 cm - <strong>Šířka: </strong>179 cm - <strong>Délka: </strong>222
  cm - <strong>Výška: </strong>99 cm</p><p>Dvojlůžko II.</p><p>180 × 200 cm - <strong>Šířka:
  </strong>199 cm - <strong>Délka: </strong>222 cm - <strong>Výška: </strong>99 cm</p><p>Dvojlůžko
  III.</p><p>200 × 200 cm - <strong>Šířka: </strong>219 cm - <strong>Délka: </strong>222
  cm - <strong>Výška: </strong>99 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Výplň:</strong>
  pěna N 25/38 v potahu z vaty</p><p><strong>Základ:</strong> pěna N 18/30</p><p><strong>Nožičky:
  </strong>dřevěné nohy lakované PU lakem</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>33 800 Kč</p><p><strong>Cena v látce od:
  </strong>20 800 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/6ca4660b6a90243339100d808d0ff34ccf18731b.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643105199/forestry/ATL_MIA_postel_5_en3axa.png"
- "/v1643105202/forestry/ATL_MIA_postel_4_lnigir.png"
- "/v1643105201/forestry/ATL_MIA_postel_3_jaq5c8.png"
- "/v1643105201/forestry/ATL_MIA_postel_2_quxgwg.png"

---
