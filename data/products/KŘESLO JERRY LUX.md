---
brand: data/brands/Atlas.md
name: JERRY LUX KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Křeslo Jerry lux je výsledkem práce našich
  kreativních designérů. Neobvyklý design, pohodlné a zároveň jednoduché křeslo. Opěradlo
  a sedák tvoří dva vzájemně se doplňující oblé tvary. To, co dělá tento model charakteristickým,
  je opěradlo s prošíváním a dřevěný rám. Kombinace křesla a otomanu Jerry zkrášlí
  váš obývací i pracovní prostor.</p>'
product_images:
- "/v1643111217/forestry/ATL_JERRYLUX_k%C5%99eslo_1_uk7n4g.png"
dimensions: "<p><strong>Šířka: </strong>74 cm - <strong>Délka: </strong>55 cm - <strong>Výška:
  </strong>87 cm - <strong>Výška sedu:</strong> 45 cm - <strong>Hloubka sedu:</strong>
  55 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Podpěra:</strong>
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 v potahu vaty</p><p><strong>Opěradlo:</strong> pěna N 25/38</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>plastové nožičky 50 mm</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>20 300 Kč </p><p><strong>Cena v látce
  od: </strong>14 900 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/ef63d33bf36978ac9a6283f9b688f28d6c9b5234.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643111329/forestry/ATL_JERRYLUX_k%C5%99eslo_5_mwomzr.png"
- "/v1643111329/forestry/ATL_JERRYLUX_k%C5%99eslo_4_st6utd.png"
- "/v1643111329/forestry/ATL_JERRYLUX_k%C5%99eslo_6_jqguhq.png"

---
