---
brand: data/brands/Woak.md
name: REPETA KOMODA 01
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Repeta je sbírka malých komod s velmi
  složitou strukturou. Počínaje montážním systémem, který mohou provádět pouze odborné
  ruce tesaře. Ale především pečlivé drážky na povrchů, která jsou výsledkem přání
  přenést grafický vzor na povrch, a tedy dvourozměrnou techniku do trojrozměrného
  prvku.</p>'
product_images:
- "/v1607086386/forestry/WHO_REPETA_komoda01_4_gnclp0.png"
- "/v1607086385/forestry/WHO_REPETA_komoda01_1_ofthks.png"
- "/v1607086386/forestry/WHO_REPETA_komoda01_2_jdfgam.png"
- "/v1607086386/forestry/WHO_REPETA_komoda01_3_ngrzht.png"
dimensions: "<p><strong>Šířka: </strong>110 cm - <strong>Délka: </strong>50 cm - <strong>Výška:</strong>
  140 cm</p>"
technical_info: "<p>materiál: ořech nebo dub, mramor</p><p>povrchová úprava: olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>117 900 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Repeta01_cupboard_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607086668/forestry/WHO_REPETA_komoda01_7_tgstdr.png"
- "/v1607086674/forestry/WHO_REPETA_komoda01_6_iaifau.png"
- "/v1607086674/forestry/WHO_REPETA_komoda01_5_qki8e7.png"
- "/v1607086673/forestry/WHO_REPETA_komoda01_8_r53ukk.png"

---
