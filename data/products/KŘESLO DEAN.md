---
brand: data/brands/Atlas.md
name: DEAN KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Kombinace jednoduchého, nadčasového designu
  a vysokého pohodlí. Výsledkem je křeslo Dean. Při výrobě tohoto křesla jsme věnovali
  zvláštní pozornost tomu, aby bylo pohodlné. Díky vysokému opěradlu a polštáři, který
  podpírá záda, jsme toho dosáhli. Pořiďte si jedno z nich jen pro sebe, pohodlně
  se usaďte a užívejte si.</p>'
product_images:
- "/v1643109856/forestry/ATL_DEAN_k%C5%99eslo_1_zupofh.png"
- "/v1643109856/forestry/ATL_DEAN_k%C5%99eslo_5_gyox83.png"
dimensions: "<p><strong>Šířka: </strong>71 cm - <strong>Délka: </strong>78 cm - <strong>Výška:
  </strong>86 cm - <strong>Výška sedu:</strong> 43 cm - <strong>Hloubka sedu:</strong>
  51 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Potah:</strong>
  látka</p><p><strong>Sedák:</strong> HR pěna 35/36</p><p><strong>Opěradlo: </strong>HR
  pěna 35/30</p><p><strong>Područky: </strong>N pěna 25/38</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>masivní bukové dřevo</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>18 100 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/f58768e7cecd21ac3dc44da71b3d2a57085fe56a.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643110028/forestry/ATL_DEAN_k%C5%99eslo_4_iu5j4o.png"
- "/v1643110027/forestry/ATL_DEAN_k%C5%99eslo_6_p9ffew.png"

---
