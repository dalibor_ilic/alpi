---
brand: data/brands/Artisan.md
name: BOAZ ODKLÁDACÍ STOLEK
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645438447/forestry/Product_foto_konferen%C4%8Dn%C3%AD_stolek_mawgsd.png"
dimensions: "<p><strong>Výška: </strong>50 cm - <strong>Průměr:</strong> 35 cm</p>"
technical_info: "<p>Tloušťka horní desky: 2 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>13 100 Kč</p>"
product_info: ''
product_photos:
- "/v1645438463/forestry/Z_interi%C3%A9ru_konferen%C4%8Dn%C3%AD_stolek_kgd2sa.png"
- "/v1645438463/forestry/Z_interi%C3%A9ru-2_konferen%C4%8Dn%C3%AD_stolek_kkaaxk.png"

---
