---
brand: data/brands/Atlas.md
name: AGORA KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Kombinace přírodních materiálů, luxusní
  kůže, nebo látky a masivního dřeva působí sofistikovaným a pohodlným dojmem. Toto
  úžasné křeslo vám zpříjemní domov ještě více. Hodí se pro odpočinek i zábavu. Tento
  model je navržen tak, aby velmi snadno zapadl do moderního i klasického stylu.</p>'
product_images:
- "/v1643107598/forestry/ATL_AGORA_k%C5%99eslo_1_w3zln0.png"
- "/v1643107598/forestry/ATL_AGORA_k%C5%99eslo_6_g3lv5r.png"
- "/v1643107598/forestry/ATL_AGORA_k%C5%99eslo_5_w6ruox.png"
- "/v1643107598/forestry/ATL_AGORA_k%C5%99eslo_7_i3xj9t.png"
dimensions: "<p><strong>Šířka: </strong>72 cm - <strong>Délka: </strong>77 cm - <strong>Výška:
  </strong>68 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> HR pěna 35/36, husí peří</p><p><strong>Opěradlo:
  </strong>HR pěna 35/36, husí peří</p><p><strong>Područky: </strong>dřevěné</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>součást konstrukce</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>23 500 Kč </p><p><strong>Cena v látce
  od: </strong>20 000 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/409d21f3dbff18a3e154b277492627f465423baa.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643107774/forestry/ATL_AGORA_k%C5%99eslo_3_faigov.png"
- "/v1643107775/forestry/ATL_AGORA_k%C5%99eslo_4_z4tpuk.png"
- "/v1643107773/forestry/ATL_AGORA_k%C5%99eslo_2_i6hqwn.png"
- "/v1643107789/forestry/ATL_AGORA_k%C5%99eslo_10_x6aauo.png"
- "/v1643107775/forestry/ATL_AGORA_k%C5%99eslo_8_apcjrg.png"
- "/v1643107789/forestry/ATL_AGORA_k%C5%99eslo_9_vow7qi.png"

---
