---
brand: data/brands/Artisan.md
name: INVITO KOMODA
categories:
- Sideboard
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a><strong> </strong></p>'
product_images:
- "/v1645441543/forestry/Product_foto-2komoda_czk2j5.png"
- "/v1645441543/forestry/Product_foto-4komoda_xh4lbs.png"
- "/v1645441543/forestry/Product_foto-3komoda_dli42s.png"
dimensions: "<p><strong>Šířka: </strong>180/210 cm - <strong>Délka: </strong>42 cm<strong>
  </strong>- <strong>Výška: </strong>50 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka komody: 2 cm</p><p>Přední tloušťka:
  2,2 cm</p><p>Zadní tloušťka: 1,5 cm </p><p></p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 51 300 Kč</p>"
product_info: ''
product_photos:
- "/v1645441620/forestry/Z_interi%C3%A9ru-3komoda_ndklyx.png"
- "/v1645441599/forestry/Z_interi%C3%A9ru-5komoda_vjbqbu.png"
- "/v1645441599/forestry/Z_interi%C3%A9ru-4komoda_akbd3h.png"

---
