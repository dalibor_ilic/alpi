---
brand: data/brands/Artisan.md
name: HANNY VĚŠÁK
categories: []
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644248180/forestry/Product_foto-11_rd9ucw.png"
dimensions: "<p><strong>Šířka: </strong>47<strong> </strong>cm - <strong>Délka: </strong>47
  cm - <strong>Výška: </strong>180 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Průměr nohou: spodní část
  2x3 cm, vrchní část 5x3 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 32 400 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605529690/forestry/ART_HANNY_v%C4%9B%C5%A1%C3%A1k_3_klqc3c.png"
- "/v1605529690/forestry/ART_HANNY_v%C4%9B%C5%A1%C3%A1k_2_tr3teb.png"

---
