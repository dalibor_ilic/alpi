---
brand: data/brands/Atlas.md
name: COMBO SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Přímé linie a jednoduchý design doplněný
  detaily činí tento model atraktivním. Je vyroben pouze v látce. Potahy jsou upevněny
  pomocí pásků na suchý zip, takže je lze snadno sundat a vyprat, což je velmi praktické.
  Combo je variabilní a sedačka s velkým množstvím kombinací. </p>'
product_images:
- "/v1642434326/forestry/ATL_COMBO_sofa_1_greqmr.png"
dimensions: "<p><strong>Šířka: </strong>302 cm - <strong>Délka: </strong>257 cm -
  <strong>Výška: </strong>69 cm - <strong>Výška sedu:</strong> 42 cm - <strong>Hloubka
  sedu:</strong> 64 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka</p><p><strong>Sedák:</strong> HR pěna
  35/36 a HR pěna 35/30</p><p><strong>Opěradlo: </strong>HR 35/30</p><p><strong>Vlnové
  pružiny</strong></p><p><strong>Nožičky: </strong>plastové nožičky 50 mm</p><p><strong>Možnosti:</strong>
  Odnímatelný potah</p>"
materials: <p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong> </a>| <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p></p>
price: "<p><strong>Cena od: </strong>66 200 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/99d67696d86d8afeb582934e37087e4c31aca0c3.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642434598/forestry/ATL_COMBO_sofa_5_vc72vd.png"
- "/v1642434598/forestry/ATL_COMBO_sofa_4_cwsiif.png"
- "/v1642434598/forestry/ATL_COMBO_sofa_3_gnoirb.png"
- "/v1642434598/forestry/ATL_COMBO_sofa_2_ekwzom.png"
- "/v1642434598/forestry/ATL_COMBO_sofa_6_l9ghlm.png"

---
