---
brand: data/brands/Artisan.md
name: 'KALOTA KONZOLOVÝ STOLEK '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645442649/forestry/Product_foto-2konozol.stolek_ssuoq2.png"
- "/v1645442650/forestry/Product_foto-3konozol.stolek_pg9rop.png"
- "/v1645442650/forestry/Product_fotokonozol.stolek_hcxj3c.png"
dimensions: "<p><strong>Šířka: </strong>50 cm - <strong>Délka: </strong>130/150 cm<strong>
  </strong>- <strong>Výška: </strong>85 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka desky: 8 cm </p><p>Vizuální tloušťka
  desky: 4 cm </p><p>Průměr nohou: spodní část 4x6x4 cm, horní část 6x8x6 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 55 000 Kč</p>"
product_info: ''
product_photos:
- "/v1645442846/forestry/Z_interi%C3%A9rukonozol.stolek_dssg8u.png"

---
