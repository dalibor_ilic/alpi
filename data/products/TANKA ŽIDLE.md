---
brand: data/brands/Artisan.md
name: 'TANKA ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644252646/forestry/Product_foto-46_at6a61.png"
- "/v1644252646/forestry/Product_foto-47_h6zh1c.png"
- "/v1644252646/forestry/Product_foto-48_myzzio.png"
- "/v1644252646/forestry/Product_foto-49_lvkogn.png"
dimensions: "<p><strong>Šířka: </strong>44 cm - <strong>Délka: </strong>53 cm - <strong>Výška:
  </strong>79 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Průměr
  nohou: spodní část 3 cm, vrchní část 4 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 13 500 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604667663/forestry/ART_TANKA_%C5%BEidle_5_s11g3t.png"
- "/v1604667651/forestry/ART_TANKA_%C5%BEidle_8_qmbsyl.png"
- "/v1604667651/forestry/ART_TANKA_%C5%BEidle_6_w4h6kz.png"
- "/v1604667650/forestry/ART_TANKA_%C5%BEidle_7_z8jmtg.png"

---
