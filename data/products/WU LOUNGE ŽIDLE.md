---
brand: data/brands/Artisan.md
name: WU LOUNGE ŽIDLE
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645446348/forestry/Product_foto-2lounge_cb4on2.png"
- "/v1645446348/forestry/Product_foto-3lounge_n3tbtl.png"
- "/v1645446348/forestry/Product_fotolounge_p6gox3.png"
dimensions: "<p><strong>Šířka: </strong>60 cm - <strong>Délka: </strong>74 cm<strong>
  </strong>- <strong>Výška: </strong>70 cm - <strong>Výška sedu: </strong>40 cm<strong>
  </strong></p>"
technical_info: "<p>Velikost sedu: 60x64cm</p><p>Průměr nohou: spodní část 2,9 cm,
  vrchní část 4 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 39 300 Kč</p>"
product_info: ''
product_photos:
- "/v1645446541/forestry/Z_interi%C3%A9ru-2lounge_n1dqny.png"
- "/v1645446541/forestry/Z_interi%C3%A9rulounge_ckcmkx.png"

---
