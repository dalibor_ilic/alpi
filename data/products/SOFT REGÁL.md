---
brand: data/brands/Artisan.md
name: 'SOFT REGÁL '
categories:
- Shelf
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605265455/forestry/ART_SOFT_reg%C3%A1l_1_vdnkm5.png"
- "/v1605265466/forestry/ART_SOFT_reg%C3%A1l_7_n2v8up.png"
- "/v1605265478/forestry/ART_SOFT_reg%C3%A1l_10_q9zgkz.png"
- "/v1644251956/forestry/Product_foto-45_aaxzt1.png"
- "/v1605265454/forestry/ART_SOFT_reg%C3%A1l_2_geirrl.png"
dimensions: "<p><strong>Šířka: </strong>120/160<strong> </strong>cm - <strong>Délka:
  </strong>40 cm - <strong>Výška: </strong>120/160/200 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka polic: 2 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1DKp-AqR_-rESWlFNEpcr1prxEig5SpKq/view?usp=sharing
  " title=""><strong>BAREVNÉ DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 64 700 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605265665/forestry/ART_SOFT_reg%C3%A1l_9_hqhd2k.png"
- "/v1605265663/forestry/ART_SOFT_reg%C3%A1l_8_eydsyu.png"
- "/v1605265662/forestry/ART_SOFT_reg%C3%A1l_6_z7opzg.png"
- "/v1605265661/forestry/ART_SOFT_reg%C3%A1l_5_qwwvdk.png"
- "/v1605265665/forestry/ART_SOFT_reg%C3%A1l_4_rcpgvd.png"

---
