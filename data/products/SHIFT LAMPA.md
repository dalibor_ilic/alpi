---
brand: data/brands/Artisan.md
name: 'SHIFT LAMPA '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605174196/forestry/ART_SHIFT_lampa_1_qyy6lx.png"
- "/v1605174214/forestry/ART_SHIFT_lampa_13_isxo3n.png"
- "/v1605174214/forestry/ART_SHIFT_lampa_12_tytiza.png"
- "/v1605174215/forestry/ART_SHIFT_lampa_15_apjtmn.png"
- "/v1605174214/forestry/ART_SHIFT_lampa_14_cg5qcy.png"
- "/v1605174215/forestry/ART_SHIFT_lampa_16_owzpzm.png"
dimensions: "<p><strong>Výška: </strong>40/130/200 cm (S/M/L)</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, možnost kovového stínidla.</p><p>Průměr
  nohou: 1,5 cm/ 2,2 cm/ 2,7 cm </p><p>Rozměry rozpětí nohou po ploše: 24x24x24 cm/
  45x45x45 cm/ 74x74x74 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 22 300 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605174378/forestry/ART_SHIFT_lampa_2_je2znu.png"
- "/v1605174380/forestry/ART_SHIFT_lampa_11_uzfilc.png"
- "/v1605174379/forestry/ART_SHIFT_lampa_8_v5sgzc.png"
- "/v1605174381/forestry/ART_SHIFT_lampa_7_okrliu.png"
- "/v1605174379/forestry/ART_SHIFT_lampa_6_ccqdab.png"
- "/v1605174380/forestry/ART_SHIFT_lampa_10_hkr7oi.png"
- "/v1605174380/forestry/ART_SHIFT_lampa_5_rc85ph.png"
- "/v1605174379/forestry/ART_SHIFT_lampa_9_p352rn.png"
- "/v1605174377/forestry/ART_SHIFT_lampa_3_n1xix9.png"
- "/v1605174376/forestry/ART_SHIFT_lampa_4_uhzbnh.png"

---
