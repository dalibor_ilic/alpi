---
brand: data/brands/Atlas.md
name: CHARLES KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Křeslo Charles se vyznačuje elegantními
  liniemi, vysokým opěradlem a propracovanými detaily vysoce kvalitní výroby. Je pohodlné
  a stvořené pro potěšení. Toto křeslo vám poskytne pohodlí a zároveň bude moderním
  a výrazným detailem ve vaší domácnosti. Tvoří dokonalou volbu pro váš obytný prostor.</p>'
product_images:
- "/v1643108529/forestry/ATL_CHARLES_k%C5%99eslo_7_emwdhf.png"
- "/v1643108529/forestry/ATL_CHARLES_k%C5%99eslo_5_wqdgyf.png"
- "/v1643108528/forestry/ATL_CHARLES_k%C5%99eslo_1_iqdpdc.png"
- "/v1643108529/forestry/ATL_CHARLES_k%C5%99eslo_6_m9jkbh.png"
dimensions: "<p><strong>Šířka: </strong>65 cm - <strong>Délka: </strong>72 cm - <strong>Výška:
  </strong>102 cm - <strong>Výška sedu:</strong> 46 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> HR pěna 35/36</p><p><strong>Opěradlo:
  </strong>N pěna 18/30, L pěna 25/15</p><p><strong>Područky: </strong>dřevěné, nebo
  MDF</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky: </strong>dřevěné</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>21 100 Kč </p><p><strong>Cena v látce
  od: </strong>14 000 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/71c57491b1c85b3b8fb4a62623bc997687907572.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643108730/forestry/ATL_CHARLES_k%C5%99eslo_4_ugravr.png"
- "/v1643108731/forestry/ATL_CHARLES_k%C5%99eslo_3_mfef8f.png"
- "/v1643108726/forestry/ATL_CHARLES_k%C5%99eslo_2_icqq1b.png"

---
