---
brand: data/brands/Woak.md
name: REPETA KOMODA 03
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Repeta je sbírka malých komod s velmi
  složitou strukturou. Počínaje montážním systémem, který mohou provádět pouze odborné
  ruce tesaře. Ale především pečlivé drážky na povrchů, která jsou výsledkem přání
  přenést grafický vzor na povrch, a tedy dvourozměrnou techniku do trojrozměrného
  prvku.</p>'
product_images:
- "/v1607087070/forestry/WHO_REPETA_komoda03_2_nlvcmx.png"
- "/v1607087070/forestry/WHO_REPETA_komoda03_3_q91vkh.png"
- "/v1607087070/forestry/WHO_REPETA_komoda03_1_zmokfi.png"
- "/v1607087070/forestry/WHO_REPETA_komoda03_4_tej0rv.png"
dimensions: "<p><strong>Šířka: </strong>49,5 cm - <strong>Délka: </strong>49,5 cm
  - <strong>Výška:</strong> 140 cm</p>"
technical_info: "<p>materiál: ořech nebo dub, mramor</p><p>povrchová úprava: olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>51 900 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Repeta03_cupboard_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607087178/forestry/WHO_REPETA_komoda03_6_ljy8ld.png"
- "/v1607087180/forestry/WHO_REPETA_komoda03_5_hnmtsb.png"
- "/v1607087180/forestry/WHO_REPETA_komoda03_7_rywnl4.png"

---
