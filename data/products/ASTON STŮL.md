---
brand: data/brands/Atlas.md
name: ASTON STŮL
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Jídelní stůl Aston vás zaujme na první
  pohled. Pevná ocelová podnož dokonale ladí s eliptickým tvarem stolové desky, která
  je vyrobena z MDF v dubové dýze. Okraj stolové desky lze lakovat v barvách RAL,
  což je další detail, který dodává celé kompozici dávku šarmu a atraktivního vzhledu.</p>'
product_images:
- "/v1643278015/forestry/ATL_ASTON_st%C5%AFl_1_pwievu.png"
dimensions: "<p><strong>Šířka: </strong>160 cm - <strong>Délka: </strong>90 cm - <strong>Výška:
  </strong>76 cm</p>"
technical_info: "<p><strong>Materiál:</strong> dub, MDF 40 mm</p><p><strong>Nožičky:
  </strong>ocelové</p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>23 800 Kč </p>"
product_info: ''
product_photos:
- "/v1643278189/forestry/ATL_ASTON_st%C5%AFl_2_ltl2os.png"

---
