---
brand: data/brands/Atlas.md
name: METROPOLIS SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Tato souprava navzdory svému minimalistickému
  designu poskytuje pocit maximálního pohodlí. Sedák a opěradlo vyplněné peřím poskytnou
  dokonalý pocit potěšení. Pokud patříte k těm, jejichž prostor vyžaduje nábytek malých
  rozměrů, a vašimi požadavky jsou pohodlí a moderní design v malém balení, pak je
  pro vás sedací souprava Metropolis tou správnou volbou.</p>'
product_images:
- "/v1643015532/forestry/ATL_METROPOLIS_sofa_1_jctofe.png"
dimensions: "<p>Dvojsed I.</p><p><strong>Šířka: </strong>184 cm - <strong>Délka: </strong>97
  cm - <strong>Výška: </strong>87 cm - <strong>Výška sedu:</strong> 45 cm- <strong>Hloubka
  sedu:</strong> 67 cm</p><p>Dvojsed II.</p><p><strong>Šířka: </strong>164 cm - <strong>Délka:
  </strong>97 cm - <strong>Výška: </strong>87 cm - <strong>Výška sedu:</strong> 45
  cm- <strong>Hloubka sedu:</strong> 67 cm</p><p>Dvojsed II.</p><p><strong>Šířka:
  </strong>144 cm - <strong>Délka: </strong>97 cm - <strong>Výška: </strong>87 cm
  - <strong>Výška sedu:</strong> 45 cm- <strong>Hloubka sedu:</strong> 67 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36, peří se silikonem</p><p><strong>Opěradlo: </strong>peří se silikonem</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>kovové nohy - Inox, nebo lakované
  v barvě RAL</p><p></p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>49 100 Kč </p><p><strong>Cena v látce
  od: </strong>36 200 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/00aa7ac23a94ff71828275f7d3c0fd72f496e88f.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643015909/forestry/ATL_METROPOLIS_sofa_5_zncbga.png"
- "/v1643015910/forestry/ATL_METROPOLIS_sofa_4_hbzy5s.png"
- "/v1643015910/forestry/ATL_METROPOLIS_sofa_3_iydiiy.png"
- "/v1643015907/forestry/ATL_METROPOLIS_sofa_2_mnvkv4.png"

---
