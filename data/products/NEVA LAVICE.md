---
brand: data/brands/Artisan.md
name: 'NEVA LAVICE '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605178240/forestry/ART_NEVA_lavice_5_nchsh8.png"
- "/v1605178239/forestry/ART_NEVA_lavice_2_jci8sb.png"
- "/v1605178239/forestry/ART_NEVA_lavice_4_cwsaak.png"
- "/v1605178239/forestry/ART_NEVA_lavice_1_fyffig.png"
- "/v1605178239/forestry/ART_NEVA_lavice_3_qqje7g.png"
dimensions: "<p><strong>Šířka: </strong>64 cm - <strong>Délka: </strong>47 cm - <strong>Výška:
  </strong>40 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení ve 2 kategoriích látek
  a 3 kategoriích kůží.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální tloušťka desky:
  4 cm</p><p>Průměr nohou: spodní část 3,7x3,7 cm, vrchní část 4,3x4,3 cm </p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE </strong></a>|<a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong> DŘEVO</strong></a><strong> </strong><br></p>
price: "<p><strong>Cena od: </strong>29 900 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605178642/forestry/ART_NEVA_lavice_6_taqqn0.png"
- "/v1605178643/forestry/ART_NEVA_lavice_7_jv2vil.png"
- "/v1605178643/forestry/ART_NEVA_lavice_8_zmiupg.png"

---
