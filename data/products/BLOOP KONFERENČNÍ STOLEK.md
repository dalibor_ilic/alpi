---
brand: data/brands/Artisan.md
name: 'BLOOP KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645437986/forestry/Product_foto-2stolek_baluv8.png"
- "/v1645437986/forestry/Product_fotostolek_gnohdg.png"
- "/v1645437986/forestry/Product_foto-4stolek_kpkx2c.png"
- "/v1645437986/forestry/Product_foto-3stolek_cmzss8.png"
dimensions: "<p><strong>Výška: </strong>35/45 cm -<strong> Průměr:</strong> 40/45/50/60/70/80
  cm <br></p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka horní desky: 3 cm </p><p></p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 20 700 Kč</p>"
product_info: ''
product_photos:
- "/v1645438134/forestry/Z_interi%C3%A9rustolek_mav2rf.png"
- "/v1645438135/forestry/Z_interi%C3%A9ru-2stolek_zrcoc1.png"

---
