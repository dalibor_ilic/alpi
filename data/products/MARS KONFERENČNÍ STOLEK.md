---
brand: data/brands/Artisan.md
name: 'MARS KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645711307/forestry/Product_foto_kopie_rcggdp.png"
- "/v1645711326/forestry/Product_foto_l1s63a.png"
dimensions: "<p><strong>Šířka: </strong>80/100/120 cm - <strong>Délka: </strong>80/100/120
  cm - <strong>Výška: </strong>45 cm</p>"
technical_info: ''
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO </strong></a></p>
price: "<p><strong>Cena od: </strong>29 900 Kč</p>"
product_info: ''
product_photos:
- "/v1645711498/forestry/Z_interi%C3%A9ru-2_gribw1.png"
- "/v1645711512/forestry/Z_interi%C3%A9ru_xtwfca.png"

---
