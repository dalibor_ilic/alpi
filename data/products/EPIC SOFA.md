---
brand: data/brands/Atlas.md
name: EPIC SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Elegantně zakřivené područky, hladké linie
  a mimořádně pohodlné polštáře vyplněné kombinací paměťové pěny a peří, činí atraktivní
  design vhodným pro každodenní relaxaci. Na první pohled klasický model se promění
  ve výjimečný kus nábytku, pokud zvolíte neobvyklou barvu kovových nožiček nebo jej
  doplníte dekoračními polštáři z některé z kvalitních látek, které nabízíme.</p>'
product_images:
- "/v1642241946/forestry/ATL_EPIC_sofa_1_ekv4ex.png"
- "/v1642241946/forestry/ATL_EPIC_sofa_5_pejtus.png"
- "/v1642241946/forestry/ATL_EPIC_sofa_7_ywflaq.png"
- "/v1642241946/forestry/ATL_EPIC_sofa_6_eevdso.png"
- "/v1642241945/forestry/ATL_EPIC_sofa_4_hgualn.png"
dimensions: "<p><strong>Šířka: </strong>284 cm - <strong>Délka: </strong>268 cm -
  <strong>Výška: </strong>81 cm - <strong>Výška sedu:</strong> 42 cm - <strong>Hloubka
  sedu:</strong> 58 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36, paměťová pěna, peří se silikonem</p><p><strong>Opěradlo: </strong>HR
  pěna 35/36, paměťová pěna, peří se silikonem</p><p><strong>Elastické popruhy</strong></p><p><strong>Kovové
  nožičky:</strong> lakované v barvě RAL</p><p></p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>143 900 Kč </p><p><strong>Cena v látce
  od: </strong>96 400 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/31fcb9733fda1bc297b0c94367614d7ce473467e.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642242371/forestry/ATL_EPIC_sofa_3_myprli.png"
- "/v1642242371/forestry/ATL_EPIC_sofa_2_ddxxnd.png"

---
