---
brand: data/brands/Artisan.md
name: MELA LOUNGE TRIMMED KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645444533/forestry/Product_fotok%C5%99eslo_irr35y.png"
dimensions: "<p><strong>Šířka: </strong>89 cm - <strong>Délka: </strong>92 cm<strong>
  </strong>- <strong>Výška: </strong>80 cm - <strong>Výška sedu: </strong>42 cm </p>"
technical_info: "<p>Velikost sedu: 95x74 cm</p><p>Průměr nohou: spodní část 3,5 cm,
  vrchní část 4 cm<br></p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 71 300 Kč</p>"
product_info: ''
product_photos:
- "/v1645444589/forestry/Z_interi%C3%A9ruk%C5%99eslo_exnyin.png"

---
