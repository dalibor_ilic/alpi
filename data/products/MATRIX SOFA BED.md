---
brand: data/brands/Atlas.md
name: MATRIX SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Matrix
  je perfektní volbou do obývacího pokoje, ložnice nebo do pokoje pro hosty. Díky
  opěrkám, hlubokému sedu a měkkosti je ideální pro zajištění pohodlí při každé příležitosti
  a uklidní Vás i Vaši mysl pokaždé, kdy budete odpočívat. </p>'
product_images:
- "/v1648814397/forestry/Product_foto_rvkhvw.png"
- "/v1648814397/forestry/Product_foto-6_c5s6gv.png"
- "/v1648814397/forestry/Product_foto-5_hyuhmd.png"
- "/v1648814397/forestry/Product_foto-4_xjomjt.png"
- "/v1648814397/forestry/Product_foto-3_y0ttkf.png"
- "/v1648814397/forestry/Product_foto-2_xi8wqw.png"
- "/v1648814397/forestry/Product_foto-7_mcxez0.png"
dimensions: "<p><strong>Šířka: </strong>207/227/228<strong> </strong>cm - <strong>Délka:
  </strong>195 cm - <strong>Výška: </strong>85 cm - <strong>Výška sedu:</strong> 46
  cm - <strong>Hloubka sedu:</strong> 64 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, kůže</p><p><strong>Sedák: </strong>vysoce
  elastická polyuretanová HR pěna 35/36</p><p><strong>Opěradlo: </strong>vysoce elastická
  polyuretanová pěna L 25/15</p><p><strong>Rozkládací mechanismus s matrací 13 cm
  </strong></p><p><strong>Nožičky: </strong>dřevěný rám - lazura, polyuretanový lak,
  krycí polyuretanová barva dle vzorníku RAL</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p><p>Kůže:
  <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"><strong>CAT
  300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"><strong>DUB</strong></a></p>'
price: ''
product_info: ''
product_photos:
- "/v1648814736/forestry/Z_interi%C3%A9ru-3_e9ndyt.png"
- "/v1648814737/forestry/Z_interi%C3%A9ru-7_rtxgj2.png"
- "/v1648814737/forestry/Z_interi%C3%A9ru-6_qy3keq.png"
- "/v1648814736/forestry/Z_interi%C3%A9ru-5_a7tzrz.png"
- "/v1648814735/forestry/Z_interi%C3%A9ru-4_g2qekp.png"
- "/v1648814708/forestry/Z_interi%C3%A9ru-2_aigxsr.png"

---
