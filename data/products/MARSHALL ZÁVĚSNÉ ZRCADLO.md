---
brand: data/brands/Woak.md
name: 'MARSHALL ZÁVĚSNÉ ZRCADLO '
categories:
- Accessories
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Dřevěné části tohoto nábytku jsou ručně
  vybírány jedna část po druhé, aby dokonale pasovaly s ostatními materiály, které
  jsou zcela inovativní a recyklovatelné, jako je nezpracovaná kůže, čistý mramor
  nebo lité sklo s měnícími se barvami. Kombinace teplých a studených materiálů vede
  k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem je mužný,
  ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací pokoj nebo
  koutek na čtení.</p>'
product_images:
- "/v1606897085/forestry/WHO_MARSHALL_z%C3%A1v%C4%9Bsn%C3%A9_zrcadlo_3_hdky5y.png"
- "/v1606897076/forestry/WHO_MARSHALL_z%C3%A1v%C4%9Bsn%C3%A9_zrcadlo_1_fhktz1.png"
dimensions: "<p><strong>Šířka: </strong>33 cm - <strong>Délka: </strong>7 cm - <strong>Výška:</strong>
  140 - 168 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>povrchová úprava: olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>18 500 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Marshall_wall_mirror_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606897236/forestry/WHO_MARSHALL_z%C3%A1v%C4%9Bsn%C3%A9_zrcadlo_2_cpqckh.png"

---
