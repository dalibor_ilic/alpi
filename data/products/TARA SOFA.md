---
brand: data/brands/Artisan.md
name: TARA SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645712042/forestry/Tara_sofaProduct_foto_t0pr6h.png"
- "/v1645712042/forestry/Tara_sofaProduct_foto-2_xmrrkx.png"
- "/v1645712042/forestry/Tara_sofaProduct_foto-3_vabxrs.png"
- "/v1645712042/forestry/Tara_sofaProduct_foto-4_ojoely.png"
dimensions: "<p>Dvojsed I:</p><p><strong>Šířka: </strong>220 cm - <strong>Délka: </strong>93
  cm - <strong>Výška: </strong>71 cm</p><p>Dvojsed II:</p><p><strong>Šířka: </strong>310
  cm - <strong>Délka: </strong>93 cm - <strong>Výška: </strong>71 cm</p><p>Trojsed
  I:</p><p><strong>Šířka: </strong>240 cm - <strong>Délka: </strong>93 cm - <strong>Výška:
  </strong>71 cm</p><p>Trojsed II:</p><p><strong>Šířka: </strong>330 cm - <strong>Délka:
  </strong>93 cm - <strong>Výška: </strong>71 cm</p><p>Čtyřsed:</p><p><strong>Šířka:
  </strong>263 cm - <strong>Délka: </strong>93 cm - <strong>Výška: </strong>71 cm</p>"
technical_info: ''
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO </strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>98 600 Kč </p>"
product_info: ''
product_photos:
- "/v1645712990/forestry/Tara_sofaZ_interi%C3%A9ru-2_yndkw1.png"
- "/v1645712971/forestry/Tara_sofaZ_interi%C3%A9ru_gppsur.png"
- "/v1645712989/forestry/Tara_sofaZ_interi%C3%A9ru-3_xn047k.png"

---
