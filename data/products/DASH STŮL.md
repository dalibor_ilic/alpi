---
brand: data/brands/Artisan.md
name: DASH STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605536552/forestry/ART_DASH_rozkl%C3%A1dac%C3%AD_st%C5%AF_5_rpt7oz.png"
- "/v1605536552/forestry/ART_DASH_rozkl%C3%A1dac%C3%AD_st%C5%AF_4_wnbc0e.png"
- "/v1605536552/forestry/ART_DASH_rozkl%C3%A1dac%C3%AD_st%C5%AF_3_ijaxzf.png"
- "/v1605536542/forestry/ART_DASH_rozkl%C3%A1dac%C3%AD_st%C5%AF_1_fhk0pr.png"
dimensions: "<p><strong>Šířka: </strong>125-185<strong> </strong>cm - <strong>Délka:
  </strong>125-185 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 6 cm</p><p>Vizuální
  tloušťka desky: 6 cm</p><p>Průměr nohou: spodní část 6 cm, vrchní část 6 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 89 600 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605536728/forestry/ART_DASH_rozkl%C3%A1dac%C3%AD_st%C5%AF_2_leuqsg.png"

---
