---
brand: data/brands/Atlas.md
name: OMNIA LUX VK 120 KOMODA
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Komoda s prvky z masivního dubového dřeva
  a s efektem poškrábaného dřeva zaujme svým vzhledem. Je vizuálně přitažlivá a praktická,
  protože má velký úložný prostor. Komoda Omnia lux VK 120 má čtyři čela, která jsou
  lakována podle vašeho výběru. Skvělá kombinace moderních, čistých linií, harmonických
  detailů a přírodního půvabu.</p>'
product_images:
- "/v1643202728/forestry/ATL_OMNIALUXVK120_komoda_lqm3ye.png"
dimensions: "<p><strong>Šířka: </strong>120 cm - <strong>Délka: </strong>52 cm - <strong>Výška:
  </strong>141,5 cm</p>"
technical_info: "<p><strong>Základ: </strong>dýhované MDF</p><p><strong>Přední část:
  </strong>masivní dubové dřevo, dýhovaná MDF deska</p><p><strong>Kování: </strong>BLUM</p><p><strong>Nožičky:
  </strong>ocelové nohy lakované v barvě RAL</p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>34 300 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/14528a484bcbdbb79bdc25b238df410cde09d899.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos: []

---
