---
brand: data/brands/Atlas.md
name: MORFEO POSTEL
categories:
- Bed
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Vysoké čelo se zaoblenými hranami a dubovými
  nohami pro milovníky moderního designu. Čalouněný dřevěný rám se zvýšenými nožičkami,
  dodává jemný a sofistikovaný vzhled, který nemá vliv na jeho funkčnost a stabilitu.
  S touto postelí získá vaše ložnice svěží a nevšední vzhled.</p>'
product_images:
- "/v1643105379/forestry/ATL_MORFEO_postel_1_m0c0ni.png"
- "/v1643105379/forestry/ATL_MORFEO_postel_3_peb0bm.png"
dimensions: "<p>Dvojlůžko I.</p><p>160 × 200 cm - <strong>Šířka: </strong>173 cm -
  <strong>Délka: </strong>218 cm - <strong>Výška: </strong>136 cm</p><p>Dvojlůžko
  II.</p><p>180 × 200 cm - <strong>Šířka: </strong>193 cm - <strong>Délka: </strong>218
  cm - <strong>Výška: </strong>136 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Podpěra: </strong>vysoce
  elastická polyuretanová pěna N 25/38</p><p><strong>Základ:</strong> rám Latoflex</p><p><strong>Nožičky:
  </strong>mořidlo na dřevo, polyuretanový lak, krycí polyuretanová barva podle RAL</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>49 100 Kč </p><p><strong>Cena v látce
  od: </strong>35 600 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/e654586b0065491b8720b5370f5a80af8bbdb22b.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643105631/forestry/ATL_MORFEO_postel_2_lxd3lg.png"

---
