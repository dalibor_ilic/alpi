---
brand: data/brands/Woak.md
name: 'NINAS ODKLÁDACÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Ústředním prvkem jsou minimalistické
  linie aplikované na kusy retro nábytku. Cílem kolekce je nabídnout novou interpretaci
  klasických kusů nábytku, prostřednictvím obnovy stylu pomocí techniky zpracované
  značkou Woak. Záměrem je rozebrat společná místa tradičních forem, eliminovat ozdoby
  a prvky, které jsou příliš dekorativní, aby byl objekt moderní a zároveň funkční.</p>'
product_images:
- "/v1607005301/forestry/WHO_NINAS_odkl%C3%A1dac%C3%AD_stolek_1_cvnvj4.png"
dimensions: "<p><strong>Šířka: </strong>45 cm - <strong>Délka: </strong>45 cm - <strong>Výška:</strong>
  45 cm</p>"
technical_info: "<p>povrch: mramor Emperador nebo Carara</p><p>Masivní dřevo: dub
  nebo ořech, olejovaná úprava</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 27 400 Kč  </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Ninas_side_table_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607005470/forestry/WHO_NINAS_odkl%C3%A1dac%C3%AD_stolek_3_t7gc0g.png"
- "/v1607005470/forestry/WHO_NINAS_odkl%C3%A1dac%C3%AD_stolek_2_rxzb4d.png"

---
