---
brand: data/brands/Artisan.md
name: FIN POSTEL
categories:
- Bed
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605530860/forestry/ART_FIN_postel_1_c2fuaz.png"
dimensions: "<p><strong>Šířka: </strong>160 - 200<strong> </strong>cm - <strong>Délka:
  </strong>200 cm - <strong>Výška: </strong>90 cm - <strong>Výška sedu:</strong> 30
  cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Hloubka pro rošt/matraci:
  13 cm</p><p>Rám postele: 4 cm</p><p>Rošt ani matrace nejsou součástí.</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>66 600 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605531093/forestry/ART_FIN_postel_5_mkuiyz.png"
- "/v1605531092/forestry/ART_FIN_postel_4_fubqch.png"
- "/v1605531093/forestry/ART_FIN_postel_3_fxr9zi.png"
- "/v1605531093/forestry/ART_FIN_postel_2_agq7xe.png"

---
