---
brand: data/brands/Artisan.md
name: LENO BAR
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645710596/forestry/Leno_barProduct_foto-3_yuku3i.png"
- "/v1645710596/forestry/Leno_barProduct_foto_pdfx85.png"
dimensions: "<p><strong>Šířka: </strong>100<strong> </strong>cm - <strong>Délka: </strong>51
  cm - <strong>Výška: </strong>106/135 cm</p>"
technical_info: "<p>Výška nohou: 44 cm </p><p>Průměr nohou: 1,6 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>134 500 Kč</p>"
product_info: ''
product_photos:
- "/v1645710863/forestry/Leno_barZ_interi%C3%A9ru_ygdnbd.png"
- "/v1645710862/forestry/Leno_barZ_interi%C3%A9ru-2_fudlwl.png"

---
