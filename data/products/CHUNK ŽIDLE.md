---
brand: data/brands/Artisan.md
name: 'CHUNK ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644248131/forestry/Product_foto_kopie_kp4dnl.png"
- "/v1644247714/forestry/Product_foto-5_oenhs6.png"
- "/v1644247581/forestry/Product_foto-6_ngron4.png"
- "/v1644247653/forestry/Product_foto-7_knrdz8.png"
- "/v1644247653/forestry/Product_foto-8_jerrsd.png"
dimensions: "<p><strong>Šířka: </strong>62,5 cm - <strong>Délka: </strong>62 cm -
  <strong>Výška: </strong>85 cm - <strong>Výška sedu: </strong>46,7 cm - <strong>Hloubka
  sedu: </strong>43 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a kovové nohy.</p><p>Velikost
  sedu: 62,5x43 cm</p><p>Tloušťka sedací a opěrné plochy: 5 cm</p><p>Průměr nohou:
  1,6 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 33 100 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604583103/forestry/ART_CHUNK_%C5%BEidle_3_y2x7lf.png"
- "/v1604583103/forestry/ART_CHUNK_%C5%BEidle_2_ady2dq.png"

---
