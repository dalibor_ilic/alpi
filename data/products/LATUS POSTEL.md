---
brand: data/brands/Artisan.md
name: 'LATUS POSTEL '
categories:
- Bed
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605531699/forestry/ART_LATUS_postel_1_npiyva.png"
dimensions: "<p><strong>Šířka: </strong>80/140/160/180/200<strong> </strong>cm - <strong>Délka:
  </strong>200 cm - <strong>Výška: </strong>96 cm - <strong>Výška sedu:</strong> 37
  cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, možná varianta s úložnými šuplíky
  ( + 12 cm z každé strany).</p><p>Rám postele: 12 cm </p><p>Hloubka pro rošt/matraci:
  8 cm  </p><p>Rošt ani matrace nejsou součástí.</p><p></p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO </strong></a></p>
price: "<p><strong>Cena od: </strong>94 900 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605532517/forestry/ART_LATUS_postel_4_tzxdc0.png"
- "/v1605532517/forestry/ART_LATUS_postel_3_afoawv.png"
- "/v1605532517/forestry/ART_LATUS_postel_2_lnoh6i.png"

---
