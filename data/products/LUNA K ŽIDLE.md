---
brand: data/brands/Atlas.md
name: LUNA K ŽIDLE
categories:
- Chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Klasický, nadčasový model, který stále
  přitahuje velké množství zákazníků. Konstrukce z masivního bukového dřeva poskytuje
  skvělou oporu při sezení, zatímco čalouněný sedák a opěrák se starají o pohodlí
  židle Luna K. Díky svému univerzálnímu designu je ideální volbou pro obytné i pracovní
  prostory.</p>'
product_images:
- "/v1643282671/forestry/ATL_LUNAK_%C5%BEidle_1_xwx9a6.png"
dimensions: "<p><strong>Šířka: </strong>49 cm - <strong>Délka: </strong>44 cm - <strong>Výška:
  </strong>80 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> HR pěna 35/36 </p><p><strong>Opěradlo:</strong>
  pěna N 25/38</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky: </strong>dřevěné</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/814c7058a005490a003f3898e6820b501838e1c1.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643282813/forestry/ATL_LUNAK_%C5%BEidle_3_os40m9.png"
- "/v1643282811/forestry/ATL_LUNAK_%C5%BEidle_2_ynjt73.png"

---
