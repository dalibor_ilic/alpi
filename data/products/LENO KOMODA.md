---
brand: data/brands/Artisan.md
name: 'LENO KOMODA '
categories:
- Sideboard
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645710961/forestry/Leno_komodaProduct_foto-2_wdifvf.png"
- "/v1645710961/forestry/Leno_komodaProduct_foto_u42ser.png"
dimensions: "<p><strong>Šířka: </strong>150/180/210 cm - <strong>Délka: </strong>51
  cm - <strong>Výška: </strong>60 cm</p>"
technical_info: "<p>Výška nohou: 24 cm </p><p>Průměr nohou: 1,6 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO </strong></a></p>
price: "<p><strong>Cena od: </strong>57 400 Kč</p>"
product_info: ''
product_photos:
- "/v1645711051/forestry/Leno_komodaZ_interi%C3%A9ru-3_q6hqhf.png"
- "/v1645711050/forestry/Leno_komodaZ_interi%C3%A9ru_muf3zu.png"
- "/v1645711049/forestry/Leno_komodaZ_interi%C3%A9ru-2_abj3xu.png"

---
