---
brand: data/brands/Woak.md
name: 'TRIA STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p>'
product_images:
- "/v1606999349/forestry/WHO_TRIA_st%C5%AFl_1_xi31ev.png"
- "/v1606999360/forestry/WHO_TRIA_st%C5%AFl_3_o7cpan.png"
dimensions: "<p><strong>Šířka: </strong>80 cm - <strong>Délka: </strong>80 cm - <strong>Výška:</strong>
  42 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>deska stolu: ručně vyráběné strukturované
  sklo nebo dřevo</p><p>povrchová úprava: lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>24 600 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Tria_coffee_table_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606999711/forestry/WHO_TRIA_st%C5%AFl_2_a8wzar.png"

---
