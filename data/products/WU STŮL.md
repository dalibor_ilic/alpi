---
brand: data/brands/Artisan.md
name: WU STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1604997058/forestry/ART_WU_st%C5%AFl_2_illzcv.png"
- "/v1604997058/forestry/ART_WU_st%C5%AFl_1_fu7ohu.png"
- "/v1604997058/forestry/ART_WU_st%C5%AFl_5_sidx5s.png"
dimensions: "<p><strong>Šířka: </strong>165/185/205/225/245/265/280 cm - <strong>Délka:
  </strong>110/115 cm - <strong>Výška:</strong> 76 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální
  tloušťka desky: 4 cm</p><p>Průměr nohou: spodní část 4x5 cm, vrchní 4x8 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 64 300 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604997122/forestry/ART_WU_st%C5%AFl_4_lsoyoh.png"
- "/v1604997099/forestry/ART_WU_st%C5%AFl_3_p5dvhl.png"

---
