---
brand: data/brands/Artisan.md
name: 'TESA HOUPACÍ ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a><br><br></p>'
product_images:
- "/v1604668311/forestry/ART_TESA_houpac%C3%AD_%C5%BEidle_1_ewqenr.png"
- "/v1644484260/forestry/Product_foto-51_mgxxsh.png"
dimensions: "<p><strong>Šířka: </strong>53<strong> </strong>cm - <strong>Délka: </strong>55
  cm - <strong>Výška: </strong>73,6 cm - <strong>Výška sedu: </strong>45 cm - <strong>Hloubka
  sedu: </strong>48,4 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 48,4 x 40,1 cm</p><p>Tloušťka sedací a opěrné plochy: 2,3 cm</p><p>Průměr
  nohou: 3 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> |<a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong> DŘEVO </strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 16 300 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604668497/forestry/ART_TESA_houpac%C3%AD_%C5%BEidle_4_tovuae.png"
- "/v1604668497/forestry/ART_TESA_houpac%C3%AD_%C5%BEidle_3_oiwqv0.png"
- "/v1604668496/forestry/ART_TESA_houpac%C3%AD_%C5%BEidle_2_niiycn.png"

---
