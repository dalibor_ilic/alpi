---
brand: data/brands/Woak.md
name: 'MALIN KOMODA S DŘEVĚNÝM PODNOŽÍM '
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606484138/forestry/WHO_MALIN_komoda_s_d%C5%99ev%C4%9Bn%C3%BDm_podno%C5%BE%C3%ADm_1_y07jm1.png"
- "/v1606484146/forestry/WHO_MALIN_komoda_s_d%C5%99ev%C4%9Bn%C3%BDm_podno%C5%BE%C3%ADm_5_ttjmno.png"
dimensions: "<p><strong>Šířka: </strong>120/180 cm - <strong>Délka: </strong>40/50
  cm - <strong>Výška:</strong> 78 cm</p>"
technical_info: "<p>Tato komoda má 3 zásuvky, 2 dvířka, dřevěné nohy a je vyrobena
  z masivního dřeva (dubu nebo ořechu) a má olejovou úpravu. </p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 52 400 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_sideboard_wooden_legs_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod:<a href="https://drive.google.com/file/d/1ub45U30MbgcCpsJmwtVjuu7PE2M3HJx1/view?usp=sharing"
  title=""><strong> Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606485032/forestry/WHO_MALIN_komoda_s_d%C5%99ev%C4%9Bn%C3%BDm_podno%C5%BE%C3%ADm_4_uk5x4s.png"
- "/v1606485035/forestry/WHO_MALIN_komoda_s_d%C5%99ev%C4%9Bn%C3%BDm_podno%C5%BE%C3%ADm_3_ycovs2.png"
- "/v1606485036/forestry/WHO_MALIN_komoda_s_d%C5%99ev%C4%9Bn%C3%BDm_podno%C5%BE%C3%ADm_2_qgpqcy.png"

---
