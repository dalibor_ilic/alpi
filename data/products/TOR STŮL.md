---
brand: data/brands/Artisan.md
name: 'TOR STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605684450/forestry/ART_TOR_st%C5%AFl_1_yfkuns.png"
dimensions: "<p><strong>Šířka: </strong>160/180/200/220/240/260/280/300 cm - <strong>Délka:
  </strong>100 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Tloušťka desky: 4 cm</p><p>Vizuální tloušťka desky: 8 cm</p><p>Průměr
  nohou: 8 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>41 300 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605684635/forestry/ART_TOR_st%C5%AFl_2_wbpw66.png"
- "/v1605684635/forestry/ART_TOR_st%C5%AFl_3_nh56xa.png"

---
