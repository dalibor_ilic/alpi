---
brand: data/brands/Atlas.md
name: LIWA SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Liwa
  pohovka Vám po rušných a náročných dnech dopřeje pohodlí, které si zasloužíte. Svým
  vzhledem vyzařuje teplo a přinese tak Vašemu domovu hřejivou a útulnou atmosféru.
  </p>'
product_images:
- "/v1648809189/forestry/Product_foto_tnwp4y.png"
- "/v1648809188/forestry/Product_foto-2_oa2oxo.png"
- "/v1648809188/forestry/Product_foto-3_cwpt7r.png"
- "/v1648809189/forestry/Product_foto-4_hqrcvu.png"
- "/v1648809189/forestry/Product_foto-5_j3h3oy.png"
- "/v1648809188/forestry/Product_foto-6_faapcp.png"
dimensions: "<p><strong>Šířka: </strong>295<strong> </strong>cm - <strong>Délka: </strong>187
  cm - <strong>Výška: </strong>78 cm - <strong>Výška sedu:</strong> 45 cm - <strong>Hloubka
  sedu:</strong> 59 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka</p><p><strong>Sedák: </strong>vysoce
  elastická polyuretanová HR pěna 35/36</p><p><strong>Opěradlo: </strong>vysoce elastická
  polyuretanová HR pěna 35/36, polštáře s paměťovou pěnou, silikonové kuličky v povlaku
  polštáře</p><p><strong>Přímé vlnové pružiny</strong></p><p><strong>Nožičky: </strong>plastové</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/e90d27343efd8cec83b046d1feea9e2acb490b59.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1648810492/forestry/Z_interi%C3%A9ru-2_kopie_shvrmx.png"
- "/v1648810535/forestry/Z_interi%C3%A9ru-4_oifb2n.png"
- "/v1648810535/forestry/Z_interi%C3%A9ru-3_tj529x.png"
- "/v1648810535/forestry/Z_interi%C3%A9ru-5_f3u39p.png"
- "/v1648810535/forestry/Z_interi%C3%A9ru-6_o5i9oo.png"
- "/v1648810505/forestry/Z_interi%C3%A9ru-2_gfyal9.png"

---
