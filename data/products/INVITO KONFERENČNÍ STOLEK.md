---
brand: data/brands/Artisan.md
name: INVITO KONFERENČNÍ STOLEK
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645440330/forestry/Product_foto-2.png_konferen%C4%8Dn%C3%AD_zww9sk.png"
- "/v1645440310/forestry/Product_foto.png_konferen%C4%8Dn%C3%AD_vyydyp.png"
dimensions: "<p><strong>Šířka: </strong>42/60/70/80/90/100/110/120/130/135/140/160
  cm - <strong>Délka: </strong>42/60/65/70/80/90/ 100/110/120/130 cm - <strong>Výška:
  </strong>40 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Horní a boční tloušťka: 2 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 7 200 Kč</p>"
product_info: ''
product_photos:
- "/v1645440410/forestry/Z_interi%C3%A9ru-2.png_konferen%C4%8Dn%C3%AD_bm7g8n.png"

---
