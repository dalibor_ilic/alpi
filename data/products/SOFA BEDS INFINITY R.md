---
brand: data/brands/Atlas.md
name: INFINITY R SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Funkční model výjimečného vzhledu a pohodlí.
  IINFINITY R je verze našeho nejprodávanějšího modelu. Pohovka výrazného designu,
  která je synonymem pohodlí, se po rozložení spacího mechanismu stává velmi funkční.
  Svou oblíbenou hranatou pohovku můžete snadno proměnit v pohodlné lůžko na spaní.</p>'
product_images:
- "/v1642437771/forestry/ATL_INFINITYR_sofa_1_j3aw04.png"
- "/v1642437773/forestry/ATL_INFINITYR_sofa_9_npa7ur.png"
- "/v1642437773/forestry/ATL_INFINITYR_sofa_11_zxv4xb.png"
- "/v1642437773/forestry/ATL_INFINITYR_sofa_12_ogd4sc.png"
- "/v1642437773/forestry/ATL_INFINITYR_sofa_13_jfupf3.png"
- "/v1642437772/forestry/ATL_INFINITYR_sofa_8_gnjr3c.png"
- "/v1642437773/forestry/ATL_INFINITYR_sofa_10_aarhh4.png"
dimensions: "<p><strong>Šířka: </strong>345 cm - <strong>Délka: </strong>168 cm -
  <strong>Výška: </strong>83/100 cm - <strong>Výška sedu:</strong> 40 cm - <strong>Hloubka
  sedu:</strong> 61 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36, HR pěna 35/30, peří se silikonem</p><p><strong>Opěradlo: </strong>pěna
  L 25/15 a silikon</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:</strong>
  kovové -inox nohy</p><p><strong>Možnosti: </strong>mechanismus rozkládací postele
  </p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1x3qtWSfvDZh45kB8JxujwYgvlKTEPGhz/view?usp=sharing"
  title="Látky ELEGANT"><strong>ELEGANT</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>112 600 Kč </p><p><strong>Cena v látce
  od: </strong>82 900 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/2cc11cdde1fedc42283628036048ad9c83a14ebd.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642438149/forestry/ATL_INFINITYR_sofa_6_blvvtl.png"
- "/v1642438149/forestry/ATL_INFINITYR_sofa_5_zlbukt.png"
- "/v1642438149/forestry/ATL_INFINITYR_sofa_4_k1yckz.png"
- "/v1642438149/forestry/ATL_INFINITYR_sofa_3_jt6gdq.png"
- "/v1642438148/forestry/ATL_INFINITYR_sofa_2_nm5uym.png"
- "/v1642438149/forestry/ATL_INFINITYR_sofa_7_mrykmi.png"

---
