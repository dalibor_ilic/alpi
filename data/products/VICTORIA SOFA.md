---
brand: data/brands/Atlas.md
name: VICTORIA SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Ve
  světě luxusních pohovek záleží také na velikosti. Victoria je o maximalizaci prostoru,
  ale zároveň se nemusíte bát, že Váš útulný domov vzplane, jelikož je navržena tak,
  aby vnesla nádech moderního designu, poskytla pohodlí a pokud toužíte po útulném
  domově bez potíží s nedostatkem místa, tak právě pohovka Victoria je pro Vás správnou
  volbou.  </p>'
product_images:
- "/v1648812529/forestry/Product_foto_wvllyg.png"
- "/v1648812529/forestry/Product_foto-2_e6wtcm.png"
- "/v1648812528/forestry/Product_foto-3_zy1pkz.png"
- "/v1648812529/forestry/Product_foto-4_yytec2.png"
- "/v1648812529/forestry/Product_foto-5_dnnand.png"
- "/v1648812527/forestry/Product_foto-6_hyxmgl.png"
- "/v1648812529/forestry/Product_foto-7_ebze6j.png"
dimensions: "<p>Dvojsed: <strong>Šířka: </strong>230/260<strong> </strong>cm - <strong>Délka:
  </strong>100 cm - <strong>Výška: </strong>71 cm - <strong>Výška sedu:</strong> 43
  cm - <strong>Hloubka sedu:</strong> 90 cm</p><p>Trojsed: <strong>Šířka: </strong>350<strong>
  </strong>cm - <strong>Délka: </strong>100 cm - <strong>Výška: </strong>71 cm - <strong>Výška
  sedu:</strong> 43 cm - <strong>Hloubka sedu:</strong> 90 cm</p><p>Rohová: <strong>Šířka:
  </strong>320<strong> </strong>cm - <strong>Délka: </strong>130 cm - <strong>Výška:
  </strong>71 cm - <strong>Výška sedu:</strong> 43 cm - <strong>Hloubka sedu:</strong>
  90 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, kůže</p><p><strong>Sedák: </strong>vysoce
  elastická polyuretanová HR pěna 35/36 v splývavém krytu</p><p><strong>Opěradlo:
  </strong>vysoce elastická polyuretanová pěna N 25/38, paměťová pěna, silikon</p><p><strong>Obloukové
  vlnové pružiny </strong></p><p><strong>Nožičky: </strong>plastové</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p><p>Kůže:
  <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"><strong>CAT
  300-1000</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/e865fb7c644391333adbee9c2813a4db5531fe26.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1648813229/forestry/Z_interi%C3%A9ru-4_sca0lp.png"
- "/v1648813231/forestry/Z_interi%C3%A9ru-2_jh4zmz.png"
- "/v1648813231/forestry/Z_interi%C3%A9ru-5_hjspip.png"
- "/v1648813231/forestry/Z_interi%C3%A9ru-3_fildwy.png"
- "/v1648813230/forestry/Z_interi%C3%A9ru-6_xkbdsx.png"

---
