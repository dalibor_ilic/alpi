---
brand: data/brands/Atlas.md
name: BRIDGE STŮL
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Vysoce kvalitní materiály se snoubí s
  ruční výrobou, což ze stolu Bridge činí jedinečný produkt, který se skvěle hodí
  do jídelny zařízené s nádechem minimalistického industriálního designu. Obdélníková
  deska je ideální v případě, že chcete pozvat svou rodinu nebo přátele na večeři,
  zejména během svátků. Přímé, klidné linie přinášejí příjemný a přitažlivý vzhled.</p>'
product_images:
- "/v1643278656/forestry/ATL_BRIDGE_st%C5%AFl_1_mnmyqw.png"
dimensions: "<p><strong>Šířka: </strong>160 cm - <strong>Délka: </strong>90 cm - <strong>Výška:
  </strong>74 cm</p>"
technical_info: "<p><strong>Materiál:</strong> dub, MDF 40 mm, nebo MDF 20 mm</p><p></p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>18 100 Kč</p>"
product_info: ''
product_photos:
- "/v1643278738/forestry/ATL_BRIDGE_st%C5%AFl_2_fojjh1.png"

---
