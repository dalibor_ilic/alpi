---
brand: data/brands/Woak.md
name: 'LAVADO JÍDELNÍ STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p>'
product_images:
- "/v1607001333/forestry/WHO_LAVADO_st%C5%AFl_1_ri3fsk.png"
- "/v1607001344/forestry/WHO_LAVADO_st%C5%AFl_5_aimw3f.png"
- "/v1607001344/forestry/WHO_LAVADO_st%C5%AFl_4_bup7jk.png"
dimensions: "<p><strong>Šířka: </strong>160 - 300 cm - <strong>Délka: </strong>90
  - 100 cm - <strong>Výška:</strong> 75 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>povrchová úprava: lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>51 500 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Marshall_rocking_armchair_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1sGYVhxbcwfLjTuv3pl7m6ygtdSCPxLXo/view?usp=sharing"
  title=""><strong>Stáhnout PDF </strong></a></p>'
product_photos:
- "/v1607001503/forestry/WHO_LAVADO_st%C5%AFl_3_hyz0jw.png"
- "/v1607001503/forestry/WHO_LAVADO_st%C5%AFl_2_dsdc2l.png"

---
