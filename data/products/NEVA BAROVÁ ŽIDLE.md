---
brand: data/brands/Artisan.md
name: 'NEVA BAROVÁ ŽIDLE '
categories:
- Bar chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644249827/forestry/Product_foto-28_yapsvw.png"
- "/v1644250098/forestry/Product_foto2_c5ukuy.png"
- "/v1644249828/forestry/Product_foto-30_rivqio.png"
- "/v1644249828/forestry/Product_foto-29_ket680.png"
dimensions: "<p><strong>Šířka: </strong>42<strong> </strong>cm - <strong>Délka: </strong>50
  cm - <strong>Výška:</strong> 102 cm - <strong>Výška sedu: </strong>65 cm - <strong>Hloubka
  sedu: </strong>40 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 40x40 cm</p><p>Průměr nohou: spodní část 3 cm, vrchní část 4,3 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a> </p>
price: "<p><strong>Cena od:</strong> 20 700 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604665519/forestry/ART_NEVA_barov%C3%A1_%C5%BEidle_6_qlqecc.png"
- "/v1604665518/forestry/ART_NEVA_barov%C3%A1_%C5%BEidle_5_fj41sw.png"

---
