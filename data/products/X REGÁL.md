---
brand: data/brands/Artisan.md
name: X REGÁL
categories:
- Shelf
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605265939/forestry/ART_X_reg%C3%A1l_2_ikf4ff.png"
- "/v1605265939/forestry/ART_X_reg%C3%A1l_1_p8wwqs.png"
dimensions: "<p><strong>Šířka: </strong>180<strong> </strong>cm - <strong>Délka: </strong>47
  cm - <strong>Výška: </strong>163 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka polic: 2,5 cm</p><p>Průměr
  police: 4x6 cm</p><p>Hloubka polic: 35 cm</p><p>Výška mezi policemi: 38 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 72 300 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605266048/forestry/ART_X_reg%C3%A1l_5_s9rmlo.png"
- "/v1605266047/forestry/ART_X_reg%C3%A1l_4_fd0ovn.png"
- "/v1605266048/forestry/ART_X_reg%C3%A1l_3_bbk3yd.png"

---
