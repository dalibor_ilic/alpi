---
brand: data/brands/Extraform.md
name: 'DOLOMIA SOFA '
categories:
- Sofa
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"
  title="EXTRAFORM"><strong>EXTRAFORM</strong></a></p><p>Dolomia je pohovka, která
  upoutá pozornost svým komfortem a elegancí, která je zajištěna relaxačním polstrováním,
  měkkým materiálem na dotyk a jedinečným designem. Dolomia kolekce přináší přírodní
  tvary do Vašeho domova a kanceláře. Pomocí kombinace barev pro Vaše moderní prostory.</p>'
product_images:
- "/v1644502716/forestry/EXT_DOLOMIA_sofa_1_r8gvly.png"
- "/v1644502706/forestry/EXT_DOLOMIA_sofa_5_gzhqle.png"
- "/v1644502680/forestry/EXT_DOLOMIA_sofa_6_d4xp9h.png"
- "/v1644502680/forestry/EXT_DOLOMIA_sofa_7_l6axu1.png"
- "/v1644502679/forestry/EXT_DOLOMIA_sofa_8_yxf5ty.png"
- "/v1644502681/forestry/EXT_DOLOMIA_sofa_9_ogvahm.png"
dimensions: '<p><strong>Dvojsed: Šířka: </strong>194 cm - <strong>Délka:</strong>
  95 cm - <strong>Výška:</strong> 79 cm - <strong>Výška sedu:</strong> 44 cm - <strong>Hloubka
  sedu:</strong> 48 cm</p><p class="Paragraph SCXW256536814 BCX4"><strong>Trojsed:
  Šířka: </strong>214 cm - <strong>Délka:</strong> 95 cm - <strong>Výška:</strong>
  79 cm - <strong>Výška sedu:</strong> 44 cm - <strong>Hloubka sedu:</strong> 48 cm</p>'
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, lepenky, 18 mm silné
  překližky, MDF desky.</p><p class="Paragraph SCXW140904606 BCX4">Sedáky - vysoce
  elastická HR pěna, 200 g tepelně pojené plsti, elastické popruhy.</p><p class="Paragraph
  SCXW140904606 BCX4">Opěrky - ergonomická vysoce elastická pěna, 400 g tepelně pojené
  plsti.</p><p class="Paragraph SCXW140904606 BCX4">Nohy z dubového dřeva, naolejované.
  Volitelně z bukového dřeva v šesti barvách.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 42 500 Kč </p>"
product_info: ''
product_photos:
- "/v1644502824/forestry/EXT_DOLOMIA_sofa_2_xynzof.png"
- "/v1644502817/forestry/EXT_DOLOMIA_sofa_4_kxnygh.png"
- "/v1644502821/forestry/EXT_DOLOMIA_sofa_3_yvhe3h.png"

---
