---
brand: data/brands/Atlas.md
name: CITY SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Avantgardní vzhled nenarušuje komfort
  a technické vlastnosti tohoto modelu. Zakřivený, neobvyklý tvar, prošívání na opěradle
  a sedáku činí tento model zajímavým a atraktivním. Zakřivená konstrukce a polštáře
  kopírují linii těla a umožňují naprostou relaxaci při sezení. Ať už se jedná o dvoumístné
  křeslo či křeslo s područkami, je to právě design, který dělá ze City výjimečný
  model.</p>'
product_images:
- "/v1642251246/forestry/ATL_CITY_sofa_8_qouhfi.png"
- "/v1642251246/forestry/ATL_CITY_sofa_7_kfa2ui.png"
- "/v1642251246/forestry/ATL_CITY_sofa_1_vhxqzd.png"
dimensions: "<p><strong>Šířka: </strong>176 cm - <strong>Délka: </strong>102 cm -
  <strong>Výška: </strong>92 cm - <strong>Výška sedu:</strong> 45 cm - <strong>Hloubka
  sedu:</strong> 56 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  kombinace HR pěny 35/30 a 35/36</p><p><strong>Opěradlo: </strong>HR pěna 35/36 a
  35/30</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:</strong> plastové
  nožičky 20mm</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>49 400 Kč</p><p><strong>Cena v látce od:
  </strong>35 400 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/f65e9b6d3d70f2e21e9cc45c5d95f3b5a64d9470.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642251512/forestry/ATL_CITY_sofa_5_qzlbof.png"
- "/v1642251512/forestry/ATL_CITY_sofa_4_i7ijlm.png"
- "/v1642251513/forestry/ATL_CITY_sofa_2_pccvbc.png"

---
