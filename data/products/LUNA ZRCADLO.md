---
brand: data/brands/Artisan.md
name: LUNA ZRCADLO
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645708386/forestry/Luna_zrcadloProduct_foto_udlnih.png"
dimensions: "<p><strong>Rozměry: </strong>Ø60x4 cm/ Ø80x5 cm/ Ø100x5 cm </p>"
technical_info: ''
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>13 400 Kč </p>"
product_info: ''
product_photos:
- "/v1645708426/forestry/Luna_zrcadloZ_interi%C3%A9ru-2_bwdrjr.png"
- "/v1645708427/forestry/Luna_zrcadloZ_interi%C3%A9ru-4_wvjmpb.png"
- "/v1645708427/forestry/Luna_zrcadloZ_interi%C3%A9ru-3_x1i4wa.png"
- "/v1645708426/forestry/Luna_zrcadloZ_interi%C3%A9ru-5_mgam4o.png"

---
