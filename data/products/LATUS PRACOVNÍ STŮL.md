---
brand: data/brands/Artisan.md
name: LATUS PRACOVNÍ STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605082713/forestry/ART_LATUS_pracovn%C3%AD_st%C5%AFl_2_exqz0w.png"
- "/v1605082713/forestry/ART_LATUS_pracovn%C3%AD_st%C5%AFl_1_numrln.png"
- "/v1605082721/forestry/ART_LATUS_pracovn%C3%AD_st%C5%AFl_6_hlaa8l.png"
dimensions: "<p><strong>Šířka: </strong>140-160<strong> </strong>cm - <strong>Délka:
  </strong>70 cm - <strong>Výška: </strong>75 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální
  tloušťka desky: 6 cm</p><p>Průměr nohou: spodní část 3 cm, vrhcní část 6 cm</p><p>Dvě
  zásuvky.</p><p>Jiné úpravy a ceny na vyžádání.</p>"
materials: <p><a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 89 000 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605083003/forestry/ART_LATUS_pracovn%C3%AD_st%C5%AFl_5_nb7gem.png"
- "/v1605083003/forestry/ART_LATUS_pracovn%C3%AD_st%C5%AFl_4_bcjmfj.png"
- "/v1605083003/forestry/ART_LATUS_pracovn%C3%AD_st%C5%AFl_3_w0495x.png"

---
