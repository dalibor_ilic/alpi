---
brand: data/brands/Artisan.md
name: 'NEVA BAROVÁ STOLIČKA  '
categories:
- Bar chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1604587676/forestry/ART_NEVA_barov%C3%A1_stoli%C4%8Dka_3_gmtil7.png"
- "/v1604587673/forestry/ART_NEVA_barov%C3%A1_stoli%C4%8Dka_2_ws7fmq.png"
- "/v1604587673/forestry/ART_NEVA_barov%C3%A1_stoli%C4%8Dka_4_ujqrfu.png"
- "/v1604587673/forestry/ART_NEVA_barov%C3%A1_stoli%C4%8Dka_1_csofuu.png"
dimensions: "<p><strong>Šířka: </strong>40<strong> </strong>cm - <strong>Délka: </strong>43
  cm - <strong>Výška: </strong>79 cm - <strong>Výška sedu:</strong> 79 cm - <strong>Hloubka
  sedu: </strong>38 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 38x40 cm</p><p>Průměr nohou: spodní část 3 cm, vrchní část 4,3 cm </p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 18 200 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604587834/forestry/ART_NEVA_barov%C3%A1_stoli%C4%8Dka_8_hzkhdi.png"
- "/v1604587833/forestry/ART_NEVA_barov%C3%A1_stoli%C4%8Dka_7_j32spk.png"
- "/v1604587833/forestry/ART_NEVA_barov%C3%A1_stoli%C4%8Dka_6_p6bpay.png"
- "/v1604587834/forestry/ART_NEVA_barov%C3%A1_stoli%C4%8Dka_5_qgkqx7.png"

---
