---
brand: data/brands/Woak.md
name: MALIN KOMODA
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606486534/forestry/WHO_MALIN_komoda_1_bv7hg2.png"
- "/v1606486534/forestry/WHO_MALIN_komoda_3_mhbu2v.png"
- "/v1606486534/forestry/WHO_MALIN_komoda_2_g3hj6d.png"
dimensions: "<p><strong>Šířka: </strong>90 cm - <strong>Délka: </strong>40 cm - <strong>Výška:</strong>
  160 cm</p>"
technical_info: "<p>Tato komoda je vyrobena z masivního dřeva (dubu nebo ořechu) a
  má olejovou úpravu.</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 67 700 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_highboard_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1BiE3zis91vsAYtkpQ0gao7OO_BVvn_UC/view?usp=sharing"
  title=""><strong>Stáhnout PDF</strong></a> </p>'
product_photos:
- "/v1606486807/forestry/WHO_MALIN_komoda_6_knz4zk.png"
- "/v1606486812/forestry/WHO_MALIN_komoda_5_ktuwqr.png"
- "/v1606486812/forestry/WHO_MALIN_komoda_4_iolw4c.png"

---
