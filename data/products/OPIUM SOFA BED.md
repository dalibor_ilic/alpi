---
brand: data/brands/Atlas.md
name: OPIUM SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Opium patří do kolekce funkčních rozkládacích
  pohovek vhodných pro každodenní spaní. Snadno použitelná a zároveň elegantní pohovka
  může být tou nejlepší volbou pro vybavení městského bydlení. Kombinace umírněné
  elegance a klasiky v podobě lineárně prošívaných polštářů, opěradel a područek dodává
  tomuto modelu jedinečný i univerzální vzhled.</p>'
product_images:
- "/v1643017654/forestry/ATL_OPIUM_sofa_bed_1_m3ku5i.png"
- "/v1643017654/forestry/ATL_OPIUM_sofa_bed_3_fjyytb.png"
- "/v1643017654/forestry/ATL_OPIUM_sofa_bed_5_ed4nnw.png"
- "/v1643017654/forestry/ATL_OPIUM_sofa_bed_6_cbatrz.png"
- "/v1643017654/forestry/ATL_OPIUM_sofa_bed_4_d01exu.png"
- "/v1643017654/forestry/ATL_OPIUM_sofa_bed_2_la7r4h.png"
- "/v1643017654/forestry/ATL_OPIUM_sofa_bed_10_eaawld.png"
- "/v1643017654/forestry/ATL_OPIUM_sofa_bed_11_ke74mu.png"
- "/v1643017654/forestry/ATL_OPIUM_sofa_bed_12_tbunhg.png"
dimensions: "<p>Dvojsed:</p><p><strong>Šířka: </strong>210 cm - <strong>Délka: </strong>108
  cm - <strong>Výška: </strong>81 cm - <strong>Výška sedu:</strong> 45 cm- <strong>Hloubka
  sedu:</strong> 59 cm</p><p>Trojsed:</p><p><strong>Šířka: </strong>250 cm - <strong>Délka:
  </strong>108 cm - <strong>Výška: </strong>81 cm - <strong>Výška sedu:</strong> 45
  cm- <strong>Hloubka sedu:</strong> 59 cm</p><p>Rohová souprava verze I.</p><p><strong>Šířka:
  </strong>327 cm - <strong>Délka: </strong>183 cm - <strong>Výška: </strong>81 cm
  - <strong>Výška sedu:</strong> 45 cm- <strong>Hloubka sedu:</strong> 59 cm</p><p>Rohová
  souprava verze II.</p><p><strong>Šířka: </strong>287 cm - <strong>Délka: </strong>183
  cm - <strong>Výška: </strong>81 cm - <strong>Výška sedu:</strong> 45 cm- <strong>Hloubka
  sedu:</strong> 59 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 a L 25/15</p><p><strong>Opěradlo: </strong>silikon</p><p><strong>Zkřížené
  elastické popruhy/bonneli pružiny</strong></p><p><strong>Nožičky: </strong>plastové
  nožičky 20 mm</p><p><strong>Možnosti:</strong> spací mechanismus </p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od:</strong> 82 600 Kč </p><p><strong>Cena v látce
  od: </strong>50 000 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/0e571fde96f8a34ce220459ed1a8b19df6dd4603.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643018212/forestry/ATL_OPIUM_sofa_bed_9_kuuqli.png"
- "/v1643018212/forestry/ATL_OPIUM_sofa_bed_8_ptifle.png"
- "/v1643018211/forestry/ATL_OPIUM_sofa_bed_7_sxdtl4.png"

---
