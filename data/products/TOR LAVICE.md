---
brand: data/brands/Artisan.md
name: 'TOR LAVICE '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605178888/forestry/ART_TOR_lavice_2_pioc8z.png"
- "/v1605178914/forestry/ART_TOR_lavice_7_af9y9k.png"
- "/v1605178888/forestry/ART_TOR_lavice_1_t007jw.png"
dimensions: "<p><strong>Šířka: </strong>160 - 220 cm - <strong>Délka: </strong>40
  cm - <strong>Výška: </strong>45 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální
  tloušťka desky: 8 cm</p><p>Průměr nohou: 8 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>32 000 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605179127/forestry/ART_TOR_lavice_6_cmctzh.png"
- "/v1605179125/forestry/ART_TOR_lavice_3_vw8hvg.png"
- "/v1605179127/forestry/ART_TOR_lavice_5_kpjz3b.png"
- "/v1605179127/forestry/ART_TOR_lavice_4_kafcdf.png"

---
