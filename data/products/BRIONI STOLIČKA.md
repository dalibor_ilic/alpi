---
brand: data/brands/Woak.md
name: 'BRIONI STOLIČKA '
categories:
- Chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Kompaktní, odolný, hravý. Toto je souhrn
  Brioni stoličky. K dispozici v černé nebo hnědé barvě, dubu nebo ořechu s materiálem
  sedáku, který může být v látce nebo kůži pro nejvyšší zákaznické potřeby.</p><p>Dřevěné
  části tohoto nábytku jsou ručně vybírány jedna část po druhé, aby dokonale pasovaly
  s ostatními materiály, které jsou zcela inovativní a recyklovatelné, jako je nezpracovaná
  kůže, čistý mramor nebo lité sklo s měnícími se barvami. Kombinace teplých a studených
  materiálů vede k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem
  je mužný, ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací
  pokoj nebo koutek na čtení.</p>'
product_images:
- "/v1607089343/forestry/WHO_BRIONI_stoli%C4%8Dka_1_dlbmj9.png"
- "/v1607089351/forestry/WHO_BRIONI_stoli%C4%8Dka_4_uobmwj.png"
dimensions: "<p><strong>Šířka: </strong>46 cm - <strong>Délka: </strong>50 cm - <strong>Výška:</strong>
  60 cm</p>"
technical_info: "<p>materiál: ořech nebo dub, látka, kůže</p><p>povrchová úprava:
  lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>17 700 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Brioni_stool_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607089457/forestry/WHO_BRIONI_stoli%C4%8Dka_3_i29v76.png"
- "/v1607089458/forestry/WHO_BRIONI_stoli%C4%8Dka_2_wz9gys.png"

---
