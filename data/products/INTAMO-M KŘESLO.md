---
brand: data/brands/Extraform.md
name: INTAMO-M KŘESLO
categories:
- Arm chair
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Intamo
  je křeslo, které je dynamické a lehké, se šikmými a tenkými liniemi. Zadní opěradlo
  je navrženo ve třech velikostech, které Vám umožní získat různá řešení pro Vaše
  vnitřní prostory.</p>'
product_images:
- "/v1644506093/forestry/EXT_INTAMO_k%C5%99eslo_7_hbqskf.png"
- "/v1644506101/forestry/EXT_INTAMO_k%C5%99eslo_4_rwy8j2.png"
- "/v1644506093/forestry/EXT_INTAMO_k%C5%99eslo_8_tyliu6.png"
- "/v1644506093/forestry/EXT_INTAMO_k%C5%99eslo_9_a6vlzs.png"
dimensions: "<p><strong>Šířka: </strong>69 cm - <strong>Délka:</strong> 78 cm - <strong>Výška:</strong>
  87 cm - <strong>Výška sedu:</strong> 44 cm - <strong>Hloubka sedu:</strong> 51 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, překližky o tloušťce
  18 mm.</p><p class="Paragraph SCXW113003605 BCX4">Sedák - ergonomická pěna RG 35,
  100 g tepelně pojené plsti, elastické popruhy.</p><p class="Paragraph SCXW113003605
  BCX4">Opěrka - ergonomická pěna RG 30, 100 g tepelně pojené plsti.</p><p class="Paragraph
  SCXW113003605 BCX4">Nohy z dubového dřeva, naolejované. Volitelně z bukového dřeva
  v šesti barvách.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>19 300 Kč </p>"
product_info: ''
product_photos: []

---
