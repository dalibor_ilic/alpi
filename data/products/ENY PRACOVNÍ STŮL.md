---
brand: data/brands/Artisan.md
name: 'ENY PRACOVNÍ STŮL '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645707766/forestry/Eny_pracovn%C3%AD_st%C5%AFlProduct_foto-3_mjg7ju.png"
- "/v1645707766/forestry/Eny_pracovn%C3%AD_st%C5%AFlProduct_foto-4_l8g9h2.png"
- "/v1645707766/forestry/Eny_pracovn%C3%AD_st%C5%AFlProduct_foto_s6uicb.png"
- "/v1645707766/forestry/Eny_pracovn%C3%AD_st%C5%AFlProduct_foto-2_z5a6ej.png"
dimensions: "<p><strong>Šířka: </strong>120/140/160/<strong> </strong>cm - <strong>Délka:
  </strong>65 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: ''
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>56 900 Kč</p>"
product_info: ''
product_photos:
- "/v1645707959/forestry/Eny_pracovn%C3%AD_st%C5%AFlZ_interi%C3%A9ru_btwr0b.png"
- "/v1645707960/forestry/Eny_pracovn%C3%AD_st%C5%AFlZ_interi%C3%A9ru-2_bgxhgd.png"

---
