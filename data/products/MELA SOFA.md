---
brand: data/brands/Artisan.md
name: 'MELA SOFA '
categories:
- Sofa
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605189013/forestry/ART_MELA_sofa_1_giithk.png"
dimensions: "<p><strong>Šířka: </strong>178<strong> </strong>cm - <strong>Délka: </strong>82
  cm - <strong>Výška: </strong>79 cm - <strong>Výška sedu: </strong>43 cm - <strong>Hloubka
  sedu: </strong>74 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a provedení v dvou kategoriích
  látek.</p><p>Průměr nohou: spodní část 3,5 cm, vrchní část 4 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 118 200 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605189263/forestry/ART_MELA_sofa_2_yof1tv.png"

---
