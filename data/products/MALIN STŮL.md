---
brand: data/brands/Woak.md
name: 'MALIN STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606743479/forestry/WHO_MALIN_st%C5%AFl_3_g9euhl.png"
- "/v1606743478/forestry/WHO_MALIN_st%C5%AFl_1_n7i246.png"
- "/v1606743479/forestry/WHO_MALIN_st%C5%AFl_2_qmfaou.png"
dimensions: "<p><strong>Šířka: </strong>160 - 240 cm - <strong>Délka: </strong>90
  - 100 cm - <strong>Výška:</strong> 75 cm</p>"
technical_info: "<p>Tento stůl je vyroben z masivního dřeva (dubu nebo ořechu) a má
  olejovou úpravu.</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 49 100 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_dining_table_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1ek6qOQtBsCNlTjppifGLl6IHKlVDtjF7/view?usp=sharing"
  title=""><strong>Stáhnout PDF</strong></a><strong> </strong></p>'
product_photos:
- "/v1606743796/forestry/WHO_MALIN_st%C5%AFl_6_dc2esz.png"
- "/v1606743801/forestry/WHO_MALIN_st%C5%AFl_4_qff4qj.png"
- "/v1606743800/forestry/WHO_MALIN_st%C5%AFl_5_qcvg29.png"

---
