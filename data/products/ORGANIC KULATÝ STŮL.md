---
brand: data/brands/Woak.md
name: 'ORGANIC KULATÝ STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p>'
product_images:
- "/v1607002184/forestry/WHO_ORGANIC_kulat%C3%BD_st%C5%AFl_1_l0uwbg.png"
- "/v1607002190/forestry/WHO_ORGANIC_kulat%C3%BD_st%C5%AFl_5_hsnp0j.png"
dimensions: "<p><strong>Šířka: </strong>130 cm - <strong>Délka: </strong>130 cm -
  <strong>Výška:</strong> 75 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>povrchová úprava: lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: ''
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Organic_dining_table_round_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607002376/forestry/WHO_ORGANIC_kulat%C3%BD_st%C5%AFl_4_gk9xnb.png"
- "/v1607002376/forestry/WHO_ORGANIC_kulat%C3%BD_st%C5%AFl_3_ftqfgq.png"
- "/v1607002376/forestry/WHO_ORGANIC_kulat%C3%BD_st%C5%AFl_2_xi6xf7.png"

---
