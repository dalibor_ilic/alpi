---
brand: data/brands/Artisan.md
name: 'WU ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644485574/forestry/Product_foto-59_nikppb.png"
- "/v1604669726/forestry/ART_WU_%C5%BEidle_8_t35qra.png"
- "/v1604669711/forestry/ART_WU_%C5%BEidle_1_de9vpj.png"
- "/v1644485571/forestry/Product_foto-57_xqwkks.png"
- "/v1644485576/forestry/Product_foto-58_dps1dm.png"
- "/v1644485575/forestry/Product_foto-60_uxtdbl.png"
- "/v1604669728/forestry/ART_WU_%C5%BEidle_10_i66iug.png"
dimensions: "<p><strong>Šířka: </strong>53<strong> </strong>cm - <strong>Délka: </strong>50
  cm - <strong>Výška: </strong>78 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 47x48 cm</p><p>Průměr nohou: spodní část 2,7 cm, vrchní část 3,7 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 19 900 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604669906/forestry/ART_WU_%C5%BEidle_5_qtykrd.png"
- "/v1604669906/forestry/ART_WU_%C5%BEidle_6_xuw763.png"
- "/v1604669906/forestry/ART_WU_%C5%BEidle_4_czwy89.png"
- "/v1604669906/forestry/ART_WU_%C5%BEidle_3_cngktr.png"
- "/v1604669905/forestry/ART_WU_%C5%BEidle_2_dta0wg.png"

---
