---
brand: data/brands/Artisan.md
name: 'LATUS MODULÁRNÍ SYSTÉM '
categories: []
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605191698/forestry/ART_LATUS_modul%C3%A1rn%C3%AD_syst%C3%A9m_1_ztevku.png"
dimensions: "<p><strong>Šířka: </strong>180 cm - <strong>Délka: </strong>55 cm - <strong>Výška:
  </strong>30<strong> </strong>cm</p>"
technical_info: "<p>Tloušťka komod a předních částí: 2 cm</p><p>Kabelový systém pro
  systematickou organizaci kabelů.</p><p>Jiné úpravy možné na vyžádání. </p><p></p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>6 500 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605191803/forestry/ART_LATUS_modul%C3%A1rn%C3%AD_syst%C3%A9m_3_bx5jqw.png"
- "/v1605191803/forestry/ART_LATUS_modul%C3%A1rn%C3%AD_syst%C3%A9m_2_axlbql.png"

---
