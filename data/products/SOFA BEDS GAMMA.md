---
brand: data/brands/Atlas.md
name: GAMMA SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Model, který je exkluzivní a funkční.
  Výkonný a stylový. Rafinovaný a minimalistický. Celá pohovka se rozkládá na lůžko
  s matrací určenou pro každodenní spaní. Pečlivě zpracované detaily dodávají nádech
  elegance. Dokonalé linie, ozdobné okraje a tenké područky se speciálně navrženými
  stehy. Mohutná a stylová pohovka Gamma zušlechtí každý prostor.</p><p></p>'
product_images:
- "/v1642244896/forestry/gamma1---d4ef6dbe0cfe3535277f48147d63d66d7a4d0c98_hli4jy.jpg"
- "/v1642244896/forestry/gamma2---14d217ce5731ee3d4b1bf436e23cd2a06e51d130_ydrfab.jpg"
- "/v1642244896/forestry/gamma3---e5d3ad803ad0eb0753fc1388b2caaa561c8cddd8_zqxuxc.jpg"
- "/v1642244896/forestry/gamma4---a1068189f4cb9a2e8572568dca85c0f49d157c8f_sqfznq.jpg"
- "/v1642244896/forestry/gamma5---6d36984852ad7b6923134b40e5280f4c6092e32d_azv8ju.jpg"
- "/v1642244896/forestry/gamma6---40d0c97cd2a9d4576ec5cd2059a4f239fe332312_czstoh.jpg"
- "/v1642244896/forestry/gamma7---44dde61e701aaf8f2c268abab5be699fa5607018_pjcdat.jpg"
dimensions: "<p><strong>Šířka: </strong>276 cm - <strong>Délka: </strong>227 cm -
  <strong>Výška: </strong>90 cm - <strong>Výška sedu:</strong> 46 cm - <strong>Hloubka
  sedu:</strong> 55 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36</p><p><strong>Opěradlo: </strong>pěna L 25/15 </p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky:</strong> kovové nohy lakované v barvě RAL
  / dřevěné nohy - lakované PU lakem, nebo v barvě RAL</p><p><strong>Možnosti:</strong>
  mechanismus rozkládací postele</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1x3qtWSfvDZh45kB8JxujwYgvlKTEPGhz/view?usp=sharing"
  title="Látky ELEGANT"><strong>ELEGANT</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>141 200 Kč</p><p><strong>Cena v látce
  od:</strong> 93 400 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/1799472a25ff58771ee772b2a353c8093823ea07.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642244243/forestry/ATL_GAMMA_sofa_bed_5_omafvd.png"
- "/v1642244243/forestry/ATL_GAMMA_sofa_bed_4_b6aphd.png"
- "/v1642244244/forestry/ATL_GAMMA_sofa_bed_3_xm5h4v.png"
- "/v1642244242/forestry/ATL_GAMMA_sofa_bed_2_vlrr6p.png"

---
