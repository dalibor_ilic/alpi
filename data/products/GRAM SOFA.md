---
brand: data/brands/Atlas.md
name: GRAM SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Gram odráží sofistikovaný design zabalený
  do harmonických rozměrů, který se hodí do všech prostor. Úžasné pohodlí zajišťuje
  peří a silikonové polstrování v kombinaci s kvalitní pěnou. Tato pohovka poskytne
  nepopsatelné pohodlí každému, kdo se těší z jejího objetí. Pečlivě zpracovaný design
  a detaily šití ji činí neuvěřitelně přitažlivou.</p>'
product_images:
- "/v1642245575/forestry/ATL_GRAM_sofa_1_c2k6fj.png"
- "/v1642245575/forestry/ATL_GRAM_sofa_11_m8eftt.png"
- "/v1642245575/forestry/ATL_GRAM_sofa_13_sitl6l.png"
- "/v1642245575/forestry/ATL_GRAM_sofa_15_ty5cdg.png"
- "/v1642245575/forestry/ATL_GRAM_sofa_14_ukuyx4.png"
- "/v1642245575/forestry/ATL_GRAM_sofa_12_wbi502.png"
dimensions: "<p>Dvojsed:</p><p><strong>Šířka: </strong>184 cm - <strong>Délka: </strong>98
  cm - <strong>Výška: </strong>88 cm  </p><p>Trojsed:</p><p><strong>Šířka: </strong>214
  cm - <strong>Délka: </strong>98 cm - <strong>Výška: </strong>88 cm </p><p></p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36</p><p><strong>Opěradlo: </strong>HR 35/30 se silikonem</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Dřevěné nožičky:</strong> mořené - lakované PU lakem,
  nebo v barvě RAL</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>60 500 Kč </p><p><strong>Cena v látce
  od: </strong>41 600 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/99f5dd3ab9d4462fde9a0d5a6e4891d4aba01460.pdf"
  title="">Stáhnout PDF</a></p><p></p>'
product_photos:
- "/v1642246614/forestry/ATL_GRAM_sofa_8_i1roxi.png"
- "/v1642246613/forestry/ATL_GRAM_sofa_7_ejgupp.png"
- "/v1642246614/forestry/ATL_GRAM_sofa_6_ma6n4o.png"
- "/v1642246615/forestry/ATL_GRAM_sofa_2_iu3sut.png"
- "/v1642246615/forestry/ATL_GRAM_sofa_9_czoh8m.png"

---
