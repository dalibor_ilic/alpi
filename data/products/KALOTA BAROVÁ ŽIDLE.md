---
brand: data/brands/Artisan.md
name: KALOTA BAROVÁ ŽIDLE
categories:
- Bar chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1604586051/forestry/ART_KALOTA_barov%C3%A1_%C5%BEidle_1_ey3eiv.png"
dimensions: "<p><strong>Šířka: </strong>49<strong> </strong>cm - <strong>Délka: </strong>49
  cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 34x34 cm </p><p>Průměr nohou: spodní část 2,5 cm, vrchní část 8 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY </strong></a>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> |<a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong> DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>21 700 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604586200/forestry/ART_KALOTA_barov%C3%A1_%C5%BEidle_2_vjixhw.png"

---
