---
brand: data/brands/Woak.md
name: 'MALIN KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606747136/forestry/WHO_MALIN_Konferen%C4%8Dn%C3%AD_stolek_4_q6xeqk.png"
- "/v1606747130/forestry/WHO_MALIN_Konferen%C4%8Dn%C3%AD_stolek_1_oeq7rb.png"
dimensions: "<p><strong>Šířka: </strong>84 - 140 cm - <strong>Délka: </strong>45 -
  75 cm - <strong>Výška:</strong> 30,5 - 38 cm</p>"
technical_info: "<p>Tento konferenční stolek je vyroben z masivního dřeva (dubu nebo
  ořechu), má olejovou úpravu a dřevěné nožičky.</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 16 200 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_coffee_tables_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1RIRDWETEAfdixAq-DZCoFFu_gOdx1kec/view?usp=sharing"
  title=""><strong>Stáhnout PDF </strong></a></p>'
product_photos:
- "/v1606747403/forestry/WHO_MALIN_Konferen%C4%8Dn%C3%AD_stolek_3_ciqs5r.png"
- "/v1606747405/forestry/WHO_MALIN_Konferen%C4%8Dn%C3%AD_stolek_2_jtbjed.png"

---
