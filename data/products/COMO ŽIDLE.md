---
brand: data/brands/Atlas.md
name: COMO ŽIDLE
categories:
- Chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Toto křeslo může být celé čalouněné jedním
  typem látky, ale velmi zajímavě vypadá, když je čalouněné kombinací dvou látek.
  Je ergonomicky dobře navržené a pohodlné, a zároveň odráží vysoké estetické hodnoty.
  Díky menším rozměrům a neobvyklým krátkým područkám se židle Como skvěle hodí do
  různých prostor.</p>'
product_images:
- "/v1643281542/forestry/ATL_COMO_%C5%BEidle_1_vbavrf.png"
dimensions: "<p><strong>Šířka: </strong>48 cm - <strong>Délka: </strong>61 cm - <strong>Výška:
  </strong>87 cm - <strong>Výška sedu:</strong> 48 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Podpěra:</strong>
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 </p><p><strong>Opěradlo:</strong> N pěna 25/38</p><p><strong>Područky:</strong>
  panely, čalounění</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:
  </strong>barevné ve spodní části</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>11 300 Kč</p><p><strong>Cena v látce od:
  </strong>8 400 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/65b9b20f78e6b547ee4f81a9def5dd798c8f5c22.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643281691/forestry/ATL_COMO_%C5%BEidle_3_elwkya.png"
- "/v1643281690/forestry/ATL_COMO_%C5%BEidle_2_ixxkra.png"

---
