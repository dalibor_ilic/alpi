---
brand: data/brands/Artisan.md
name: ALBA STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605535371/forestry/ART_ALBA_st%C5%AFl_2_lwob0z.png"
- "/v1605535371/forestry/ART_ALBA_st%C5%AFl_1_g9dq2b.png"
dimensions: "<p><strong>Šířka: </strong>160/180/200/240 cm - <strong>Délka: </strong>90/95/100
  cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální
  tloušťka desky: 4 cm</p><p>Průměr nohou: spodní část 5,5 cm, vrchní část 6,8 cm
  </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 47 500 CZK</p>"
product_info: "<p></p>"
product_photos:
- "/v1605535698/forestry/ART_ALBA_st%C5%AFl_3_f3haus.png"

---
