---
brand: data/brands/Artisan.md
name: LUC STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605012270/forestry/ART_LUC_ov%C3%A1ln%C3%BD_st%C5%AFl_6_mgovdw.png"
- "/v1605012270/forestry/ART_LUC_ov%C3%A1ln%C3%BD_st%C5%AFl_5_fxclno.png"
- "/v1605012270/forestry/ART_LUC_ov%C3%A1ln%C3%BD_st%C5%AFl_2_fdgi3g.png"
- "/v1605012272/forestry/ART_LUC_ov%C3%A1ln%C3%BD_st%C5%AFl_3_muwe0v.png"
dimensions: "<p><strong>Šířka: </strong>165 - 265 cm - <strong>Délka: </strong>110
  -<strong> </strong>120 cm - <strong>Výška: </strong>74 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální
  tloušťka desky: 0,8 cm</p><p>Průměr nohou: spodní část 4x5 cm, vrchní část 4x8 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>75 100 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605012346/forestry/ART_LUC_ov%C3%A1ln%C3%BD_st%C5%AFl_7_dxqgxc.png"
- "/v1605012329/forestry/ART_LUC_ov%C3%A1ln%C3%BD_st%C5%AFl_4_u2cs2y.png"

---
