---
brand: data/brands/Woak.md
name: MALIN PRACOVNÍ STŮL
categories:
- Accessories
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606487545/forestry/WHO_MALIN_pracovn%C3%AD_st%C5%AFl_4_h0vhqu.png"
- "/v1606487537/forestry/WHO_MALIN_pracovn%C3%AD_st%C5%AFl_1_wna2qk.png"
dimensions: "<p><strong>Šířka: </strong>120-140 cm - <strong>Délka: </strong>60 cm
  - <strong>Výška:</strong> 75 cm</p>"
technical_info: "<p>Tento stůl je vyroben z masivního dřeva (dubu nebo ořechu) a má
  olejovou úpravu.</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 39 800 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_working_desk_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1ZPBpbDupBxJ4ULQSF8GWUgm6S-4EK93P/view?usp=sharing"
  title=""><strong>Stáhnout PDF</strong></a> </p>'
product_photos:
- "/v1606487710/forestry/WHO_MALIN_pracovn%C3%AD_st%C5%AFl_5_grt2wf.png"
- "/v1606487713/forestry/WHO_MALIN_pracovn%C3%AD_st%C5%AFl_3_hifwvi.png"
- "/v1606487713/forestry/WHO_MALIN_pracovn%C3%AD_st%C5%AFl_2_wbj4cv.png"

---
