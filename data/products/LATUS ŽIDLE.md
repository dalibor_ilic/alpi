---
brand: data/brands/Artisan.md
name: 'LATUS ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644249070/forestry/Product_foto-22_uq6abp.png"
- "/v1644249071/forestry/Product_foto-23_ap44uk.png"
- "/v1644249070/forestry/Product_foto-24_ubpaud.png"
dimensions: "<p><strong>Šířka: </strong>50<strong> </strong>cm - <strong>Délka: </strong>47
  cm - <strong>Výška: </strong>79 cm - <strong>Výška sedu: </strong>48 cm - <strong>Hloubka
  sedu: </strong>45 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 45x40 cm</p><p>Tlouštka předních nohou: spodní část 3 cm, vrchní část 3,5
  cm</p><p>Tloušťka zadních nohou: spodní část 2 cm, vrchní část 3,5 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 20 600 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604586613/forestry/ART_LATUS_%C5%BEidle_2_ap6ryt.png"
- "/v1604586614/forestry/ART_LATUS_%C5%BEidle_5_l3yic1.png"
- "/v1604586614/forestry/ART_LATUS_%C5%BEidle_3_jpt6s0.png"
- "/v1604586614/forestry/ART_LATUS_%C5%BEidle_4_ocyokc.png"

---
