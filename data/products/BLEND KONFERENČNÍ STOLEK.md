---
brand: data/brands/Artisan.md
name: 'BLEND KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645437004/forestry/Product_foto-2konferen%C4%8Dn%C3%AD_stolek_qu5lfm.png"
- "/v1645437004/forestry/Product_fotokonferen%C4%8Dn%C3%AD_stolek_e2pnzn.png"
dimensions: "<p><strong>Šířka: </strong>70 cm - <strong>Délka: </strong>70/90 cm<strong>
  </strong> - <strong>Výška: </strong>30/40 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka spodní a horní desky: 2 cm</p><p>Vizuální
  tloušťka: 3 cm</p><p>Průměr nohou: spodní část 5x5 cm, horní část 5x5 cm</p><p>Jiné
  úpravy a ceny na vyžádání. </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 36 700 Kč</p>"
product_info: ''
product_photos:
- "/v1645437189/forestry/Z_interi%C3%A9ru-2konferen%C4%8Dn%C3%AD_stolek_aizih5.png"
- "/v1645437188/forestry/Z_interi%C3%A9rukonferen%C4%8Dn%C3%AD_stolek_dd9axe.png"

---
