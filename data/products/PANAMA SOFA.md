---
brand: data/brands/Atlas.md
name: PANAMA SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Kombinace neobvyklého designu a pohodlí
  vás nenechá lhostejnými. Panama je kus nábytku, který váš prostor dotvoří, zdokonalí,
  zmodernizuje a přitáhne pozornost každého. Vybrat si do svého prostoru nábytek Panama
  je správný krok, protože bude odpovídat všem vašim požadavkům jak po estetické,
  tak po funkční stránce.</p>'
product_images:
- "/v1643018503/forestry/ATL_PANAMA_sofa_bed_7_zx0rae.png"
- "/v1643018503/forestry/ATL_PANAMA_sofa_bed_1_wmsn0q.png"
- "/v1643018503/forestry/ATL_PANAMA_sofa_bed_6_yjjz3h.png"
dimensions: "<p>Dvojsed:</p><p><strong>Šířka: </strong>201 cm - <strong>Délka: </strong>92
  cm - <strong>Výška: </strong>76 cm - <strong>Výška sedu:</strong> 49 cm- <strong>Hloubka
  sedu:</strong> 60 cm</p><p>Trojsed I.</p><p><strong>Šířka: </strong>240 cm - <strong>Délka:
  </strong>92 cm - <strong>Výška: </strong>76 cm - <strong>Výška sedu:</strong> 49
  cm- <strong>Hloubka sedu:</strong> 60 cm</p><p>Trojsed II.</p><p><strong>Šířka:
  </strong>228 cm - <strong>Délka: </strong>92 cm - <strong>Výška: </strong>80 cm
  - <strong>Výška sedu:</strong> 46 cm- <strong>Hloubka sedu:</strong> 60 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36, HR pěna 35/30 ve vatovém potahu</p><p><strong>Opěradlo: </strong>pěna
  N 25/38</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky: </strong>kovové
  nohy v barvě RAL</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>54 800 Kč</p><p><strong>Cena v látce od:
  </strong>40 200 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/bcd851fdeb4cbaac391ec9bc1655593a4dcb8d3d.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643019231/forestry/ATL_PANAMA_sofa_bed_5_op5b8o.png"
- "/v1643019230/forestry/ATL_PANAMA_sofa_bed_4_aowhza.png"
- "/v1643019231/forestry/ATL_PANAMA_sofa_bed_3_bk8wmx.png"
- "/v1643019231/forestry/ATL_PANAMA_sofa_bed_2_dzjkus.png"

---
