---
brand: data/brands/Artisan.md
name: INVITO LAVICE
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605177307/forestry/ART_INVITO_lavice_3_ppuzhg.png"
- "/v1605177306/forestry/ART_INVITO_lavice_2_ut2mez.png"
- "/v1605177306/forestry/ART_INVITO_lavice_1_c7wvb4.png"
dimensions: "<p><strong>Šířka: </strong>160/180/200 cm - <strong>Délka: </strong>40
  cm - <strong>Výška: </strong>45 cm</p>"
technical_info: "<p>Tloušťka desky: 2 cm</p><p>Vizuální tloušťka desky: 7,5 cm</p><p>Průměr
  nohou: spodní část 5,8x4 cm, vrchní část 11x9,7 cm</p><p>Jiné rozměry možné na vyžádání.
  </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 43 300 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605177564/forestry/ART_INVITO_lavice_5_kfxsrt.png"
- "/v1605177564/forestry/ART_INVITO_lavice_4_znh1tz.png"

---
