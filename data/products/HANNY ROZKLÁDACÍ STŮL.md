---
brand: data/brands/Artisan.md
name: HANNY ROZKLÁDACÍ STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605537357/forestry/ART_HANNY_rozkl%C3%A1dac%C3%AD_st%C5%AFl_3_d1ameb.png"
- "/v1605537357/forestry/ART_HANNY_rozkl%C3%A1dac%C3%AD_st%C5%AFl_1_tkh8yb.png"
- "/v1605537357/forestry/ART_HANNY_rozkl%C3%A1dac%C3%AD_st%C5%AFl_2_h4lpaj.png"
dimensions: "<p><strong>Šířka: </strong>120-320 cm - <strong>Délka: </strong>90 cm
  - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 7 cm</p><p>Vizuální
  tloušťka desky: 5 cm</p><p>Průměr nohou: spodní část 5x4 cm, vrchní část 11x8 cm</p><p>Rozměry
  rozšíření: 1x50 a 2x50 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 89 300 Kč</p>"
product_info: "<p></p>"
product_photos: []

---
