---
brand: data/brands/Artisan.md
name: 'PASCAL KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645444895/forestry/Product_fotokonferen%C4%8Dn%C3%AD_stolek_fvnoin.png"
dimensions: "<p><strong>Průměr: </strong>100/120/140 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka desky: 2 cm </p><p>Vizuální tloušťka
  desky: 2 cm </p><p>Průměr nohou: spodní část 4,5 cm, vrchní část 5 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 57 400 Kč</p>"
product_info: ''
product_photos:
- "/v1645445042/forestry/Z_interi%C3%A9rukonferen%C4%8Dn%C3%AD_stolek_ljov4q.png"
- "/v1645445042/forestry/Z_interi%C3%A9ru-2konferen%C4%8Dn%C3%AD_stolek_rjczva.png"

---
