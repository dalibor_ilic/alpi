---
brand: data/brands/Woak.md
name: 'BRIONI ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Zdánlivě jednoduchý objekt, jako je
  židle, se stává centrem pozornosti zaměřené na získání kombinace lehkosti, odolnosti
  a všestrannosti. Klíčové faktory spočívají nejen v designu, ale také ve vybírání
  materiálů.</p><p>Dřevěné části tohoto nábytku jsou ručně vybírány jedna část po
  druhé, aby dokonale pasovaly s ostatními materiály, které jsou zcela inovativní
  a recyklovatelné, jako je nezpracovaná kůže, čistý mramor nebo lité sklo s měnícími
  se barvami. Kombinace teplých a studených materiálů vede k dokonalé rovnováze nejen
  na pohled, ale i na dotek. Protiklad forem je mužný, ale i ladný a výsledkem je
  harmonická směsice, ať už se jedná o obývací pokoj nebo koutek na čtení.</p>'
product_images:
- "/v1607090699/forestry/WHO_BRIONI_%C5%BEidle_9_qpclsc.png"
- "/v1607090699/forestry/WHO_BRIONI_%C5%BEidle_8_l2q7hg.png"
- "/v1607090690/forestry/WHO_BRIONI_%C5%BEidle_1_sofiht.png"
- "/v1607090681/forestry/WHO_BRIONI_%C5%BEidle_4_vahcna.png"
- "/v1607090679/forestry/WHO_BRIONI_%C5%BEidle_3_gwdkhk.png"
- "/v1607090678/forestry/WHO_BRIONI_%C5%BEidle_2_oieqne.png"
dimensions: "<p><strong>Šířka: </strong>49 cm - <strong>Délka: </strong>53 cm - <strong>Výška:</strong>
  75 cm</p>"
technical_info: "<p>materiál: ořech nebo dub, látka, kůže</p><p>povrchová úprava:
  lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>21 100 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Brioni_chair_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607090788/forestry/WHO_BRIONI_%C5%BEidle_7_gbivjw.png"
- "/v1607090794/forestry/WHO_BRIONI_%C5%BEidle_6_jmkyey.png"
- "/v1607090791/forestry/WHO_BRIONI_%C5%BEidle_5_oypozy.png"

---
