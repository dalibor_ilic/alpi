---
brand: data/brands/Atlas.md
name: NOA KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Ztělesnění
  módy a detailu, smysl pro svěžest, který Vám umožní vést luxusní život se stylem.</p>'
product_images:
- "/v1648837376/forestry/Product_foto_wp8av5.png"
- "/v1648837376/forestry/Product_foto-6_eitfo8.png"
- "/v1648837376/forestry/Product_foto-5_vdltm9.png"
- "/v1648837376/forestry/Product_foto-4_arggbi.png"
- "/v1648837376/forestry/Product_foto-3_ymexcz.png"
- "/v1648837376/forestry/Product_foto-2_jzqtch.png"
dimensions: "<p><strong>Šířka: </strong>61<strong> </strong>cm - <strong>Délka: </strong>80
  cm - <strong>Výška: </strong>81 cm - <strong>Výška sedu:</strong> 47 cm - <strong>Hloubka
  sedu:</strong> 53 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, kůže</p><p><strong>Sedák: </strong>vysoce
  elastická polyuretanová HR pěna 35/36</p><p><strong>Opěradlo: </strong>vysoce elastická
  polyuretanová HR pěna 35/36</p><p><strong>Přímé vlnité pružiny</strong></p><p><strong>Nožičky:
  </strong>dřevěný rám - lazura, polyuretanový lak, krycí polyuretanová barva dle
  vzorníku RAL</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p><p>Kůže:
  <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"><strong>CAT
  300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"><strong>DUB</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/2d20cbc8ad50a4d08b877cd425601bd912fa662b.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1648837618/forestry/Z_interi%C3%A9ru-4_azf3gh.png"
- "/v1648837618/forestry/Z_interi%C3%A9ru-3_bdgxsn.png"
- "/v1648837627/forestry/Z_interi%C3%A9ru-2_z54idj.png"

---
