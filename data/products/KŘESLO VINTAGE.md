---
brand: data/brands/Atlas.md
name: VINTAGE KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Minimalistický přístup k designu. Správné
  linie a redukované tvary jsou tím, co dělá tuto židli přitažlivou. Má malé rozměry
  a snadno se vejde do interiéru malých rozměrů.</p>'
product_images:
- "/v1643202201/forestry/ATL_VINTAGE_k%C5%99eslo_1_lplbos.png"
- "/v1643202381/forestry/ATL_VINTAGE_k%C5%99eslo_3_imbzdd.png"
- "/v1643202381/forestry/ATL_VINTAGE_k%C5%99eslo_4_cckcct.png"
- "/v1643202382/forestry/ATL_VINTAGE_k%C5%99eslo_5_saiuyk.png"
dimensions: "<p><strong>Šířka: </strong>66 cm - <strong>Délka: </strong>85 cm - <strong>Výška:
  </strong>70 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> HR pěna 35/36</p><p><strong>Opěradlo:</strong>
  N pěna 25/38</p><p><strong>Područky:</strong> pěna</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:
  </strong>plastové nožičky 20 mm</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>18 600 Kč</p><p><strong>Cena v látce od:
  </strong>11 900 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/dfec34e9e3b7fdc55fe4e8020c9b37a10e9bc5c1.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643202363/forestry/ATL_VINTAGE_k%C5%99eslo_2_amz2zp.png"

---
