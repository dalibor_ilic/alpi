---
brand: data/brands/Atlas.md
name: BARCELONA SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Dokonale vyvážená kombinace pohodlí a
  funkčnosti je něco, co dělá tuto sadu nábytku tak výjimečnou. Rozkládací pohovka
  Barcelona je navržena tak, aby poskytovala maximální pohodlí a zároveň si zachovala
  dávku sofistikovanosti ve svém vzhledu. Na své si přijdou jak příznivci klasiky,
  tak i ti, kteří mají rádi moderní design. Snadno zapadne do každého typu domácnosti.</p>'
product_images:
- "/v1642240010/forestry/ATL_BARCELONA_sofa_bed_1_ijp8du.png"
- "/v1642240010/forestry/ATL_BARCELONA_sofa_bed_4_jchs1d.png"
dimensions: "<p><strong>Šířka: </strong>276 cm - <strong>Délka: </strong>176 cm -
  <strong>Výška: </strong>98 cm - <strong>Výška sedu:</strong> 42 cm - <strong>Hloubka
  sedu:</strong> 58 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah: </strong>látka, nebo kůže</p><p><strong>Sedák: </strong>HR
  pěna 35/36 ve vatovém potahu</p><p><strong>Opěradlo: </strong>silikon ve vatovém
  potahu</p><p><strong>Elastické popruhy a vlnové pružiny</strong></p><p><strong>Dřevěné
  nožičky:</strong> lakované PU lakem, nebo v barvě RAL</p><p><strong>Možnosti:</strong>
  zvedací mechanismus</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>109 100 Kč </p><p><strong>Cena v látce
  od: </strong>69 700 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/b57f0463e86886b5ac2ac3823cc328a0dc171923.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642240305/forestry/ATL_BARCELONA_sofa_bed_3_mw6mbu.png"
- "/v1642240305/forestry/ATL_BARCELONA_sofa_bed_2_lkgo25.png"

---
