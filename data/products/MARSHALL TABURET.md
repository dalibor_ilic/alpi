---
brand: data/brands/Woak.md
name: 'MARSHALL TABURET '
categories:
- Accessories
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Dřevěné části tohoto nábytku jsou ručně
  vybírány jedna část po druhé, aby dokonale pasovaly s ostatními materiály, které
  jsou zcela inovativní a recyklovatelné, jako je nezpracovaná kůže, čistý mramor
  nebo lité sklo s měnícími se barvami. Kombinace teplých a studených materiálů vede
  k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem je mužný,
  ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací pokoj nebo
  koutek na čtení.</p>'
product_images:
- "/v1606896729/forestry/WHO_MARSHALL_taburet_1_khlq2a.png"
dimensions: "<p><strong>Šířka: </strong>55 cm - <strong>Délka: </strong>41,5 cm -
  <strong>Výška:</strong> 40 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>kůže: kašmír</p><p>povrchová úprava:
  lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 23 000 Kč  </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Marshall_tabouret_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606896896/forestry/WHO_MARSHALL_taburet_3_blvksd.png"
- "/v1606896896/forestry/WHO_MARSHALL_taburet_2_ewvsn8.png"

---
