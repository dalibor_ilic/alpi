---
brand: data/brands/Artisan.md
name: 'INVITO SKŘÍŇ '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605188571/forestry/ART_INVITO_sk%C5%99%C3%AD%C5%88_5_ncytz9.png"
- "/v1605188562/forestry/ART_INVITO_sk%C5%99%C3%AD%C5%88_1_ducrzm.png"
- "/v1605188571/forestry/ART_INVITO_sk%C5%99%C3%AD%C5%88_6_irz4in.png"
- "/v1605188571/forestry/ART_INVITO_sk%C5%99%C3%AD%C5%88_7_jfcuhd.png"
dimensions: "<p><strong>Šířka: </strong>120/240<strong> </strong>cm - <strong>Délka:
  </strong>42 cm - <strong>Výška: </strong>148 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka rámu: 2 cm</p><p>Tloušťka
  přední části: 2,2 cm</p><p>Tloušťka zadní části: 1,5 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1DKp-AqR_-rESWlFNEpcr1prxEig5SpKq/view?usp=sharing
  " title=""><strong>BAREVNÉ DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 81 000 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605188761/forestry/ART_INVITO_sk%C5%99%C3%AD%C5%88_4_jry80z.png"
- "/v1605188761/forestry/ART_INVITO_sk%C5%99%C3%AD%C5%88_3_l83tii.png"
- "/v1605188761/forestry/ART_INVITO_sk%C5%99%C3%AD%C5%88_2_ibd7pr.png"

---
