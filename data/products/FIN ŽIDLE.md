---
brand: data/brands/Artisan.md
name: 'FIN ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644247883/forestry/Product_foto-9_thuiar.png"
- "/v1644247884/forestry/Product_foto-10_um3wur.png"
dimensions: "<p><strong>Šířka: </strong>44<strong> </strong>cm - <strong>Délka:</strong>
  58 cm - <strong>Výška:</strong> 80 cm - <strong>Výška sedu:</strong> 45 cm - <strong>Hloubka
  sedu:</strong> 44 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 44x44 cm</p><p>Průměr nohou: spodní část 3,1 cm, vrchní část 3,7 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY </strong></a>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title="KŮŽE"><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><br></p>
price: "<p><strong>Cena od:</strong> 15 800 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604480664/forestry/ART_FIN_%C5%BEidle_4_h12vq9.png"
- "/v1604480664/forestry/ART_FIN_%C5%BEidle_2_o5i7tt.png"
- "/v1604480665/forestry/ART_FIN_%C5%BEidle_5_c2nkpd.png"
- "/v1604480664/forestry/ART_FIN_%C5%BEidle_3_ytig4t.png"

---
