---
brand: data/brands/Atlas.md
name: OPERA STŮL
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>S tímto jídelním stolem vnesete do svého
  prostoru moderní styl. Stůl Opera má kulatou desku s hladkou hranou s obráceným
  zkosením, kterou lze nabarvit v barvách RAL. Design dotváří kovová podnož, jejíž
  součástí jsou nohy zkřížené do tvaru písmene X. Díky svým rozměrům je vhodný do
  menších bytů.</p>'
product_images:
- "/v1643279760/forestry/ATL_OPERA_st%C5%AFl_1_m17w8b.png"
dimensions: "<p><strong>Šířka: </strong>120 cm - <strong>Délka: </strong>120 cm -
  <strong>Výška: </strong>70 cm</p>"
technical_info: "<p><strong>Materiál:</strong> dub, MDF 40 mm</p><p><strong>Nožičky:
  </strong>kovové</p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>22 400 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/eb55e98adfca29a7a247a9f14b467f15e5c3e329.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643279873/forestry/ATL_OPERA_st%C5%AFl_2_joupmn.png"

---
