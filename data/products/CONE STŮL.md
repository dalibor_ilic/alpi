---
brand: data/brands/Atlas.md
name: CONE STŮL
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Klasický styl stolní desky kontrastuje
  s moderním vzhledem podstavce tvořeného dvěma kovovými nohami zkříženými k sobě.
  Stůl Cone má výrazné a přesné linie a skvěle se hodí do moderně zařízených jídelen.
  Deska vyrobená z dubové dýhované MDF desky dodává tomuto stolu elegantní vzhled.</p>'
product_images:
- "/v1643278854/forestry/ATL_CONE_st%C5%AFl_1_y42faq.png"
dimensions: "<p><strong>Šířka: </strong>160 cm - <strong>Délka: </strong>90 cm - <strong>Výška:
  </strong>74 cm</p>"
technical_info: "<p><strong>Materiál:</strong> dub, MDF 20 mm</p><p><strong>Nožičky:
  </strong>kovové</p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>17 800 Kč</p>"
product_info: ''
product_photos:
- "/v1643278968/forestry/ATL_CONE_st%C5%AFl_2_sbblzj.png"

---
