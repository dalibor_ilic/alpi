---
brand: data/brands/Atlas.md
name: CT-19 KONFERENČNÍ STOLEK
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Ať
  už se jedná o konferenční stolek v obývacím pokoji, předsíni nebo salónu, tento
  moderní designový stolek bude vypadat perfektně na jakémkoli místě. </p>'
product_images:
- "/v1648839555/forestry/Product_foto_kemnyz.png"
- "/v1648839555/forestry/Product_foto-4_r3mbdv.png"
- "/v1648839555/forestry/Product_foto-3_hlk2mz.png"
- "/v1648839555/forestry/Product_foto-2_f6a00s.png"
dimensions: "<p>S - <strong>Průměr desky: </strong>47 cm - <strong>Výška: </strong>47
  cm </p><p>M - <strong>Průměr desky: </strong>65 cm - <strong>Výška: </strong>35
  cm </p><p>L - <strong>Průměr desky: </strong>95 cm - <strong>Výška: </strong>42
  cm </p><p></p>"
technical_info: "<p><strong>Materiál desky: </strong>mramor</p><p><strong>Nožičky:
  </strong>masivní dřevo/RAL</p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"><strong>DUB</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/346ccd4acbe0e87b4e7dcbed3e87b1b786b925d2.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1648839915/forestry/Z_interi%C3%A9ru-2_jri2rh.png"
- "/v1648839919/forestry/Z_interi%C3%A9ru-3_fbic7u.png"
- "/v1648839898/forestry/Z_interi%C3%A9ru_viee1c.png"

---
