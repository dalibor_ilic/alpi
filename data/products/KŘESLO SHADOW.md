---
brand: data/brands/Atlas.md
name: SHADOW KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Shadow je elegantní multifunkční křeslo.  V
  kombinaci dvou kusů se z něj může stát pohovka. Kovové nohy mu dodávají klasickou
  a moderní identitu. Tento model je určen do prostor, kde elegance slouží odpočinku.</p>'
product_images:
- "/v1643201184/forestry/ATL_SHADOW_k%C5%99eslo_3_iqmat2.png"
- "/v1643201185/forestry/ATL_SHADOW_k%C5%99eslo_1_fnxew0.png"
- "/v1643201179/forestry/ATL_SHADOW_k%C5%99eslo_2_jssb8r.png"
dimensions: "<p><strong>Šířka: </strong>70 cm - <strong>Délka: </strong>70 cm - <strong>Výška:
  </strong>73 cm - <strong>Výška sedu:</strong> 41 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> HR pěna 35/36 </p><p><strong>Opěradlo:</strong>
  N pěna 25/38</p><p><strong>Područky:</strong> N pěna 25/38</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>INOX</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>23 800 Kč </p><p><strong>Cena v látce
  od: </strong>19 400 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/119394a39c594d1656323393af5cb438f28915af.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643201489/forestry/ATL_SHADOW_k%C5%99eslo_5_lxtlhu.png"
- "/v1643201486/forestry/ATL_SHADOW_k%C5%99eslo_4_toxnec.png"

---
