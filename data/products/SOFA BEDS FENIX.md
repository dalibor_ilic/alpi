---
brand: data/brands/Atlas.md
name: FENIX SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Model s inovativním mechanismem opěrky
  hlavy pro výjimečný komfort. Díky speciálně navrženým kovovým nožičkám vypadá pohovka
  Fenix sofistikovaně, sedáky zaručují vynikající pohodlí. Vytvořte si pohodlí, nastavte
  si opěrky hlavy do jedné z pěti možných poloh a užívejte si kouzlo, které tato pohovka
  poskytuje.</p>'
product_images:
- "/v1642242631/forestry/ATL_FENIX_sofa_1_rqjzpk.png"
- "/v1642242632/forestry/ATL_FENIX_sofa_7_soi9xm.png"
- "/v1642242632/forestry/ATL_FENIX_sofa_9_m8kvpj.png"
- "/v1642242632/forestry/ATL_FENIX_sofa_8_fywyhh.png"
- "/v1642242632/forestry/ATL_FENIX_sofa_10_yiww3z.png"
- "/v1642242632/forestry/ATL_FENIX_sofa_6_kwkir9.png"
dimensions: "<p><strong>Šířka: </strong>315 cm - <strong>Délka: </strong>249 cm -
  <strong>Výška: </strong>75/90 cm - <strong>Výška sedu:</strong> 43 cm - <strong>Hloubka
  sedu:</strong> 62 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/30</p><p><strong>Opěradlo: </strong>L 25/15 a pěna N 25/38</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Kovové nožičky:</strong> kovové-inox nožičky</p><p><strong>Možnosti:</strong>
  opěrky hlavy s mechanismem</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1x3qtWSfvDZh45kB8JxujwYgvlKTEPGhz/view?usp=sharing"
  title="Látky ELEGANT"><strong>ELEGANT</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>143 600 Kč </p><p><strong>Cena v látce
  od: </strong>99 400 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/4c65ed76fe2c13fe2be3f2e71236d59f52bf2dbe.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642242975/forestry/ATL_FENIX_sofa_5_ec4wdd.png"
- "/v1642242973/forestry/ATL_FENIX_sofa_4_qrbwzk.png"
- "/v1642242974/forestry/ATL_FENIX_sofa_3_slx9zw.png"
- "/v1642242975/forestry/ATL_FENIX_sofa_2_nvegci.png"

---
