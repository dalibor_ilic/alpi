---
brand: data/brands/Artisan.md
name: ENY KOMODA
categories:
- Sideboard
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605188007/forestry/ART_ENY_komoda_1_rlji7e.png"
- "/v1605188015/forestry/ART_ENY_komoda_4_kim4jk.png"
dimensions: "<p><strong>Šířka: </strong>90/56/50<strong> </strong>cm - <strong>Délka:
  </strong>44 cm - <strong>Výška: </strong>100/120/122/160 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka: 2 cm</p><p>Tloušťka
  přední části: 2 cm</p><p>Tloušťka zadní části: 1 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>40 000 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605188327/forestry/ART_ENY_komoda_2_l14anw.png"
- "/v1605188327/forestry/ART_ENY_komoda_3_g9wsxp.png"

---
