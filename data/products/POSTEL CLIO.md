---
brand: data/brands/Atlas.md
name: CLIO POSTEL
categories:
- Bed
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Jednoduché, ale elegantní. Minimalistický
  přístup k designu, redukované tvary a rovné linie jsou hlavními rysy této postele.
  Skutečná moderní elegance, která se hodí do všech typů i stylů ložnic či do  ložnic
  malých rozměrů, je to, co z ní dělá častou volbu našich zákazníků.</p>'
product_images:
- "/v1643102844/forestry/ATL_CLIO_postel_1_hynf03.png"
- "/v1643102844/forestry/ATL_CLIO_postel_6_cwvd1j.png"
dimensions: "<p>Jednolůžko I.</p><p>100 × 200 cm - <strong>Šířka: </strong>121 cm
  - <strong>Délka: </strong>221 cm - <strong>Výška: </strong>120 cm</p><p>Jednolůžko
  II.</p><p>120 × 200 cm - <strong>Šířka: </strong>141 cm - <strong>Délka: </strong>221
  cm - <strong>Výška: </strong>120 cm</p><p>Jednolůžko III.</p><p>140 × 200 cm - <strong>Šířka:
  </strong>161 cm - <strong>Délka: </strong>221 cm - <strong>Výška: </strong>120 cm</p><p>Dvojlůžko
  I.</p><p>160 × 200 cm - <strong>Šířka: </strong>181 cm - <strong>Délka: </strong>221
  cm - <strong>Výška: </strong>120 cm</p><p>Dvojlůžko II.</p><p>180 × 200 cm - <strong>Šířka:
  </strong>201 cm - <strong>Délka: </strong>221 cm - <strong>Výška: </strong>120 cm</p><p>Dvojlůžko
  III.</p><p>200 × 200 cm - <strong>Šířka: </strong>221 cm - <strong>Délka: </strong>221
  cm - <strong>Výška: </strong>120 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Výplň:</strong>
  pěna N 25/38 v potahu z vaty</p><p><strong>Základ:</strong> pěna N 18/30</p><p><strong>Nožičky:
  </strong>dřevěné nohy lakované PU lakem</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong> </a>| <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>36 700 Kč</p><p><strong>Cena v látce od:
  </strong>22 700 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/a3bbeb990a3d843efac49aa36dc5f5d2c9b4a5f5.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643103025/forestry/ATL_CLIO_postel_5_eyydnn.png"
- "/v1643103024/forestry/ATL_CLIO_postel_4_cwrhak.png"
- "/v1643103023/forestry/ATL_CLIO_postel_3_v81xwa.png"
- "/v1643103025/forestry/ATL_CLIO_postel_2_ivznxc.png"

---
