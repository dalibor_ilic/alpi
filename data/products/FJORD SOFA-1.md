---
brand: data/brands/Extraform.md
name: FJORD SOFA
categories:
- Sofa
description: <p>Výrobce :<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>FJORD
  je mimořádně pohodlná a klasická souprava nábytku naplněná peřím. Autentický rys
  této pohovky jsou dubové nohy. Je k dispozici možnost s kůží nebo s látkou.</p>
product_images:
- "/v1644503865/forestry/EXT_FJORD_sofa_6_wnasz1.png"
- "/v1644503865/forestry/EXT_FJORD_sofa_7_u3pjg3.png"
dimensions: "<p><strong>Šířka: </strong>195 cm - <strong>Délka: </strong>100 cm -
  <strong>Výška: </strong>80 cm - <strong>Výška sedu: </strong>44 cm - <strong>Hloubka
  sedu: </strong>61 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, 18 mm silná překližka,
  16 mm silná dřevotříska, lepenka.</p><p class="Paragraph SCXW141215766 BCX4">Kombinovaná
  pěna RG 30, polštáře plněné peřím, 300 g tepelně pojené bavlněné vaty, vinuté pružiny.</p><p
  class="Paragraph SCXW141215766 BCX4">Ergonomická pěna RG 25 a RG 30, polštáře plněné
  peřím, gumičky, 100 g bavlněné vatové výplně</p><p class="Paragraph SCXW141215766
  BCX4">Pěna RG 30, polštáře plněné peřím, 100 g bavlněné textilie pojené tepelnou
  vazbou</p><p class="Paragraph SCXW141215766 BCX4">Nohy z dubového dřeva, naolejované.
  Volitelně z bukového dřeva v šesti barvách.</p><p class="Paragraph SCXW141215766
  BCX4">Pevná rohová pohovka.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>53 700 Kč </p>"
product_info: ''
product_photos: []

---
