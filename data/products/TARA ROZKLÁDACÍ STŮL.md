---
brand: data/brands/Artisan.md
name: 'TARA ROZKLÁDACÍ STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1604927112/forestry/ART_TARA_st%C5%AFl_4_cuyrg5.png"
- "/v1604927111/forestry/ART_TARA_st%C5%AFl_5_ni9stu.png"
- "/v1604927111/forestry/ART_TARA_st%C5%AFl_3_d85i4o.png"
- "/v1605683573/forestry/ART_TARA_st%C5%AFl_2_hfdgo1.png"
dimensions: "<p><strong>Šířka: </strong>120 - 330 cm - <strong>Délka: </strong>94
  - 104 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a za příplatek 10% možnost šíře
  114 cm.</p><p>Tloušťka desky: 2 cm</p><p>Vizuální tloušťka desky: 7 cm</p><p>Průměr
  nohou: spodní část 8x8cm, vrchní část 5,5x5,5 cm</p><p>Rozměry rozšíření: 1x40,
  1x50 a 1x55 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>96 400 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605683699/forestry/ART_TARA_st%C5%AFl_6_d1o2fe.png"
- "/v1605683699/forestry/ART_TARA_st%C5%AFl_7_tnnwpz.png"

---
