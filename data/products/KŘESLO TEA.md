---
brand: data/brands/Atlas.md
name: TEA KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Tea je atraktivní a pohodlné křeslo, které
  reprezentuje klasické křeslo na čtení a  pro odpočinek. Sedák a opěradlo mají dokonalé
  proporce. Jemný vzhled zaujme milovníky klasického nábytku.</p>'
product_images:
- "/v1643201644/forestry/ATL_TEA_k%C5%99eslo_1_gtnbeb.png"
dimensions: "<p><strong>Šířka: </strong>80 cm - <strong>Délka: </strong>80 cm - <strong>Výška:
  </strong>87 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Podpěra:</strong>
  vlna</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 </p><p><strong>Opěradlo:</strong> N pěna 25/38</p><p><strong>Područky:</strong>
  N pěna 25/38</p><p><strong>Nožičky: </strong>masivní bukové dřevo</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od:</strong> 26 700 Kč</p><p><strong>Cena v látce od:
  </strong>18 100 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/8e607f7ae7ee3a6e3e1c41c717aff70207ee7004.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643201798/forestry/ATL_TEA_k%C5%99eslo_8_o1asb8.png"
- "/v1643201864/forestry/ATL_TEA_k%C5%99eslo_7_o9scbr.png"
- "/v1643201867/forestry/ATL_TEA_k%C5%99eslo_6_p6oe16.png"
- "/v1643201867/forestry/ATL_TEA_k%C5%99eslo_5_frg7gc.png"

---
