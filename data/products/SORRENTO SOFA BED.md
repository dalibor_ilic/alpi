---
brand: data/brands/Atlas.md
name: SORRENTO SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Sofistikovaný vzhled pro lidi se speciálním
  vkusem. Sorrento je model, který dokáže vyrazit dech i těm zákazníkům, kterým jsou
  modely s nádechem elegance lhostejné. Základní tvar rovných linií je tvarován speciálním
  způsobem, prošívání a detaily, jako jsou ozdobné knoflíky, obnovují ducha starých
  časů a vnášejí do designu této pohovky nádech elegance.</p>'
product_images:
- "/v1643023892/forestry/ATL_SORRENTO_sofa_2_uugfs6.png"
- "/v1643023892/forestry/ATL_SORRENTO_sofa_1_cucfqu.png"
dimensions: "<p>Trojsed:</p><p><strong>Šířka: </strong>230 cm - <strong>Délka: </strong>97
  cm - <strong>Výška: </strong>67 cm - <strong>Výška sedu:</strong> 42 cm- <strong>Hloubka
  sedu:</strong> 64 cm</p><p>Rohová souprava verze I.</p><p><strong>Šířka: </strong>313
  cm - <strong>Délka: </strong>198 cm - <strong>Výška: </strong>67 cm - <strong>Výška
  sedu:</strong> 42 cm- <strong>Hloubka sedu:</strong> 64 cm</p><p>Rohová souprava
  verze II.</p><p><strong>Šířka: </strong>398 cm - <strong>Délka: </strong>398 cm
  - <strong>Výška: </strong>67 cm - <strong>Výška sedu:</strong> 42 cm- <strong>Hloubka
  sedu:</strong> 64 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/30 a HR pěna 35/36 ve vatovém potahu</p><p><strong>Opěradlo: </strong>pěna
  L 25/15 a N 25/38</p><p><strong>Vlnové pružiny a elastické popruhy</strong></p><p><strong>Nožičky:
  </strong>dřevěné nohy lakované PU lakem, nebo v barvě RAL</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>54 800 Kč</p><p><strong>Cena v látce od:
  </strong>36 500 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/69278111d3147af7d7a8634af39e2c32b5fd333b.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643024278/forestry/ATL_SORRENTO_sofa_6_xzyzpk.png"
- "/v1643024278/forestry/ATL_SORRENTO_sofa_5_goo3ig.png"
- "/v1643024278/forestry/ATL_SORRENTO_sofa_4_ew20jq.png"
- "/v1643024279/forestry/ATL_SORRENTO_sofa_3_irrolp.png"

---
