---
brand: data/brands/Artisan.md
name: 'INVITO ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644248789/forestry/Product_foto-20_xhmetx.png"
- "/v1644248789/forestry/Product_foto-19_ojzeer.png"
dimensions: "<p><strong>Šířka: </strong>43 cm - <strong>Délka: </strong>50 cm - <strong>Výška:
  </strong>77 cm - <strong>Výška sedu: </strong>43 cm - <strong>Hloubka sedu: </strong>43
  cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v kůži.</p><p>Velikost
  sedu: 43x40 cm</p><p>Průmer nohou: spodní část 2,6 cm, vrchní část 3,6 cm</p><p>Je
  možné objednat pouze v černé kůži.</p>"
materials: <p><a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 22 600 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604584129/forestry/ART_INVITO_%C5%BEidle_4_leyll2.png"
- "/v1604584129/forestry/ART_INVITO_%C5%BEidle_2_fsbqbx.png"
- "/v1604584129/forestry/ART_INVITO_%C5%BEidle_3_ule8pg.png"

---
