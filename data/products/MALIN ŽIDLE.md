---
brand: data/brands/Woak.md
name: 'MALIN ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606744520/forestry/WHO_MALIN_%C5%BEidle_4_ihf3nl.png"
- "/v1606744501/forestry/WHO_MALIN_%C5%BEidle_1_khmxx6.png"
dimensions: "<p><strong>Šířka: </strong>57,5 cm - <strong>Délka: </strong>49 cm -
  <strong>Výška:</strong> 83,5 cm</p>"
technical_info: "<p>materiál: ořech nebo dub, látka a kůže</p><p>povrchová úprava:
  lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>23 000 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_chair_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606745149/forestry/WHO_MALIN_%C5%BEidle_5_qozonv.png"
- "/v1606745141/forestry/WHO_MALIN_%C5%BEidle_2_ixso78.png"
- "/v1606745141/forestry/WHO_MALIN_%C5%BEidle_3_vbnz50.png"

---
