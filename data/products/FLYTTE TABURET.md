---
brand: data/brands/Extraform.md
name: 'FLYTTE TABURET '
categories:
- Accessories
description: <p>Výrobce :<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Flytte
  díky svému nadčasovému a klasickému designu se hodí do jakéhokoliv prostoru. Vysoká
  úroveň flexibility umožňuje přizpůsobit se jakémukoliv požadavku, a to i zpětně.
  Flytte je snadné zamilovat si.</p>
product_images:
- "/v1644504424/forestry/EXT_FLYTTE_taburet_1_orkjcp.png"
dimensions: "<p><strong>Šířka: </strong>80 cm - <strong>Délka: </strong>80 cm - <strong>Výška:
  </strong>43 cm - <strong>Výška sedu: </strong>43 cm - <strong>Hloubka sedu:</strong>
  80cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, 18 mm silná překližka,
  MDF deska, lepenka.</p><p class="Paragraph SCXW151689098 BCX4">Sedáky z vysoce elastické
  pěny HR, 100 g tepelně pojené plsti, elastických popruhů.</p><p class="Paragraph
  SCXW151689098 BCX4">Nohy z dubového dřeva, naolejované. Možnost z bukového dřeva
  v šesti barvách.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>16 400 Kč </p>"
product_info: ''
product_photos:
- "/v1644504413/forestry/EXT_FLYTTE_taburet_2_ivlz5h.png"

---
