---
brand: data/brands/Extraform.md
name: HIPPO SOFA
categories:
- Sofa bed system
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Jednolitá
  struktura ukrývá rozkládací mechanismus, který promění HIPPO v pohodlnou postel.
  Její spolehlivá a kompaktní forma je zdůrazněna ozdobnými hranatými detaily, díky
  nimž se tato pohovka hodí do jakéhokoliv typu prostoru.</p><p></p>'
product_images:
- "/v1644505355/forestry/EXT_HIPPO_sofa_1_uehl79.png"
- "/v1644505336/forestry/EXT_HIPPO_sofa_5_bubdiy.png"
- "/v1644505333/forestry/EXT_HIPPO_sofa_4_zt2qmg.png"
- "/v1644505311/forestry/EXT_HIPPO_sofa_6_cp3hca.png"
- "/v1644505311/forestry/EXT_HIPPO_sofa_8_vsr0ds.png"
- "/v1644505310/forestry/EXT_HIPPO_sofa_7_mhvgrq.png"
dimensions: "<p><strong>Šířka: </strong>268 - 298<strong> </strong>cm - <strong>Délka:
  </strong>193 cm - <strong>Výška: </strong>85 cm - <strong>Výška sedu:</strong> 48
  cm - <strong>Hloubka sedu: </strong>57 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, dřevotříska o tloušťce
  16 mm, lepenka, překližka o tloušťce 18 mm, 3 mm silná sololitová deska.</p><p class="Paragraph
  SCXW36922489 BCX4">Vysoce elastická pěna HR, 200 g bavlněné textilie s tepelnou
  vazbou, 500 g rouna, vinuté pružiny.</p><p class="Paragraph SCXW36922489 BCX4">Ergonomická
  pěna RG 22, 200 g bavlněné textilie s tepelnou vazbou.</p><p class="Paragraph SCXW36922489
  BCX4">Pěna RG 30, 200 g bavlněné vatové směsi s tepelnou vazbou.</p><p class="Paragraph
  SCXW36922489 BCX4">Nohy z bukového dřeva, mořené a lakované. K dispozici v šesti
  barvách.</p><p class="Paragraph SCXW36922489 BCX4">Výsuvná postel je volitelná pro
  dvoumístné a třímístné pohovky.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>77 000 Kč</p>"
product_info: ''
product_photos:
- "/v1644505274/forestry/EXT_HIPPO_sofa_2_lpeqls.png"
- "/v1644505279/forestry/EXT_HIPPO_sofa_3_eangwi.png"

---
