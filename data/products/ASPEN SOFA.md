---
brand: data/brands/Atlas.md
name: 'ASPEN SOFA '
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a><strong> </strong></p><p>Naprostý klid a absolutní
  pohodlí jsou pocity, které ve Vás vyvolá sedací souprava Aspen. Umožní Vám žít luxusní
  životní styl tím, že ve Vašem domově vytvoří atmosféru ohromující, chladné a vznešené
  krásy.</p>'
product_images:
- "/v1648648343/forestry/Product_foto_1_uqhb1v.png"
- "/v1648648351/forestry/Product_foto_6_cgjqzb.png"
- "/v1648648355/forestry/Product_foto_5_s024pt.png"
- "/v1648648355/forestry/Product_foto_4_g2twhp.png"
- "/v1648648356/forestry/Product_foto_3_m8l2kq.png"
- "/v1648648350/forestry/Product_foto_2_uc8twc.png"
dimensions: "<p><strong>Šířka: </strong>102/104<strong> </strong>cm - <strong>Délka:
  </strong>182/202/222/242 cm - <strong>Výška: </strong>92 cm - <strong>Výška sedu:</strong>
  69 cm - <strong>Hloubka sedu:</strong> 63 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, kůže</p><p><strong>Sedák: </strong>vysoce
  elastická polyuretanová HR pěna 35/36, silikonové kuličky v potahu polštáře</p><p><strong>Opěradlo:
  </strong>vysoce elastická polyuretanová pěna N 25/38, paměťová pěna, silikon</p><p><strong>Elastické
  popruhy široké 7 cm</strong></p><p><strong>Nožičky: </strong>dřevěný rám - lazura,
  polyuretanový lak, krycí polyuretanová barva dle vzorníku RAL</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a> </p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/4bb2faa2c6e0874ede3ec9836005e3c8214190fe.pdf"
  title="Produktová karta"><strong>Stáhnout PDF</strong></a> </p>'
product_photos:
- "/v1648648395/forestry/Z_interi%C3%A9ru-4_ijjeq7.png"
- "/v1648648393/forestry/Z_interi%C3%A9ru-2_sw7g0r.png"
- "/v1648648395/forestry/Z_interi%C3%A9ru-3_wt1265.png"
- "/v1648648395/forestry/Z_interi%C3%A9ru_jiyimq.png"
- "/v1648648391/forestry/Z_interi%C3%A9ru-5_fnbpzg.png"

---
