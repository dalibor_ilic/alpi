---
brand: data/brands/Extraform.md
name: 'PLEASURE RELAX SOFA '
categories:
- Sofa
description: <p>Výrobce<strong> </strong>:<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Sedací
  souprava, kterou lze použít na relaxaci. Díky své pružnosti, opěrce hlavy a flexibilní
  konstrukci nabízí vynikající pohodlí.</p>
product_images:
- "/v1645089311/forestry/EXT_PLEASURE_sofa_1_alr3ha.png"
- "/v1645089303/forestry/EXT_PLEASURE_sofa_5_aopgi7.png"
- "/v1645089293/forestry/EXT_PLEASURE_sofa_6_lvmnpw.png"
- "/v1645089293/forestry/EXT_PLEASURE_sofa_9_fhqcza.png"
- "/v1645089293/forestry/EXT_PLEASURE_sofa_8_m95qpv.png"
- "/v1645089293/forestry/EXT_PLEASURE_sofa_7_ohzdit.png"
dimensions: '<p><strong>Dvojsed: Šířka: </strong>184 cm - <strong>Délka:</strong>
  102 cm - <strong>Výška:</strong> 80-100 cm - <strong>Výška sedu:</strong> 44 cm
  - <strong>Hloubka sedu:</strong> 52 cm</p><p class="Paragraph SCXW67310076 BCX4"><strong>Trojsed:
  Šířka: </strong>251 cm - <strong>Délka:</strong> 102 cm - <strong>Výška:</strong>
  80-100 cm - <strong>Výška sedu:</strong> 44 cm - <strong>Hloubka sedu:</strong>
  52 cm</p>'
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, dřevotříska o tloušťce
  16 mm, lepenka, překližka o tloušťce 18 mm, 3 mm silná sololitová deska.</p><p class="Paragraph
  SCXW153886159 BCX4">Sedáky - Vysoce elastická HR pěna, 300 g tepelně pojené bavlněné
  vaty, 500 g rouna, vinuté pružiny.</p><p class="Paragraph SCXW153886159 BCX4">Opěrky
  - Ergonomická vysoce elastická pěna, gumičky, nastavitelná opěrka hlavy.</p><p class="Paragraph
  SCXW153886159 BCX4">Nohy z bukového dřeva, mořené a lakované. K dispozici v šesti
  barvách. Chromované nohy jsou volitelné.</p><p class="Paragraph SCXW153886159 BCX4">Relaxační
  mechanismus je možnost.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 37 400 Kč </p>"
product_info: ''
product_photos:
- "/v1645089370/forestry/EXT_PLEASURE_sofa_2_lqiltm.png"
- "/v1645089371/forestry/EXT_PLEASURE_sofa_3_zyk7ds.png"
- "/v1645089370/forestry/EXT_PLEASURE_sofa_4_wggq2i.png"

---
