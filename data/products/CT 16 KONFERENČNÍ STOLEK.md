---
brand: data/brands/Atlas.md
name: CT 16 KONFERENČNÍ STOLEK
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Tento konferenční stolek představuje pravděpodobně
  jeden z nejinovativnějších nápadů, pokud jde o design konferenčního stolku. Podstavec
  je vyroben z masivního bukového dřeva a lze jej nalakovat v barvě, která se vám
  líbí. Horní deska z MDF je potažena nejjemnější zvířecí kožešinou a tento detail
  ji činí zvláštní a jedinečnou. Konferenční stolek CT 16 se díky svým menším rozměrům
  snadno vejde do prostor s různým využitím.</p>'
product_images:
- "/v1643285097/forestry/ATL_CT16_konferen%C4%8Dn%C3%AD_stolek_1_bw6phx.png"
- "/v1643285097/forestry/ATL_CT16_konferen%C4%8Dn%C3%AD_stolek_3_oapjdm.png"
- "/v1643285097/forestry/ATL_CT16_konferen%C4%8Dn%C3%AD_stolek_2_zf71oz.png"
dimensions: "<p>Rozměr I.</p><p><strong>Šířka: </strong>50 cm - <strong>Délka: </strong>50
  cm - <strong>Výška: </strong>76 cm</p><p>Rozměr II.</p><p><strong>Šířka: </strong>30
  cm - <strong>Délka: </strong>30 cm - <strong>Výška: </strong>64,5 cm</p>"
technical_info: "<p><strong>Materiál:</strong> buk, kůže</p>"
materials: ''
price: "<p><strong>Cena od: </strong>5 700 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/4776e67a0168145f040553efa1251f1e4d6d8edd.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos: []

---
