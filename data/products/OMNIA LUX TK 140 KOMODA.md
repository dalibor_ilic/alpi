---
brand: data/brands/Atlas.md
name: OMNIA LUX TK 140 KOMODA
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Model TK 140 doplňuje kolekci Omnia Lux,
  protože je kombinací otevřených polic a výklopných dvířek. Univerzálnost systému
  se projevuje také v různých povrchových úpravách, které dávají různý styl - klasičtější
  nebo modernější.</p>'
product_images:
- "/v1643203740/forestry/om1---d2a024c9577a0c798829c34a2cd4c813771d962f_s1iatr.jpg"
- "/v1643203740/forestry/om2---1a35ba1e18460a45c3dab88764afbcede5cb9ad3_vk0o8a.jpg"
dimensions: "<p><strong>Šířka: </strong>140 cm - <strong>Délka: </strong>42 cm - <strong>Výška:
  </strong>66,5 cm</p>"
technical_info: "<p></p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>24 800 Kč</p>"
product_info: ''
product_photos: []

---
