---
brand: data/brands/Artisan.md
name: 'PASHA STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605609321/forestry/ART_PASHA_st%C5%AFl_1_dvkfwy.png"
dimensions: "<p><strong>Šířka: </strong>100 - 260 cm - <strong>Délka: </strong>100
  cm - <strong>Výška: </strong>75 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 4,2 cm
  </p><p>Vizuální tloušťka desky: 0,8 cm</p><p>Průměr nohou: 0,6 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>38 900 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605609433/forestry/ART_PASHA_st%C5%AFl_3_dgc3f8.png"
- "/v1605609433/forestry/ART_PASHA_st%C5%AFl_2_i8ecaq.png"

---
