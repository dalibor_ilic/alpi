---
brand: data/brands/Artisan.md
name: NEVA SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605190066/forestry/ART_NEVA_sofa_1_gbaks1.png"
- "/v1644250462/forestry/Product_foto-35_ws9hu8.png"
- "/v1644250562/forestry/Product_foto3_du2noy.png"
dimensions: "<p><strong>Šířka: </strong>140<strong> </strong>cm - <strong>Délka: </strong>85
  cm - <strong>Výška: </strong>93 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v 2 kategoriích látek
  i 3 kůže.</p><p>Velikost sedu: 132x48 cm </p><p>Průměr nohou: spodní část 2,6 cm,
  vrchní část 4,4 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO </strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 79 000 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605190233/forestry/ART_NEVA_sofa_3_tjmlve.png"
- "/v1605190234/forestry/ART_NEVA_sofa_2_nqb7hz.png"

---
