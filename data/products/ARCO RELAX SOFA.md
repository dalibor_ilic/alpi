---
brand: data/brands/Extraform.md
name: ARCO RELAX SOFA
categories:
- Sofa
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"
  title="EXTRAFORM"><strong>EXTRAFORM</strong></a></p><p>Reprezentativní pohovka naplněná
  peřím se skrytým elektrickým motorem, který umožňuje 16 cm prodloužení střední sedací
  části. Opěradlo a područky jsou pevné. Varianta v kůži zdůrazňuje strukturu z masivního
  dubového dřeva. </p>'
product_images:
- "/v1644499639/forestry/EXT_ARCO_sofa_5_esil3n.png"
- "/v1644499639/forestry/EXT_ARCO_sofa_4_ryjm2d.png"
- "/v1644499639/forestry/EXT_ARCO_sofa_2_njeo5q.png"
- "/v1644499639/forestry/EXT_ARCO_sofa_3_zgisqv.png"
- "/v1644499639/forestry/EXT_ARCO_sofa_1_bkqlwh.png"
- "/v1644499642/forestry/EXT_ARCO_sofa_6_tsbfhv.png"
dimensions: "<p><strong>Dvojsed: Šířka:</strong> 191 cm - <strong>Délka: </strong>96
  cm - <strong>Výška:</strong> 71 cm -  <strong>Výška sedu: </strong>43 cm - <strong>Hloubka
  sedu:</strong> 71 - 87 cm </p><p><strong>Trojsed: Šířka: </strong>230 cm - <strong>Délka:</strong>
  96 cm - <strong>Výška: </strong>71 cm - <strong>Výška sedu: </strong>43 cm - <strong>Hloubka
  sedu:</strong> 71 - 87 cm</p>"
technical_info: "<p>Nosná konstrukce ze sušeného bukového dřeva, překližky o tloušťce
  18 mm, lepenky. </p><p>Kombinovaná pěna RG 30, polštáře plněné peřím, 100 + 300
  g tepelně vázaného bavlněného vatelínu, vinuté pružiny. </p><p>Ergonomická pěna
  RG 30, 100 g bavlněné textilie s termoplastem, další polštáře plněné peřím. </p><p>Nohy
  a rám nohou z dubového dřěva. </p><p>Na přání je k dispozici mechanismus sedadla
  o 16 cm větší hloubky sedadla. </p>"
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 68 700 Kč </p>"
product_info: ''
product_photos:
- "/v1644500138/forestry/EXT_ARCO_sofa_8_dkimjm.png"
- "/v1644500111/forestry/EXT_ARCO_sofa_7_ykotwf.png"
- "/v1644500139/forestry/EXT_ARCO_sofa_10_sabdsq.png"
- "/v1644500139/forestry/EXT_ARCO_sofa_9_mmou3r.png"

---
