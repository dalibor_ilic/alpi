---
brand: data/brands/Woak.md
name: 'MARSHALL ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Dřevěné části tohoto nábytku jsou ručně
  vybírány jedna část po druhé, aby dokonale pasovaly s ostatními materiály, které
  jsou zcela inovativní a recyklovatelné, jako je nezpracovaná kůže, čistý mramor
  nebo lité sklo s měnícími se barvami. Kombinace teplých a studených materiálů vede
  k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem je mužný,
  ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací pokoj nebo
  koutek na čtení.</p>'
product_images:
- "/v1606898264/forestry/WHO_MARSHALL_%C5%BEidle_6_tka1ix.png"
- "/v1606898264/forestry/WHO_MARSHALL_%C5%BEidle_5_ocoyjh.png"
- "/v1606898256/forestry/WHO_MARSHALL_%C5%BEidle_1_fdw5uv.png"
- "/v1606898264/forestry/WHO_MARSHALL_%C5%BEidle_4_akwbnr.png"
dimensions: "<p><strong>Šířka: </strong>48 cm - <strong>Délka: </strong>53/55 cm -
  <strong>Výška:</strong> 78 cm</p>"
technical_info: "<p>materiál: ořech nebo dub, látka a kůže</p><p>povrchová úprava:
  lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>20 400 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Marshall_chair_without_with_armrest_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606898379/forestry/WHO_MARSHALL_%C5%BEidle_3_idq4qr.png"
- "/v1606898379/forestry/WHO_MARSHALL_%C5%BEidle_2_zhmofm.png"

---
