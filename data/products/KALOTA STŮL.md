---
brand: data/brands/Artisan.md
name: 'KALOTA STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605605689/forestry/ART_KALOTA_st%C5%AFl_1_dpl8hm.png"
- "/v1605605689/forestry/ART_KALOTA_st%C5%AFl_2_qchsaw.png"
- "/v1605605689/forestry/ART_KALOTA_st%C5%AFl_3_yggllz.png"
dimensions: "<p><strong>Šířka: </strong>180/200/220/240 cm - <strong>Délka: </strong>90
  cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a keramika ve variantě bílá a
  černá(6 mm).</p><p>Tloušťka desky: 8 cm </p><p>Vizuální tloušťka desky: 5 cm</p><p>Průměr
  nohou: spodní část 4x4 cm, vrchní část 6x6 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 84 300 Kč</p>"
product_info: ''
product_photos: []

---
