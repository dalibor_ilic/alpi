---
brand: data/brands/Artisan.md
name: '7 BAROVÁ ŽIDLE '
categories:
- Bar chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644247175/forestry/Product_foto-2_kusfv8.png"
- "/v1644247160/forestry/Product_foto_glhxnd.png"
dimensions: "<p><strong>Šířka: </strong>52,5<strong> </strong>cm - <strong>Délka:</strong>
  52 cm - <strong>Výška:</strong> 109 cm</p>"
technical_info: "<p>Velikost sedu: 37x43 cm</p><p>Tloušťka předních nohou: spodní
  část 2,3 cm, vrchní část 3,3 cm </p><p>Tloušťka zadních nohou : 2,3x4 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: ''
product_info: "<p></p>"
product_photos:
- "/v1604479870/forestry/ART_7_barov%C3%A1_%C5%BEidle_4_iukeax.png"
- "/v1604479870/forestry/ART_7_barov%C3%A1_%C5%BEidle_3_w2rxuh.png"
- "/v1604479870/forestry/ART_7_barov%C3%A1_%C5%BEidle_2_ybeage.png"

---
