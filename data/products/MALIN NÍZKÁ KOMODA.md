---
brand: data/brands/Woak.md
name: MALIN NÍZKÁ KOMODA
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606487042/forestry/WHO_MALIN_n%C3%ADzk%C3%A1_komoda_1_xqhlix.png"
- "/v1606487048/forestry/WHO_MALIN_n%C3%ADzk%C3%A1_komoda_5_uuexnd.png"
dimensions: "<p><strong>Šířka: </strong>160/180/200/220/240 cm - <strong>Délka: </strong>40
  cm - <strong>Výška:</strong> 45 cm</p>"
technical_info: "<p>Tato komoda je vyrobena z masivního dřeva (dubu nebo ořechu) a
  má olejovou úpravu.</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 64 000 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_lowboard_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1QsTQiGS7xD9UD-t-zr97R1BZvvvXEAIn/view?usp=sharing"
  title=""><strong>Stáhnout PDF </strong></a><strong> </strong></p>'
product_photos:
- "/v1606487372/forestry/WHO_MALIN_n%C3%ADzk%C3%A1_komoda_4_lbc3xh.png"
- "/v1606487374/forestry/WHO_MALIN_n%C3%ADzk%C3%A1_komoda_3_n1uon2.png"
- "/v1606487374/forestry/WHO_MALIN_n%C3%ADzk%C3%A1_komoda_2_ekp2po.png"

---
