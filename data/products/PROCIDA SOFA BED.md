---
brand: data/brands/Atlas.md
name: PROCIDA SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Jednoduché linie a minimalistický design,
  mechanismus s nastavitelnými opěrkami, které umožňují různou hloubku sezení. Posunutím
  opěradla do koncové polohy lze kompaktní segment k sezení proměnit v lůžko.</p>'
product_images:
- "/v1643020310/forestry/ATL_PROCIDA_sofa_1_mis4cb.png"
- "/v1643020311/forestry/ATL_PROCIDA_sofa_6_zewxkf.png"
- "/v1643020311/forestry/ATL_PROCIDA_sofa_10_mkeiks.png"
- "/v1643020311/forestry/ATL_PROCIDA_sofa_7_ywjjqn.png"
- "/v1643020311/forestry/ATL_PROCIDA_sofa_8_iyelya.png"
- "/v1643020311/forestry/ATL_PROCIDA_sofa_9_xaq3gn.png"
dimensions: "<p><strong>Šířka: </strong>322 cm - <strong>Délka: </strong>259 cm -
  <strong>Výška: </strong>88 cm - <strong>Výška sedu:</strong> 39 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 a HR pěna 35/30 ve vatovém potahu</p><p><strong>Opěradlo: </strong>pěna
  L 25/15 a peří se silikonem</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:
  </strong>kovové -inox nohy</p><p><strong>Možnosti:</strong> mechanismus pro změnu
  hloubky polštářů opěradla </p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>155 000 Kč </p><p><strong>Cena v látce
  do: </strong>109 600 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/825855935f9b201cff18a11539c9dee4262d68ce.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643020635/forestry/ATL_PROCIDA_sofa_5_dn7vdd.png"
- "/v1643020633/forestry/ATL_PROCIDA_sofa_4_jvboix.png"
- "/v1643020634/forestry/ATL_PROCIDA_sofa_3_yecmgs.png"
- "/v1643020634/forestry/ATL_PROCIDA_sofa_2_auoeur.png"

---
