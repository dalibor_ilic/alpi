---
brand: data/brands/Atlas.md
name: GROUND SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Za vznikem sedací soupravy GROUND stojí
  citlivost italských designérů. Jejími přednostmi jsou funkčnost díky modularitě
  prvků a pohodlí díky nejmodernějším technologiím. Sedadla jsou vyplněna 100% husím
  peřím, které v kombinaci s pěnou a silikonovými kuličkami poskytuje dokonalé pohodlí
  pro opravdové hédonisty. Moderní pohovka, která se díky správné volbě barev a materiálů
  hodí do každého prostředí. Její design přináší pocit dokonalého požitku.</p>'
product_images:
- "/v1642435226/forestry/ATL_GROUND_sofa_1_sxikbr.png"
- "/v1642435227/forestry/ATL_GROUND_sofa_9_p43ygs.png"
- "/v1642435228/forestry/ATL_GROUND_sofa_11_danfls.png"
- "/v1642435228/forestry/ATL_GROUND_sofa_13_l7vjnm.png"
- "/v1642435228/forestry/ATL_GROUND_sofa_12_dj7boh.png"
- "/v1642435228/forestry/ATL_GROUND_sofa_10_olxws2.png"
dimensions: "<p>Dvojsed menší:</p><p><strong>Šířka: </strong>185 cm - <strong>Délka:
  </strong>104 cm - <strong>Výška: </strong>80 cm </p><p>Dvojsed větší:</p><p><strong>Šířka:
  </strong>205 cm - <strong>Délka: </strong>104 cm - <strong>Výška: </strong>80 cm
  </p><p>Trojsed menší:</p><p><strong>Šířka:</strong> 225 cm - <strong>Délka: </strong>104
  cm - <strong>Výška: </strong>80 cm </p><p>Trojsed větší: </p><p><strong>Šířka:</strong>
  245 cm - <strong>Délka: </strong>104 cm - <strong>Výška: </strong>80 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 a HR 35/30, peří se silikonem</p><p><strong>Opěradlo: </strong>silikon</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>plastové nožičky 25 mm</p><p></p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>54 800 Kč </p><p><strong>Cena v látce
  od: </strong>38 300 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/892ebf78c7907420533fb9cfc889664a4f65dbac.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642435632/forestry/ATL_GROUND_sofa_8_tsj5rf.png"
- "/v1642435631/forestry/ATL_GROUND_sofa_7_ofskxg.png"
- "/v1642435632/forestry/ATL_GROUND_sofa_6_zrfjgc.png"
- "/v1642435631/forestry/ATL_GROUND_sofa_5_l8dgka.png"
- "/v1642435632/forestry/ATL_GROUND_sofa_4_lxmdpr.png"
- "/v1642435632/forestry/ATL_GROUND_sofa_3_vrx7z0.png"
- "/v1642435632/forestry/ATL_GROUND_sofa_2_numrbp.png"

---
