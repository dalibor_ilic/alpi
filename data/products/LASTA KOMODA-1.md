---
brand: data/brands/Artisan.md
name: 'LASTA KOMODA '
categories:
- Sideboard
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645710103/forestry/Lasta_komoda_dv%C4%9B_nohyProduct_foto-2_iowb74.png"
- "/v1645710103/forestry/Lasta_komoda_dv%C4%9B_nohyProduct_foto_khylsc.png"
dimensions: "<p><strong>Šířka: </strong>160/180/200/220 cm - <strong>Délka: </strong>51
  cm - <strong>Výška: </strong>45/70/82 cm</p>"
technical_info: "<p>Výška nohou: 20 cm </p><p>Průměr nohou: 1,5 cm </p><p>Materiál
  nohou: nerezová ocel </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>102 900 Kč</p>"
product_info: ''
product_photos:
- "/v1645710459/forestry/Lasta_komoda_dv%C4%9B_nohyZ_interi%C3%A9ru-2_yorpez.png"
- "/v1645710459/forestry/Lasta_komoda_dv%C4%9B_nohyZ_interi%C3%A9ru_ctfvax.png"

---
