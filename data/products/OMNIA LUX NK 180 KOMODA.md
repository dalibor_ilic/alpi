---
brand: data/brands/Atlas.md
name: OMNIA LUX NK 180 KOMODA
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Nová kolekce komody Omnia Lux umožňuje
  nekonečné množství kombinací povrchových úprav dřeva a kůže. Komoda NK 180 je elegantní
  a rafinovaná, stejně jako samotná kolekce Omnia Lux. Díky velkorysému úložnému prostoru
  a kombinaci dřeva a kůže je vhodná, jak do obchodních prostor, tak do každé domácnosti.</p>'
product_images:
- "/v1643204413/forestry/omnia1---543afa014f973bfe1d4bbe17c7a0a39dcffe8d2a_brzkv0.jpg"
- "/v1643204413/forestry/omnia2---0ecf19f049e2fa64bdd45a64cb9843c849190379_g7qvgw.jpg"
dimensions: ''
technical_info: "<p><strong>Šířka: </strong>180 cm - <strong>Délka: </strong>42 cm
  - <strong>Výška: </strong>51,5 cm</p>"
materials: '<p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>27 800 Kč</p>"
product_info: ''
product_photos: []

---
