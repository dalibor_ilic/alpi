---
brand: data/brands/Atlas.md
name: INFINITY SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Absolutní bestseller z naší kolekce. Harmonický
  design poskytuje pocit naprostého potěšení a činí z ní dokonalou pohovku pro relaxaci.
  Široký výběr prvků umožňuje vytvořit kompozici, která dokonale zapadne do vašeho
  prostoru. Vyvážené proporce, pohodlné polstrování sedáku a opěradla dělají z INFINITY
  skutečný klenot v kvalitě pro dlouhá léta potěšení.</p>'
product_images:
- "/v1642436927/forestry/ATL_INFINITY_sofa_1_cdlkz7.png"
- "/v1642436928/forestry/ATL_INFINITY_sofa_11_gcrf76.png"
- "/v1642436928/forestry/ATL_INFINITY_sofa_13_rlrzil.png"
- "/v1642436928/forestry/ATL_INFINITY_sofa_12_p0ofy5.png"
- "/v1642436928/forestry/ATL_INFINITY_sofa_14_qksmgg.png"
- "/v1642436928/forestry/ATL_INFINITY_sofa_10_pj3ulg.png"
dimensions: "<p><strong>Šířka: </strong>291 cm - <strong>Délka: </strong>208 cm -
  <strong>Výška: </strong>84/101 cm - <strong>Výška sedu:</strong> 40 cm - <strong>Hloubka
  sedu:</strong> 61 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36, HR pěna 35/30, peří se silikonem</p><p><strong>Opěradlo: </strong>pěna
  L 25/15 a silikon</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:</strong>
  kovové -inox nohy</p><p></p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1x3qtWSfvDZh45kB8JxujwYgvlKTEPGhz/view?usp=sharing"
  title="Látky ELEGANT"><strong>ELEGANT</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>135 000 Kč </p><p><strong>Cena v látce
  od: </strong>98 000 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/c1872abc1e79388526f5f29c7718d0bccf01be2b.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642437424/forestry/ATL_INFINITY_sofa_9_bbwgoh.png"
- "/v1642437420/forestry/ATL_INFINITY_sofa_5_zu9ycx.png"
- "/v1642437425/forestry/ATL_INFINITY_sofa_4_oqdc0b.png"
- "/v1642437425/forestry/ATL_INFINITY_sofa_3_jvwlr4.png"
- "/v1642437426/forestry/ATL_INFINITY_sofa_7_sudlwe.png"
- "/v1642437426/forestry/ATL_INFINITY_sofa_6_gqdswe.png"
- "/v1642437426/forestry/ATL_INFINITY_sofa_8_fg8sfw.png"

---
