---
brand: data/brands/Artisan.md
name: 'SWING VĚŠÁK '
categories: []
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605529963/forestry/ART_SWING_v%C4%9B%C5%A1%C3%A1k_3_dx0hhl.png"
- "/v1605529969/forestry/ART_SWING_v%C4%9B%C5%A1%C3%A1k_6_wwxzh7.png"
- "/v1644252518/forestry/Product_foto009_xoltt7.png"
- "/v1644252518/forestry/Product_foto008-2_uu23ly.png"
dimensions: "<p><strong>Šířka: </strong>130/160/185 cm - <strong>Délka: </strong>65<strong>
  </strong>cm - <strong>Výška: </strong>140/160 cm</p>"
technical_info: "<p>Průměr nohou: spodní část 4x4cm, vrchní část 9x4 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>47 300 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605530361/forestry/ART_SWING_v%C4%9B%C5%A1%C3%A1k_4_hdna8z.png"
- "/v1605530361/forestry/ART_SWING_v%C4%9B%C5%A1%C3%A1k_5_tjnm9i.png"

---
