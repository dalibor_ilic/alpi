---
brand: data/brands/Atlas.md
name: 'BEE KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Stolová souprava Bee má geometrický, moderní
  a jednoduchý, ale elegantní vzhled. Kombinace kovové podnože a desky z mramoru,
  nebo MDF poskytuje velké množství různých kombinací, které se budou skvěle hodit
  do obývacích pokojů různých designů. Funkčnost tohoto modelu se odráží v možnosti
  snadného přemisťování v prostoru podle vašich potřeb.</p>'
product_images:
- "/v1643284358/forestry/ATL_BEE_konferen%C4%8Dn%C3%AD_stolek_1_wl9mdq.png"
- "/v1643284358/forestry/ATL_BEE_konferen%C4%8Dn%C3%AD_stolek_5_ufgcqc.png"
- "/v1643284358/forestry/ATL_BEE_konferen%C4%8Dn%C3%AD_stolek_4_uzm3e1.png"
dimensions: "<p>Rozměr I.</p><p><strong>Šířka: </strong>66 cm - <strong>Délka: </strong>57
  cm - <strong>Výška: </strong>50 cm</p><p>Rozměr II.</p><p><strong>Šířka: </strong>46
  cm - <strong>Délka: </strong>40 cm - <strong>Výška: </strong>37 cm</p>"
technical_info: "<p><strong>Materiál:</strong> MDF- RAL, mramor</p>"
materials: ''
price: "<p><strong>Cena od: </strong>6 200 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/0b9f73ccceb39e45826302238d080d3e1ba965ab.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643284497/forestry/ATL_BEE_konferen%C4%8Dn%C3%AD_stolek_3_ibv5kd.png"
- "/v1643284499/forestry/ATL_BEE_konferen%C4%8Dn%C3%AD_stolek_2_obvwvt.png"

---
