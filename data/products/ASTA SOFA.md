---
brand: data/brands/Atlas.md
name: ASTA SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Pohyblivé opěrky hlavy, které si můžete
  přizpůsobit podle svých představ a moderní design, nabízejí zážitek z hluboké relaxace.
  Díky propracovaným detailům se tato pohovka stane ústředním prvkem každého prostoru.
  Model s výjimečnou realizační silou v moderním designovém provedení je ideální pro
  odpolední odpočinek.</p>'
product_images:
- "/v1642238993/forestry/ATL_ASTA_sofa_4_k01tnv.png"
- "/v1642238993/forestry/ATL_ASTA_sofa_6_dmwlf2.png"
- "/v1642238993/forestry/ATL_ASTA_sofa_8_jkxo0z.png"
- "/v1642238993/forestry/ATL_ASTA_sofa_7_snrlhl.png"
- "/v1642238993/forestry/ATL_ASTA_sofa_9_swjluo.png"
- "/v1642238993/forestry/ATL_ASTA_sofa_5_uw00fg.png"
- "/v1642238993/forestry/ATL_ASTA_sofa_1_zg76gl.png"
dimensions: "<p><strong>Šířka: </strong>324 cm - <strong>Délka: </strong>113 cm -
  <strong>Výška: </strong>68/93 cm - <strong>Výška sedu:</strong> 41 cm - <strong>Hloubka
  sedu:</strong> 58 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36</p><p><strong>Opěradlo: </strong>pěna L 25/15</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Kovové nožičky:</strong> kovové-inox nožičky</p><p><strong>Možnosti:</strong>
  opěrky hlavy s mechanismem, dekorativní část na loketní opěrce, nox</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>99 400 Kč </p><p><strong>Cena v látce
  od: </strong>67 500 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/0e587f420eded8afe8fccf5b5ef855975f7355de.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642239463/forestry/ATL_ASTA_sofa_10_gzm6jc.png"
- "/v1642239463/forestry/ATL_ASTA_sofa_3_xeils5.png"
- "/v1642239462/forestry/ATL_ASTA_sofa_2_lkyoqw.png"

---
