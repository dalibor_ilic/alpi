---
brand: data/brands/Atlas.md
name: TIFFANY SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Sofistikovaná a pohodlná pohovka v kombinaci
  dřevěných detailů a chromované konstrukce učiní každý prostor jedinečným. Je vyrobena
  z vysoce kvalitních konstrukcí a látek s dřevěnými a inoxovými prvky. Dřevěné obložení
  područek s lesklou inoxovou základnou tvoří kombinaci inovativního designu a vynikajícího
  řemeslného zpracování a díky tomu se stane dokonalým kouskem pro každé luxusní sídlo.
  Tato pohovka je jedinečná také svým prošíváním na bocích područek.</p>'
product_images:
- "/v1643026256/forestry/ATL_TIFFANY_sofa_1_vsblax.png"
- "/v1643026256/forestry/ATL_TIFFANY_sofa_2_xuegvp.png"
- "/v1643026256/forestry/ATL_TIFFANY_sofa_5_tp99mx.png"
- "/v1643026256/forestry/ATL_TIFFANY_sofa_6_pjumwg.png"
- "/v1643026256/forestry/ATL_TIFFANY_sofa_4_qi5udk.png"
- "/v1643026256/forestry/ATL_TIFFANY_sofa_3_ircfym.png"
dimensions: "<p>Dvojsed:</p><p><strong>Šířka: </strong>153 cm - <strong>Délka: </strong>89
  cm - <strong>Výška: </strong>79 cm - <strong>Výška sedu:</strong> 41 cm - <strong>Hloubka
  sedu:</strong> 54 cm</p><p>Trojsed:</p><p><strong>Šířka: </strong>218 cm - <strong>Délka:
  </strong>89 cm - <strong>Výška: </strong>79 cm - <strong>Výška sedu:</strong> 41
  cm - <strong>Hloubka sedu:</strong> 54 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka</p><p><strong>Sedák:</strong> HR pěna
  35/36 a HR pěna 35/30 ve vatovém potahu</p><p><strong>Opěradlo: </strong>pěna L
  25/15 ve vatovém potahu</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:
  </strong>kovové -inox nohy</p><p><strong>Možnosti</strong>: dřevěný rám v loketní
  opěrce mořený, nebo natřený barvou RAL, nebo s efektem „páleného“ dřeva</p>"
materials: <p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p>
price: "<p><strong>Cena od: </strong>31 300 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/18448698a19ff14c84a79c0e0a60fd0804cc6b17.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643026705/forestry/ATL_TIFFANY_sofa_8_moiqln.png"
- "/v1643026705/forestry/ATL_TIFFANY_sofa_7_ddwvwk.png"

---
