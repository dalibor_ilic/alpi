---
brand: data/brands/Woak.md
name: 'MALIN KOMODA S KOVOVÝM PODNOŽÍM '
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606485200/forestry/WHO_MALIN_komoda_s_kovov%C3%BDm_podno%C5%BE%C3%ADm_6_peqrv2.png"
- "/v1606485200/forestry/WHO_MALIN_komoda_s_kovov%C3%BDm_podno%C5%BE%C3%ADm_7_zviv5o.png"
- "/v1606485200/forestry/WHO_MALIN_komoda_s_kovov%C3%BDm_podno%C5%BE%C3%ADm_8_qexupi.png"
- "/v1606485191/forestry/WHO_MALIN_komoda_s_kovov%C3%BDm_podno%C5%BE%C3%ADm_1_gztg6c.png"
dimensions: "<p><strong>Šířka: </strong>120/180 cm - <strong>Délka: </strong>40/50
  cm - <strong>Výška:</strong> 75 cm</p>"
technical_info: "<p>Tato komoda má 3 zásuvky, 2 dvířka, kovové nohy a je vyrobena
  z masivního dřeva (dubu nebo ořechu) a má olejovou úpravu.</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 57 400 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_sideboard_wooden_legs_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1WQS1E2pRcFI6KgVHyGBVf8UOxr5Eit48/view?usp=sharing"
  title=""><strong>Stáhnout PDF </strong></a></p>'
product_photos:
- "/v1606485497/forestry/WHO_MALIN_komoda_s_kovov%C3%BDm_podno%C5%BE%C3%ADm_5_zrffqc.png"
- "/v1606485500/forestry/WHO_MALIN_komoda_s_kovov%C3%BDm_podno%C5%BE%C3%ADm_2_ewbvzj.png"
- "/v1606485500/forestry/WHO_MALIN_komoda_s_kovov%C3%BDm_podno%C5%BE%C3%ADm_3_fcalrj.png"
- "/v1606485500/forestry/WHO_MALIN_komoda_s_kovov%C3%BDm_podno%C5%BE%C3%ADm_4_td4hji.png"

---
