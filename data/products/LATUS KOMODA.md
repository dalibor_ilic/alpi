---
brand: data/brands/Artisan.md
name: 'LATUS KOMODA '
categories:
- Sideboard
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645442970/forestry/Product_foto.png_komoda_kopie_2_nk9hmv.png"
- "/v1645442970/forestry/Product_foto.png_komoda_kopie_3_tt3c5v.png"
- "/v1645442970/forestry/Product_foto.png_komoda_kopie_kqcnoc.png"
- "/v1645442970/forestry/Product_foto.png_komoda_x6wiap.png"
dimensions: "<p><strong>Šířka: </strong>180/200/220 cm - <strong>Délka: </strong>40
  cm<strong> </strong>- <strong>Výška: </strong>75 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka komody a předních částí: 2 cm </p><p>Interiér
  zásuvek: polotvrdá dřevovláknitá deska 1,6 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>96 800 Kč</p>"
product_info: ''
product_photos:
- "/v1645443442/forestry/Z_interi%C3%A9ru-3.png_komoda_sxoamr.png"
- "/v1645443442/forestry/Z_interi%C3%A9ru.png_komoda_nhwo8y.png"
- "/v1645443562/forestry/Z_interi%C3%A9ru.png_komoda_kopie_3_mdh2bk.png"
- "/v1645443481/forestry/Z_interi%C3%A9ru.png_komoda_kopie_agwwri.png"
- "/v1645443467/forestry/Z_interi%C3%A9ru.png_komoda_kopie_2_llfi4t.png"

---
