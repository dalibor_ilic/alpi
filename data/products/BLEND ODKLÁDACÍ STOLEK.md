---
brand: data/brands/Artisan.md
name: 'BLEND ODKLÁDACÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645437572/forestry/Product_foto_niymql.png"
- "/v1645437572/forestry/Product_foto-2_onor9q.png"
dimensions: "<p><strong>Šířka: </strong>45 cm - <strong>Délka: </strong>45 cm - <strong>Výška:
  </strong>55 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka spodní a horní desky: 2 cm</p><p>Vizuální
  tloušťka: 3 cm</p><p>Průměr nohou: spodní část 5x5 cm, horní část 5x5 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 22 700 Kč</p>"
product_info: ''
product_photos: []

---
