---
brand: data/brands/Artisan.md
name: LATUS STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605011796/forestry/ART_LATUS_st%C5%AFl_7_j0pxgf.png"
- "/v1605011796/forestry/ART_LATUS_st%C5%AFl_1_mca7bc.png"
- "/v1605011798/forestry/ART_LATUS_st%C5%AFl_4_n2ncou.png"
dimensions: "<p><strong>Šířka: </strong>70 - 300 cm - <strong>Délka:  </strong>70
  -<strong> </strong>100 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a za příplatek 10% možnost šíře
  110 cm.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální tloušťka desky: 6 cm</p><p>Průměr
  nohou : spodní část 3 cm, vrchní část 6 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>40 100 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605606749/forestry/ART_LATUS_st%C5%AFl_5_mccijd.png"
- "/v1605606748/forestry/ART_LATUS_st%C5%AFl_3_cwygoz.png"
- "/v1605606748/forestry/ART_LATUS_st%C5%AFl_2_nwxmhu.png"

---
