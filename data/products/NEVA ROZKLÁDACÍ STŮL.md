---
brand: data/brands/Artisan.md
name: 'NEVA ROZKLÁDACÍ STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605010902/forestry/ART_NEVA_rozkl%C3%A1dac%C3%AD_st%C5%AFl_5_w4djoq.png"
- "/v1605010904/forestry/ART_NEVA_rozkl%C3%A1dac%C3%AD_st%C5%AFl_8_qobhgo.png"
- "/v1605010909/forestry/ART_NEVA_rozkl%C3%A1dac%C3%AD_st%C5%AFl_12_rhosk3.png"
- "/v1605010900/forestry/ART_NEVA_rozkl%C3%A1dac%C3%AD_st%C5%AFl_3_pwcv9d.png"
- "/v1605010902/forestry/ART_NEVA_rozkl%C3%A1dac%C3%AD_st%C5%AFl_6_w8v9cy.png"
dimensions: "<p><strong>Šířka: </strong>160 - 320 cm - <strong>Délka: </strong>94
  - 104 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 2 cm</p><p>Vizuální
  tloušťka desky: 2 cm</p><p>Průměr nohou: spodní část 6x6 cm, vrchní část 7,5x7,5
  cm </p><p>Rozměry rozšíření: 1x70 a 1x100 cm </p><p>Stůl o rozměru 160 cm má rozšíření
  o velikosti 70 cm.</p><p>Stoly o rozměrech 180, 200, 220 cm mají rozšíření o velikosti
  100 cm. </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 96 700 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605608257/forestry/ART_NEVA_rozkl%C3%A1dac%C3%AD_st%C5%AFl_13_les0k3.png"
- "/v1605608257/forestry/ART_NEVA_rozkl%C3%A1dac%C3%AD_st%C5%AFl_12_rujito.png"
- "/v1605608257/forestry/ART_NEVA_rozkl%C3%A1dac%C3%AD_st%C5%AFl_11_qu3iu0.png"
- "/v1605608257/forestry/ART_NEVA_rozkl%C3%A1dac%C3%AD_st%C5%AFl_10_f4kmm6.png"
- "/v1605608257/forestry/ART_NEVA_rozkl%C3%A1dac%C3%AD_st%C5%AFl_9_jntol8.png"

---
