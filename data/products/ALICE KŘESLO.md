---
brand: data/brands/Atlas.md
name: ALICE KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Ztělesnění
  módy a detailu, smysl pro svěžest, který Vám umožní vést luxusní život se stylem.</p>'
product_images:
- "/v1648835001/forestry/Product_foto_d7qjvg.png"
- "/v1648835001/forestry/Product_foto-6_wtxcoj.png"
- "/v1648835000/forestry/Product_foto-5_hkpfzf.png"
- "/v1648835001/forestry/Product_foto-4_yppzmz.png"
- "/v1648835000/forestry/Product_foto-3_jjn841.png"
- "/v1648835001/forestry/Product_foto-2_rhucgn.png"
dimensions: "<p><strong>Šířka: </strong>76<strong> </strong>cm - <strong>Délka: </strong>66
  cm - <strong>Výška: </strong>72 cm - <strong>Výška sedu:</strong> 46 cm - <strong>Hloubka
  sedu:</strong> 55 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka</p><p><strong>Sedák: </strong>vysoce
  elastická polyuretanová HR pěna 35/36</p><p><strong>Opěradlo: </strong>vysoce elastická
  polyuretanová pěna N 25/38</p><p><strong>Nožičky: </strong>otočný mechanismus, MDF
  základová lazura, polyuretanový lak, krycí polyuretanová barva dle vzorníku RAL</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/9914d65c8e4dd8ba3a88a576d7b27e5fa3553418.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1648836672/forestry/Z_interi%C3%A9ru-3_iqzap7.png"
- "/v1648836663/forestry/Z_interi%C3%A9ru-2_sxbo1g.png"
- "/v1648836652/forestry/Z_interi%C3%A9ru_h6nmnv.png"

---
