---
brand: data/brands/Artisan.md
name: 'NEVA ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644251717/forestry/Product_foto-42_zsdfdz.png"
- "/v1644251717/forestry/Product_foto-43_frvvi0.png"
- "/v1604666550/forestry/ART_NEVA_%C5%BEidle_1_wpdqli.png"
- "/v1604666550/forestry/ART_NEVA_%C5%BEidle_2_cue4rn.png"
- "/v1644251757/forestry/Product_foto-44_bimzjq.png"
- "/v1644251914/forestry/Product_foto0007_h0vtan.png"
- "/v1604666565/forestry/ART_NEVA_%C5%BEidle_14_ykcbf2.png"
dimensions: "<p><strong>Šířka: </strong>52 cm - <strong>Délka: </strong>49,2 cm -
  <strong>Výška: </strong>78,3 cm - <strong>Výška sedu: </strong>44 cm - <strong>Hloubka
  sedu: </strong>47,5 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 50,8x47,5 cm</p><p>Průměr nohou: 3,3 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 19 500 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604666777/forestry/ART_NEVA_%C5%BEidle_12_sruwd4.png"
- "/v1604666787/forestry/ART_NEVA_%C5%BEidle_8_tcqxcj.png"
- "/v1604666787/forestry/ART_NEVA_%C5%BEidle_10_w7linf.png"
- "/v1604666789/forestry/ART_NEVA_%C5%BEidle_11_nnuesr.png"
- "/v1604666786/forestry/ART_NEVA_%C5%BEidle_9_f8uhqa.png"
- "/v1604666773/forestry/ART_NEVA_%C5%BEidle_13_ibltee.png"
- "/v1604666790/forestry/ART_NEVA_%C5%BEidle_7_qqdadq.png"
- "/v1604666789/forestry/ART_NEVA_%C5%BEidle_6_w4bo7z.png"
- "/v1604666789/forestry/ART_NEVA_%C5%BEidle_5_rvbh9v.png"

---
