---
brand: data/brands/Extraform.md
name: 'VARIO KŘESLO '
categories:
- Arm chair
description: <p>Výrobce<strong> </strong>:<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Flexibilní
  a elegantně navržená pohovka harmonických proporcí. Nenápadný detaily jako schované
  dřevěné nohy a lehce zaoblené opěradlo a loketní opěrky zdůrazňuji jedinečnost tohoto
  kusu nábytku.</p>
product_images:
- "/v1645090087/forestry/EXT_VARIO_armchair_2_su5d99.png"
- "/v1645090087/forestry/EXT_VARIO_armchair_1_ggorni.png"
- "/v1645090087/forestry/EXT_VARIO_armchair_4_d4qenw.png"
- "/v1645090087/forestry/EXT_VARIO_armchair_5_aozggy.png"
- "/v1645090086/forestry/EXT_VARIO_armchair_3_ywnnjc.png"
dimensions: "<p><strong>Šířka: </strong>82 cm - <strong>Délka:</strong> 105 cm - <strong>Výška:</strong>
  88 cm - <strong>Výška sedu:</strong> 46 cm - <strong>Hloubka sedu:</strong> 62 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, překližky o tloušťce
  18 mm, lepenky.</p><p class="Paragraph SCXW200721506 BCX4">Vysoce elastická HR pěna,
  500 g tepelně pojená bavlněná vata, rouno, vinuté pružiny.</p><p class="Paragraph
  SCXW200721506 BCX4">Ergonomická pěna RG 25, 300 g bavlněná vatová směs s tepelnou
  vazbou.</p><p class="Paragraph SCXW200721506 BCX4">Pěna RG 30, 300 g tepelně vázané
  bavlněné výplně.</p><p class="Paragraph SCXW200721506 BCX4">Nohy z bukového dřeva,
  mořené a lakované. K dispozici v šesti barvách.</p><p class="Paragraph SCXW200721506
  BCX4">Pevná rohová pohovka.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 20 460 Kč </p>"
product_info: ''
product_photos: []

---
