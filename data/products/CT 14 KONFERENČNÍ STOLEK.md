---
brand: data/brands/Atlas.md
name: CT 14 KONFERENČNÍ STOLEK
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Při pohledu na tento model můžete vidět
  původní krásu použitých přírodních materiálů, jako je mramor, nebo dřevo. Tyto dva
  materiály s inoxovou základnou tvoří dokonalou kombinaci, která dodá vašemu prostoru
  pocit elegance. Tento model se vyrábí v různých rozměrech a s různými tvary podstavce,
  takže si snadno můžete vytvořit dokonalou sestavu.</p>'
product_images:
- "/v1643284604/forestry/ATL_CT14_konferen%C4%8Dn%C3%AD_stolek_5_wmg4a2.png"
- "/v1643284604/forestry/ATL_CT14_konferen%C4%8Dn%C3%AD_stolek_4_ci3eid.png"
- "/v1643284605/forestry/ATL_CT14_konferen%C4%8Dn%C3%AD_stolek_1_jph5uh.png"
dimensions: "<p>Rozměr I.</p><p><strong>Šířka: </strong>90 cm - <strong>Délka: </strong>90
  cm - <strong>Výška: </strong>41 cm</p><p>Rozměr II.</p><p><strong>Šířka: </strong>70
  cm - <strong>Délka: </strong>70 cm - <strong>Výška: </strong>41 cm</p><p>Rozměr
  III.</p><p><strong>Šířka: </strong>35 cm - <strong>Délka: </strong>35 cm - <strong>Výška:
  </strong>56 cm</p>"
technical_info: "<p><strong>Materiál:</strong> MDF, buk, mramor</p><p><strong>Nožičky:
  </strong>INOX</p>"
materials: ''
price: "<p><strong>Cena od:</strong> 6 500 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/e72e47f9433f1e58d0257c19216c4ca991067145.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643284731/forestry/ATL_CT14_konferen%C4%8Dn%C3%AD_stolek_3_ei5ylp.png"
- "/v1643284728/forestry/ATL_CT14_konferen%C4%8Dn%C3%AD_stolek_2_s0xuyo.png"

---
