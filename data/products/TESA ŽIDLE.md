---
brand: data/brands/Artisan.md
name: 'TESA ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644485107/forestry/Product_foto_kopie_2_kfqns1.png"
- "/v1644484675/forestry/Product_foto-53_fk5yo1.png"
- "/v1644485108/forestry/Product_foto_kopie_3_kjmdtq.png"
- "/v1644484675/forestry/Product_foto-54_nrxgeu.png"
dimensions: "<p><strong>Šířka: </strong>53<strong> </strong>cm - <strong>Délka: </strong>55
  cm - <strong>Výška: </strong>73,6 cm - <strong>Výška sedu: </strong>45 cm - <strong>Hloubka
  sedu: </strong>48,4 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 48,4 x 40,1 cm</p><p>Tloušťka sedací a opěrné plochy: 2,3 cm</p><p>Průměr
  nohou: 3 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 15 100 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604668930/forestry/ART_TESA_%C5%BEidle_5_wknwev.png"
- "/v1604668931/forestry/ART_TESA_%C5%BEidle_4_ovicwk.png"
- "/v1604668929/forestry/ART_TESA_%C5%BEidle_3_qp7h3n.png"
- "/v1604668931/forestry/ART_TESA_%C5%BEidle_2_ziosb1.png"

---
