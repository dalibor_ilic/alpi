---
brand: data/brands/Artisan.md
name: MELA STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605607509/forestry/ART_MELA_st%C5%AFl_2_kcpfcr.png"
- "/v1605607509/forestry/ART_MELA_st%C5%AFl_1_ns3cdz.png"
dimensions: "<p><strong>Šířka: </strong>140-240<strong> </strong>cm - <strong>Délka:
  </strong>90 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava a za příplatek 10% možnost šíře
  100 cm.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální tloušťka desky: 4 cm</p><p>Průměr
  nohou: spodní část 6x6 cm, vrchní část 7,5x7,5 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 62 500 Kč</p>"
product_info: ''
product_photos:
- "/v1605607816/forestry/ART_MELA_st%C5%AFl_3_sr32ew.png"

---
