---
brand: data/brands/Extraform.md
name: FLYTTE SOFA
categories:
- Sofa
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Flytte
  díky svému nadčasovému a klasickému designu se hodí do jakéhokoliv prostoru. Vysoká
  úroveň flexibility umožňuje přizpůsobit se jakémukoliv požadavku, a to i zpětně.
  Flytte je snadné zamilovat si.</p>'
product_images:
- "/v1644504870/forestry/EXT_FLYTTE_sofa_2_cwbzvp.png"
- "/v1644504870/forestry/EXT_FLYTTE_sofa_4_eoth0s.png"
- "/v1644504891/forestry/EXT_FLYTTE_sofa_13_aejrhb.png"
- "/v1644504868/forestry/EXT_FLYTTE_sofa_1_chls5e.png"
- "/v1644504870/forestry/EXT_FLYTTE_sofa_5_ollgj9.png"
- "/v1644504868/forestry/EXT_FLYTTE_sofa_8_karyke.png"
- "/v1644504869/forestry/EXT_FLYTTE_sofa_6_qgk9sy.png"
- "/v1644504870/forestry/EXT_FLYTTE_sofa_7_h7kzjv.png"
- "/v1644504870/forestry/EXT_FLYTTE_sofa_3_gad5pv.png"
- "/v1644504869/forestry/EXT_FLYTTE_sofa_9_fdlqk8.png"
- "/v1644504870/forestry/EXT_FLYTTE_sofa_10_wetarv.png"
- "/v1644504891/forestry/EXT_FLYTTE_sofa_12_srtkau.png"
- "/v1644504890/forestry/EXT_FLYTTE_sofa_11_lh3s20.png"
dimensions: "<p><strong>Výška:</strong> 71 cm - <strong>Výška sedu: </strong>43<strong>
  </strong>cm - <strong>Hloubka sedu: </strong>60 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, 18 mm silná překližka,
  MDF deska, lepenka.</p><p class="Paragraph SCXW112440476 BCX4">Sedáky z vysoce elastické
  pěny HR, 100 g tepelně pojené plsti, elastických popruhů.</p><p class="Paragraph
  SCXW112440476 BCX4">Opěrky z vysoce elastická HR pěny, 100 g tepelně pojené plsti.</p><p
  class="Paragraph SCXW112440476 BCX4">Nohy z dubového dřeva, naolejované. Možnost
  z bukového dřeva v šesti barvách.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>45 900 Kč </p>"
product_info: ''
product_photos:
- "/v1644505062/forestry/EXT_FLYTTE_sofa_19_ijqslp.png"
- "/v1644505022/forestry/EXT_FLYTTE_sofa_14_hm2q9a.png"
- "/v1644505024/forestry/EXT_FLYTTE_sofa_15_odwciy.png"
- "/v1644505068/forestry/EXT_FLYTTE_sofa_16_ikfi6e.png"
- "/v1644505068/forestry/EXT_FLYTTE_sofa_17_r82trl.png"
- "/v1644505069/forestry/EXT_FLYTTE_sofa_18_uwsa8t.png"

---
