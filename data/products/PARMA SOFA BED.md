---
brand: data/brands/Atlas.md
name: PARMA SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Model moderní rozkládací pohovky, která
  svým designem, funkčností a pohodlím vyhovuje různým vkusům a definicím krásy. Tato
  pohovka je jedinečná svou jednoduchostí. Jednoduché tvary obohacené dekorativními
  stehy přispívají k estetické hodnotě tohoto modelu.</p>'
product_images:
- "/v1643019581/forestry/ATL_PARMA_sofa_bed_1_xoc0qy.png"
- "/v1643019590/forestry/ATL_PARMA_sofa_bed_4_hgvv4l.png"
dimensions: "<p>Dvojsed I.</p><p><strong>Šířka: </strong>183 cm - <strong>Délka: </strong>100
  cm - <strong>Výška: </strong>86 cm - <strong>Výška sedu:</strong> 52 cm- <strong>Hloubka
  sedu:</strong> 61 cm</p><p>Dvojsed II.</p><p><strong>Šířka: </strong>203 cm - <strong>Délka:
  </strong>100 cm - <strong>Výška: </strong>86 cm - <strong>Výška sedu:</strong> 52
  cm- <strong>Hloubka sedu:</strong> 61 cm</p><p>Trojsed I.</p><p><strong>Šířka: </strong>223
  cm - <strong>Délka: </strong>100 cm - <strong>Výška: </strong>86 cm - <strong>Výška
  sedu:</strong> 52 cm- <strong>Hloubka sedu:</strong> 61 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 ve vatovém potahu</p><p><strong>Opěradlo: </strong>M pěna 18/15 ve
  vatovém potahu</p><p><strong>Nožičky: </strong>kovové nohy</p><p><strong>Možnosti:</strong>
  mechanismus rozkládací pohovky</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>54 000 Kč<strong> </strong></p><p><strong>Cena
  v látce od: </strong>31 300 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/690a5f3e2cad6b97ff7bb193ae225fed03c925ef.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643019927/forestry/ATL_PARMA_sofa_bed_3_ktkz7f.png"
- "/v1643019926/forestry/ATL_PARMA_sofa_bed_2_zcjed3.png"

---
