---
brand: data/brands/Atlas.md
name: SAMOA SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Minimalistický vzhled, harmonie tvarů
  a sedáky vyplněné peřím díky nimž je tento model dokonale pohodlný. Dřevěný rám
  v kombinaci s kovovými nohami jej činí sofistikovaným, zatímco pohyblivé polštáře
  jej odlišují. Speciální design této pohovky nenechá nikoho lhostejným.</p>'
product_images:
- "/v1643022042/forestry/ATL_SAMOA_sofa_1_ugu9it.png"
- "/v1643022043/forestry/ATL_SAMOA_sofa_5_tz3dl8.png"
- "/v1643022043/forestry/ATL_SAMOA_sofa_6_uvwqhd.png"
- "/v1643022043/forestry/ATL_SAMOA_sofa_8_wqeuqs.png"
- "/v1643022043/forestry/ATL_SAMOA_sofa_9_hyqpsq.png"
- "/v1643022043/forestry/ATL_SAMOA_sofa_10_lykmkr.png"
- "/v1643022043/forestry/ATL_SAMOA_sofa_7_l3g03j.png"
dimensions: "<p><strong>Šířka: </strong>312 cm - <strong>Délka: </strong>177 cm -
  <strong>Výška: </strong>79 cm - <strong>Výška sedu:</strong> 42 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo </p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> HR pěna 35/36, peří, paměťová pěna
  a silikon ve vatovém potahu</p><p><strong>Opěradlo: </strong>kovový rám, pěna N
  25/38, HR 25/30 ve vatovém potahu</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:
  </strong>kovové nohy lakované v barvě RAL</p><p><strong>Možnosti:</strong> dřevěný
  rám, základna mořená, nebo natřená v barvě RAL, nebo s efektem „páleného“ dřeva</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>91 300 Kč </p><p><strong>Cena v látce
  od: </strong>67 500 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/51668d8a033de8ba3de9b802301a23dc2496b675.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643022696/forestry/ATL_SAMOA_sofa_4_immsqx.png"
- "/v1643022696/forestry/ATL_SAMOA_sofa_3_wyqh9p.png"
- "/v1643022696/forestry/ATL_SAMOA_sofa_2_ipzpyd.png"

---
