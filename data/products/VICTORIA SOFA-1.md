---
brand: data/brands/Atlas.md
name: VICTORIA SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Ve
  světě luxusních pohovek záleží také na velikosti. Victoria je o maximalizaci prostoru,
  ale zároveň se nemusíte bát, že Váš útulný domov vzplane, jelikož je navržena tak,
  aby vnesla nádech moderního designu, poskytla pohodlí a pokud toužíte po útulném
  domově bez potíží s nedostatkem místa, tak právě pohovka Victoria je pro Vás správnou
  volbou.</p>'
product_images:
- "/v1648813506/forestry/Product_foto_fr5euz.png"
- "/v1648813506/forestry/Product_foto-6_cp18vb.png"
- "/v1648813506/forestry/Product_foto-5_l2swf9.png"
- "/v1648813506/forestry/Product_foto-4_wtlr8a.png"
- "/v1648813506/forestry/Product_foto-3_zobeoy.png"
- "/v1648813506/forestry/Product_foto-2_tegzjn.png"
- "/v1648813506/forestry/Product_foto_kopie_qkmxo6.png"
dimensions: "<p>Dvojsed: <strong>Šířka: </strong>265/295<strong> </strong>cm - <strong>Délka:
  </strong>105 cm - <strong>Výška: </strong>71 cm - <strong>Výška sedu:</strong> 43
  cm - <strong>Hloubka sedu:</strong> 90 cm</p><p>Trojsed: <strong>Šířka: </strong>385<strong>
  </strong>cm - <strong>Délka: </strong>105 cm - <strong>Výška: </strong>71 cm - <strong>Výška
  sedu:</strong> 43 cm - <strong>Hloubka sedu:</strong> 90 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, kůže</p><p><strong>Sedák: </strong>vysoce
  elastická polyuretanová HR pěna 35/36</p><p><strong>Opěradlo: </strong>vysoce elastická
  polyuretanová pěna N 25/38, paměťová pěna, silikon</p><p><strong>Obloukové vlnové
  pružiny</strong></p><p><strong>Nožičky: </strong>plastové</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p><p>Kůže:
  <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"><strong>CAT
  300-1000</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/e865fb7c644391333adbee9c2813a4db5531fe26.pdf"
  title=""><strong>Stáhnout PDF</strong></a><strong> </strong></p>'
product_photos:
- "/v1648813843/forestry/Z_interi%C3%A9ru-3_y8isfh.png"
- "/v1648813844/forestry/Z_interi%C3%A9ru-5_lt80p0.png"
- "/v1648813843/forestry/Z_interi%C3%A9ru-4_gzkh2l.png"
- "/v1648813844/forestry/Z_interi%C3%A9ru-2_cd2qwx.png"

---
