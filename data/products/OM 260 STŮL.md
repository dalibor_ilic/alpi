---
brand: data/brands/Atlas.md
name: OM 260 STŮL
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Tento model má výsuvnou vložku, díky čemuž
  je velmi funkční a přizpůsobitelný prostorům různých velikostí. Je vyroben v kombinaci
  masivního dřeva a dýhované MDF desky. Jeho klasický vzhled nepodceňuje estetickou
  hodnotu, díky tomu jej lze dokonale implementovat do všech druhů prostor</p>'
product_images:
- "/v1643279530/forestry/ATL_OM260_st%C5%AFl_1_myniz0.png"
- "/v1643279530/forestry/ATL_OM260_st%C5%AFl_5_ejhdmg.png"
dimensions: "<p><strong>Šířka: </strong>160/220 cm - <strong>Délka: </strong>90 cm
  - <strong>Výška: </strong>78 cm</p>"
technical_info: "<p><strong>Materiál:</strong> masivní dřevo a dýhovaná MDF deska,
  buk/dub</p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od:</strong> 16 700 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/5646ef1b39a077a121696d33a02763f9ce8c64e1.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643279634/forestry/ATL_OM260_st%C5%AFl_4_broou8.png"
- "/v1643279635/forestry/ATL_OM260_st%C5%AFl_3_lwmpt5.png"
- "/v1643279635/forestry/ATL_OM260_st%C5%AFl_2_sizqju.png"

---
