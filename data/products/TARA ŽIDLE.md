---
brand: data/brands/Artisan.md
name: 'TARA ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644253102/forestry/Product_foto-2_dgxodm.png"
- "/v1644253103/forestry/Product_foto-4_yeh92x.png"
- "/v1644253103/forestry/Product_foto-3_rdt2uo.png"
- "/v1644252759/forestry/Product_foto-50_oic5ue.png"
- "/v1644253103/forestry/Product_foto_cl2kui.png"
dimensions: "<p><strong>Šířka: </strong>52 cm - <strong>Délka: </strong>49,2 cm -
  <strong>Výška: </strong>78 cm - <strong>Výška sedu: </strong>44 cm - <strong>Hloubka
  sedu: </strong>47,5 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Velikost sedu: 50,8x47,5
  cm</p><p>Průměr nohou: 3,3 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>22 500 Kč</p>"
product_info: ''
product_photos:
- "/v1604668161/forestry/ART_TARA_%C5%BEidle_2_odievn.png"
- "/v1604668161/forestry/ART_TARA_%C5%BEidle_3_sob5rf.png"
- "/v1604668161/forestry/ART_TARA_%C5%BEidle_6_uuzsfd.png"
- "/v1604668162/forestry/ART_TARA_%C5%BEidle_5_un5emt.png"
- "/v1604668160/forestry/ART_TARA_%C5%BEidle_4_ign4b5.png"

---
