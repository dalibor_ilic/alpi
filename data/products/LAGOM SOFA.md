---
brand: data/brands/Atlas.md
name: LAGOM SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Sedací
  souprava Lagom s nádechem Švédska - ani moc, ani málo, do Vašeho interiéru přinese
  klid a teplo, je vytvořena pro relax a o své hosty pečuje do nejmenšího detailu,
  vřelým a velkorysým způsobem. </p>'
product_images:
- "/v1648651255/forestry/Product_foto_rnjxma.png"
- "/v1648651255/forestry/Product_foto-5_jfdbgm.png"
- "/v1648651255/forestry/Product_foto-4_ddmgvp.png"
- "/v1648651255/forestry/Product_foto-3_o3tf4e.png"
- "/v1648651255/forestry/Product_foto-2_hilind.png"
dimensions: "<p>Dvojsed:<strong> Šířka: </strong>96<strong> </strong>cm - <strong>Délka:
  </strong>192 cm - <strong>Výška: </strong>81 cm - <strong>Výška sedu:</strong> 60
  cm - <strong>Hloubka sedu:</strong> 23 cm</p><p>Trojsed: <strong>Šířka: </strong>96<strong>
  </strong>cm - <strong>Délka: </strong>255 cm - <strong>Výška: </strong>81 cm - <strong>Výška
  sedu:</strong> 60 cm - <strong>Hloubka sedu:</strong> 23 cm</p><p>Rohová: <strong>Šířka:
  </strong>201<strong> </strong>cm - <strong>Délka: </strong>261 cm - <strong>Výška:
  </strong>81 cm - <strong>Výška sedu:</strong> 60 cm - <strong>Hloubka sedu:</strong>
  23 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, kůže</p><p><strong>Sedák: </strong>vysoce
  elastická polyuretanová HR pěna 35/36, silikonové kuličky v potahu polštáře</p><p><strong>Opěradlo:
  </strong>vysoce elastická polyuretanová HR pěna 35/36, L paměťová pěna 25/15, silikonové
  kuličky v potahu polštáře</p><p><strong>Nožičky: </strong>dřevěný rám - lazura,
  polyuretanový lak, krycí polyuretanová barva dle vzorníku RAL</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p><p>Kůže:
  <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"><strong>CAT
  300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"><strong>DUB</strong></a></p>'
price: ''
product_info: "<p></p>"
product_photos:
- "/v1648651378/forestry/Z_interi%C3%A9ru-5_m0swje.png"
- "/v1648651376/forestry/Z_interi%C3%A9ru-3_kvtgtw.png"
- "/v1648651377/forestry/Z_interi%C3%A9ru-2_xf6dda.png"
- "/v1648651377/forestry/Z_interi%C3%A9ru-4_mrlsz0.png"
- "/v1648651373/forestry/Z_interi%C3%A9ru_sqjmcu.png"

---
