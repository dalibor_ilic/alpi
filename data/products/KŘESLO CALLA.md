---
brand: data/brands/Atlas.md
name: CALLA KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Elegantní zakřivený tvar základny opěradla
  je první, čeho si na tomto krásném křesle všimnete. V kombinaci se zaoblenou sedací
  částí tvoří harmonickou kompozici, a to zejména díky dekorativnímu štepování na
  opěradle, jehož nakloněná poloha slibuje nejvyšší pohodlí. Doplňte design svého
  domova, nebo kancelářských prostor, je to jistě ta správná volba!</p>'
product_images:
- "/v1643108116/forestry/ATL_CALLA_k%C5%99eslo_1_vzzxiw.png"
dimensions: "<p><strong>Šířka: </strong>74 cm - <strong>Délka: </strong>67 cm - <strong>Výška:
  </strong>85 cm - <strong>Výška sedu:</strong> 49 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Podpěra:</strong>
  desky</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36</p><p><strong>Opěradlo: </strong>HR pěna 25/38</p><p><strong>Područky:
  </strong>součást konstrukce</p><p><strong>Nožičky: </strong>kovové</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>21 300 Kč </p><p><strong>Cena v látce
  od: </strong>15 900 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/c154279472903f1aff1706596095c9a50f36bd3f.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643108341/forestry/ATL_CALLA_k%C5%99eslo_2_qakw5o.png"
- "/v1643108342/forestry/ATL_CALLA_k%C5%99eslo_4_qf32pd.png"
- "/v1643108342/forestry/ATL_CALLA_k%C5%99eslo_3_ncbmn9.png"

---
