---
brand: data/brands/Extraform.md
name: ISOLA SOFA
categories:
- Sofa
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Modulární
  pohovka Isola plněná peřím, s flexibilními opěrkami, může fungovat jako celek nebo
  jako kombinace několika jednotlivých prvků. Její vzhled zdůrazňuje její mimořádný
  komfort. Charakteristické hrany zvýrazňují profil pohovky a dodávají jí šarm a autentičnost.</p>'
product_images:
- "/v1644506405/forestry/EXT_ISOLA_sofa_1_y98z8y.png"
- "/v1644506417/forestry/EXT_ISOLA_sofa_4_lzzvrl.png"
- "/v1644506416/forestry/EXT_ISOLA_sofa_5_w59faq.png"
dimensions: "<p><strong>Šířka: </strong>301 cm - <strong>Délka:</strong> 177 cm -
  <strong>Výška:</strong> 75/98 cm - <strong>Výška sedu:</strong> 41 cm - <strong>Hloubka
  sedu:</strong> 61 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, dřevotříska o tloušťce
  16 mm, lepenka, překližka o tloušťce 18 mm, 3 mm silná sololitová deska.</p><p class="Paragraph
  SCXW104555200 BCX4">Kombinace tvrdé pěny a vysoce elastické HR pěny, polštáře plněné
  peřím, 300 g termální bavlněná vata, vinuté pružiny.</p><p class="Paragraph SCXW104555200
  BCX4">Ergonomická vysoce elastická pěna, polštáře plné peří, gumičky, nastavitelná
  opěrka hlavy.</p><p class="Paragraph SCXW104555200 BCX4">Nohy z bukového dřeva,
  mořené a lakované. K dispozici v šesti barvách. Chromované nohy jsou volitelné.</p><p
  class="Paragraph SCXW104555200 BCX4">Pevná rohová pohovka.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>64 900 Kč </p>"
product_info: ''
product_photos:
- "/v1644506385/forestry/EXT_ISOLA_sofa_3_xlas5y.png"
- "/v1644506387/forestry/EXT_ISOLA_sofa_2_lavzzd.png"

---
