---
brand: data/brands/Artisan.md
name: 'INVITO NÁSTĚNNÉ MODULY '
categories:
- Shelf
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645440076/forestry/1Product_fotowall_modul_vxdfl9.png"
dimensions: "<p></p>"
technical_info: "<p>Maisivní dřevo.</p><p>Tloušťka polic: 2 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO </strong></a></p>
price: "<p><strong>Cena od: </strong>9 500 Kč</p>"
product_info: ''
product_photos:
- "/v1645440085/forestry/1Z_interi%C3%A9ru-2wall_modul_qrpiwl.png"
- "/v1645440086/forestry/1Z_interi%C3%A9ruwall_modul_biuboc.png"

---
