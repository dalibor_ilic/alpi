---
brand: data/brands/Artisan.md
name: COCO STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645707277/forestry/Product_foto_fdlf8w.png"
- "/v1645707276/forestry/Product_foto-2_dgfzrb.png"
dimensions: "<p><strong>Šířka: </strong>90 cm - <strong>Délka: </strong>160/180/200/220/240
  cm - <strong>Výška: </strong>76 cm</p>"
technical_info: ''
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>50 600 Kč</p>"
product_info: ''
product_photos:
- "/v1645707419/forestry/Z_interi%C3%A9ru_xzwxs0.png"
- "/v1645707419/forestry/Z_interi%C3%A9ru-2_dyis8b.png"

---
