---
brand: data/brands/Atlas.md
name: 'CORNER KŘESLO '
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Moderní design a neobvyklý tvar dělají
  otočné křeslo Corner výjimečným. Tvar válečku jako základny je neobvyklý a zajímavý
  tvar. Pokud jste kreativní člověk a máte rádi neobvyklé kusy nábytku, křeslo Corner
  je pro vás tou správnou volbou a otočná základna přispívá k jeho pohodlí.</p>'
product_images:
- "/v1643109470/forestry/ATL_CORNER_k%C5%99eslo_1_bvphle.png"
dimensions: "<p><strong>Šířka: </strong>77 cm - <strong>Délka: </strong>77 cm - <strong>Výška:
  </strong>67 cm - <strong>Výška sedu:</strong> 44 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> L pěna 25/15</p><p><strong>Opěradlo:
  </strong>L pěna 25/15</p><p><strong>Područky: </strong>L pěna 25/15</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>otočný mechanismus</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>25 400 Kč </p><p><strong>Cena v látce
  od: </strong>17 000 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/51e5bdae6aef133c028aab822713e5c7a708e5ce.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643109638/forestry/ATL_CORNER_k%C5%99eslo_3_yonvee.png"
- "/v1643109639/forestry/ATL_CORNER_k%C5%99eslo_2_jerea1.png"

---
