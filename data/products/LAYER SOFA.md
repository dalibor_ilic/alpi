---
brand: data/brands/Extraform.md
name: 'LAYER SOFA '
categories:
- Sofa bed system
description: <p>Výrobce :<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Tato
  pohodlná a sestavitelná pohovka má klasický jednoduchý design, který má spací mechanismus
  a lze jej rozložit na postel jediným pohybem. Má nenápadný a skrytý úložný box.
  Díky svému jednoduchému designu a funkčnosti se snadno vejde do jakéhokoliv typu
  prostoru.</p>
product_images:
- "/v1644506781/forestry/EXT_LAYER_sofa_bed_7_e4yjbh.png"
- "/v1644506781/forestry/EXT_LAYER_sofa_bed_6_o8bsyb.png"
- "/v1644506781/forestry/EXT_LAYER_sofa_bed_8_cucn2u.png"
- "/v1644506807/forestry/EXT_LAYER_sofa_bed_1_sjuluo.png"
- "/v1644506795/forestry/EXT_LAYER_sofa_bed_5_zffydh.png"
dimensions: '<p><strong>Rohová sedací souprava - Šířka: </strong>291<strong> </strong>cm
  - <strong>Délka: </strong>241 cm - <strong>Výška: </strong>93 cm - <strong>Výška
  sedu: </strong>47 cm - <strong>Hloubka sedu: </strong>56 cm</p><p class="Paragraph
  SCXW176176206 BCX4"><strong>Dvojsed - Šířka: </strong>186 -206<strong> </strong>cm
  - <strong>Délka: </strong>98 cm - <strong>Výška:</strong> 93cm - <strong>Výška sedu:
  </strong>47 cm - <strong>Hloubka sedu: </strong>56<strong> </strong>cm</p>'
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, dřevotříska o tloušťce
  16 mm, lepenka, překližka o tloušťce 18 mm, 3 mm silná sololitová deska.</p><p class="Paragraph
  SCXW69306336 BCX4">Sedák - Vysoce elastická HR pěna, 300 g tepelně pojené bavlněné
  vaty, cívka.</p><p class="Paragraph SCXW69306336 BCX4">Opěrky - Ergonomická vysoce
  elastická pěna, gumičky.</p><p class="Paragraph SCXW69306336 BCX4">Nohy z bukového
  dřeva, mořené a lakované. K dispozici v šesti barvách.</p><p class="Paragraph SCXW69306336
  BCX4">Výsuvná postel je volitelná pro dvoumístné a třímístné pohovky.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>45 700 Kč</p>"
product_info: ''
product_photos:
- "/v1644506930/forestry/EXT_LAYER_sofa_bed_2_h0dtys.png"
- "/v1644506932/forestry/EXT_LAYER_sofa_bed_3_lmvoc9.png"
- "/v1644506931/forestry/EXT_LAYER_sofa_bed_4_sa8wmr.png"

---
