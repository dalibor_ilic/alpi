---
brand: data/brands/Atlas.md
name: MARKO SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Model jednoduchého designu s mimořádně
  kvalitním lůžkem na spaní. Nabízí tři různé typy područek a poskytuje velký prostor
  pro personalizaci a kreativitu. Celá pohovka se rozkládá na lůžko s matrací určenou
  pro každodenní spaní. Může být vyrobena v některé z vysoce kvalitních kůží a látek
  z naší skvělé nabídky. Přidáním opěrky hlavy bude tato pohovka ještě pohodlnější
  a vhodnější pro krátké přestávky i delší odpočinek.</p><p></p>'
product_images:
- "/v1643014201/forestry/ATL_MARCO_sofa_bed_1_hoye4y.png"
- "/v1643014203/forestry/ATL_MARCO_sofa_bed_25_bhwcpr.png"
- "/v1643014202/forestry/ATL_MARCO_sofa_bed_3_eweplb.png"
- "/v1643014203/forestry/ATL_MARCO_sofa_bed_6_f8hvny.png"
- "/v1643014202/forestry/ATL_MARCO_sofa_bed_4_xhbuza.png"
- "/v1643014203/forestry/ATL_MARCO_sofa_bed_24_rcjvsi.png"
- "/v1643014202/forestry/ATL_MARCO_sofa_bed_2_ecqsp2.png"
- "/v1643014203/forestry/ATL_MARCO_sofa_bed_7_v7adr6.png"
- "/v1643014203/forestry/ATL_MARCO_sofa_bed_5_cxu8j5.png"
dimensions: "<p>Dvojsed I.</p><p><strong>Šířka: </strong>173/183 cm - <strong>Délka:
  </strong>104 cm - <strong>Výška: </strong>87 cm - <strong>Výška sedu:</strong> 45
  cm- <strong>Hloubka sedu:</strong> 61 cm</p><p>Dvojsed II.</p><p><strong>Šířka:
  </strong>193/203 cm -  <strong>Délka: </strong>104 cm - <strong>Výška: </strong>87
  cm - <strong>Výška sedu:</strong> 45 cm- <strong>Hloubka sedu:</strong> 61 cm</p><p>Trojsed
  I. </p><p><strong>Šířka: </strong>213/223 cm -  <strong>Délka: </strong>104 cm -
  <strong>Výška: </strong>87 cm - <strong>Výška sedu:</strong> 45 cm- <strong>Hloubka
  sedu:</strong> 61 cm</p><p>Trojsed II.</p><p><strong>Šířka: </strong>213 cm -  <strong>Délka:
  </strong>104 cm - <strong>Výška: </strong>87 cm - <strong>Výška sedu:</strong> 45
  cm- <strong>Hloubka sedu:</strong> 61 cm</p><p>Rohová souprava verze I.</p><p><strong>Šířka:
  </strong>267 cm - <strong>Délka: </strong>154 cm - <strong>Výška: </strong>87 cm
  - <strong>Výška sedu:</strong> 45 cm- <strong>Hloubka sedu:</strong> 61 cm</p><p>Rohová
  souprava verze II.</p><p><strong>Šířka: </strong>287 cm - <strong>Délka: </strong>154
  cm - <strong>Výška: </strong>87 cm - <strong>Výška sedu:</strong> 45 cm- <strong>Hloubka
  sedu:</strong> 61 cm</p><p>Rohová souprava verze III.</p><p><strong>Šířka: </strong>307
  cm - <strong>Délka: </strong>154 cm - <strong>Výška: </strong>87 cm - <strong>Výška
  sedu:</strong> 45 cm- <strong>Hloubka sedu:</strong> 61 cm</p><p>Rohová souprava
  verze IV.</p><p><strong>Šířka: </strong>301 cm - <strong>Délka: </strong>225 cm
  \ - <strong>Výška: </strong>87 cm - <strong>Výška sedu:</strong> 45 cm- <strong>Hloubka
  sedu:</strong> 61 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 v potahu vaty</p><p><strong>Opěradlo: </strong>pěna L 25/15 a N 25/38</p><p><strong>Nožičky:
  </strong>dřevěné nohy - lakované PU lakem nebo v barvě RAL</p><p><strong>Možnosti:</strong>
  mechanismus rozkládací postele</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong> 68 900 Kč </p><p><strong>Cena v látce
  od: </strong>48 600 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/629651f5a0b3f123ed1d4cfd37f523cafe603ec5.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643014976/forestry/ATL_MARCO_sofa_bed_23_und3b0.png"
- "/v1643014979/forestry/ATL_MARCO_sofa_bed_22_k5bcqp.png"
- "/v1643014976/forestry/ATL_MARCO_sofa_bed_21_ltdtku.png"
- "/v1643014980/forestry/ATL_MARCO_sofa_bed_20_hosjse.png"
- "/v1643014979/forestry/ATL_MARCO_sofa_bed_19_iqvdin.png"
- "/v1643014980/forestry/ATL_MARCO_sofa_bed_18_o66ger.png"
- "/v1643014976/forestry/ATL_MARCO_sofa_bed_17_b1g643.png"
- "/v1643014978/forestry/ATL_MARCO_sofa_bed_16_ajrmbk.png"
- "/v1643014981/forestry/ATL_MARCO_sofa_bed_15_jxfskv.png"
- "/v1643014981/forestry/ATL_MARCO_sofa_bed_14_ddydld.png"
- "/v1643014979/forestry/ATL_MARCO_sofa_bed_13_hxqhqa.png"
- "/v1643014979/forestry/ATL_MARCO_sofa_bed_12_zaaja0.png"
- "/v1643014980/forestry/ATL_MARCO_sofa_bed_11_ueucul.png"
- "/v1643014981/forestry/ATL_MARCO_sofa_bed_10_qwfrbj.png"
- "/v1643014981/forestry/ATL_MARCO_sofa_bed_9_qsr12a.png"
- "/v1643014967/forestry/ATL_MARCO_sofa_bed_8_o8rwb7.png"

---
