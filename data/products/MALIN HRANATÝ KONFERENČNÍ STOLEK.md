---
brand: data/brands/Woak.md
name: 'MALIN HRANATÝ KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606746361/forestry/WHO_MALIN_%C4%8Ctvercov%C3%BD_odkl%C3%A1dac%C3%AD_stolek_1_fjnw7g.png"
dimensions: "<p><strong>Šířka: </strong>40 cm - <strong>Délka: </strong>40 cm - <strong>Výška:</strong>
  50 cm</p>"
technical_info: "<p>Tento konferenční stolek je vyroben z masivního dřeva (dubu nebo
  ořechu) a má olejovou úpravu. </p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 15 000 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_side_table_square_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1nGZ_EZpGb6Cl29ekLdpcPii4sGKWOpF8/view"
  title=""><strong>Stáhnout PDF</strong></a></p><p></p>'
product_photos:
- "/v1606746848/forestry/WHO_MALIN_%C4%8Ctvercov%C3%BD_odkl%C3%A1dac%C3%AD_stolek_2_w67at2.png"
- "/v1606746848/forestry/WHO_MALIN_%C4%8Ctvercov%C3%BD_odkl%C3%A1dac%C3%AD_stolek_3_udpqcf.png"
- "/v1606746849/forestry/WHO_MALIN_%C4%8Ctvercov%C3%BD_odkl%C3%A1dac%C3%AD_stolek_4_qg1omj.png"

---
