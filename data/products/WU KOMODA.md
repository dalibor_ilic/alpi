---
brand: data/brands/Artisan.md
name: WU KOMODA
categories:
- Sideboard
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645445877/forestry/Product_fotokomoda_hf9rmu.png"
- "/v1645445877/forestry/Product_foto-2komoda_crflsc.png"
dimensions: "<p><strong>Šířka: </strong>180/200/220 cm cm - <strong>Délka: </strong>45
  cm<strong> </strong>- <strong>Výška: </strong>42 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Průměr nohou: 5 cm </p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>100 600 Kč</p>"
product_info: ''
product_photos:
- "/v1645446037/forestry/Z_interi%C3%A9ru-2komoda_kcpgp2.png"
- "/v1645446050/forestry/Z_interi%C3%A9rukomoda_xfxigb.png"
- "/v1645446051/forestry/Z_interi%C3%A9ru-3komoda_pj0p5u.png"

---
