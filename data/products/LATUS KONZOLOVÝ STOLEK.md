---
brand: data/brands/Artisan.md
name: 'LATUS KONZOLOVÝ STOLEK '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605533200/forestry/ART_LATUS_konzolov%C3%BD_stolek_1_h2a97y.png"
- "/v1605533211/forestry/ART_LATUS_konzolov%C3%BD_stolek_5_td6uvo.png"
dimensions: "<p><strong>Šířka: </strong>120/140/160/180/200<strong> </strong>cm -
  <strong>Délka: </strong>40/50 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální
  tloušťka desky: 6 cm</p><p>Průměr nohou: spodní část 3 cm, vrchní část 6 cm</p><p>Tři
  zásuvky pro rozměry: 160, 180, 200 cm</p><p>Dvě zásuvky pro rozměry: 120 a 140 cm</p><p>Jiné
  úpravy a ceny na vyžádání.</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>63 300 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605533413/forestry/ART_LATUS_konzolov%C3%BD_stolek_4_y6zhfq.png"
- "/v1605533413/forestry/ART_LATUS_konzolov%C3%BD_stolek_2_hv4s0k.png"
- "/v1605533413/forestry/ART_LATUS_konzolov%C3%BD_stolek_3_zeuvln.png"

---
