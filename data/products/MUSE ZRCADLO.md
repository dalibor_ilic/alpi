---
brand: data/brands/Artisan.md
name: 'MUSE ZRCADLO '
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644249310/forestry/Product_foto-25_qhpew7.png"
dimensions: "<p><strong>Šířka: </strong>50/100<strong> </strong>cm - <strong>Výška:
  </strong>60 cm</p>"
technical_info: "<p>Tloušťka: 2 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>13 600 Kč</p>"
product_info: "<p></p>"
product_photos: []

---
