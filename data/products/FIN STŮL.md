---
brand: data/brands/Artisan.md
name: 'FIN STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645439268/forestry/Product_foto-2_efnbq3.png"
- "/v1645439269/forestry/Product_foto_snkbro.png"
dimensions: "<p><strong>Šířka: </strong>160/180/200/220/240 cm - <strong>Délka: </strong>95/100
  cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka desky: 1,5 cm</p><p>Vizuální tloušťka
  desky: 2 cm</p><p>Průměr nohou: spodní část 5x4,6 cm, horní část 7,5x5,6 cm </p>"
materials: ''
price: ''
product_info: ''
product_photos:
- "/v1645439433/forestry/Z_interi%C3%A9ru_ymbhzq.png"

---
