---
brand: data/brands/Artisan.md
name: 'CLOUD KOMODA '
categories:
- Sideboard
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645438799/forestry/Product_foto-3komoda_ylucgf.png"
- "/v1645438799/forestry/Product_foto-5komoda_lcchnx.png"
- "/v1645438799/forestry/Product_foto-4komoda_b1bsfk.png"
- "/v1645438799/forestry/Product_fotokomoda_igued4.png"
- "/v1645438799/forestry/Product_foto-2komoda_vp0gzl.png"
dimensions: "<p><strong>Šířka: </strong>180/210/115 cm - <strong>Délka: </strong>45
  cm - <strong>Výška: </strong>75/95 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka komody: 2 cm</p><p>Přední tloušťka
  2 cm</p><p>Zadní tloušťka: 2 cm</p><p></p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>77 800 Kč</p>"
product_info: ''
product_photos:
- "/v1645438900/forestry/Z_interi%C3%A9ru-2komoda_csfu1l.png"
- "/v1645438946/forestry/Z_interi%C3%A9rukomoda_gp6fx2.png"
- "/v1645438859/forestry/Z_interi%C3%A9ru-4komoda_v3s4je.png"
- "/v1645438932/forestry/Z_interi%C3%A9ru-5komoda_xumppa.png"
- "/v1645438916/forestry/Z_interi%C3%A9ru-3komoda_w3i0jg.png"

---
