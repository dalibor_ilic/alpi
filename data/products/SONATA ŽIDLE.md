---
brand: data/brands/Atlas.md
name: SONATA ŽIDLE
categories:
- Chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Kombinace dřevěné konstrukce a zakřivených
  čalouněných panelů dodává modelu Sonata neobvyklý vzhled. Skvěle se hodí do moderního
  prostředí, ale i do prostředí inspirovaného retro stylem, protože zaujme svým neobvyklým
  vzhledem. Široký sedák i neobvyklé područky zajišťují vysoký komfort.</p>'
product_images:
- "/v1643283415/forestry/ATL_SONATA_%C5%BEidle_1_bed5so.png"
dimensions: "<p><strong>Šířka: </strong>60 cm - <strong>Délka: </strong>63 cm - <strong>Výška:
  </strong>76 cm - <strong>Výška sedu:</strong> 45 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Podpěra:</strong>
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  pěna N 25/38</p><p><strong>Opěradlo:</strong> pěna N 25/38, panely</p><p><strong>Područky:
  </strong>dřevěné</p><p><strong>Nožičky: </strong>dřevěné</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a><strong> </strong>|<strong>
  </strong><a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>16 700 Kč</p><p><strong>Cena v látce od:
  </strong>15 400 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/8e162c0f2d5b3d332e0f9e10312dc37ca9728fde.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643283535/forestry/ATL_SONATA_%C5%BEidle_4_ozhhki.png"
- "/v1643283534/forestry/ATL_SONATA_%C5%BEidle_3_u0uazi.png"
- "/v1643283534/forestry/ATL_SONATA_%C5%BEidle_2_zfjcgm.png"

---
