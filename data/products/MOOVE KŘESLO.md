---
brand: data/brands/Extraform.md
name: MOOVE KŘESLO
categories:
- Arm chair
description: <p>Výrobce :<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Tato
  „skládačka“ představuje sestavitelný systém, který lze využít jako pohovku, křeslo
  nebo stůl. Každý kus může fungovat ve spojení s ostatními nebo samostatně. Je skvěle
  přizpůsoben jakémukoli typu prostoru, zejména kancelářím.</p>
product_images:
- "/v1644507905/forestry/EXT_MOOVE_armchair_3_jxk68o.png"
- "/v1644507905/forestry/EXT_MOOVE_armchair_1_oyvqmh.png"
- "/v1644507904/forestry/EXT_MOOVE_armchair_2_stubws.png"
dimensions: "<p><strong>Šířka: </strong>80<strong> </strong>cm - <strong>Délka:</strong>
  89 cm - <strong>Výška:</strong> 81 cm - <strong>Výška sedu: </strong>42 cm - <strong>Hloubka
  sedu:</strong> 60 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, 18 mm silná překližka,
  16 mm silná dřevotříska, lepenka.</p><p class="Paragraph SCXW119155048 BCX4">Vysoce
  elastická HR pěna, 200 g bavlněné vatové textilie pojené za tepla, rouno, vinuté
  pružiny.</p><p class="Paragraph SCXW119155048 BCX4">Ergonomická pěna RG 25, 200
  g bavlněné textilie s tepelnou vazbou.</p><p class="Paragraph SCXW119155048 BCX4">Pěna
  RG 30, 200 g bavlněné vatové směsi s tepelnou vazbou.</p><p class="Paragraph SCXW119155048
  BCX4">Nohy z dubového dřeva, naolejované. Volitelně z bukového dřeva v šesti barvách.</p><p
  class="Paragraph SCXW119155048 BCX4">Pevná rohová pohovka.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: ''
product_info: ''
product_photos: []

---
