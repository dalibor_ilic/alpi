---
brand: data/brands/Atlas.md
name: CLASSIC SHELL SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a><strong> </strong></p><p>Elegantní, sofistikovaný
  a neobvyklý design. Tajemství této pohovky spočívá v elegantním lineárním opěradle,
  které připomíná tvar mušle. Dokonalý vzhled doplňuje možnost zabudování spacího
  mechanismu, takže se tento model velmi snadno promění v pohodlné lůžko.</p>'
product_images:
- "/v1642433853/forestry/ATL_CLASSICSHELL_sofa_1_phoxy5.png"
- "/v1642433854/forestry/ATL_CLASSICSHELL_sofa_4_od6kmh.png"
- "/v1642433854/forestry/ATL_CLASSICSHELL_sofa_6_coarrc.png"
- "/v1642433854/forestry/ATL_CLASSICSHELL_sofa_8_ntchhe.png"
- "/v1642433854/forestry/ATL_CLASSICSHELL_sofa_7_wyl0er.png"
- "/v1642433854/forestry/ATL_CLASSICSHELL_sofa_5_mofwjq.png"
dimensions: "<p>Dvojsed:</p><p><strong>Šířka: </strong>186 cm - <strong>Délka: </strong>104
  cm - <strong>Výška: </strong>74 cm - <strong>Výška sedu:</strong> 45 cm - <strong>Hloubka
  sedu:</strong> 64 cm</p><p>Trojsed:</p><p><strong>Šířka:</strong> 237 cm - <strong>Délka:
  </strong>104 cm - <strong>Výška: </strong>74 cm - <strong>Výška sedu:</strong> 45
  cm - <strong>Hloubka sedu:</strong> 64 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36, paměťová pěna, peří se silikonem</p><p><strong>Opěradlo: </strong>pěna
  N 25/38</p><p><strong>Elastické popruhy</strong></p><p><strong>Dřevěné nožičky:
  </strong>mořené, lakované v PU laku, nebo v barvě RAL</p><p><strong>Možnosti:</strong>
  Mechanismus rozkládací postele</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>63 500 Kč </p><p><strong>Cena v látce
  od : </strong>41 000 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/3ea41d3efc559ce86b19af45eb706c560f1de96e.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642434075/forestry/ATL_CLASSICSHELL_sofa_3_s8wljv.png"
- "/v1642434074/forestry/ATL_CLASSICSHELL_sofa_2_tijgoj.png"

---
