---
brand: data/brands/Extraform.md
name: LOOP KŘESLO
categories:
- Arm chair
description: <p>Výrobce :<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Loop
  je křeslo, které má moderní vzhledem. Měkké uvnitř a zvenčí výrazné. Toto dává křeslu
  Loop jeho charakter. Obvodový vede kolem opěradla. Loop Vám nabízí řadu modelů a
  materiály. Nabízí pohodlné sezení.</p>
product_images:
- "/v1644507120/forestry/EXT_LOOP_k%C5%99eslo_5_gjb4d9.png"
- "/v1644507120/forestry/EXT_LOOP_k%C5%99eslo_1_bsfbvs.png"
- "/v1644507120/forestry/EXT_LOOP_k%C5%99eslo_4_c50rwe.png"
- "/v1644507120/forestry/EXT_LOOP_k%C5%99eslo_3_dcyghn.png"
- "/v1644507119/forestry/EXT_LOOP_k%C5%99eslo_2_k4cknd.png"
dimensions: "<p><strong>Šířka: </strong>88 cm - <strong>Délka:</strong> 86 cm - <strong>Výška:</strong>
  82 cm - <strong>Výška sedu:</strong> 46 cm - <strong>Hloubka sedu:</strong> 54 cm</p>"
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, lepenky, 18 mm silné
  překližky, 22 mm MDF.</p><p class="Paragraph SCXW151249368 BCX4">Sedáky - vysoce
  elastická HR pěna, 200 g tepelně pojené plsti, elastické popruhy.</p><p class="Paragraph
  SCXW151249368 BCX4">Opěrky - ergonomická pěna RG 22, 400 g tepelně pojené plsti.</p><p
  class="Paragraph SCXW151249368 BCX4">Nohy z dubového dřeva, naolejované. Volitelně
  z bukového dřeva v šesti barvách.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>22 300 Kč</p>"
product_info: ''
product_photos:
- "/v1644507088/forestry/EXT_LOOP_k%C5%99eslo_7_ezpvhl.png"
- "/v1644507086/forestry/EXT_LOOP_k%C5%99eslo_6_zeynvk.png"

---
