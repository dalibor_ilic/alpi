---
brand: data/brands/Atlas.md
name: MORFEO POSTEL
categories:
- Bed
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Vášeň
  pro řemeslo se prolíná celým designem postele Morfeo. Řada rovných švů a strukturovaných
  stehů, vzhled je harmonický a vyvážený. Rovné linie jsou součástí designu, který
  dotváří celkovou harmonii postele Morfeo.</p>'
product_images:
- "/v1648834566/forestry/Product_foto_juyzcd.png"
- "/v1648834566/forestry/Product_foto-2_rlfqdw.png"
dimensions: "<p>170 x 200 cm - <strong>Šířka: </strong>173<strong> </strong>cm - <strong>Délka:
  </strong>218 cm - <strong>Výška: </strong>136 cm </p><p>190 x 200 cm - <strong>Šířka:
  </strong>193<strong> </strong>cm - <strong>Délka: </strong>218 cm - <strong>Výška:
  </strong>136 cm </p>"
technical_info: "<p><strong>Potah: </strong>látka, kůže</p><p><strong>Nožičky:</strong>
  dřevěné</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p><p>Kůže:
  <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"><strong>CAT
  300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"><strong>DUB</strong></a></p>'
price: ''
product_info: ''
product_photos:
- "/v1648834813/forestry/Z_interi%C3%A9ru-5_knoow1.png"
- "/v1648834814/forestry/Z_interi%C3%A9ru-4_icgq9i.png"
- "/v1648834814/forestry/Z_interi%C3%A9ru-3_ier9k6.png"
- "/v1648834794/forestry/Z_interi%C3%A9ru-2_kae2k0.png"

---
