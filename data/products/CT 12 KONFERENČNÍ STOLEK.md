---
brand: data/brands/Atlas.md
name: " CT 12 KONFERENČNÍ STOLEK"
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Pevná kovová podnož ve tvaru pavučiny
  v kombinaci s kvalitní deskou MDF dodává tomuto klubovému stolku definovaný, kompaktní
  vzhled, který se hodí do interiérů s moderním, industriálním designem. Je vyroben
  pouze v jednom rozměru, ale lze jej velmi snadno personalizovat podle potřeb zákazníka,
  protože podstavec i desku lze natřít v barvách RAL.</p>'
product_images:
- "/v1643284176/forestry/ATL_CT12_konferen%C4%8Dn%C3%AD_stolek_hqmvh6.png"
dimensions: "<p><strong>Šířka: </strong>120 cm - <strong>Délka: </strong>60 cm - <strong>Výška:
  </strong>35 cm</p>"
technical_info: "<p><strong>Materiál:</strong> MDF, buk</p><p><strong>Nožičky: </strong>kovové,<strong>
  </strong>možnost natřít barvou</p>"
materials: ''
price: "<p><strong>Cena od:</strong> 11 600 Kč</p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/0b2ba3dc7457a8c5a8d04385171411d510bb7298.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos: []

---
