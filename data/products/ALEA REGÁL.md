---
brand: data/brands/Artisan.md
name: ALEA REGÁL
categories:
- Shelf
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605192104/forestry/ART_ALEA_reg%C3%A1l_4_xdysxc.png"
- "/v1605192104/forestry/ART_ALEA_reg%C3%A1l_1_qabaha.png"
- "/v1605192104/forestry/ART_ALEA_reg%C3%A1l_3_s3cu2i.png"
- "/v1605192104/forestry/ART_ALEA_reg%C3%A1l_2_qi5rd5.png"
dimensions: "<p><strong>Šířka: </strong>106/209<strong> </strong>cm - <strong>Délka:
  </strong>43,5 cm - <strong>Výška: </strong>79/117/155 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka polic: 3 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 72 600 CZK</p>"
product_info: "<p></p>"
product_photos:
- "/v1605192337/forestry/ART_ALEA_reg%C3%A1l_5_vknfxf.png"
- "/v1605192337/forestry/ART_ALEA_reg%C3%A1l_7_ll3txr.png"
- "/v1605192336/forestry/ART_ALEA_reg%C3%A1l_6_bajy3a.png"

---
