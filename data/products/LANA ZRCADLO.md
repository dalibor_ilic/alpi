---
brand: data/brands/Artisan.md
name: LANA ZRCADLO
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645709239/forestry/Lana_zrcadloProduct_foto_ymykch.png"
dimensions: "<p><strong>Rozměry: </strong>Ø80x3 cm/ Ø100x3 cm/ Ø120x4 cm </p>"
technical_info: ''
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>17 000 Kč</p>"
product_info: ''
product_photos:
- "/v1645709262/forestry/Lana_zrcadloZ_interi%C3%A9ru-2_u4rn5j.png"
- "/v1645709262/forestry/Lana_zrcadloZ_interi%C3%A9ru_m8wsac.png"
- "/v1645709261/forestry/Lana_zrcadloZ_interi%C3%A9ru-3_t9a29v.png"
- "/v1645709260/forestry/Lana_zrcadloZ_interi%C3%A9ru-4_x6vgso.png"

---
