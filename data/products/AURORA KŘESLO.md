---
brand: data/brands/Atlas.md
name: AURORA KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Ztělesnění
  módy a detailu, smysl pro svěžest, který Vám umožní vést luxusní život se stylem.</p>'
product_images:
- "/v1648836800/forestry/Product_foto_fyrgtz.png"
- "/v1648836800/forestry/Product_foto-4_ni4mbm.png"
- "/v1648836800/forestry/Product_foto-3_qlwfpv.png"
- "/v1648836800/forestry/Product_foto-2_jwytcy.png"
dimensions: "<p><strong>Šířka: </strong>80<strong> </strong>cm - <strong>Délka: </strong>75
  cm - <strong>Výška: </strong>80 cm - <strong>Výška sedu:</strong> 50 cm - <strong>Hloubka
  sedu:</strong> 49 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, kůže</p><p><strong>Sedák: </strong>vysoce
  elastická polyuretanová HR pěna 47/90, HR pěna 35/36</p><p><strong>Opěradlo: </strong>vysoce
  elastická polyuretanová HR pěna 47/90, pěna L 25/15</p><p><strong>Nožičky: </strong>dřevěný
  rám - lazura, polyuretanový lak, krycí polyuretanová barva dle vzorníku RAL</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p><p>Kůže:
  <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"><strong>CAT
  300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"><strong>DUB</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/421545d8cdcdf9a589241d7b90a7da5229662f72.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1648837242/forestry/Z_interi%C3%A9ru-2_leyuhk.png"
- "/v1648837242/forestry/Z_interi%C3%A9ru-3_tiztqn.png"
- "/v1648837241/forestry/Z_interi%C3%A9ru_dngugq.png"
- "/v1648837229/forestry/Z_interi%C3%A9ru_kopie_dsdpws.png"

---
