---
brand: data/brands/Artisan.md
name: 'ENY KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645708028/forestry/Eny_konferen%C4%8Dn%C3%AD_stolekProduct_foto_ra4tmh.png"
- "/v1645708029/forestry/Eny_konferen%C4%8Dn%C3%AD_st%C5%AFlProduct_foto-2_ucnage.png"
- "/v1645708029/forestry/Eny_konferen%C4%8Dn%C3%AD_st%C5%AFlProduct_foto-3_lo5xpb.png"
dimensions: "<p><strong>Šířka: </strong>60/80/100/120<strong> </strong>cm - <strong>Délka:
  </strong>60/80/100/60 cm - <strong>Výška: </strong>45 cm</p>"
technical_info: ''
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>25 000 Kč</p>"
product_info: ''
product_photos:
- "/v1645708254/forestry/Eny_konferen%C4%8Dn%C3%AD_st%C5%AFlZ_interi%C3%A9ru_xdrgvf.png"

---
