---
brand: data/brands/Artisan.md
name: 'HANNY KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1645439570/forestry/Product_foto_konferen%C4%8Dn%C3%AD_stolek_f4rpzx.png"
dimensions: "<p><strong>Šířka: </strong>120/140/160/180/200/220/240/260 cm - <strong>Délka:
  </strong>80/90 cm - <strong>Výška: </strong>45 cm</p>"
technical_info: "<p>Masivní dřevo.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální tloušťka
  desky: 2 cm</p><p>Průměr nohou: spodní část 6x4 cm, horní část 11x4 cm</p>"
materials: ''
price: ''
product_info: ''
product_photos: []

---
