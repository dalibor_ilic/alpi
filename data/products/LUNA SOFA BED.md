---
brand: data/brands/Extraform.md
name: 'LUNA SOFA BED '
categories:
- Sofa bed system
description: <p>Výrobce :<strong> </strong><a href="https://www.alpicollection.com/znacky?brand=extraform#detail"><strong>EXTRAFORM</strong></a></p><p>Krásná
  kompaktní forma a harmonický objem. Elegantní linie opěradla a boků jsou tvořeny
  měkkými polštáři, které je možné pomocí mechanismu přeměněny na pohodlnou postel.
  Lattoflex a matrace o tloušťce 18 cm přispívají k velkému pohodlí.</p>
product_images:
- "/v1644507733/forestry/EXT_LUNA_sofa_bed_1_dgpqhd.png"
- "/v1644507723/forestry/EXT_LUNA_sofa_bed_9_nal0w2.png"
- "/v1644507722/forestry/EXT_LUNA_sofa_bed_8_vz4llz.png"
- "/v1644507723/forestry/EXT_LUNA_sofa_bed_7_whn3ab.png"
- "/v1644507723/forestry/EXT_LUNA_sofa_bed_6_ewpasd.png"
- "/v1644507723/forestry/EXT_LUNA_sofa_bed_13_ulx1sm.png"
- "/v1644507723/forestry/EXT_LUNA_sofa_bed_11_ktqibw.png"
- "/v1644507723/forestry/EXT_LUNA_sofa_bed_12_zbxdwt.png"
dimensions: '<p><strong>Dvojsed: Šířka: </strong>200<strong> </strong>cm - <strong>Délka:
  </strong>108 cm - <strong>Výška: </strong>88 cm - <strong>Výška sedu: </strong>48
  cm - <strong>Hloubka sedu: </strong>56 cm</p><p class="Paragraph SCXW145603816 BCX4"><strong>Trojsed:
  Šířka: </strong>220<strong> </strong>cm - <strong>Délka: </strong>108 cm - <strong>Výška:
  </strong>88 cm - <strong>Výška sedu: </strong>48 cm - <strong>Hloubka sedu: </strong>56
  cm</p><p class="Paragraph SCXW145603816 BCX4"><strong>Rohová sedací souprava: Šířka:
  </strong>300<strong> </strong>cm - <strong>Délka: </strong>200 cm - <strong>Výška:
  </strong>88 cm - <strong>Výška sedu: </strong>48 cm - <strong>Hloubka sedu: </strong>56
  cm</p>'
technical_info: <p>Nosná konstrukce ze sušeného bukového dřeva, dřevotříska o tloušťce
  16 mm, lepenka, překližka o tloušťce 18 mm, 3 mm silná sololitová deska.</p><p class="Paragraph
  SCXW177365329 BCX4">Vysoce elastická HR pěna, 200 g bavlněné vatové textilie pojené
  za tepla, 500 g mušek, vinuté pružiny.</p><p class="Paragraph SCXW177365329 BCX4">Ergonomická
  pěna RG 22, 200 g bavlněné textilie s tepelnou vazbou.</p><p class="Paragraph SCXW177365329
  BCX4">Nohy z bukového dřeva, mořené a lakované. K dispozici v šesti barvách.</p><p
  class="Paragraph SCXW177365329 BCX4">Výsuvná postel je volitelná pro dvoumístné
  a třímístné pohovky.</p>
materials: <p><a href="https://drive.google.com/file/d/1BR68K3c1S-AW8eUcL5sa6WG2jd5KWXLn/view?usp=sharing  "
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/1IrVdnEu8CVFJxFZKYJtnWM4H-1pEqURD/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1Gc25-pzH60zhIYJBDmIVtltlS0cwQZmp/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>51 400 Kč </p>"
product_info: ''
product_photos:
- "/v1644507818/forestry/EXT_LUNA_sofa_bed_4_fiebz2.png"
- "/v1644507819/forestry/EXT_LUNA_sofa_bed_2_ymfhjj.png"
- "/v1644507818/forestry/EXT_LUNA_sofa_bed_5_p0vkee.png"
- "/v1644507818/forestry/EXT_LUNA_sofa_bed_3_g1t6ra.png"

---
