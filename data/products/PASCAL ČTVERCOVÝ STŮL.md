---
brand: data/brands/Artisan.md
name: 'PASCAL ČTVERCOVÝ STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605608731/forestry/ART_PASCAL_%C4%8Dtvercov%C3%BD_st%C5%AFl_1_kpm0jr.png"
- "/v1605608731/forestry/ART_PASCAL_%C4%8Dtvercov%C3%BD_st%C5%AFl_2_g1erry.png"
dimensions: "<p><strong>Šířka: </strong>100-120 cm - <strong>Délka: </strong>100-120
  cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a varianta s čirým či kouřovým
  sklem.</p><p>Tloušťka desky: 2 cm</p><p>Vizuální tloušťka desky: 2 cm</p><p>Průměr
  nohou: spodní část 4,5x4,5 cm, vrchní část 5x5 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>69 600 Kč</p>"
product_info: ''
product_photos: []

---
