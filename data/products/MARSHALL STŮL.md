---
brand: data/brands/Woak.md
name: MARSHALL STŮL
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Dřevěné části tohoto nábytku jsou ručně
  vybírány jedna část po druhé, aby dokonale pasovaly s ostatními materiály, které
  jsou zcela inovativní a recyklovatelné, jako je nezpracovaná kůže, čistý mramor
  nebo lité sklo s měnícími se barvami. Kombinace teplých a studených materiálů vede
  k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem je mužný,
  ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací pokoj nebo
  koutek na čtení.</p>'
product_images:
- "/v1606896344/forestry/WHO_MARSHALL_st%C5%AFl_2_pltyof.png"
- "/v1606896344/forestry/WHO_MARSHALL_st%C5%AFl_1_flkrcs.png"
dimensions: "<p><strong>Šířka: </strong>130 - 300 cm - <strong>Délka: </strong>90
  - 130 cm - <strong>Výška:</strong> 75 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>deska stolu: ručně vyráběné a strukturované,
  stříbrné  sklo nebo dřevo</p><p>povrchová úprava: lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>68 500 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Marshall_dining_table_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606896570/forestry/WHO_MARSHALL_st%C5%AFl_6_fskfcc.png"
- "/v1606896572/forestry/WHO_MARSHALL_st%C5%AFl_5_tjs7va.png"
- "/v1606896571/forestry/WHO_MARSHALL_st%C5%AFl_4_jgopda.png"
- "/v1606896571/forestry/WHO_MARSHALL_st%C5%AFl_3_yzujcv.png"

---
