---
brand: data/brands/Atlas.md
name: CITY KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Avantgardní styl, zakřivení, neobvyklý
  tvar, prošívané opěradlo a sedáky dělají tento model zajímavým a přitažlivým. Ergonomický
  tvar opěradla a sedáku kopíruje linii těla a umožňuje úplnou relaxaci. Křeslo City
  se vyrábí s kovovou otočnou podnoží, nebo v podobě jednomístného křesla.</p>'
product_images:
- "/v1643109001/forestry/ATL_CITY_k%C5%99eslo_6_imrqfd.png"
- "/v1643109000/forestry/ATL_CITY_k%C5%99eslo_1_mn5ihg.png"
dimensions: "<p><strong>Šířka: </strong>89 cm - <strong>Délka: </strong>102 cm - <strong>Výška:
  </strong>87 cm - <strong>Výška sedu:</strong> 40 cm - <strong>Hloubka sedu:</strong>
  56 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Podpěra:</strong>
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36</p><p><strong>Opěradlo: </strong>HR pěna 35/30</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>kovové nohy Ø50cm, výška 20cm</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a>  | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>34 300 Kč </p><p><strong>Cena v látce
  od: </strong>27 000 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/1ee0d7f43f04944cdfe35d901894f0dc47ebbd98.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643109183/forestry/ATL_CITY_k%C5%99eslo_12_sobjhw.png"
- "/v1643109186/forestry/ATL_CITY_k%C5%99eslo_8_mt2toa.png"
- "/v1643109183/forestry/ATL_CITY_k%C5%99eslo_7_wcj0gm.png"
- "/v1643109187/forestry/ATL_CITY_k%C5%99eslo_13_ymc9dm.png"
- "/v1643109187/forestry/ATL_CITY_k%C5%99eslo_11_pfifdw.png"
- "/v1643109187/forestry/ATL_CITY_k%C5%99eslo_10_evvi65.png"
- "/v1643109187/forestry/ATL_CITY_k%C5%99eslo_9_ze2uu8.png"

---
