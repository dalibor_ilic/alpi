---
brand: data/brands/Atlas.md
name: MATEO POSTEL
categories:
- Bed
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Kombinace dřevěného rámu a moderního čela
  pro pohodlný spánek. Zajímavá kombinace materiálů otevírá nový rozměr designu postele.
  Ručně vyrobené dřevěné nožky a organický tvar čela vytvářejí velmi zvláštní pocit
  bezpečí. Díky této posteli bude váš pokoj krásnější a vy budete spát ještě lépe.</p>'
product_images:
- "/v1643104154/forestry/ATL_MATEO_postel_1_zmge7n.png"
- "/v1643104154/forestry/ATL_MATEO_postel_4_un4lsp.png"
dimensions: "<p>Dvojlůžko I.</p><p>160 × 200 cm - <strong>Šířka: </strong>174 cm -
  <strong>Délka: </strong>227 cm - <strong>Výška: </strong>99 cm</p><p>Dvojlůžko II.</p><p>180
  × 200 cm - <strong>Šířka: </strong>194 cm - <strong>Délka: </strong>227 cm - <strong>Výška:
  </strong>99 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Výplň:</strong>
  pěna N 25/38</p><p><strong>Základ:</strong> pěna N 25/38</p><p><strong>Nožičky:
  </strong>dřevěné nohy lakované PU lakem</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>39 200 Kč </p><p><strong>Cena v látce
  od: </strong>31 100 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/0a931c38fb91aa525209c25ebeb8f01a2d0ba60b.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643104351/forestry/ATL_MATEO_postel_3_ljvhho.png"
- "/v1643104350/forestry/ATL_MATEO_postel_2_zrrwgl.png"

---
