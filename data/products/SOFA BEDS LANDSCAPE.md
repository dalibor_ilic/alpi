---
brand: data/brands/Atlas.md
name: LANDSCAPE SOFA BED
categories:
- Sofa bed system
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Elegantní, neodolatelná, exkluzivní, to
  je to, co vás jako první napadne, když uvidíte tuto pohovku. Rovné, redukované linie
  dělají z tohoto modelu tu správnou volbu pro milovníky estetiky, zatímco možnost
  kombinovat prvky umožňuje splnit všechny funkční a rozměrové požadavky. Díky neobvyklým
  válečkovým polštářům a kovovým klubovým stolkům můžete doplnit vzhled této soupravy
  a dodat jí další úroveň pohodlí a funkčnosti. Prošívání detailů na prvcích je to,
  co tento model odlišuje od zbytku naší nabídky a dává vám možnost vytvořit si dokonalou
  sedací soupravu pro váš domov.</p><p></p>'
product_images:
- "/v1643012545/forestry/ATL_LANDSCAPE_sofa_1_dq64rx.png"
- "/v1643012545/forestry/ATL_LANDSCAPE_sofa_11_nxgp9u.png"
- "/v1643012545/forestry/ATL_LANDSCAPE_sofa_10_hw2obi.png"
- "/v1643012543/forestry/ATL_LANDSCAPE_sofa_12_iodoai.png"
- "/v1643012543/forestry/ATL_LANDSCAPE_sofa_13_saiyp3.png"
- "/v1643012544/forestry/ATL_LANDSCAPE_sofa_15_tqs2jp.png"
- "/v1643012544/forestry/ATL_LANDSCAPE_sofa_14_tph0vr.png"
dimensions: "<p>Rohová souprava verze I.</p><p><strong>Šířka: </strong>340 cm - <strong>Délka:
  </strong>160 cm - <strong>Výška: </strong>75 cm - <strong>Výška sedu:</strong> 38
  cm- <strong>Hloubka sedu:</strong> 75 cm</p><p>Rohová souprava verze II.</p><p><strong>Šířka:
  </strong>315 cm - <strong>Délka: </strong>225 cm - <strong>Výška: </strong>75 cm
  - <strong>Výška sedu:</strong> 38 cm- <strong>Hloubka sedu:</strong> 75 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo/kovový rám</p><p><strong>Potah:</strong>
  látka, nebo kůže</p><p><strong>Sedák:</strong> HR pěna 35/36 a HR pěna 35/30, peří
  se silikonem</p><p><strong>Opěradlo: </strong>peří se silikonem</p><p><strong>Nožičky:
  </strong>kovové nohy lakované v barvě RAL a efekt surového železa</p><p><strong>Možnosti:</strong>
  stolní lakované MDF nebo naolejované masivní dubové dřevo</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1x3qtWSfvDZh45kB8JxujwYgvlKTEPGhz/view?usp=sharing"
  title="Látky ELEGANT"><strong>ELEGANT</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>119 600 Kč </p><p><strong>Cena v látce
  od: </strong>86 100 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/45b5fdb43c4d62a5f47fb31d47e7e98d8ccf3645.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643013217/forestry/ATL_LANDSCAPE_sofa_2_d7xuv1.png"
- "/v1643013218/forestry/ATL_LANDSCAPE_sofa_7_y6aqiw.png"
- "/v1643013218/forestry/ATL_LANDSCAPE_sofa_6_jl4biz.png"
- "/v1643013216/forestry/ATL_LANDSCAPE_sofa_5_akceir.png"
- "/v1643013216/forestry/ATL_LANDSCAPE_sofa_4_vycst7.png"
- "/v1643013217/forestry/ATL_LANDSCAPE_sofa_3_orx1gg.png"

---
