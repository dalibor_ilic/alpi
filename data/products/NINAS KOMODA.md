---
brand: data/brands/Woak.md
name: 'NINAS KOMODA '
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Ústředním prvkem jsou minimalistické
  linie aplikované na kusy retro nábytku. Cílem kolekce je nabídnout novou interpretaci
  klasických kusů nábytku, prostřednictvím obnovy stylu pomocí techniky zpracované
  značkou Woak. Záměrem je rozebrat společná místa tradičních forem, eliminovat ozdoby
  a prvky, které jsou příliš dekorativní, aby byl objekt moderní a zároveň funkční.</p>'
product_images:
- "/v1607004476/forestry/WHO_NINAS_komoda_1_zmn8wz.png"
- "/v1607004482/forestry/WHO_NINAS_komoda_5_vzxwya.png"
dimensions: "<p><strong>Šířka: </strong>120 cm - <strong>Délka: </strong>45 cm - <strong>Výška:</strong>
  85 cm</p>"
technical_info: "<p>Vrchní deska: mramor Emperador</p><p>Masivní dřevo: dub nebo ořech,
  olejovaná úprava</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 67 580 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Ninas_sideboard_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1DHUbmy9gV4TQwA8DvvBWpqbvnpD9Pbgl/view?usp=sharing"
  title=""><strong>Stáhnout PDF</strong></a> </p>'
product_photos:
- "/v1607004676/forestry/WHO_NINAS_komoda_4_fyq8qz.png"
- "/v1607004676/forestry/WHO_NINAS_komoda_3_ekgxo1.png"
- "/v1607004676/forestry/WHO_NINAS_komoda_2_zkibmb.png"

---
