---
brand: data/brands/Artisan.md
name: THOR LAVICE
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1604996271/forestry/ART_TOR_st%C5%AFl_1_guga2s.png"
- "/v1604996272/forestry/ART_TOR_st%C5%AFl_3_p1q6cj.png"
dimensions: "<p><strong>Šířka: </strong>120 - 180 cm - <strong>Délka: </strong>45/50
  cm - <strong>Výška: </strong>43 cm</p>"
technical_info: "<p>Výška bez matrace: 33 cm </p><p>Průměr nohou: spodní část 3,2
  cm, vrchní část 4,2 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a> | <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 57 000 Kč</p>"
product_info: ''
product_photos:
- "/v1604996310/forestry/ART_TOR_st%C5%AFl_2_dlzasb.png"

---
