---
brand: data/brands/Woak.md
name: 'MARSHALL KONFERENČNÍ STOLEK '
categories:
- Coffee table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Dřevěné části tohoto nábytku jsou ručně
  vybírány jedna část po druhé, aby dokonale pasovaly s ostatními materiály, které
  jsou zcela inovativní a recyklovatelné, jako je nezpracovaná kůže, čistý mramor
  nebo lité sklo s měnícími se barvami. Kombinace teplých a studených materiálů vede
  k dokonalé rovnováze nejen na pohled, ale i na dotek. Protiklad forem je mužný,
  ale i ladný a výsledkem je harmonická směsice, ať už se jedná o obývací pokoj nebo
  koutek na čtení.</p>'
product_images:
- "/v1606811865/forestry/WHO_MARSHALL_konferen%C4%8Dn%C3%AD_stolek_3_cdrsi7.png"
- "/v1606811865/forestry/WHO_MARSHALL_konferen%C4%8Dn%C3%AD_stolek_1_fhjm2v.png"
- "/v1606811865/forestry/WHO_MARSHALL_konferen%C4%8Dn%C3%AD_stolek_2_p5piqg.png"
dimensions: "<p><strong>Šířka: </strong>44 cm - <strong>Délka: </strong>44 cm - <strong>Výška:</strong>
  42,5 cm</p>"
technical_info: "<p>materiál: ořech nebo dub,</p><p>deska stolu: mramorová carrara,
  kožený kašmír nebo dřevo</p><p>povrchová úprava: lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>15 100 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Marshall_coffee_table_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606812081/forestry/WHO_MARSHALL_konferen%C4%8Dn%C3%AD_stolek_4_r2fh7q.png"
- "/v1606812082/forestry/WHO_MARSHALL_konferen%C4%8Dn%C3%AD_stolek_5_gydzyn.png"

---
