---
brand: data/brands/Woak.md
name: 'MALIN ZRCADLO '
categories:
- Accessories
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606750004/forestry/WHO_MALIN_Zrcadlo_3_rdhznk.png"
- "/v1606750004/forestry/WHO_MALIN_Zrcadlo_1_icn4qf.png"
- "/v1606750004/forestry/WHO_MALIN_Zrcadlo_2_o29i7x.png"
dimensions: "<p><strong>Šířka: </strong>42 cm - <strong>Délka: </strong>25,5 cm -
  <strong>Výška:</strong> 165 cm</p>"
technical_info: "<p>Toto je zrcadlo je vyrobeno z masivního dřeva (dubu nebo ořechu)
  a má olejovou úpravu. </p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 24 500 Kč</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_mirror_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1adJJLh00fsCJoCa3z2sEDG43dmxFik3s/view?usp=sharing"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606750363/forestry/WHO_MALIN_Zrcadlo_5_gbr3it.png"
- "/v1606750368/forestry/WHO_MALIN_Zrcadlo_4_zfdj1k.png"

---
