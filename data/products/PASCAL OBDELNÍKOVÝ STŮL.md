---
brand: data/brands/Artisan.md
name: PASCAL OBDELNÍKOVÝ STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605609016/forestry/ART_PASCAL_obd%C3%A9ln%C3%ADkov%C3%BD_st%C5%AFl_1_trxmpj.png"
dimensions: "<p><strong>Šířka: </strong>180-280<strong> </strong>cm - <strong>Délka:
  </strong>80-120 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava a varianta s čirým či kouřovým
  sklem.</p><p>Tloušťka desky: 2 cm</p><p>Vizuální tloušťka desky: 2 cm</p><p>Průměr
  nohou: spodní část 4,5x4,5 cm, vrchní část 5x5 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od: </strong>126 400 Kč</p>"
product_info: ''
product_photos:
- "/v1605609123/forestry/ART_PASCAL_obd%C3%A9ln%C3%ADkov%C3%BD_st%C5%AFl_2_awtu7f.png"

---
