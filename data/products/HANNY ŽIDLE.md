---
brand: data/brands/Artisan.md
name: 'HANNY ŽIDLE '
categories:
- Chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1644248488/forestry/Product_foto_kopie_2_hwnaa9.png"
- "/v1604581750/forestry/ART_HANNY_%C5%BEidle_5_ga9amt.png"
- "/v1604581750/forestry/ART_HANNY_%C5%BEidle_8_mnlsdl.png"
- "/v1604581750/forestry/ART_HANNY_%C5%BEidle_7_icrlrf.png"
- "/v1644248521/forestry/Product_foto-14_ypp3hg.png"
dimensions: "<p><strong>Šířka: </strong>47 cm - <strong>Délka:</strong> 52,8 cm -
  <strong>Výška: </strong>80 cm - <strong>Výška sedu: </strong>47 cm - <strong>Hloubka
  sedu: </strong>45,5 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p>Velikost
  sedu: 48x45,5 cm</p><p>Přední nohy: 4,6x2,4 cm</p><p>Zadní nohy:12,5x2,4 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong></a> | <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 12 100 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604582215/forestry/ART_HANNY_%C5%BEidle_3_hxlalt.png"
- "/v1604582215/forestry/ART_HANNY_%C5%BEidle_2_qkfqwc.png"
- "/v1604582215/forestry/ART_HANNY_%C5%BEidle_4_jxa3cc.png"

---
