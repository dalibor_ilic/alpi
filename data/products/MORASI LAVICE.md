---
brand: data/brands/Woak.md
name: 'MORASI LAVICE '
categories:
- Accessories
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p>'
product_images:
- "/v1607001761/forestry/WHO_MORASI_lavice_1_sl8a00.png"
- "/v1607001843/forestry/WHO_MORASI_lavice_4_m1zk7f.png"
dimensions: "<p><strong>Šířka: </strong>140 - 220 cm - <strong>Délka: </strong>36
  cm - <strong>Výška:</strong> 47 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>povrchová úprava: lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>30 100 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Morasi_bench_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607001986/forestry/WHO_MORASI_lavice_3_xfiwwe.png"
- "/v1607001986/forestry/WHO_MORASI_lavice_2_ebxqan.png"

---
