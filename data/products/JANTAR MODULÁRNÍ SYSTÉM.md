---
brand: data/brands/Artisan.md
name: JANTAR MODULÁRNÍ SYSTÉM
categories: []
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605191357/forestry/ART_JANTAR_modul%C3%A1rn%C3%AD_syst%C3%A9m_1_bcree7.png"
dimensions: "<p><strong>Šířka:</strong> 160/200<strong> </strong>cm - <strong>Délka:</strong>
  28/45 cm - <strong>Výška: </strong>31/36 cm</p>"
technical_info: ''
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 50 300 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605191534/forestry/ART_JANTAR_modul%C3%A1rn%C3%AD_syst%C3%A9m_2_nswij4.png"

---
