---
brand: data/brands/Atlas.md
name: TETRIS STŮL
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>K výrobě jídelního stolu Tetris používáme
  speciálně vybrané masivní dubové dřevo. Masivní rustikální deska stolu zaujme svou
  tloušťkou a obráceně zkosenými hranami, což je opravdu neobvyklý detail. Vzhledem
  k tomu, že dřevo má své přirozené vlastnosti, je každá deska stolu jiná a okouzlující
  svou jedinečností. Jednoduché kovové nohy dodávají tomuto masivnímu stolu velmi
  minimalistický vzhled a usnadňují jeho začlenění do moderních interiérů.</p>'
product_images:
- "/v1643280336/forestry/ATL_TETRIS_st%C5%AFl_frecv9.png"
dimensions: "<p><strong>Šířka: </strong>160 cm - <strong>Délka: </strong>90 cm - <strong>Výška:
  </strong>77 cm</p>"
technical_info: "<p><strong>Materiál:</strong> dub</p><p><strong>Nožičky: </strong>kovové</p>"
materials: '<p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena od: </strong>41 600 Kč</p>"
product_info: ''
product_photos: []

---
