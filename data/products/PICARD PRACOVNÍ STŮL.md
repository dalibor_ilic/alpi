---
brand: data/brands/Artisan.md
name: PICARD PRACOVNÍ STŮL
categories:
- Accessories
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605171565/forestry/ART_PICARD_pracovn%C3%AD_st%C5%AFl_2_yvhfat.png"
- "/v1605171565/forestry/ART_PICARD_pracovn%C3%AD_st%C5%AFl_5_x4uxvu.png"
- "/v1605171565/forestry/ART_PICARD_pracovn%C3%AD_st%C5%AFl_1_rzduoy.png"
- "/v1605171574/forestry/ART_PICARD_pracovn%C3%AD_st%C5%AFl_11_x8wi7m.png"
- "/v1605171565/forestry/ART_PICARD_pracovn%C3%AD_st%C5%AFl_3_jdzqzx.png"
- "/v1605171565/forestry/ART_PICARD_pracovn%C3%AD_st%C5%AFl_4_zj7wle.png"
dimensions: "<p><strong>Šířka: </strong>183 cm - <strong>Délka: </strong>88 cm - <strong>Výška:</strong>
  75 cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, varianta s corianem a také varianta
  se 2-3 šuplíky.</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1DKp-AqR_-rESWlFNEpcr1prxEig5SpKq/view?usp=sharing
  " title=""><strong>BAREVNÉ DŘEVO</strong></a> | <a href="https://drive.google.com/file/d/1IzBeGCwk5y_mmtZhT0N6B9KJjNL0vCaB/view?usp=sharing
  " title=""><strong>CORIAN</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 71 100 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605172674/forestry/ART_PICARD_pracovn%C3%AD_st%C5%AFl_10_ncl8jv.png"
- "/v1605172671/forestry/ART_PICARD_pracovn%C3%AD_st%C5%AFl_9_oxzr13.png"
- "/v1605172673/forestry/ART_PICARD_pracovn%C3%AD_st%C5%AFl_8_kocvuv.png"
- "/v1605172674/forestry/ART_PICARD_pracovn%C3%AD_st%C5%AFl_7_k4ybts.png"
- "/v1605172675/forestry/ART_PICARD_pracovn%C3%AD_st%C5%AFl_6_qvwp3p.png"

---
