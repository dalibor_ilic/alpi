---
brand: data/brands/Artisan.md
name: 'AGILIS MODULÁRNÍ SYSTÉM '
categories: []
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605190503/forestry/ART_AGILIS_modul%C3%A1rn%C3%AD_syst%C3%A9m_2_pczcva.png"
- "/v1605190503/forestry/ART_AGILIS_modul%C3%A1rn%C3%AD_syst%C3%A9m_1_ncm0b2.png"
dimensions: "<p><strong>Šířka: </strong>160/200/240<strong> </strong>cm - <strong>Délka:
  </strong>47 cm - <strong>Výška: </strong>46 cm</p>"
technical_info: "<p>Tloušťka rámu: 4 cm</p><p>Tloušťka vnitřních prvků: 2,5 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: ''
product_info: "<p></p>"
product_photos:
- "/v1605191265/forestry/ART_AGILIS_modul%C3%A1rn%C3%AD_syst%C3%A9m_5_bqzbn1.png"
- "/v1605191266/forestry/ART_AGILIS_modul%C3%A1rn%C3%AD_syst%C3%A9m_4_xlimp8.png"
- "/v1605191265/forestry/ART_AGILIS_modul%C3%A1rn%C3%AD_syst%C3%A9m_3_hrblyg.png"

---
