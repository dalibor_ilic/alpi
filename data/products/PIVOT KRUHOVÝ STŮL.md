---
brand: data/brands/Artisan.md
name: PIVOT KRUHOVÝ STŮL
categories: []
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605609963/forestry/ART_PIVOT_kruhov%C3%BD_st%C5%AFl_6_d2hijn.png"
- "/v1605609953/forestry/ART_PIVOT_kruhov%C3%BD_st%C5%AFl_1_vscacd.png"
- "/v1605609953/forestry/ART_PIVOT_kruhov%C3%BD_st%C5%AFl_2_sj6kz3.png"
dimensions: "<p><strong>Šířka: </strong>140/120<strong> </strong>cm - <strong>Délka:</strong>
  140/120<strong> </strong>cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Tloušťka desky: 2 cm</p><p>Vizuální tloušťka desky: 0,8 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>86 700 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605610148/forestry/ART_PIVOT_kruhov%C3%BD_st%C5%AFl_5_ldzeg0.png"
- "/v1605610148/forestry/ART_PIVOT_kruhov%C3%BD_st%C5%AFl_4_ubucaz.png"
- "/v1605610148/forestry/ART_PIVOT_kruhov%C3%BD_st%C5%AFl_3_vuhiek.png"

---
