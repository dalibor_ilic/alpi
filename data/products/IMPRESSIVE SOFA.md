---
brand: data/brands/Atlas.md
name: IMPRESSIVE SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Měkké polštáře plněné nejjemnějším peřím
  a nastavitelná opěrka hlavy s pěti různými polohami splní všechny podmínky ideálního
  pohodlí. IMPRESSIVE jistě promění váš obývací pokoj v oázu pohodlí a jeho výrazný
  design mu dodá luxusní nádech, který nenechá nikoho lhostejným. Vyšívané kosočtverce
  na područkách a polštářích opěradla dělají tento model jedinečným.</p>'
product_images:
- "/v1642435884/forestry/ATL_IMPRESSIVE_sofa_1_u9pd7i.png"
- "/v1642435885/forestry/ATL_IMPRESSIVE_sofa_8_mt9ufv.png"
- "/v1642435885/forestry/ATL_IMPRESSIVE_sofa_10_buvai0.png"
- "/v1642435885/forestry/ATL_IMPRESSIVE_sofa_12_b5aomw.png"
- "/v1642435885/forestry/ATL_IMPRESSIVE_sofa_11_qbotwl.png"
- "/v1642435885/forestry/ATL_IMPRESSIVE_sofa_9_z8ynu6.png"
dimensions: "<p>Dvojsed:</p><p><strong>Šířka: </strong>187 cm - <strong>Délka: </strong>109
  cm - <strong>Výška: </strong>64/89 cm - <strong>Výška sedu:</strong> 41 cm - <strong>Hloubka
  sedu:</strong> 69 cm</p><p>Trojsed:</p><p><strong>Šířka:</strong> 221 cm - <strong>Délka:
  </strong>109 cm - <strong>Výška: </strong>64/89 cm - <strong>Výška sedu:</strong>
  41 cm - <strong>Hloubka sedu:</strong> 69 cm</p><p>Čtyřsed: </p><p><strong>Šířka:</strong>
  275 cm - <strong>Délka: </strong>109 cm - <strong>Výška: </strong>64/89 cm - <strong>Výška
  sedu:</strong> 41 cm - <strong>Hloubka sedu:</strong> 69 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36, peří se silikonem</p><p><strong>Opěradlo: </strong>pěna L 25/15</p><p><strong>Elastické
  popruhy</strong></p><p><strong>Nožičky: </strong>plastové nožičky 20 mm</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>66 400 Kč</p><p><strong>Cena v látce od:
  </strong>45 100 Kč  </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/f558ca49580d74097b0bb2b4b59876e0ec0ba8fc.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1642436160/forestry/ATL_IMPRESSIVE_sofa_7_xyeeff.png"
- "/v1642436159/forestry/ATL_IMPRESSIVE_sofa_6_s5kw9j.png"
- "/v1642436159/forestry/ATL_IMPRESSIVE_sofa_5_hbtso4.png"
- "/v1642436160/forestry/ATL_IMPRESSIVE_sofa_4_vvtnhp.png"
- "/v1642436160/forestry/ATL_IMPRESSIVE_sofa_3_yf0sge.png"
- "/v1642436159/forestry/ATL_IMPRESSIVE_sofa_2_pbasoe.png"

---
