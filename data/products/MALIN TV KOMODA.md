---
brand: data/brands/Woak.md
name: MALIN TV KOMODA
categories:
- Sideboard
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p><p>Duší kolekce je plynulost tvarů, která
  vyplňuje každý spoj. Čistota materiálu vybízí k doteku dokonale hladkých ploch,
  které jsou ale vizuálně zajímavé. Přírodní tvar nábytku připomíná tradici severoevropského
  truhlářství s odkazem na některé ikonické prvky designu z padesátých let, ale s
  přepracováním ideálním pro současnost.</p>'
product_images:
- "/v1606744014/forestry/WHO_MAIL_tv_komoda_1_iakhhx.png"
- "/v1606744023/forestry/WHO_MAIL_tv_komoda_6_qvbscw.png"
dimensions: "<p><strong>Šířka: </strong>180 - 240 cm - <strong>Délka: </strong>45
  - 50 cm - <strong>Výška:</strong> 40 cm</p>"
technical_info: "<p>Tato TV komoda je vyrobena z masivního dřeva (dubu nebo ořechu),
  má olejovou úpravu a kovové nožičky. </p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od:</strong> 63 990 CZK</p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Malin_tv_unit_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1606744325/forestry/WHO_MAIL_tv_komoda_4_u3iur8.png"
- "/v1606744329/forestry/WHO_MAIL_tv_komoda_5_xq5ufl.png"
- "/v1606744329/forestry/WHO_MAIL_tv_komoda_3_zerz0f.png"
- "/v1606744329/forestry/WHO_MAIL_tv_komoda_2_aizvhe.png"

---
