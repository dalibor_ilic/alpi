---
brand: data/brands/Extraform.md
name: DOLOMIA KŘESLO
categories:
- Arm chair
description: '<p>Výrobce : <a href="https://www.alpicollection.com/znacky?brand=extraform#detail"
  title="EXTRAFORM"><strong>EXTRAFORM</strong></a></p><p>Dolomia je křeslo, které
  upoutá pozornost svým komfortem a elegancí, která je zajištěna relaxačním polstrováním,
  měkkým materiálem na dotyk a jedinečným designem. Dolomia kolekce přináší přírodní
  tvary do Vašeho domova a kanceláře. Pomocí kombinace barev pro Vaše moderní prostory.
  </p>'
product_images:
- "/v1644500556/forestry/EXT_DOLOMIA_k%C5%99eslo_6_rk5oyb.png"
- "/v1644500556/forestry/EXT_DOLOMIA_k%C5%99eslo_1_h6l8sd.png"
- "/v1644500550/forestry/EXT_DOLOMIA_k%C5%99eslo_2_afy0qq.png"
- "/v1644500556/forestry/EXT_DOLOMIA_k%C5%99eslo_3_golsap.png"
- "/v1644500556/forestry/EXT_DOLOMIA_k%C5%99eslo_4_shme1d.png"
- "/v1644500556/forestry/EXT_DOLOMIA_k%C5%99eslo_5_biecnv.png"
dimensions: "<p><strong>Šířka: </strong>104 cm - <strong>Délka: </strong>95 cm - <strong>Výška:</strong>
  79 cm - <strong>Výška sedu:</strong> 44 cm - <strong>Hloubka sedu: </strong>48 cm</p>"
technical_info: "<p>Nosná konstrukce ze sušeného bukového dřeva, lepenky, 18 mm silné
  překližky, MDF desky. </p><p>Sedáky - vysoce elastická HR pěna, 200 g tepelně pojené
  plsti, elastické popruhy. </p><p>Opěrky - ergonomická vysoce elastická pěna, 400
  g tepelně pojené plsti. </p><p>Nohy z dubového dřeva, naolejované. Volitelně z bukového
  dřěva v šesti barvách. </p>"
materials: "<p>K dispozici je široká škála látek a kůže. </p>"
price: "<p><strong>Cena od:</strong> 21 700 Kč</p>"
product_info: ''
product_photos:
- "/v1644500817/forestry/EXT_DOLOMIA_k%C5%99eslo_7_kuesbs.png"

---
