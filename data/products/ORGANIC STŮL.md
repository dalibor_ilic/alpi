---
brand: data/brands/Woak.md
name: 'ORGANIC STŮL '
categories:
- Table
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=woak#detail"
  title="WOAK"><strong>WOAK</strong></a></p>'
product_images:
- "/v1607003641/forestry/WHO_ORGANIC_st%C5%AFl_1_mb5fka.png"
- "/v1607003647/forestry/WHO_ORGANIC_st%C5%AFl_4_swd07p.png"
dimensions: "<p><strong>Šířka: </strong>160 - 300 cm - <strong>Délka: </strong>90
  - 100 cm - <strong>Výška:</strong> 75 cm</p>"
technical_info: "<p>materiál: ořech nebo dub</p><p>povrchová úprava: lak nebo olej</p>"
materials: <p><a href="https://drive.google.com/file/d/1RBGpKLccTlya04hr3tPYXfcLmSolvslt/view"
  title=""><strong>VZORNÍK MATERIÁLŮ</strong></a><strong> </strong></p>
price: "<p><strong>Cena od: </strong>54 100 Kč </p>"
product_info: '<p>Více informací: <a href="http://www.woakdesign.com/wp-content/uploads/2020/01/Woak_Organic_dining_table_2020.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p><p>Montážní návod: <a href="https://drive.google.com/file/d/1qFwarGYuX-S5wb-3CWfHhWgyw5wHIFd-/view?usp=sharing"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1607003886/forestry/WHO_ORGANIC_st%C5%AFl_3_bsqn8o.png"
- "/v1607003886/forestry/WHO_ORGANIC_st%C5%AFl_2_yleqqp.png"

---
