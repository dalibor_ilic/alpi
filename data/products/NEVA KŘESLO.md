---
brand: data/brands/Artisan.md
name: NEVA KŘESLO
categories:
- Arm chair
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1604398869/forestry/ART_NEVA_k%C5%99eslo_1_kr2a1i.png"
- "/v1604398869/forestry/ART_NEVA_k%C5%99eslo_3_ntj58k.png"
- "/v1604398869/forestry/ART_NEVA_k%C5%99eslo_9_torhj0.png"
- "/v1604398868/forestry/ART_NEVA_k%C5%99eslo_8_dmhtnk.png"
- "/v1644251592/forestry/Product_foto0002_qja3ea.png"
- "/v1644251239/forestry/Product_foto-3_kopie_k0rezi.png"
- "/v1644251268/forestry/Product_foto-4_kopie_ilrfyj.png"
- "/v1644250762/forestry/Product_foto-38_p1gktu.png"
- "/v1644250763/forestry/Product_foto-40_rmerv9.png"
- "/v1644250763/forestry/Product_foto-39_miyheq.png"
- "/v1644251579/forestry/Product_foto000_u1kxcm.png"
dimensions: "<p><strong>Nízké křeslo:<br>Šířka: </strong>71<strong> </strong>cm -
  <strong>Délka:</strong> 86 cm - <strong>Výška:</strong> 74 cm - <strong>Výška sedu:</strong>
  39 cm - <strong>Hloubka sedu:</strong> cm</p><p><strong>Vysoké křeslo:<br>Šířka:
  </strong>71<strong> </strong>cm - <strong>Délka:</strong> 86 cm - <strong>Výška:</strong>
  93 cm - <strong>Výška sedu:</strong> 39 cm - <strong>Hloubka sedu:</strong> cm</p>"
technical_info: "<p>Masivní dřevo, olejovaná úprava, provedení v látce i kůži.</p><p><strong>Nízké
  křeslo</strong></p><p>Velikost sedu: 62x50 cm</p><p>Průměr nohou: spodní část 2,6
  cm, vrchní část 4,4 cm</p><p><strong>Vysoké křeslo</strong></p><p>Velikost sedu:
  62x50 cm</p><p>Průměr nohou: spodní část 2,6 cm, vrchní část 4,4 cm</p>"
materials: <p><a href=" https://drive.google.com/file/d/1JAKjxN-REnV0yCIuRsUZxSVK_Hd8coHV/view?usp=sharing"
  title=""><strong>LÁTKY</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/165KbrK1NeZjuPcVuGZxCFpixtuW0Od-V/view?usp=sharing"
  title=""><strong>KŮŽE</strong> </a>| <a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong> </a> </p>
price: "<p><strong>Cena od:</strong> 49 900 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1604408444/forestry/ART_NEVA_k%C5%99eslo_7_zwiq9t.png"
- "/v1604408442/forestry/ART_NEVA_k%C5%99eslo_5_fugrbm.png"
- "/v1604408444/forestry/ART_NEVA_k%C5%99eslo_4_yydpxd.png"
- "/v1604408442/forestry/ART_NEVA_k%C5%99eslo_6_gnsr3c.png"

---
