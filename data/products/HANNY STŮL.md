---
brand: data/brands/Artisan.md
name: HANNY STŮL
categories:
- Table
description: '<p>Výrobce: <a href="https://www.alpicollection.com/znacky?brand=artisan&amp;page=2"
  title=""><strong>ARTISAN</strong></a></p>'
product_images:
- "/v1605537631/forestry/ART_HANNY_st%C5%AFl_8_i1fvti.png"
- "/v1605537631/forestry/ART_HANNY_st%C5%AFl_6_c0jrrs.png"
- "/v1605537622/forestry/ART_HANNY_st%C5%AFl_1_xwe5ci.png"
- "/v1605537631/forestry/ART_HANNY_st%C5%AFl_7_skzuvt.png"
dimensions: "<p><strong>Šířka: </strong>120/140/160/180/200/220/240/260 cm - <strong>Délka:</strong>
  90 cm - <strong>Výška: </strong>76 cm</p>"
technical_info: "<p>Masivní dřevo a olejovaná úprava.</p><p>Tloušťka desky: 4 cm</p><p>Vizuální
  tloušťka desky: 2 cm </p><p>Průměr nohou: spodní část 6x4 cm, vrchní část 11x4 cm</p>"
materials: <p><a href="https://drive.google.com/file/d/1ntlbN5yZBMjCz3eD8QcrmLdO20p4Phx_/view?usp=sharing"
  title=""><strong>DŘEVO</strong></a></p>
price: "<p><strong>Cena od:</strong> 25 800 Kč</p>"
product_info: "<p></p>"
product_photos:
- "/v1605537812/forestry/ART_HANNY_st%C5%AFl_5_jtkip9.png"
- "/v1605537813/forestry/ART_HANNY_st%C5%AFl_4_fwlp6c.png"
- "/v1605537811/forestry/ART_HANNY_st%C5%AFl_3_djspds.png"
- "/v1605537812/forestry/ART_HANNY_st%C5%AFl_2_gh8kes.png"

---
