---
brand: data/brands/Atlas.md
name: IBICA ŽIDLE
categories:
- Chair
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Nadčasový design této židle si okamžitě
  zamilujete. Ergonomický tvar opěradla zajišťuje výjimečné pohodlí. Výběr dřevěných,
  nebo čalouněných nohou umožňuje dokonalé zasazení do požadovaného interiéru. Židle
  Ibica existuje také ve variantě se sklopným opěradlem a jako taková je skutečným
  příkladem elegance a sofistikovanosti.</p>'
product_images:
- "/v1643281905/forestry/ATL_IBICA_%C5%BEidle_1_esfvjm.png"
- "/v1643281905/forestry/ATL_IBICA_%C5%BEidle_6_w5n7la.png"
- "/v1643281905/forestry/ATL_IBICA_%C5%BEidle_7_ytv0v8.png"
dimensions: "<p><strong>Šířka: </strong>53 cm - <strong>Délka: </strong>57 cm - <strong>Výška:
  </strong>81 cm - <strong>Výška sedu:</strong> 50 cm </p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo</p><p><strong>Podpěra:</strong>
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/36 </p><p><strong>Nožičky: </strong>dřevěné</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p><p>Dřevo: <a href="https://drive.google.com/file/d/1wLvT0Uaoq_lXQwgquJ_V3iXncpQHVI6w/view?usp=sharing"
  title="Dřevo DUB"><strong>DUB</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>13 500 Kč</p><p><strong>Cena v látce od:
  </strong>11 100 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/da2dab0c76ef11c33bba0e552d6f4d007a18f2d8.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643282023/forestry/ATL_IBICA_%C5%BEidle_5_wai5wz.png"
- "/v1643282023/forestry/ATL_IBICA_%C5%BEidle_4_gprcju.png"
- "/v1643282022/forestry/ATL_IBICA_%C5%BEidle_3_tkkyp2.png"
- "/v1643282023/forestry/ATL_IBICA_%C5%BEidle_2_f21zkk.png"

---
