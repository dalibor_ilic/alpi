---
brand: data/brands/Atlas.md
name: KOENA POSTEL
categories:
- Bed
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"><strong>ATLAS</strong></a></p><p>Ostrov
  Havaj je často označován jako „uprostřed ničeho s tím nejlepším ze všeho“. Můžete
  to považovat za akt přírody duchovně i fyzicky. Koena je dalším krokem v relaxaci,
  spolu s jemností a „reiki-oo“ tvarem Vám poskytne pohodlí . Vyvážené, jak má být
  všechno.</p>'
product_images:
- "/v1648814928/forestry/Product_foto_fo97ur.png"
- "/v1648814928/forestry/Product_foto-2_fmbpv2.png"
dimensions: "<p>180x 200 cm<strong> </strong>- <strong>Šířka: </strong>270<strong>
  </strong>cm - <strong>Délka: </strong>212 cm - <strong>Výška: </strong>90 cm</p>"
technical_info: "<p><strong>Potah:</strong> látka, kůže</p>"
materials: '<p>Látky: <a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"><strong>BASIC</strong></a>
  | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"><strong>LUX</strong></a>
  | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"><strong>ELEGANT
  1</strong></a> | <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"><strong>ELEGANT
  2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"><strong>EXCLUSIVE</strong></a>
  | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"><strong>PREMIUM</strong></a>
  | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"><strong>LUXURY</strong></a></p><p>Kůže:
  <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"><strong>CAT
  300-1000</strong></a></p>'
price: ''
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/dc836b86c3bb6be871f667a6702ea4fefef4990c.pdf"
  title=""><strong>Stáhnout PDF</strong></a></p>'
product_photos:
- "/v1648815263/forestry/Z_interi%C3%A9ru_dvimy0.png"
- "/v1648815264/forestry/Z_interi%C3%A9ru-2_i8uxmb.png"

---
