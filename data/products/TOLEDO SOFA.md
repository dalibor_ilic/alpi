---
brand: data/brands/Atlas.md
name: TOLEDO SOFA
categories:
- Sofa
description: '<p>Výrobce: <a href="http://www.alpicollection.com/znacky?brand=atlas&amp;fbclid=IwAR3iKq2FkBaBzdzTfUZizA5CQrSYC0ImKv-zUMVruKZvU2lJDg8o3djDRus#detail"
  title=""><strong>ATLAS</strong></a></p><p>Neobvyklý model, který vykazuje výjimečné
  řemeslné zpracování. Toledo je navrženo jako modulární pohovka, která se snadno
  přizpůsobí rozměrům prostoru. Precizně provedené ozdobné prošití na opěradle a sedácích
  jí dodává atraktivní a moderní vzhled. Lineární stehy zdobí také taburet ze stejné
  řady, který bude dokonalým doplňkem této zajímavé pohovky.</p>'
product_images:
- "/v1643099649/forestry/ATL_TOLEDO_sofa_1_opabct.png"
dimensions: "<p><strong>Šířka: </strong>290 cm - <strong>Délka: </strong>100 cm -
  <strong>Výška: </strong>71 cm - <strong>Výška sedu:</strong> 40 cm - <strong>Hloubka
  sedu:</strong> 70 cm</p>"
technical_info: "<p><strong>Materiál: </strong>masivní bukové dřevo vyztužené dřevěnými
  panely</p><p><strong>Potah:</strong> látka, nebo kůže</p><p><strong>Sedák:</strong>
  HR pěna 35/30 a pěna L 25/15 ve vatovém potahu</p><p><strong>Opěradlo: </strong>pěna
  L 25/15 a HR 35/30</p><p><strong>Elastické popruhy</strong></p><p><strong>Nožičky:
  </strong>plastové nožičky 25 mm</p>"
materials: '<p>Látky:<strong> </strong><a href="https://drive.google.com/file/d/140cfcIZTNwb-Zf3SjxHyShNSAPb2eKme/view?usp=sharing"
  title="Látky BASIC"><strong>BASIC</strong></a> | <a href="https://drive.google.com/file/d/19sbJk4T-86yFAcE-T32K96QJl0C6HTQw/view?usp=sharing"
  title="Látky LUX"><strong>LUX</strong></a> | <a href="https://drive.google.com/file/d/1h2YnItKcoDAUWmmMDrLMxkh7FeIQNBsP/view?usp=sharing"
  title="Látky ELEGANT 1"><strong>ELEGANT 1</strong></a><strong> </strong>| <a href="https://drive.google.com/file/d/1Arj-tizBu44trWX2nWdmAyomlVnpNWlT/view?usp=sharing"
  title="Látky ELEGANT 2"><strong>ELEGANT 2</strong></a> | <a href="https://drive.google.com/file/d/14cQhrwNDFm8Vk-tqCSKSCc9LJ86Q4hpy/view?usp=sharing"
  title="Látky EXCLUSIVE"><strong>EXCLUSIVE</strong></a> | <a href="https://drive.google.com/file/d/11skvDfLECW5ETwOdXUxPjuJUeICl6AXb/view?usp=sharing"
  title="Látky PREMIUM"><strong>PREMIUM</strong></a> | <a href="https://drive.google.com/file/d/1J8Uo5LmtjA5zSQWnCUrZz7gGZviP9IS3/view?usp=sharing"
  title="Látky LUXURY"><strong>LUXURY</strong></a></p><p>Kůže: <a href="https://drive.google.com/file/d/1LUI7mJu-egie9LTCP8H3ZuWs2W9NvwA9/view?usp=sharing"
  title="Kůže CAT 300-1000"><strong>CAT 300-1000</strong></a></p>'
price: "<p><strong>Cena v kůži od: </strong>83 400 Kč </p><p><strong>Cena v látce
  od: </strong>59 900 Kč </p>"
product_info: '<p>Produktová karta: <a href="https://atlassofas.eu/login/files/b6eb7504d81b1fe0f15adbd794dfaed7ed109265.pdf"
  title="">Stáhnout PDF</a></p>'
product_photos:
- "/v1643099796/forestry/ATL_TOLEDO_sofa_2_lgkznj.png"

---
