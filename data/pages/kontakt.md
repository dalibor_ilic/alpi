---
headline: Vítáme vás v našem showroomu v centru Prahy
gallery:
- "/v1608649024/forestry/ALPI_-_PRAHA_1_-_KONTAKT_2_vw54z1.jpg"
- "/v1608649027/forestry/ALPI_-_PRAHA_1_-_KONTAKT_1_jlg3h7.jpg"
content:
- text: |-
    SHOWROOM PRAHA
    ALPI home s.r.o.
    Nekázanka 882/7
    110 00 Praha 1
  subscribe_button: false
  button_text: Ukázat na mapě
  button_link: https://g.page/alpihome-nekazanka?share
- text: |-
    +420 737 209 200
    obchod@alpihome.cz
  subscribe_button: false
  button_text: Napište nám
  button_link: mailto:obchod@alpihome.cz
- text: |-
    Otevírací doba:
    Po-Pá: 09:00-18:00
    So: 10:00-13:00
    Ne: zavřeno

    Individuální termín návštěvy je možný po předchozí domluvě.
  subscribe_button: true
  button_text: ''
  button_link: ''
- text: |2-


    Pro architekty a designéry
  subscribe_button: false
  button_text: Stáhnout 2D/3D modely
  button_link: https://drive.google.com/drive/folders/0B568gBBHq87ha21NYVhTQlhOX3M

---
