---
headline: Reference
content: "<p></p>"
references:
- name: WOAK - Martis Forum Residence, Croatia
  gallery:
  - "/v1645097946/forestry/DSC_5921-Edit_lrqi2h.jpg"
  - "/v1645097944/forestry/DSC_6022_brzel7.jpg"
  - "/v1645097945/forestry/DSC_6065-Edit_jrgyfi.jpg"
  - "/v1645097943/forestry/DSC_6106-Edit-Edit_mc53ll.jpg"
  - "/v1645097940/forestry/DSC_6098-Edit_oxxsqi.jpg"
  - "/v1645097939/forestry/DSC_5939-Edit_p14viy.jpg"
  - "/v1645097939/forestry/DSC_5953_e0sf0o.jpg"
  - "/v1645097948/forestry/DSC_6040_gbzjf4.jpg"
  - "/v1645097947/forestry/DSC_5961_c88iga.jpg"
  - "/v1645097950/forestry/DSC_6033_r9kxvo.jpg"
  reference_note: WOAK - Martis Forum Residence, Croatia
- name: " WOAK - Brown Beach House, Croatia"
  gallery:
  - "/v1645097782/forestry/the_hotel_brown_beach_house_croatia_2_vcxwhv.jpg"
  - "/v1645097784/forestry/the_hotel_brown_beach_house_croatia_3_jbaztp.jpg"
  - "/v1645097781/forestry/the_hotel_brown_beach_house_croatia_1_udjgsc.jpg"
  reference_note: " WOAK - Brown Beach House, Croatia"
- name: 'Artisan - Furniture Fair, Belgrade, Serbia '
  gallery:
  - "/v1645097703/forestry/Belgrade_furniture_fair_Serbia_5_rajezm.jpg"
  - "/v1645097704/forestry/Belgrade_furniture_fair_Serbia_4_fplqwj.jpg"
  - "/v1645097704/forestry/Belgrade_furniture_fair_Serbia_2_ep3obn.jpg"
  - "/v1645097704/forestry/Belgrade_furniture_fair_Serbia_3_izl4qz.jpg"
  reference_note: 'Artisan - Furniture Fair, Belgrade, Serbia '
- name: Artisan - Cloud Apartment, Dubrovnik, Croatia
  gallery:
  - "/v1645097433/forestry/Cloud_apartment_Dubrovnik_Croatia_9_g5hxsz.jpg"
  - "/v1645097433/forestry/Cloud_apartment_Dubrovnik_Croatia_4_vwttjj.jpg"
  - "/v1645097434/forestry/Cloud_apartment_Dubrovnik_Croatia_3_dlkhui.jpg"
  - "/v1645097433/forestry/Cloud_apartment_Dubrovnik_Croatia_10_hiqwen.jpg"
  - "/v1645097433/forestry/Cloud_apartment_Dubrovnik_Croatia_5_vqvxnj.jpg"
  - "/v1645097431/forestry/Cloud_apartment_Dubrovnik_Croatia_1_uuqk79.jpg"
  - "/v1645097434/forestry/Cloud_apartment_Dubrovnik_Croatia_7_fzoues.jpg"
  - "/v1645097433/forestry/Cloud_apartment_Dubrovnik_Croatia_6_fduwvt.jpg"
  - "/v1645097433/forestry/Cloud_apartment_Dubrovnik_Croatia_8_a9vazf.jpg"
  - "/v1645097433/forestry/Cloud_apartment_Dubrovnik_Croatia_2_jsg4te.jpg"
  reference_note: Artisan - Cloud Apartment, Dubrovnik, Croatia
- name: Artisan - Gumno Private House,  Krk Island, Croatia
  gallery:
  - "/v1645097248/forestry/Gumno_private_house_Krk_Island_Croatia_1a_la0fki.jpg"
  - "/v1645097250/forestry/Gumno_private_house_Krk_Island_Croatia_3a_t58eob.jpg"
  - "/v1645097250/forestry/Gumno_private_house_Krk_Island_Croatia_5a_grd3b2.jpg"
  - "/v1645097250/forestry/Gumno_private_house_Krk_Island_Croatia_2a_hs0bsi.jpg"
  - "/v1645097250/forestry/Gumno_private_house_Krk_Island_Croatia_4a_xv1elc.jpg"
  reference_note: Artisan - Gumno Private House,  Krk Island, Croatia
- name: Artisan - Olive Private House, Pag Island, Croatia
  gallery:
  - "/v1645096640/forestry/Olive_house_private_house_Pag_Island_Croatia_1_ukh0vk.jpg"
  - "/v1645096640/forestry/Olive_house_private_house_Pag_Island_Croatia_3_bf7my1.jpg"
  - "/v1645096640/forestry/Olive_house_private_house_Pag_Island_Croatia_2_n7eibj.jpg"
  - "/v1645096641/forestry/Olive_house_private_house_Pag_Island_Croatia_4_lhkq8q.jpg"
  reference_note: Artisan - Olive Private House, Pag Island, Croatia
- name: 'Artisan - Ox&Klee Restaurant, Cologne, Germany '
  gallery:
  - "/v1645096505/forestry/Ox_Klee_rastaurant_in_Cologne_Germany_1_e69qlg.jpg"
  - "/v1645096504/forestry/Ox_Klee_rastaurant_in_Cologne_Germany_3_txyetw.jpg"
  - "/v1645096505/forestry/Ox_Klee_rastaurant_in_Cologne_Germany_2_ymzv9g.jpg"
  - "/v1645096504/forestry/Ox_Klee_rastaurant_in_Cologne_Germany_4_b5jvxn.jpg"
  reference_note: 'Artisan - Ox&Klee Restaurant, Cologne, Germany '
- name: Artisan - Piu Restaurant, Zurich, Schwitzerland
  gallery:
  - "/v1645096388/forestry/Piu_restaurant_Zurich_Switzerland_3_fz3rv3.jpg"
  - "/v1645096388/forestry/Piu_restaurant_Zurich_Switzerland_6_aahogd.jpg"
  - "/v1645096388/forestry/Piu_restaurant_Zurich_Switzerland_1_i9ih7e.jpg"
  - "/v1645096389/forestry/Piu_restaurant_Zurich_Switzerland_2_tcrtuw.jpg"
  - "/v1645096388/forestry/Piu_restaurant_Zurich_Switzerland_5_a2sjwj.jpg"
  reference_note: Artisan - Piu Restaurant, Zurich, Schwitzerland
- name: 'Artisan - Private Apartment, Dubrovnik, Croatia '
  gallery:
  - "/v1645096270/forestry/Private_apartment_Dubrovnik_Croatia12_frxkth.jpg"
  - "/v1645096247/forestry/Private_apartment_Dubrovnik_Croatia1_ibnxyf.jpg"
  - "/v1645096244/forestry/Private_apartment_Dubrovnik_Croatia10_lwrr0r.jpg"
  - "/v1645096246/forestry/Private_apartment_Dubrovnik_Croatia_oipxk4.jpg"
  - "/v1645096243/forestry/Private_apartment_Dubrovnik_Croatia8_jnl5hn.jpg"
  - "/v1645096241/forestry/Private_apartment_Dubrovnik_Croatia5_rb7tq1.jpg"
  reference_note: 'Artisan - Private Apartment, Dubrovnik, Croatia '
- name: Artisan - Private House, Hangzhou, China
  gallery:
  - "/v1645096117/forestry/Private_house_Hangzhou_China_10_md3ky5.jpg"
  - "/v1645096117/forestry/Private_house_Hangzhou_China_9_mobiyt.jpg"
  - "/v1645096083/forestry/Private_house_Hangzhou_China_3_nw9tsg.jpg"
  - "/v1645096042/forestry/Private_house_Hangzhou_China_6_tpwrk8.jpg"
  - "/v1645096041/forestry/Private_house_Hangzhou_China_5_k7rjrh.jpg"
  - "/v1645096037/forestry/Private_house_Hangzhou_China_4_jilleu.jpg"
  - "/v1645096043/forestry/Private_house_Hangzhou_China_8_hhjq8p.jpg"
  - "/v1645096082/forestry/Private_house_Hangzhou_China_2_uthutf.jpg"
  - "/v1645096038/forestry/Private_house_Hangzhou_China_7_han1hn.jpg"
  - "/v1645096081/forestry/Private_house_Hangzhou_China_1_bq97n7.jpg"
  - "/v1645096115/forestry/Private_house_Hangzhou_China_11_ydkysf.jpg"
  reference_note: Artisan - Private House, Hangzhou, China
- name: 'Artisan - Private House, Ljubljana, Slovenia '
  gallery:
  - "/v1645095891/forestry/Private_house_Ljubljana_Slovenia5_e6cqxm.jpg"
  - "/v1645095907/forestry/Private_house_Ljubljana_Slovenia_uycacz.jpg"
  - "/v1645095891/forestry/Private_house_Ljubljana_Slovenia3_akvznl.jpg"
  - "/v1645095891/forestry/Private_house_Ljubljana_Slovenia6_mdmkye.jpg"
  - "/v1645095891/forestry/Private_house_Ljubljana_Slovenia2_a38yvs.jpg"
  - "/v1645095906/forestry/Private_house_Ljubljana_Slovenia1_essob2.jpg"
  - "/v1645095890/forestry/Private_house_Ljubljana_Slovenia4_v1y9ti.jpg"
  reference_note: 'Artisan - Private House, Ljubljana, Slovenia '
- name: 'Artisan - Private House, Moscow, Russia '
  gallery:
  - "/v1645095740/forestry/Private_house_Moscow_Russia1_lx5yz2.jpg"
  - "/v1645095740/forestry/Private_house_Moscow_Russia3_xgndwa.jpg"
  - "/v1645095708/forestry/Private_house_Moscow_Russia7_jjbxio.jpg"
  - "/v1645095740/forestry/Private_house_Moscow_Russia2_h1dtos.jpg"
  - "/v1645095708/forestry/Private_house_Moscow_Russia8_omtjvi.jpg"
  - "/v1645095708/forestry/Private_house_Moscow_Russia6_aqiz22.jpg"
  - "/v1645095708/forestry/Private_house_Moscow_Russia4_rtxlvb.jpg"
  - "/v1645095708/forestry/Private_house_Moscow_Russia5_tvbmiw.jpg"
  - "/v1645095740/forestry/Private_house_Moscow_Russia_hifwfl.jpg"
  - "/v1645095725/forestry/Private_house_Moscow_Russia9_uwgxuf.jpg"
  reference_note: 'Artisan - Private House, Moscow, Russia '
- name: Artisan - Private House, Wien, Austria
  gallery:
  - "/v1645095541/forestry/Private_house_Wien_Austria_3_qhnaos.jpg"
  - "/v1645095543/forestry/Private_house_Wien_Austria_5_cqytgk.jpg"
  - "/v1645095543/forestry/Private_house_Wien_Austria_7_xkd6zo.jpg"
  - "/v1645095543/forestry/Private_house_Wien_Austria_4_nilfbg.jpg"
  - "/v1645095543/forestry/Private_house_Wien_Austria_6_dlhf7h.jpg"
  - "/v1645095543/forestry/Private_house_Wien_Austria_9_svlsxj.jpg"
  - "/v1645095543/forestry/Private_house_Wien_Austria_8_phjdmy.jpg"
  - "/v1645095537/forestry/Private_house_Wien_Austria_1_p9zg7l.jpg"
  - "/v1645095538/forestry/Private_house_Wien_Austria_2_s8glth.jpg"
  reference_note: Artisan - Private House, Wien, Austria
- name: Artisan - Tokyo Apartman, Jahorina, BiH
  gallery:
  - "/v1645095014/forestry/Tokyo_Aparatman_Jahorina_BiH_1_jy2uyt.jpg"
  reference_note: Artisan - Tokyo Apartman, Jahorina, BiH
- name: 'Artisan - Restaurant Asian Post Office, Stockholm, Sweden '
  gallery:
  - "/v1645094834/forestry/Restaurant_Asian_Post_Office_Stockholm_Sweden_sgsz8j.jpg"
  - "/v1645094836/forestry/Restaurant_Asian_Post_Office_Stockholm_Sweden_3_hyyaax.jpg"
  - "/v1645094835/forestry/Restaurant_Asian_Post_Office_Stockholm_Sweden_2_umjpbw.jpg"
  - "/v1645094835/forestry/Restaurant_Asian_Post_Office_Stockholm_Sweden_1_lyes1s.jpg"
  reference_note: 'Artisan - Restaurant Asian Post Office, Stockholm, Sweden '
- name: Artisan - Starbucks, Milano, Italy
  gallery:
  - "/v1608645533/forestry/ARTISAN_-_STARBUCKS_2_uf3xsj.jpg"
  - "/v1608645533/forestry/ARTISAN_-_STARBUCKS_uau5cp.jpg"
  - "/v1608645532/forestry/Artisan_-_Starbu_sipzdf.jpg"
  - "/v1608645533/forestry/ARTISAN_-_Restaurant_tfgae5.jpg"
  - "/v1608644761/forestry/starbucks_kukzh5.png"
  reference_note: Artisan - Starbucks, Milano, Italy
- name: Artisan - Kai bar, Veliko Trgovisce, Croatia
  gallery:
  - "/v1608645892/forestry/ARTISAN_-_Restaurant_4_f3pgkf.jpg"
  - "/v1608645892/forestry/ARTISAN_-_Restaurace_4_leiryk.jpg"
  - "/v1608645891/forestry/ARTISAN_-_Restaurant_5_r3uo57.jpg"
  - "/v1608645890/forestry/ARTISAN_-_Restaurace_5_zjb7fs.jpg"
  - "/v1608645887/forestry/ARTISAN_-_Restaurant_3_v5ltki.jpg"
  - "/v1645093270/forestry/Kai_bar_Veliko_Trgovisce_Croatia_3_tgzxcx.jpg"
  - "/v1608645892/forestry/ARTISAN_-_Restaurant_2_qtfcrc.jpg"
  - "/v1608645887/forestry/ARTISAN_-_Restaurant_1_wjmkw4.jpg"
  reference_note: Artisan - Kai Bar, Veliko Trgovisce, Croatia
- name: WOAK - Hotel Trogir, Croatia
  gallery:
  - "/v1608644894/forestry/WOAK_realizace_hotel_Trogir_5_bkozj6.jpg"
  - "/v1608644894/forestry/WOAK_realizace_hotel_Trogir_7_akgikt.jpg"
  - "/v1608644894/forestry/WOAK_realizace_hotel_Trogir_6_dxm9c5.jpg"
  - "/v1608644893/forestry/WOAK_realizace_hotel_Trogir_3_akdrbn.jpg"
  - "/v1608644893/forestry/WOAK_realizace_hotel_Trogir_4_fjnjxt.jpg"
  - "/v1608644896/forestry/WOAK_realizace_hotel_Trogir_8_mvcfyl.jpg"
  - "/v1608644894/forestry/WOAK_realizace_hotel_Trogir_1_yxhsbf.jpg"
  - "/v1608644896/forestry/WOAK_realizace_hotel_Trogir_9_fijtte.jpg"
  reference_note: WOAK - Hotel Trogir, Croatia
- name: Artisan - Vincek Pastry shop, Zagreb, Croatia
  gallery:
  - "/v1645093576/forestry/Vincek_pastry_shop_Zagreb_Croatia_5_mbt80k.jpg"
  - "/v1645093576/forestry/Vincek_pastry_shop_Zagreb_Croatia_3_j0mlwh.jpg"
  - "/v1645093576/forestry/Vincek_pastry_shop_Zagreb_Croatia_2_pqts5w.jpg"
  - "/v1608644849/forestry/Vincek_pastry_shop_Zagreb_Croatia_1_pje3hg.jpg"
  - "/v1645093575/forestry/Vincek_pastry_shop_Zagreb_Croatia_4_gqm847.jpg"
  reference_note: Artisan - Vincek Pastry shop, Zagreb, Croatia
- name: Artisan - Michelberger Hotel, Berlin, Germany
  gallery:
  - "/v1645093661/forestry/Michelberger_Hotel_Berlin_4_ixt57m.jpg"
  - "/v1608639016/forestry/Michelberger_Hotel_Berlin_2_zwbzwb.jpg"
  - "/v1645093661/forestry/Michelberger_Hotel_Berlin_3_vhzusn.jpg"
  - "/v1645093660/forestry/Michelberger_Hotel_Berlin_1_cpdgeb.jpg"
  reference_note: Artisan - Michelberger Hotel, Berlin, Germany
- name: Artisan - Lemon Tree Restaurant, Deventer, Netherlands
  gallery:
  - "/v1645093766/forestry/Lemon_Tree_restaurant_in_Deventer_The_Netherlands_1_kpujk4.jpg"
  - "/v1645093724/forestry/Lemon_Tree_restaurant_in_Deventer_The_Netherlands_6_evhrqx.jpg"
  - "/v1608638908/forestry/Lemon_Tree_restaurant_in_Deventer_The_Netherlands_2_tqq4q6.jpg"
  - "/v1645093704/forestry/Lemon_Tree_restaurant_in_Deventer_The_Netherlands_4_b8u4ud.jpg"
  - "/v1645093725/forestry/Lemon_Tree_restaurant_in_Deventer_The_Netherlands_5_qqlx1f.jpg"
  - "/v1645093704/forestry/Lemon_Tree_restaurant_in_Deventer_The_Netherlands_3_g2qluq.jpg"
  reference_note: Artisan - Lemon Tree Restaurant, Deventer, Netherlands
- name: Artisan - Andaz London Liverpool Street Hotel, UK
  gallery:
  - "/v1608638331/forestry/Andaz_London_Liverpool_Street_hotel_UK_1_gx5n7c.jpg"
  - "/v1645094083/forestry/Andaz_London_Liverpool_Street_hotel_UK_xgnszk.jpg"
  - "/v1645094083/forestry/Andaz_London_Liverpool_Street_hotel_UK_2_wgh1vb.jpg"
  - "/v1608645173/forestry/ARTISAN_-_Hotel_rmzdnt.jpg"
  - "/v1608638331/forestry/56589121_rv0pma.jpg"
  - "/v1608638331/forestry/77061303_ix6irp.jpg"
  reference_note: Artisan - Andaz London Liverpool Street Hotel, UK
- name: Artisan - Hotel Gasthof Hirschen, Germany
  gallery:
  - "/v1608638591/forestry/Artisan_-_Hotel_Gasthof_Hirschen_Schwarzenberg_2_ec4buv.png"
  reference_note: Artisan - Hotel Gasthof Hirschen, Germany
- gallery:
  - "/v1608637965/forestry/Cafe_Zurich_airport_Switzerland_xd2mnh.jpg"
  name: Artisan - Zurich Airport, Switzerland
  reference_note: Artisan - Zurich Airport, Switzerland
- gallery:
  - "/v1608637852/forestry/Blue_Lagoon_Iceland_restaurant_2_z0getj.jpg"
  - "/v1608637852/forestry/Blue_Lagoon_Iceland_restaurant_1_tczxur.jpg"
  name: Artisan - Blue Lagoon Restaurant, Iceland
  reference_note: Artisan - Blue Lagoon Restaurant, Iceland
- name: Artisan - Hiša Denk, Slovenia
  gallery:
  - "/v1645094520/forestry/Hisa_Denk_restaurant_Zgornja_Kungota_Slovenia_2_mf4uf0.jpg"
  - "/v1608638380/forestry/Hisa_Denk_restaurant_Zgornja_Kungota_Slovenia_9_l36kul.jpg"
  - "/v1608638380/forestry/Hisa_Denk_restaurant_Zgornja_Kungota_Slovenia_10_c2dttw.jpg"
  - "/v1645094520/forestry/Hisa_Denk_restaurant_Zgornja_Kungota_Slovenia_6_njxesq.jpg"
  - "/v1645094520/forestry/Hisa_Denk_restaurant_Zgornja_Kungota_Slovenia_8_qdvagx.jpg"
  - "/v1645094520/forestry/Hisa_Denk_restaurant_Zgornja_Kungota_Slovenia_7_l4vxro.jpg"
  - "/v1645094520/forestry/Hisa_Denk_restaurant_Zgornja_Kungota_Slovenia_3_st6odu.jpg"
  - "/v1645094520/forestry/Hisa_Denk_restaurant_Zgornja_Kungota_Slovenia_4_ucnqwo.jpg"
  - "/v1645094520/forestry/Hisa_Denk_restaurant_Zgornja_Kungota_Slovenia_5_tevifw.jpg"
  reference_note: Artisan - Hiša Denk, Slovenia

---
