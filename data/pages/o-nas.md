---
headline: SPOJENÍ S PŘEDNÍMI VÝROBCI NÁBYTKU
gallery:
- "/v1595603576/forestry/about_oc6vxi.jpg"
content:
- text: <h2>Specializujeme se na projektování, design a realizaci bytových a komerčních
    interiérů.</h2><p>Jsme rodinná společnost založená v roce 2015. Zastupujeme přední
    světové výrobce nábytku. Nabízíme individuální přístup a snažíme se vždy najít
    to nejlepší vhodné řešení pro konkrétní interiér, ve spolupráci s klientem.</p><p
    class="MsoNormal">Víme, že nábytek nestačí pouze kvalitně vyrobit. Nabídkou komplexního
    servisu s osobním přístupem upevníme vztahy a spokojenost naších klientů.</p><p>Zveme
    Vás k návštěvě našeho showroomu v Praze (Praha 1, 500 m2) a v Berouně (400 m2).
    Disponujeme vlastním skladovým areálem – 4 200 m2. Zajištujeme kompletní logistiku,
    od výrobce až do vašeho domova. Nabízíme našim zákazníkům jistotu a spolehlivost,
    profesionální a přátelský přístup.</p>
  images:
  - "/v1595231869/forestry/homepage2_s2rs4k.jpg"
- text: "<p>- rodinná firma</p><p>- kompletní servis, od návrhu až po realizaci a
    pozáruční servis</p><p>- individuální přístup</p>"
  images:
  - "/v1595315342/forestry/reference4_sv2mmv.jpg"
  - "/v1595231874/forestry/homepage3_gubv7i.jpg"
- text: "<p>Zakládáme si na spokojenosti všech našich zákazníků.</p>"
  images:
  - "/v1595231868/forestry/homepage_gyoew1.jpg"

---
