---
name: Atlas
description: '<p>Produktový katalog si můžete stáhnout zde: <a href="https://rajnabytku.static.s10.upgates.com/9/95d91be1864256-atlas-catalogue-2019.pdf"
  title="Katalog Atlas"><strong>Stáhnout katalog</strong></a></p><p>ATLAS je značkou
  nábytku, kterou tvoří mezinárodní tým designérů. ATLAS představuje inovativní a
  krásný nábytek. ATLAS se zaměřuje především na kvalitní, pohodlné a elegantní pohovky.
  Známá je především kategorie flexibilních modulárních pohovek, které se přizpůsobí
  jakémukoliv prostoru a situaci.</p><p class="MsoNormal">ATLAS pracuje s vysoce kvalitními
  materiály, používá masivních dřevěných konstrukcí, kvalitních mechanismů rozkládání
  pohovek, paměťové pěny a nejjemnějšího peří.</p><p class="MsoNormal">ATLAS věnuje
  pozornost každému detailu. Nábytek je vyráběn s mistrovskou péčí za pomoci inovativních
  metod, ale základ tvoří ruční výroba.</p>'
brand_image:
- "/v1608637024/forestry/LANDSCAPE_D5_LR_RONDO_Otaru_213_Pegasus_46069_2_tbw7qt.jpg"

---
