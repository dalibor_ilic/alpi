---
name: Artisan
description: '<p>Produktový katalog si můžete stáhnout zde: <a href="https://rajnabytku.static.s10.upgates.com/5/55fe1dcd3ba1ca-artisan-katalog-veliki-2019.pdf"
  title="Katalog Artisan"><strong>Stáhnout katalog</strong></a></p><p>Firma ARTISAN
  vychází z tradičního rodinného truhlářství, které do něj přeneslo veškerou svou
  lásku ke dřevu a 50leté zkušenosti se zpracováním dřeva.</p><p class="MsoNormal">Společnost
  Artisan se specializuje na ruční výrobu vysoce kvalitního nábytku z masivního dřeva.
  Právě specializace na ruční řemeslnou práci je to, co Artisan odlišuje.</p>'
brand_image:
- "/v1608731108/forestry/ARTISAN_-_Restaurant_1_sdmhuh.jpg"

---
