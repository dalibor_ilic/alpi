---
name: Woak
description: '<p>Produktový katalog si můžete stáhnout zde: <a href="http://www.woakdesign.com/wp-content/uploads/2020/06/Woak_Catalogue_20.pdf"
  title="Katalog WOAK"><strong>Stáhnout katalog</strong></a></p><p>Značka WOAK navazuje
  na dřevařskou tradici a řemeslné tesařství. WOAK symbolizuje sílu, duši a touhu
  otevřít se všemu novému.</p><p class="MsoNormal">Nábytek tvoříme s velkou péčí o
  zákazníka i životní prostředí. Práce s masivním dřevem nás nepřestává inspirovat.
  Specializujeme se zejména na dubové a ořechové dřevo. Velký význam přikládáme udržitelnosti.</p><p
  class="MsoNormal">Naším cílem je vytvářet nadčasový a jedinečný nábytek. Inspiraci
  čerpáme z nejvýznamnějších období v designu nábytku a opíráme se o stále se měnící
  současnou estetiku.</p><p class="MsoNormal">Práci se dřevem obohacujeme o experimenty
  s dalšími přírodními materiály a inovativními technologiemi. Vyrábíme speciální
  druh skla s duhovým vzhledem.</p>'
brand_image:
- "/v1608635782/forestry/WOAK_interier_1_lzy74m.jpg"

---
