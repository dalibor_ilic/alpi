---
name: Extraform
description: '<p>Produktový katalog si můžete stáhnout zde: <a href="http://extraform.rs/wp-content/themes/crisp-child/katalog/Extraform-Veliki_katalog-2019-RS-Spreads.pdf"
  title="Katalog Extraform"><strong>Stáhnout katalog</strong></a></p><p>Extraform
  se specializuje na designový čalouněný nábytek. Extraform vychází ze své více jak
  třicetileté tradice a zkušeností, které dále rozvíjí za podpory nových výrobních
  technologií a inovací. Všechny kusy nábytku jsou mimořádně pohodlné, pevné a funkční,
  jsou vyráběny z vysoce kvalitních a certifikovaných materiálů z celého světa. Každý
  kus nábytku je výsledkem kombinace moderní tovární a ruční řemeslné výroby.</p><p>Vizuální
  pohodlí, povědomí o životním prostředí, udržitelné, odpovědné a etické podnikání
  představují podstatu vize značky Extraform.</p>'
brand_image:
- "/v1608736313/forestry/ARCO_vds2ad.jpg"

---
