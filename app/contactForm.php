<?php
date_default_timezone_set('Europe/Prague');


$formError = array();


$formNewsletterMessage = null;
if (isset($_SESSION['formNewsletterMessage'])) {
	$formNewsletterMessage = $_SESSION['formNewsletterMessage'];
	unset($_SESSION['formNewsletterMessage']);
}

function saveContact($email, $type) {
	$soubor = fopen("save-emails-DO-NOT-DELETE.txt", "a+");
	fwrite($soubor, $email.', '.date('Y-m-d H:i:s')."\n");
	fclose($soubor);
}


$email = $name = $lastname = $phone = $message = $company = $city = $country = $unit = $formUnit = $newsletter =  $newsletterAgreement = '';
$living = $investment = false;


if (isset($_POST['newsletter'])) {
	if (isset($_POST['newsletter'])) $newsletter = trim($_POST['newsletter']);

	if (!filter_var($newsletter, FILTER_VALIDATE_EMAIL)) {
		$formError[] = "newsletter";
	}
	if (!isset($_POST['fill']) || $_POST['fill'] != '3') {
		$formError[] = "fill";
	}

	if (!count($formError)) {

		saveContact($newsletter, 'newsletter');

		$formNewsletterMessage = '<div class="text-center">'.__('newsletter_ok').'</div>';

		if (!is_ajax()) {
			$_SESSION['formNewsletterMessage'] = $formNewsletterMessage;
			header( "Location:?signup=true" );
			exit;
		}
	}
}
