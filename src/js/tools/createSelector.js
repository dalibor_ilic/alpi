import memoizeOne from 'memoize-one';

export default (selectors = [], fn = (arg) => arg) => {
	const memoizeFn = memoizeOne(fn);
	let memoizedState = {};
	return (state, prevState) => {
		const props = selectors.map((selector) => selector(state, prevState));
		memoizedState.state = state;
		memoizedState.prevState = prevState;
		return memoizeFn(...props, memoizedState);
	};
};
