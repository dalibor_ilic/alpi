import { clone } from '@superkoders/sk-tools/src/array';

const rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi;
const rtagName = /<([\w:]+)/;
const rhtml = /<|&#?\w+;/;
// We have to close these tags to support XHTML (#13200)
const wrapMap = {
	// Support: IE9
	option: [1, "<select multiple='multiple'>", '</select>'],

	thead: [1, '<table>', '</table>'],
	col: [2, '<table><colgroup>', '</colgroup></table>'],
	tr: [2, '<table><tbody>', '</tbody></table>'],
	td: [3, '<table><tbody><tr>', '</tr></tbody></table>'],

	_default: [0, '', ''],
};

export const parseHTML = (html, context) => {
	context = context || document;

	var tmp,
		tag,
		wrap,
		j,
		fragment = context.createDocumentFragment();

	if (!rhtml.test(html)) {
		fragment.appendChild(context.createTextNode(html));

		// Convert html into DOM nodes
	} else {
		tmp = fragment.appendChild(context.createElement('div'));

		// Deserialize a standard representation
		tag = (rtagName.exec(html) || ['', ''])[1].toLowerCase();
		wrap = wrapMap[tag] || wrapMap._default;
		tmp.innerHTML = wrap[1] + html.replace(rxhtmlTag, '<$1></$2>') + wrap[2];

		// Descend through wrappers to the right content
		j = wrap[0];
		while (j--) {
			tmp = tmp.lastChild;
		}

		// Remove wrappers and append created nodes to fragment
		fragment.removeChild(fragment.firstChild);
		while (tmp.firstChild) {
			fragment.appendChild(tmp.firstChild);
		}
	}

	return clone(fragment.children || fragment.childNodes);
};
