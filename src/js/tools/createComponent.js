import { query, is } from '@superkoders/sk-tools/src/selector';

const queryContain = (selector, context = document.body) => {
	if (is(context, selector)) return [context];
	return query(selector, context);
};

export const createComponent = (selector, Component, { global = false } = {}) => {
	const instances = new WeakMap();

	const init = (context) => {
		const elements = global ? query(selector) : queryContain(selector, context);

		elements.forEach((element, ...rest) => {
			instances.set(element, Component(element, ...rest));
		});

		return elements;
	};

	const destroy = (context) => {
		const elements = global ? query(selector) : queryContain(selector, context);

		elements.forEach((element) => {
			const data = instances.get(element);

			if (typeof data === 'function') data();
			else if (typeof data.destroy === 'function') data.destroy();
		});
	};

	return {
		init,
		destroy,
		get(element) {
			return instances.get(element);
		},
	};
};
