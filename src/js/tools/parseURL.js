const link = document.createElement('a');

export const parseUrl = (url) => {
	link.href = url;
	return {
		hash: link.hash,
		href: link.href,
		host: link.host,
		hostname: link.hostname,
		origin: link.origin,
		pathname: link.pathname,
		port: link.port,
		protocol: link.protocol,
		search: link.search,
	};
};
