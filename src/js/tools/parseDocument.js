import { parseHTML } from './parseHTML';

export const parseDocument = (html) => {
	let doc = document.implementation.createHTMLDocument('');
	// let fragment = doc.createDocumentFragment();
	// doc.documentElement.innerHTML = html;
	return parseHTML(html, document);
};
