export const once = (fn) => {
	let ret;
	let isCalled = false;
	return (...args) => {
		if (isCalled === true) return ret;
		isCalled = true;
		ret = fn(...args);
		return ret;
	};
};
