const raf = window.requestAnimationFrame;

let rafStack = [];
let readStack = [];
let writeStack = [];
let scheduled = false;

export const read = (fn) => {
	readStack.push(fn);
	schedule();
	return fn;
};

export const write = (fn) => {
	writeStack.push(fn);
	schedule();
	return fn;
};

export const clear = (fn) => {
	return remove(readStack, fn) || remove(writeStack, fn);
};

const schedule = () => {
	if (scheduled !== false) return;
	scheduled = true;
	raf(flush);
};

const flush = (time) => {
	scheduled = false;

	const cloneRafStack = [...rafStack];
	rafStack = [];
	runStack(cloneRafStack, time);

	const cloneReadStack = [...readStack];
	readStack = [];
	runStack(cloneReadStack);

	const cloneWriteStack = [...writeStack];
	writeStack = [];
	runStack(cloneWriteStack);
};

const runStack = (stack, time) => {
	// console.log(first, rest);

	var task;
	while ((task = stack.shift())) task(time);
};

const remove = (array, item) => {
	var index = array.indexOf(item);
	return !!~index && !!array.splice(index, 1);
};

window.requestAnimationFrame = (callback) => {
	rafStack.push(callback);
	schedule();
	return callback;
};

window.cancelAnimationFrame = (callback) => {
	return remove(rafStack, callback);
};
