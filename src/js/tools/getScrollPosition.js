import { query } from '@superkoders/sk-tools/src/selector';

export const getScrollTargetPosition = (hash, context, offset = {}) => {
	if (hash.length < 2) return;

	const scrollTarget = query(hash, context)[0];
	if (scrollTarget == null) return;

	const bbox = scrollTarget.getBoundingClientRect();
	let y = window.scrollY || window.pageYOffset;

	return [bbox.left + (offset.x || 0), bbox.top + y + (offset.y || 0)];
};
