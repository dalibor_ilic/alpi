import { query } from '@superkoders/sk-tools/src/selector';

const attrs = {
	type: true,
	src: true,
	nonce: true,
	noModule: true,
};

const rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

// It's mainly copy from jQuery
export const executeInlineScripts = (node) => {
	const doc = node.ownerDocument;
	const head = doc.getElementsByTagName('head')[0];

	query('script', node).forEach((script) => {
		const newScript = doc.createElement('script');
		newScript.text = script.textContent.replace(rcleanScript, '');

		Object.keys(attrs).forEach((name) => {
			const val = script[name] || (script.getAttribute && script.getAttribute(name));
			if (val) {
				newScript.setAttribute(name, val);
			}
		});

		head.appendChild(newScript).parentNode.removeChild(newScript);
	});
};
