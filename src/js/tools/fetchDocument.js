import { parseDocument } from './parseDocument';

export const fetchDocument = (url, data = {}) => {
	// wierd fix when url is /en/ fetch switch self to GET request without headers
	const urlNoEndingSlash = url.replace(/\/$/g, '');

	return fetch(urlNoEndingSlash, {
		method: 'POST',
		mode: 'same-origin',
		credentials: 'same-origin',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(data),
	})
		.then((response) => response.text())
		.then((html) => parseDocument(html));
};
