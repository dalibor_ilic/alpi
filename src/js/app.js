import { on as listen } from '@superkoders/sk-tools/src/emmiter';
import './tools/svg4everybody';
// import { media } from './tools/MQ';

import slider from './components/slider';
import sliderBrands from './components/sliderBrands';
import sliderProduct from './components/sliderProduct';

// Components
import * as menuMain from './components/menuMain';
import * as ajaxPage from './components/ajaxPage';
import parallax from './components/parallax';
import parallaxImage from './components/parallaxImage';
import parallaxHeader from './components/parallaxHeader';
import parallaxText from './components/parallaxText';
import parallaxSection from './components/parallaxSection';
import parallaxSliderControl from './components/parallaxSliderControl';
import parallaxLoadMore from './components/parallaxLoadMore';
import * as tabs from './components/tabs';
import * as objectfit from './components/objectfit';

// content load components
// const componentsload = [slider];
const componentsload = [
	slider,
	sliderBrands,
	sliderProduct,
	parallax,
	parallaxImage,
	parallaxHeader,
	parallaxText,
	parallaxSection,
	parallaxSliderControl,
	parallaxLoadMore,
	objectfit,
];

// once delegated components
const components = [menuMain, ajaxPage, tabs];

window.App = {
	run() {
		const targetNode = document.body;
		components.forEach((component) => component.init(targetNode));

		listen('contentload', (event, { element }) => {
			componentsload.forEach((component) => component.init(element));
		});
		listen('contentunload', (event, { element }) => {
			componentsload.forEach((component) => component.destroy(element));
		});
	},
};
