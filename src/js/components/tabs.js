import { on, delegate } from '@superkoders/sk-tools/src/event';
import { closest } from '@superkoders/sk-tools/src/traversing';
import { removeClass, addClass, toggleClass } from '@superkoders/sk-tools/src/className';
import { query } from '@superkoders/sk-tools/src/selector';
import { getParallax } from './parallax';

export const init = () => {
	on(
		document,
		'click',
		delegate('.js-tabs__link', function(event) {
			event.preventDefault();

			const target = event.target;
			const wrap = closest(target, '.js-tabs');
			const links = query('.js-tabs__link.is-active', wrap);
			const tabs = query('.js-tabs__fragment.is-active', wrap);

			links.forEach((link) => {
				removeClass(link, 'is-active');
			});
			tabs.forEach((tab) => {
				removeClass(tab, 'is-active');
			});

			addClass(target, 'is-active');
			addClass(query(target.getAttribute('href'))[0], 'is-active');

			getParallax()?.resize();
		}),
	);

	on(
		document,
		'click',
		delegate('.js-tabs__sublink', function(event) {
			event.preventDefault();

			const target = event.target;
			const parent = closest(target, '.js-tabs__fragment');

			toggleClass(parent, 'is-active');

			getParallax()?.resize();
		}),
	);
};
