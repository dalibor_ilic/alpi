import createSelector from '../tools/createSelector';
import { getProgress, clamp } from '@superkoders/sk-tools/src/math';
import { getHeight, getScrollTop, getScrollHeight } from './parallax/selectors';
import { getParallax } from './parallax';
import { parent } from '@superkoders/sk-tools/src/traversing';
import { createComponent } from '../tools/createComponent';
// import { hasClass } from '@superkoders/sk-tools/src/className';

// const createMarker = ({ color = 'black', y = 0, text = 'marker', id = 'parallax-marker' } = {}) => {
// 	const div = document.getElementById(id) || document.createElement('div');
// 	div.id = id;
// 	document.body.appendChild(div);
// 	div.style.position = 'absolute';
// 	div.style.top = `${y}px`;
// 	div.style.color = color;
// 	div.style.borderTop = '1px solid currentColor';
// 	div.innerHTML = text;
// };

export default createComponent('.b-media__item--parallax', (node) => {
	const controller = getParallax();

	const parentNode = parent(node);
	let timer = null;

	const getDimms = createSelector([getHeight, getScrollHeight], (pageHeight, scrollHeight, { state: { scrollTop } }) => {
		const { top, height } = parentNode.getBoundingClientRect();
		const { height: nodeHeight } = node.getBoundingClientRect();
		const from = Math.max(0, scrollTop + top - pageHeight);
		const to = scrollTop + top + height;
		const scroll = Math.round(nodeHeight * 0.1);

		// createMarker({ color: 'green', y: from, text: `image ${index}`, id: `start-${index}` });
		// createMarker({ color: 'red', y: to, text: `image ${index}`, id: `end-${index}` });

		return { from, to, scroll };
	});

	const getState = createSelector([getDimms, getScrollTop], ({ from, to }, scrollTop) => {
		const progress = getProgress(from, to, scrollTop);
		return clamp(-0.1, 1.1, progress);
	});

	const updateViewport = createSelector([getState], (state) => {
		const progress = clamp(0, 1, state);
		// const isComming = (state < 1.1 && state > -0.1);
		// node.style.transform = `${isComming ? 'translateZ(0) ' : ''}translateY(${scroll * progress}px)`;
		node.style.transform = `translateZ(0) translateY(${-10 + 10 * progress}%)`;
	});

	// init
	controller.register(updateViewport);

	return () => {
		clearTimeout(timer);
		controller.unregister(updateViewport);
		timer = null;
	};
});
