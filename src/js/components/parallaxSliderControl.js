import createSelector from '../tools/createSelector';
import { getProgress } from '@superkoders/sk-tools/src/math';
import { getHeight, getScrollTop, getScrollHeight } from './parallax/selectors';
import { getParallax } from './parallax';
import slider from './slider';
import { createComponent } from '../tools/createComponent';

export default createComponent('*[data-component="Slider"]', (node) => {
	const controller = getParallax();

	const sliderInstance = slider.get(node);

	const getDimms = createSelector([getHeight, getScrollHeight], (pageHeight, scrollHeight, { state: { scrollTop } }) => {
		const { top, height } = node.getBoundingClientRect();
		const from = Math.max(0, scrollTop + top - pageHeight);
		const to = scrollTop + top + height;

		return { from, to };
	});

	const getState = createSelector([getDimms, getScrollTop], ({ from, to }, scrollTop) => {
		const progress = getProgress(from, to, scrollTop);
		if (progress < 0 || progress > 1) return 'pause';
		return 'resume';
	});

	const updateViewport = createSelector([getState], (state) => {
		sliderInstance[state]();
	});

	// init
	controller.register(updateViewport);

	return () => {
		controller.unregister(updateViewport);
	};
});
