import createSelector from '../tools/createSelector';
import { getProgress, clamp } from '@superkoders/sk-tools/src/math';
import { getHeight, getScrollTop, getScrollHeight } from './parallax/selectors';
import { getParallax } from './parallax';
import { createComponent } from '../tools/createComponent';
import { query } from '@superkoders/sk-tools/src/selector';
import { boolClass } from '@superkoders/sk-tools/src/className';

export default createComponent('*[data-parallax-section]', (node) => {
	const controller = getParallax();
	const textNodes = query('*[data-parallax-section-text', node);
	const header = query('.header')[0];

	const getDimms = createSelector([getHeight, getScrollHeight], (pageHeight, scrollHeight, { state: { scrollTop } }) => {
		const { top, height } = node.getBoundingClientRect();
		const from = Math.max(0, scrollTop + top);
		const to = scrollTop + top + height;

		return { from, to, scroll };
	});

	const getState = createSelector([getDimms, getScrollTop], ({ from, to }, scrollTop) => {
		const progress = getProgress(from, to, scrollTop);
		return clamp(0, 1, progress);
	});

	const updateViewport = createSelector([getState], (state) => {
		textNodes.forEach((node) => (node.style.opacity = 1 - state));
		boolClass(header, 'header--secondary', state !== 1);
	});

	// init
	controller.register(updateViewport);

	return () => {
		controller.unregister(updateViewport);
	};
});
