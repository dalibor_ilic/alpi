import { createComponent } from '../tools/createComponent';
import { query } from '@superkoders/sk-tools/src/selector';
import sliderInstances from './slider';
import { on, delegate } from '@superkoders/sk-tools/src/event';
import { prevAll } from '@superkoders/sk-tools/src/traversing';
import { prepend } from '@superkoders/sk-tools/src/manipulation';
import { off } from '@superkoders/sk-tools/src/emmiter';
import { removeClass, addClass } from '@superkoders/sk-tools/src/className';

export default createComponent('.b-slider-detail', (introNode) => {
	const sliderNode = query('[data-component="Slider"]', introNode)[0];
	let sliderInstance = sliderInstances.get(sliderNode);

	let currentIndex = 0;
	let clickCounter = 0;
	const slides = query('[data-ref="Slider.slide"]', introNode);
	slides.forEach((node, index) => (node.dataset.index = index));

	const onOver = delegate('.b-slider-detail__dot:not(.is-active)', (event, node) => {
		const index = prevAll(node.parentNode).length + 1;

		event.preventDefault();

		if (index === currentIndex) return;

		console.log(slides, slides[index], index);

		// Hack - keen slider animate every affected slide if slider switch more than one step
		if (clickCounter % 2 === 0) {
			prepend(slides[index].parentNode, slides[index]);
			prepend(slides[currentIndex].parentNode, slides[currentIndex]);
			sliderInstance.switchSlides();
			sliderInstance.slider.next();
		} else {
			prepend(slides[currentIndex].parentNode, slides[currentIndex]);
			prepend(slides[index].parentNode, slides[index]);
			sliderInstance.switchSlides();
			sliderInstance.slider.prev();
		}

		query('.b-slider-detail__dot', introNode).forEach((linkNode, linkIndex) => {
			if (linkIndex === index - 1) {
				addClass(linkNode, 'is-active');
				return;
			}
			removeClass(linkNode, 'is-active');
		});

		clickCounter++;
		currentIndex = index;
	});

	on(introNode, 'click', onOver);

	return () => {
		off(introNode, 'click', onOver);
		sliderInstance = null;
	};
});
