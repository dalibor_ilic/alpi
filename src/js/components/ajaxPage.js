import { on, delegate } from '@superkoders/sk-tools/src/event';
import { hasClass, addClass, removeClass } from '@superkoders/sk-tools/src/className';
import { query } from '@superkoders/sk-tools/src/selector';
import { isMac } from '@superkoders/sk-tools/src/browser';
import { after, remove } from '@superkoders/sk-tools/src/manipulation';
import { trigger as emmit } from '@superkoders/sk-tools/src/emmiter';
import { afterPaint } from '@superkoders/sk-tools/src/timer';
import { executeInlineScripts } from '../tools/executeInlineScripts';
import { delay } from '../tools/delay';
import { write } from '../tools/rafSchedule';
import { last } from '@superkoders/sk-tools/src/array';
import { create } from '@superkoders/sk-tools/src/emmiter';
import { loadImage } from '../tools/loadImage';
import { parseUrl } from '../tools/parseURL';
import { getScrollTargetPosition } from '../tools/getScrollPosition';
import { fetchDocument } from '../tools/fetchDocument';
import { getParallax } from './parallax';

const is = (node, selector) => {
	if (!node.matches) return false;
	return node.matches(selector);
};

const getImagePreloadURL = (page) => {
	const set = new Set(query('img', page).map((node) => node.src));
	return [...set];
};

const createPromisePreloader = (promises) => {
	let state = {
		resolved: 0,
		rejected: 0,
		finalized: 0,
		total: promises.length,
	};

	const emmiter = create({});
	const mainPromise = new Promise((resolve) => {
		if (state.finalized === state.total) {
			resolve();
			return;
		}

		promises.forEach((promise, index) => {
			promise
				.then(() => {
					state.resolved++;
				})
				.catch(() => {
					state.rejected++;
				})
				.finally(() => {
					state.finalized++;
					emmiter.trigger('notify', { ...state, currentIndex: index });
					if (state.finalized === state.total) {
						// TODO reject?
						resolve();
					}
				});
		});
	});

	return {
		promise: mainPromise,
		notify(callback) {
			emmiter.on('notify', (event, data) => callback(data));
			return this;
		},
	};
};

const transitions = {
	default: {
		out({ oldPage }) {
			return new Promise((resolve) => {
				oldPage.addEventListener('transitionend', (event) => {
					if (event.target !== event.currentTarget) return;
					resolve();
				});
				// getParallax().disable();
				emmit('pauseanimations');
				removeClass(document.body, 'is-scrolled');
				addClass(oldPage, 'is-leaving');
				document.body.style.height = `${oldPage.scrollHeight}px`;
				let y = window.scrollY || window.pageYOffset;
				oldPage.style.position = 'fixed';
				oldPage.style.top = `${-y}px`;
			});
		},
		updateDom({ page, oldPage }) {
			after(oldPage, page);
			executeInlineScripts(page);
		},
		prepare({ page, scrollPosition }) {
			page.style.position = 'fixed';
			page.style.top = `-${scrollPosition[1]}px`;
			document.body.style.height = `${page.scrollHeight}px`;
			window.scrollTo(...scrollPosition);
			return delay(50);
		},
		in({ oldPage, page }) {
			remove(oldPage);
			addClass(page, 'is-page-visible');
			document.body.style.height = '';
			page.style.position = '';
			page.style.top = '';
		},
	},
	brandDetail: {
		out({ oldPage }) {
			return new Promise((resolve) => {
				// getParallax().disable();
				emmit('pauseanimations');
				removeClass(document.body, 'is-scrolled');

				document.body.style.height = `${oldPage.scrollHeight}px`;
				let y = window.scrollY || window.pageYOffset;
				oldPage.style.position = 'fixed';
				oldPage.style.top = `${-y}px`;

				this.y = y;
				resolve();
			});
		},
		updateDom(props) {
			transitions.default.updateDom(props);
		},
		prepare({ page }) {
			page.style.position = 'fixed';
			page.style.top = `${-this.y}px`;
			// page.style.top = `-${scrollPosition[1]}px`;
			document.body.style.height = `${page.scrollHeight}px`;
			query('.b-intro-brands *[data-parallax-text]', page).forEach((node) => {
				node.removeAttribute('data-parallax-text');
			});
			window.scrollTo(0, this.y);
			return delay(50);
		},
		in({ oldPage, page, scrollPosition }) {
			addClass(page, 'is-page-visible');
			document.body.style.height = '';
			oldPage.style.position = '';
			oldPage.style.top = '';
			page.style.position = '';
			page.style.top = '';
			window.scrollTo({
				top: scrollPosition[1],
				behavior: 'smooth',
			});
			delay(600).then(() => {
				remove(oldPage);
			});
		},
	},
	loadMore: {
		out({ oldPage }) {
			this.oldButton = query('*[data-loadmore-button]', oldPage)[0];
			addClass(query('.btn', this.oldButton)[0], 'is-loading');

			return delay(100);
		},
		updateDom({ oldPage, page }) {
			console.log();
			const oldList = last(query('*[data-loadmore-list]', oldPage));
			this.list = last(query('*[data-loadmore-list]', page));

			after(oldList, this.list);

			return {
				loadElement: this.list,
			};
		},
		prepare({ page }) {
			const height = this.list.offsetHeight;
			this.list.style.height = '0px';
			this.list.style.transition = 'height .5s';

			const button = query('*[data-loadmore-button]', page)[0];
			if (button == null) {
				this.oldButton.style.height = `${this.oldButton.offsetHeight}px`;
				this.oldButton.style.transition = 'height .3s .2s, opacity .2s';
			} else {
				query('.btn', this.oldButton)[0].setAttribute('href', query('.btn', button)[0].getAttribute('href'));
			}

			afterPaint(() => {
				this.list.style.height = `${height}px`;
				if (button == null) {
					addClass(query('.btn', this.oldButton)[0], 'is-finished');
					this.oldButton.style.height = '0px';
					this.oldButton.style.opacity = '0';
				}
			});
			return delay(550);
		},
		in() {
			this.list.style.height = '';
			this.list.style.transition = '';

			removeClass(query('.btn', this.oldButton)[0], 'is-loading');

			getParallax().resize();

			this.list = null;
			this.oldButton = null;
		},
	},
	product: {
		out(props) {
			return transitions.default.out(props);
		},
		updateDom(props) {
			transitions.default.updateDom(props);
			query('*[data-history-back]', props.page).forEach((node) => {
				addClass(node, 'is-active');
				addClass(node, 'no-ajax');
			});
		},
		prepare(props) {
			return transitions.default.prepare(props);
		},
		in(props) {
			transitions.default.in(props);
		},
	},
};

const setUrl = (url, transition) => {
	const referer = location.href;
	const historyStep = history.state.historyStep + 1;

	history.pushState({ url, referer, ajaxify: true, historyStep }, null, url);
	changeUrl({ ...history.state, transition, action: 'new' });
};

const changeUrl = ({ url, transition: transitionType = 'default', scrollY, scrollX, historyStep }) => {
	// emmit('ajaxstart');
	// nprogress.start();
	sessionStorage.setItem('lastHistoryStep', historyStep);

	const oldPage = last(query('.page', document));
	const oldHeader = query('.header', document)[0];

	let shouldShowPreloader = true;
	const transitionOutPromise = transitions[transitionType].out({ oldPage });
	Promise.all([transitionOutPromise, delay(350)]).then(() => {
		if (shouldShowPreloader === false) return;
		addClass(document.documentElement, 'is-preloading');
	});

	if (transitionType === 'loadMore') {
		shouldShowPreloader = false;
	}

	Promise.all([fetchDocument(url)])
		.then(([fragment]) => {
			const page = fragment.find((node) => is(node, '.page'));
			const header = fragment.find((node) => is(node, '.header'));
			const title = fragment.find((node) => is(node, 'title'));

			const imgPromises = getImagePreloadURL(page).map(loadImage);
			const promisePreloader = createPromisePreloader(imgPromises);

			promisePreloader.promise
				.then(() => {
					shouldShowPreloader = false;

					write(() => {
						document.title = title.textContent;

						removeClass(document.documentElement, 'is-preloading');
						write(() => {
							const href = query('.header__logo > a', header)[0].getAttribute('href');
							query('.header__logo > a', oldHeader)[0][href ? 'setAttribute' : 'removeAttribute']('href', href);
							query('.navigation', oldHeader)[0].innerHTML = query('.navigation', header)[0].innerHTML;
							oldHeader.className = header.className;
						});

						const { unloadElement, loadElement } = transitions[transitionType].updateDom({ oldPage, page }) || {
							unloadElement: oldPage,
							loadElement: page,
						};

						const urlProps = parseUrl(url);
						let scrollPosition =
							scrollY != null || scrollX != null
								? [scrollX || 0, scrollY || 0]
								: getScrollTargetPosition(urlProps.hash, page, { y: 0 }) || [0, 0];

						const transitionPreparePromise = transitions[transitionType].prepare({ page, oldPage, scrollPosition });

						if (unloadElement) emmit('contentunload', { element: unloadElement });
						if (loadElement) emmit('contentload', { element: loadElement });

						Promise.all([transitionOutPromise, transitionPreparePromise]).then(() => {
							transitions[transitionType].in({ page, oldPage, scrollPosition });
						});
					});
				})
				.catch((e) => {
					console.log(e);
				});
		})
		.catch((e) => {
			console.log(e);
		});
};

export const init = async () => {
	const page = last(query('.page', document));
	const imgPromises = getImagePreloadURL(page).map(loadImage);
	const promisePreloader = createPromisePreloader(imgPromises);
	// const urlProps = parseUrl(location.href);

	Promise.all([delay(500), promisePreloader.promise])
		.then(() => {
			removeClass(document.documentElement, 'is-preloading');
			addClass(document.documentElement, 'is-loaded');
			emmit('contentload', { element: page });

			addClass(page, 'is-page-visible');
		})
		.catch((e) => {
			console.log(e);
		});

	// init state
	const url = location.href;
	const historyStep = history.state?.historyStep ?? history.length;
	sessionStorage.setItem('lastHistoryStep', historyStep);
	history.replaceState({ url, referer: null, ajaxify: true, historyStep, ...(history.state || {}) }, null, url);

	// events
	on(window, 'popstate', (event) => {
		if (event.state == null || !event.state.ajaxify) return;
		let action = null;
		if (event.state.historyStep != null && sessionStorage.getItem('lastHistoryStep') != null) {
			const result = event.state.historyStep - sessionStorage.getItem('lastHistoryStep');
			if (result > 0) action = 'forward';
			else if (result < 0) action = 'back';
			else action = 'reload';
		}
		// why call again on relaod?
		if (action === 'reload') return;

		// pop state
		changeUrl({ ...event.state, action });
	});

	on(
		document,
		'click',
		delegate('a[href^="./"], a[href^="/"], a[href^="?"]', (event, node) => {
			if (node.href.indexOf('/admin/') !== -1 || hasClass(node, 'no-ajax') || node.getAttribute('target') == '_blank') {
				return;
			}

			// This link is inside the menu-local-tasks and should be ignored.
			if (node.closest('.admin-tabs-primary')) {
				return;
			}

			// Explicit ajax links are handled other way
			if (hasClass(node, 'ajax')) {
				return;
			}

			// only left button without modifiers
			if (event.button !== 0) return;
			if ((isMac && event.metaKey) || event.ctrlKey) return;

			// write scroll state
			const url = location.href;
			const scrollY = window.scrollY || window.pageYOffset;
			const scrollX = window.scrollX || window.pageXOffset;
			history.replaceState({ ...history.state, scrollY, scrollX }, null, url);

			event.preventDefault();
			setUrl(node.href, node.dataset.pageTransition);
		}),
	);
};
