import createSelector from '../tools/createSelector';
// import { getProgress, clamp } from '@superkoders/sk-tools/src/math';
import { getHeight, getScrollTop, getScrollHeight } from './parallax/selectors';
import { getParallax } from './parallax';
import { addClass } from '@superkoders/sk-tools/src/className';
import { media } from '../tools/MQ';
import { createComponent } from '../tools/createComponent';

export default createComponent('*[data-parallax-text]', (node) => {
	const controller = getParallax();
	const fromEdge = media('mdDown') ? 50 : 100;

	const getDimms = createSelector([getHeight, getScrollHeight], (pageHeight, scrollHeight, { state: { scrollTop } }) => {
		const { top } = node.getBoundingClientRect();

		const from = scrollTop + top - pageHeight - 40 + fromEdge;

		return { from };
	});

	const getState = createSelector([getDimms, getScrollTop], ({ from }, scrollTop) => {
		return scrollTop > from;
	});

	const updateViewport = createSelector([getState], (state) => {
		if (state) {
			addClass(node, 'is-visible');
			controller.unregister(updateViewport);
		}
	});

	// init
	controller.register(updateViewport);

	return () => {
		controller.unregister(updateViewport);
	};
});
