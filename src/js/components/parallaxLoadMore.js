import createSelector from '../tools/createSelector';
import { getProgress } from '@superkoders/sk-tools/src/math';
import { getHeight, getScrollTop, getScrollHeight } from './parallax/selectors';
import { getParallax } from './parallax';
import { createComponent } from '../tools/createComponent';
import { hasClass } from '@superkoders/sk-tools/src/className';

export default createComponent('*[data-page-transition="loadMore"]', (node) => {
	console.log(node);
	const controller = getParallax();

	const getDimms = createSelector([getHeight, getScrollHeight], (pageHeight, scrollHeight, { state: { scrollTop } }) => {
		const { top, height } = node.getBoundingClientRect();
		const from = Math.max(0, scrollTop + top - pageHeight * 1.5);
		const to = scrollTop + top + height;

		return { from, to };
	});

	const getState = createSelector([getDimms, getScrollTop], ({ from, to }, scrollTop) => {
		const progress = getProgress(from, to, scrollTop);
		if (progress < 0 || progress > 1) return false;
		return true;
	});

	const updateViewport = createSelector([getState], (state) => {
		if (hasClass(node, 'is-loading')) return;

		if (hasClass(node, 'is-finished')) {
			controller.unregister(updateViewport);
			return;
		}

		if (state) {
			node.click();
		}
	});

	// init
	controller.register(updateViewport);

	return () => {
		controller.unregister(updateViewport);
	};
});
