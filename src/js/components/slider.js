import KeenSlider from 'keen-slider';
import { createComponent } from '../tools/createComponent';
import { boolClass } from '@superkoders/sk-tools/src/className';
import { query } from '@superkoders/sk-tools/src/selector';
import { on as listen, off as ignore } from '@superkoders/sk-tools/src/emmiter';
import { delegate, on, off } from '@superkoders/sk-tools/src/event';

export default createComponent('[data-component="Slider"]', (element) => {
	const refs = {
		timer: query('[data-ref="Slider.timer"]', element)[0],
		counter: query('[data-ref="Slider.counter"]', element)[0],
	};
	const closest = element.closest('[data-local-pausing]');
	const holder = closest ?? element;
	let isPlaying = -1;
	let isOver = false;
	let current = -1;
	let size = -1;

	const changeTimer = () => {
		if (refs.timer == null) return;
		boolClass(refs.timer, 'is-animating', isPlaying > -1);
		boolClass(refs.timer, 'is-playing', isPlaying === 1);
	};

	const changeCounter = () => {
		if (refs.counter == null) return;
		refs.counter.innerHTML = `${current} / ${size}`;
	};

	const resume = () => {
		isOver = false;
		isPlaying = 1;
		changeTimer();
	};

	let pause = () => {
		isOver = true;
		isPlaying = 0;
		changeTimer();
	};

	let onMouseEnter = () => pause();
	let onMouseLeave = () => resume();

	const onAnimationEnd = () => {
		isPlaying = -1;
		changeTimer();
		slider.next();
	};

	let slides = query('[data-ref="Slider.slide"]', element);
	const slider = new KeenSlider(element, {
		loop: true,
		slides: slides.length,
		duration: 2500,
		mounted: () => {
			isPlaying = 1;
			changeTimer();
		},
		beforeChange: () => {
			isPlaying = -1;
			changeTimer();
		},
		afterChange: () => {
			isPlaying = isOver ? 0 : 1;
			changeTimer();
		},
		slideChanged: (slider) => {
			const details = slider.details();
			current = details.relativeSlide + 1;
			size = details.size;
			changeCounter();
		},
		move: (s) => {
			const opacities = s.details().positions.map((slide) => slide.portion);
			slides.forEach((element, idx) => {
				element.style.opacity = opacities[idx];
			});
		},
	});

	if (holder.dataset.localPausing) {
		onMouseEnter = delegate(holder.dataset.localPausing, onMouseEnter);
		onMouseLeave = delegate(holder.dataset.localPausing, onMouseLeave);
	}
	on(holder, 'mouseover', onMouseEnter);
	on(holder, 'mouseout', onMouseLeave);

	refs.timer?.addEventListener('animationend', onAnimationEnd);
	listen('pauseanimations', pause);
	listen('resumeanimations', resume);

	return {
		slider,
		switchSlides() {
			slides = query('[data-ref="Slider.slide"]', element);
		},
		pause,
		resume,
		destroy() {
			slider.destroy();
			off(holder, 'mouseover', onMouseEnter);
			off(holder, 'mouseout', onMouseLeave);
			refs.timer?.removeEventListener('animationend', onAnimationEnd);
			ignore('pauseanimations', pause);
			ignore('resumeanimations', resume);
		},
	};
});
