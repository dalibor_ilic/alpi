export const getScrollTop = (state) => state.scrollTop;
export const getScrollAnim = (state) => state.scrollAnim;
export const getScrollHeight = (state) => state.scrollHeight;
export const getHeight = (state) => state.height;
export const getFilter = (state) => state.filter;
