export const setScrollTop = (newScroll) => (state) => ({
	...state,
	...newScroll,
});

export const setScrollHeight = (newHeight) => (state) => ({
	...state,
	...newHeight,
});

export const setHeader = (isOpen) => (state) => ({
	...state,
	isHeaderOpen: isOpen,
});
