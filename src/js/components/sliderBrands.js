import { createComponent } from '../tools/createComponent';
import { query } from '@superkoders/sk-tools/src/selector';
import sliderInstances from './slider';
import { on, delegate } from '@superkoders/sk-tools/src/event';
import { prevAll } from '@superkoders/sk-tools/src/traversing';
import { prepend } from '@superkoders/sk-tools/src/manipulation';
import { off } from '@superkoders/sk-tools/src/emmiter';
import { removeClass, addClass } from '@superkoders/sk-tools/src/className';

export default createComponent('.b-intro-brands', (introNode) => {
	const sliderNode = query('[data-component="Slider"]', introNode)[0];
	let sliderInstance = sliderInstances.get(sliderNode);

	let currentIndex = 0;
	let hoverCounter = 0;
	const slides = query('[data-ref="Slider.slide"]', introNode);
	slides.forEach((node, index) => (node.dataset.index = index));

	const onOver = delegate('.b-tabs-brands__link:not(.is-active)', (event, node) => {
		const index = prevAll(node.parentNode).length + 1;
		if (index === currentIndex) return;

		// Hack - keen slider animate every affected slide if slider switch more than one step
		if (hoverCounter % 2 === 0) {
			prepend(slides[index].parentNode, slides[index]);
			prepend(slides[currentIndex].parentNode, slides[currentIndex]);
			sliderInstance.switchSlides();
			sliderInstance.slider.next();
		} else {
			prepend(slides[currentIndex].parentNode, slides[currentIndex]);
			prepend(slides[index].parentNode, slides[index]);
			sliderInstance.switchSlides();
			sliderInstance.slider.prev();
		}

		query('.b-tabs-brands__link', introNode).forEach((linkNode, linkIndex) => {
			if (linkIndex === index - 1) {
				addClass(linkNode, 'is-active');
				return;
			}
			removeClass(linkNode, 'is-active');
		});

		hoverCounter++;
		currentIndex = index;
	});

	on(introNode, 'mouseover', onOver);

	return () => {
		off(introNode, 'mouseover', onOver);
		sliderInstance = null;
	};
});
