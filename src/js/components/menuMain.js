import { query, is } from '@superkoders/sk-tools/src/selector';
import { toggleClass, hasClass, removeClass } from '@superkoders/sk-tools/src/className';
import { closest, next } from '@superkoders/sk-tools/src/traversing';
import { on, delegate } from '@superkoders/sk-tools/src/event';
import { trigger as emmit } from '@superkoders/sk-tools/src/emmiter';
import { getParallax } from './parallax';
import { last } from '@superkoders/sk-tools/src/array';

export const init = () => {
	const headerMenu = query('.navigation')[0];
	const selectors = {
		TRIGGER: '[data-mobile-submenu-toggle]',
		BACK: '[data-mobile-submenu-back]',
		SUBMENU: '[data-mobile-submenu]',
		SUBMENUATTR: 'data-mobile-submenu',
	};

	const open = () => {
		const page = last(query('.page', document));
		getParallax()?.disable();
		lastY = window.scrollY || window.pageYOffset;
		page.style.position = 'fixed';
		page.style.top = `${-lastY}px`;
		emmit('pauseanimations');
	};

	const close = () => {
		const page = last(query('.page', document));
		getParallax()?.enable();
		page.style.position = '';
		page.style.top = '';
		window.scrollTo(0, lastY);
		emmit('resumeanimations');
	};

	const getSubmenu = function(node) {
		if (is(next(node), selectors.SUBMENU)) {
			return next(node);
		}

		return null;
	};

	const handleClick = (event) => {
		event.preventDefault();
		let current = closest(event.target, selectors.TRIGGER);
		const submenu = getSubmenu(current);

		if (!submenu) return;

		const submenuDepth = submenu.getAttribute(selectors.SUBMENUATTR);
		const submenuHtml = submenu.innerHTML;
		const wrap = query(`.navigation__wrap--depth-${submenuDepth}`)[0];

		wrap.innerHTML = submenuHtml;
		headerMenu.setAttribute('data-mobile-submenu-depth', submenuDepth);
	};

	const handleBack = (event) => {
		event.preventDefault();

		const currentDepth = headerMenu.getAttribute('data-mobile-submenu-depth');
		headerMenu.setAttribute('data-mobile-submenu-depth', currentDepth - 1);
	};

	const handleClickLink = () => {
		removeClass(document.body, 'menu-open');
		// hack back to first level menu
		setTimeout(() => {
			headerMenu.setAttribute('data-mobile-submenu-depth', 1);
		}, 500);
	};

	// const scrollableElement = document.body;
	let lastY = 0;

	query('.header__link')[0].addEventListener('click', (event) => {
		event.preventDefault();

		toggleClass(document.body, 'menu-open');

		// eslint-disable-next-line babel/no-unused-expressions
		hasClass(document.body, 'menu-open') ? open() : close();
	});
	on(
		document,
		'click',
		delegate('.navigation__close', (event) => {
			event.preventDefault();

			query('.header__link')[0].click();
		}),
	);

	on(
		document,
		'click',
		delegate('*[data-history-back].is-active', (event) => {
			event.preventDefault();
			history.back();
		}),
	);

	// TODO e.target
	on(query('.header')[0], 'click', delegate('.m-main__link', handleClickLink));
	on(document, 'click', delegate(selectors.TRIGGER, handleClick));
	on(document, 'click', delegate(selectors.BACK, handleBack));
};
