import createStore from '@superkoders/sk-tools/src/createStore';
import createSelector from '@superkoders/sk-tools/src/createSelector';
import { isIOS, isAndroid } from '@superkoders/sk-tools/src/browser';
import { hasPassiveEvent } from '../tools/hasPassiveEvent';
import { clamp } from '@superkoders/sk-tools/src/math';

import { setScrollTop, setScrollHeight } from './parallax/actions';
import * as selectors from './parallax/selectors';
// import { write } from '../tools/rafSchedule';
import { createComponent } from '../tools/createComponent';

if (!document.scrollingElement) {
	document.scrollingElement = document.documentElement;
}

const createParallax = () => {
	const win = window;
	const body = document.body;
	let width = body.offsetWidth;

	let y = null;
	let contentHeight = null;
	let yMin = null;
	let yMax = null;
	let store = null;

	let isWheel = false;
	const updateScroll = createSelector([selectors.getScrollTop, selectors.getScrollAnim], (scrollTop, isAnim) => {
		isWheel = isAnim;
		// console.log(scrollTop);
		if (isAnim) window.scrollTo(0, scrollTop);
	});

	let components = [updateScroll];

	const ceilToInteger = (number) => {
		return number < 0 ? Math.floor(number) : Math.ceil(number);
	};

	let isRequest = null;
	const scrollLoop = () => {
		if (isRequest) return;

		isRequest = requestAnimationFrame(() => {
			const scrollTo = y;
			const scrollCurrent = store.getState().scrollTop;
			const scrollNew = Math.round(scrollCurrent + ceilToInteger((scrollTo - scrollCurrent) * 0.125));
			const isEnd = scrollTo === scrollNew;

			store.dispatch(setScrollTop({ scrollTop: scrollNew, scrollEnd: isEnd, scrollAnim: true }), true);
			isRequest = null;
			if (!isEnd) scrollLoop();
		});
	};

	const stopScrollLoop = () => {
		cancelAnimationFrame(isRequest);
		isRequest = null;
		y = win.scrollY || window.pageYOffset;
		store.dispatch(setScrollTop({ scrollTop: y, scrollEnd: true, scrollAnim: false }), true);
	};

	const onResize = () => {
		const newcCntentHeight = document.scrollingElement.scrollHeight;
		// console.log(newcCntentHeight);

		// fix resize event after hiding ui
		if ((!isIOS && !isAndroid) || width !== body.offsetWidth || newcCntentHeight !== contentHeight) {
			// console.log('parallax recalc', newcCntentHeight);
			contentHeight = newcCntentHeight;
			yMax = contentHeight - document.scrollingElement.offsetHeight;
			width = body.offsetWidth;

			store.dispatch(
				setScrollHeight({
					scrollHeight: yMax,
					height: document.scrollingElement.offsetHeight,
				}),
			);
		}
	};

	const onScroll = () => {
		if (!isWheel) stopScrollLoop();
		isWheel = false;
	};

	const onWheel = (event) => {
		event.preventDefault();

		const delta = event.deltaMode === event.DOM_DELTA_PIXEL ? event.deltaY : event.deltaY * 30;
		y = clamp(yMin, yMax, Math.round(y + delta));
		scrollLoop();
	};

	const enable = () => {
		win.addEventListener('scroll', onScroll);
		win.addEventListener('load', onResize);
		win.addEventListener('wheel', onWheel, hasPassiveEvent() ? { passive: false } : false);
		win.addEventListener('resize', onResize);
	};

	const disable = () => {
		win.removeEventListener('scroll', onScroll);
		win.removeEventListener('load', onResize);
		win.removeEventListener('wheel', onWheel);
		win.removeEventListener('resize', onResize);
	};

	const onUpdate = (state) => {
		components.map((component) => component(state));
	};

	return {
		init() {
			width = body.offsetWidth;
			y = win.scrollY || window.pageYOffset;
			contentHeight = document.scrollingElement.scrollHeight;
			yMin = 0;
			yMax = contentHeight - document.scrollingElement.offsetHeight;

			// console.log('contentheight', contentHeight, y);

			store = createStore({
				scrollTop: y,
				scrollHeight: yMax,
				scrollEnd: true,
				height: document.scrollingElement.offsetHeight,
				filter: 'all',
			});
			store.subscribe(onUpdate, true);

			enable();
		},
		destroy() {
			disable();
		},
		disable,
		enable,
		scrollTo(to) {
			y = clamp(yMin, yMax, Math.round(to));
			scrollLoop();
		},
		resize: onResize,
		register(parallaxUpdate) {
			parallaxUpdate(store.getState());
			components = [parallaxUpdate].concat(components);
		},
		unregister(parallaxUpdate) {
			components = components.filter((component) => component !== parallaxUpdate);
		},
	};
};

let currentParallax = null;

export const getParallax = () => {
	return currentParallax;
};

export default createComponent('.page', (node, index) => {
	if (index > 0) {
		console.error('To more parallax controllers');
		return;
	}
	if (currentParallax) {
		console.error('There is active parallax controler');
		return;
	}

	currentParallax = createParallax();
	currentParallax.init();

	return () => {
		currentParallax.destroy();
		currentParallax = null;
	};
});
