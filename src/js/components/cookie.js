import { addClass, removeClass } from '@superkoders/sk-tools/src/className';
import { addCookie, hasCookie } from '@superkoders/sk-tools/src/cookie';
import { on } from '@superkoders/sk-tools/src/emmiter';
import { delegate } from '@superkoders/sk-tools/src/event';

export const init = () => {
	const cookieName = 'cookieBox';
	const closeTrigger = '.js-cookie-close';
	const element = document.querySelector('.b-cookie');

	const showCookieBox = () => {
		addClass(element, 'is-visible');
	};

	const hideCookieBox = () => {
		removeClass(element, 'is-visible');
	};

	const handleCloseClick = (event) => {
		event.preventDefault();

		hideCookieBox();
		addCookie(cookieName, 'true', 30 * 12 * 5);
	};

	if (!hasCookie(cookieName) && element) {
		showCookieBox();
	}

	on(document, 'click', delegate(closeTrigger, handleCloseClick));
};
