import createSelector from '../tools/createSelector';
import { getProgress, clamp } from '@superkoders/sk-tools/src/math';
import { getHeight, getScrollTop, getScrollHeight } from './parallax/selectors';
import { getParallax } from './parallax';
import { boolClass } from '@superkoders/sk-tools/src/className';
import { createComponent } from '../tools/createComponent';

export default createComponent(
	'.header',
	() => {
		const controller = getParallax();

		const getDimms = createSelector([getHeight, getScrollHeight], () => {
			const from = 0;
			const to = 10;

			return { from, to };
		});

		const getState = createSelector([getDimms, getScrollTop], ({ from, to }, scrollTop) => {
			return clamp(0, 1, getProgress(from, to, scrollTop));
		});

		const updateViewport = createSelector([getState], (state) => {
			boolClass(document.body, 'is-scrolled', state === 1);
			// node.style.transform = `translateZ(0) translateY(${scroll * state}px)`;
		});

		// init
		controller.register(updateViewport);

		return () => {
			controller.unregister(updateViewport);
		};
	},
	{ global: true },
);
