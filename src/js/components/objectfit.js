import { closest } from '@superkoders/sk-tools/src/traversing';

export const init = () => {
	let images = document.querySelectorAll('.b-media__item');

	if (document.documentElement.classList.contains('no-objectfit')) {
		[...images].forEach((image) => {
			let url = image.getAttribute('src');
			if (url) {
				closest(image, '.b-media').style.backgroundImage = 'url(' + url + ')';
			}
		});
	}
};

export const destroy = () => {
	// nothing special is needed here
};
