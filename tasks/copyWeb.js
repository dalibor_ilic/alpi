const { src, dest } = require('gulp');

module.exports = function copyRoot() {
	return src(['./dist/**/*', './app/**/*', '.htaccess', 'config.php', 'index.php', 'download.php'], {
		base: '.',
		dot: true,
	}).pipe(dest('./build'));
};
