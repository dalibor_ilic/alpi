const { src, dest } = require('gulp');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const through2 = require('through2');

const config = require('./helpers/getConfig.js');

const YAML = require('yamljs');
const slugify = require('slugify');
const fm = require('front-matter');
const fs = require('fs');
const path = require('path');

module.exports = function buildCMS(done) {
	const onError = (error) => {
		notify.onError({
			title: 'Twig error!',
			message: '<%= error.message %>',
			sound: 'Beep',
		})(error);

		done();
	};

	const buildConfigFile = () => {
		let lastFile = null;
		const data = {};
		const stream = async function stream(file, encoding, cb) {
			lastFile = file;
			let content = file.contents.toString('utf-8');
			let config = YAML.parse(content);
			const template = config.label;

			if (!Array.isArray(config.pages)) {
				cb();
				return;
			}

			const pagesData = await Promise.all(
				config.pages.map(
					(page) =>
						new Promise((resolve) => {
							const filePath = path.resolve(process.cwd(), page);
							const { name, ext, dir } = path.parse(filePath);
							const type = dir.replace(process.cwd(), '').split(path.sep)[2];

							fs.readFile(filePath, (err, data) => {
								let result = {};
								if (ext === '.md') {
									result = fm(data.toString('utf-8'));
								} else if (ext === '.yml') {
									result = {
										attributes: YAML.parse(data.toString('utf-8')),
									};
								}

								resolve({
									...result,
									slug: `/${slugify(name)
										.replace('homepage', '')
										.toLowerCase()}`,
									path: page,
									type,
								});
							});
						}),
				),
			);
			pagesData.forEach((page) => {
				let part = data;

				if (page.type) {
					if (data[page.type] == null) {
						data[page.type] = {};
					}
					part = data[page.type];
				}

				part[page.slug] = {
					template,
					page: page.attributes || {},
					path: page.path,
				};
			});

			cb();
		};

		const streamEnd = function streamEnd(cb) {
			const joinedFile = lastFile.clone({ contents: false });
			joinedFile.contents = Buffer.from(JSON.stringify(data, null, 2));
			joinedFile.path = path.join(lastFile.base, 'routes.json');
			this.push(joinedFile);
			cb();
		};
		return through2.obj(stream, streamEnd);
	};

	return (
		src(['*.yml'], {
			cwd: '.forestry/front_matter/templates',
		})
			.pipe(
				plumber({
					errorHandler: onError,
				}),
			)
			// cache bust
			.pipe(buildConfigFile())
			.pipe(dest(config.dest.templates))
	);
};
