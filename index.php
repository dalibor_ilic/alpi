<?php
session_start();

if (strpos($_SERVER["SERVER_NAME"], '.l') !== false) {

	ini_set('display_errors', 1);
	ini_set("log_errors", 1);
	ini_set('error_reporting', E_ALL);
	//ini_set("error_log", "/log/php-error.log");
};

$analytics = false;
if (strpos($_SERVER['SERVER_NAME'],"alpicollection.com")!==FALSE) {
	$analytics = true;
}

// $loginError = false;
// if (isset($_POST['required'])) {

// 	if ($_POST['required'] == 'hagiborec01') {
// 		$_SESSION['logged'] = 1;
// 	}
// 	else {
// 		$loginError = true;
// 		$_SESSION['logged'] = false;
// 	}
// }

// if (isset($_GET['logout'])) {
// 	$_SESSION['logged'] = false;
// }

//define('ROOT', __DIR__.'/tpl');
require_once 'config.php';

// Nacteni lokalni cache velikosti obrazku
$image_sizes_cache = loadImagesCache();

// require_once 'loadUnits.php';

//print_r($units);

$url = parse_url($_SERVER['REQUEST_URI']);
$url = '/'.trim($url['path'],'/');


if (isset($pagesCZ[$url]) || isset($pagesEN[$url])) {

	if (isset($pagesEN[$url])){
		$pages = $pagesEN;
		$lg = 'en';
	}
	else {
		$pages = $pagesCZ;
		$lg = 'cs';
	}
	extract($pages[$url]);

	require_once './app/contactForm.php';

	 if (isset($_GET['pdf'])) {

	 	$pdf = explode('|', $_GET['pdf']);

	 	$outputName = array();
	 	$files = array();
	 	foreach ($pdf as $p) {

	 		$filename = 'pdf/'.$p.'.pdf';
	 		if (is_file($filename)) {

	 			$outputName[] = $p;
	 			$files[] = 'pdf/' . $p . '.pdf';
	 		}
	 	}

	 	$outputName = str_replace('.','', implode('-', $outputName));

	 	$fileMergedName = 'pdf-merged/'.$outputName.'.pdf';
	 	if (!is_file($fileMergedName)) {
	 		$cmd = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=".$fileMergedName." ".implode(" ", $files);
	 		shell_exec($cmd);
	 	}


	 	header('Content-type: application/pdf');
	 	header('Content-Disposition: inline; filename="'.$fileMergedName.'"');
	 	header('Content-Transfer-Encoding: binary');
	 	header('Accept-Ranges: bytes');
	 	echo file_get_contents($fileMergedName);
	 	die();
	 }



	// if ($template == '13-detail' && !isset($_POST) && (!isset($_GET['no']) || !isset($units[$_GET['no']]))) {
	// 	header('Location: '.url('05-cenik'));
	// 	exit;
	// }

	if ($template == '13-media' && isset($_GET['pr'])){
		$template = '14-tiskova-zprava';

		$pageId = substr($_GET['pr'], 0, strpos($_GET['pr'], '-'));
		$pageData = getPressRelease($pageId);
		$pageTitle = $pageData->name;

	}

	if (false) {
		require './dist/tpl/login.php';
	}
	else {
		require './dist/tpl/'.$template.'.php';
	}
	die();

}


// 404
if (strpos($_SERVER["REQUEST_URI"], '/en') !== false) {
	$pages = $pagesEN;
	$lg = 'en';
}
else {
	$pages = $pagesCZ;
	$lg = 'cs';
}

extract($pages['/404']);
// $lgStrings = $lgStrings[$lg];

header("HTTP/1.0 404 Not Found");
require './dist/tpl/404.php';
die();

